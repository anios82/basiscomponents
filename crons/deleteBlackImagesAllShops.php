<?

function isBlack($src){
    $img = imagecreatefromjpeg($src);
    $rgbPixel1 = imagecolorat($img, 0, 0);
    $colorsPixel1 = imagecolorsforindex($img, $rgbPixel1);
    $rgbPixel2 = imagecolorat($img, 0, 1);
    $colorsPixel2 = imagecolorsforindex($img, $rgbPixel2);
    $rgbPixel3 = imagecolorat($img, 0, 2);
    $colorsPixel3 = imagecolorsforindex($img, $rgbPixel3);

    /*
     * array(4) {
  ["red"]=>
  int(156)
  ["green"]=>
  int(157)
  ["blue"]=>
  int(162)
  ["alpha"]=>
  int(0)}
     */

    if($colorsPixel1['red'] == '156' && $colorsPixel1['green'] == '157' && $colorsPixel1['blue'] == '162' &&
        $colorsPixel2['red'] == '156' && $colorsPixel2['green'] == '157' && $colorsPixel2['blue'] == '162' &&
        $colorsPixel3['red'] == '156' && $colorsPixel3['green'] == '157' && $colorsPixel3['blue'] == '162'
    ){
        return true;
    }

    return false;
}

$shopPath = '/srv/www';
$dir = opendir($shopPath);
$dirsToCheck = [];

while(false !== ($elem = readdir($dir))){

    if($elem == '.' || $elem == '..'){
        continue;
    }

    //if($elem != 'einstecktuch.com')   continue;

    if(is_dir($shopPath.'/'.$elem)){

        echo $shopPath.'/'.$elem.PHP_EOL;
        if(is_dir($shopPath.'/'.$elem.'/images/ties')){  // os commerce shop

            $dirsToCheck[] = $shopPath.'/'.$elem.'/images/ties';
            echo $shopPath.'/'.$elem.'/images/ties'.PHP_EOL;
        }
        else {
            if(file_exists($shopPath.'/'.$elem.'/out/pictures/generated/product/1/')){
                $dir2 = opendir($shopPath.'/'.$elem.'/out/pictures/generated/product/1/');
                while(false !== ($elem2 = readdir($dir2))) {

                    if ($elem2 == '.' || $elem2 == '..') {
                        continue;
                    }

                    if(is_dir($shopPath.'/'.$elem.'/out/pictures/generated/product/1/'.$elem2)){
                        $dirsToCheck[] = $shopPath.'/'.$elem.'/out/pictures/generated/product/1/'.$elem2;
                        echo $shopPath.'/'.$elem.'/out/pictures/generated/product/1/'.$elem2.PHP_EOL;
                    }
                }
            }

            if(is_dir($shopPath.'/'.$elem.'/out/pictures/master/product/1/')){
                $dirsToCheck[] = $shopPath.'/'.$elem.'/out/pictures/master/product/1/';
                echo $shopPath.'/'.$elem.'/out/pictures/master/product/1/'.PHP_EOL;
            }
        }
    }
}

if(!empty($dirsToCheck)){

    foreach($dirsToCheck as $path):
        $imageDir = opendir($path);

        while(false !== ($elem = readdir($imageDir))) {

            if ($elem == '.' || $elem == '..') {
                continue;
            }

            if (strstr($elem, '.jpg') && isBlack($path . '/' . $elem)) {
                echo 'trying to delete '.$path . '/' . $elem . PHP_EOL;
                if(system('rm -f '.$path . '/' . $elem)){
                    echo $path . '/' . $elem . ' deleted' . PHP_EOL;
                }
            }
        }
    endforeach;
}

