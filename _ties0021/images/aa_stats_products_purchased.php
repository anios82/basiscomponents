<?php
/*
  $Id: stats_products_purchased.php,v 1.29 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<?php
// Die Daten von heute bis vor 28 Tagen sollen angezeigt werden, wenn man auf die Seite kommt und das Formular noch nicht abgeschickt wurde.

$timeArray = getdate(time());
$timeArray['mday'] += -28;
$newTime = mktime($timeArray['hours'],$timeArray['minutes'], $timeArray['seconds'], $timeArray['mon'], $timeArray['mday'], $timeArray['year']);
$newDate = date('d.m.Y', $newTime);

if (isset($_GET['anfangsdatum'])){
	$anfangsdatum = $_GET['anfangsdatum'];
} else if (isset($_POST['anfangsdatum'])){
	$anfangsdatum = $_POST['anfangsdatum'];
} else {
	$anfangsdatum = date('d',$newTime).".".date('m',$newTime).".".date('Y',$newTime);
	$jahr_a = date('Y',$newTime);
	$monat_a = date('m',$newTime);
	$day_a = date('d',$newTime);
	$anfangsdatum_oben = $anfangsdatum;
		 
}
//echo "Anfangsdatum: ".$anfangsdatum."<br>";
if (isset($_GET['enddatum'])){
	$enddatum = $_GET['enddatum'];
} else if (isset($_POST['enddatum'])){
	$enddatum = $_POST['enddatum'];
} else {
	$enddatum = date('d').".".date('m').".".date('Y');
	$jahr_e = date('Y');
	$monat_e = date('m');
	$day_e = date('d');
	$enddatum_oben = $enddatum;
}
	
$fehler = false;

$datumsteile_anfang = explode(".",$anfangsdatum);
$day_a = $datumsteile_anfang[0];
$monat_a = $datumsteile_anfang[1];;
$jahr_a = $datumsteile_anfang[2];

$datumanfang = $jahr_a."-".$monat_a."-".$day_a;

//echo "Das Anfangsdatum ist:" .$datumanfang;

$datumsteile_ende = explode(".",$enddatum);
$day_e = $datumsteile_ende[0];
$monat_e = $datumsteile_ende[1];
$jahr_e = $datumsteile_ende[2];

$datumende = $jahr_e."-".$monat_e."-".$day_e;

$datum_heute = date('Y')."-".date('m')."-".date('d');

// Eingabepr�fung --------------------------------------------------	  
$pattern = "~^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$~";

       if (!preg_match($pattern, $datumanfang))
		 

             {
      	$fehler = true;
      	$datuma_fehler = "<span style='color:red;font-weight:bold;'>Bitte Anfangsdatum so eingeben: dd.mm.yyyy!</span><br> ";
		$datum_eintrag_fehler1 = "style='border:2px solid red;color:red'";
		$day_a = "";
		$monat_a = "";
		$jahr_a = "";
		$day_e = "";
		$monat_e = "";
		$jahr_e = "";
             }
			 
		if (!preg_match($pattern, $datumende))
		
             {
      	$fehler = true;
      	$datume_fehler = "<span style='color:red;font-weight:bold;'>Bitte das Enddatum so eingeben: dd.mm.yyyy!</span><br> ";
		$datum_eintrag_fehler2 = "style='border:2px solid red;color:red'";
		$day_a = "";
		$monat_a = "";
		$jahr_a = "";
		$day_e = "";
		$monat_e = "";
		$jahr_e = "";
             }
				 
if(!$datuma_fehler AND !$datume_fehler)
      {
        
		// echo "keine Eingabefehler!!";
		 $anfangsdatum_oben = $anfangsdatum;
		 $enddatum_oben = $enddatum;
		  if($datumanfang > $datumende ){
      
         $fehler = true;
         $datum_eintrag_fehler = "<span style='color:red;font-weight:bold;'>Das Anfangsdatum muss kleiner als das Enddatum sein.</span><br> ";
		 $datum_eintrag_fehler1 = "style='border:2px solid red;color:red'";
		 
     		 }
		
		if ($datumende > $datum_heute){
		$datum_eintrag_fehler = "<span style='color:red;font-weight:bold;'>Das Enddatum liegt in der Zukunft. Das Enddatum muss kleiner oder gleich dem heutigen Datum sein.</span><br> ";
		 $datum_eintrag_fehler2 = "style='border:2px solid red;color:red'";
		
		 $day_a = "";
		$monat_a = "";
		$jahr_a = "";
		$day_e = "";
		$monat_e = "";
		$jahr_e = "";
		$anfangsdatum_oben = "0";
		 $enddatum_oben = "0";
		}
		 
		 
      }
	  else {
	  
	   	$anfangsdatum_oben = "0";
		$enddatum_oben = "0";
	  }
// Ende Eingabepr�fung ----------------------------------

//wenn das Formular mit den Priorit�ten Nummern abgeschickt wurde, wird die Priorit�t ge�ndert------
if(isset($_POST['updateprio'])) {

	//Hier loopen wir einfach ueber die $_POST Variable welche ja auch ein Array
	//ist. Die Schreibweise $_POST as $key => $value sorgt daf�r das in $key der
	//aktuelle Key aus dem Array steht  (z.B. der Feldname aus dem Formular) und
	//in $value der Wert.
	foreach($_POST as $key => $value) {
	//print_r($_POST);
	//print_r($key);
	//echo "Produkt ID: ".$key."<br>";
	//echo "Wert: ".$value."<br>";
		//Checken ob $key eines von unseren Prio-Eingabefeldern ist
		//if(preg_match('~^prio[0-9]+~', $key, $matches))
		if(preg_match('~^[0-9]+~', $key)) {
		//	$product_id = $matches{1};
			//echo "Produkt ID: ".$product_id;
			if($value) {
				$myquery = tep_db_query("update products set products_prioritaet = '{$value}'
							where products_id = {$key}  ");
				mysql_query($myquery);
			}
		}
	}
	
}	


//------ Datenbankabfrage f�r Ausgabe der Daten-----------------------------------------------
if (empty($_REQUEST['order_by_here'])) {
    $order_by_here = "verkauefe";
	 
} 
else {
$order_by_here = $_REQUEST['order_by_here'];
}
	 
$get_desc = "DESC"; 
//$get_asc = "ASC"; 

$sqlStatement = "
	SELECT 
	products.products_id, 
	products.products_quantity, 
	products.products_model, 
	
	products.products_image, 
	products.products_price,
	products.products_prioritaet,
	products.prio_test,
	orders_products.orders_id, 
	orders_products.products_id,
	orders.orders_id,
	orders.date_purchased,
	sum(orders_products.products_quantity) as bestellte_artikel,
	count(DISTINCT orders_products.orders_id) as verkauefe
from products 
left join orders_products on orders_products.products_id = products.products_id 
left join orders on orders.orders_id = orders_products.orders_id
where 
orders_products.products_quantity > '0' AND ";

$sqlStatement .= "DATE_FORMAT(orders.date_purchased, '%Y-%m-%d') >= '{$jahr_a}-{$monat_a}-{$day_a}' 
				 AND 
				 DATE_FORMAT(orders.date_purchased, '%Y-%m-%d') <= '{$jahr_e}-{$monat_e}-{$day_e}' ";

$sqlStatement .= "group by products.products_id, products.products_model";

				   
$sqlStatement .= " ORDER BY ".mysql_real_escape_string($order_by_here)." ".mysql_real_escape_string($get_desc).";";
	
$query = tep_db_query($sqlStatement);

?>
</head>
<body bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
        </table></td>
<!-- body_text //-->
   
<td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><!--<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
		  <td class="pageHeading"><?php //echo $ueberschrift; ?>
            <td class="pageHeading" align="right"><?php //echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table>--></td>
      </tr>
<?php 
// der Variablen $order_by_here werd ein neuer Text zugeordnet f�r die Ausgabe in der �berschrift ---------------------
if ($order_by_here == "verkauefe"){
	 $orderby = "Anzahl der Verk�ufe.";
	}
elseif ($order_by_here == "products_quantity"){
	 $orderby = "Lagerbestand.";
	}
else {
	 $orderby = "Anzahl der verkauften Artikel.";
	}
//Ausgabe der �berschrift -----------------------
echo "<h3>Verkaufshits</h3>";
echo "<h4 style='font-weight:bold;'>Alle Verk�ufe vom  ".$anfangsdatum_oben." bis ".$enddatum_oben." werden angezeigt, geordnet nach ". $orderby."</h4>";
?>
 
<form style="text-align:center;" action="http://www.krawatten-ties.com/_ties0021/aa_stats_products_purchased.php" method="get">
<?=$datum_eintrag_fehler?>
<?=	$datuma_fehler?>
<?=	$datume_fehler?>

Anfang: <input type="text" name="anfangsdatum" value="<?=$anfangsdatum?>" "<?=$datum_eintrag_fehler1?>" style="width:75px;"> 
Ende: <input type="text" name="enddatum" value="<?=$enddatum?>" "<?=$datum_eintrag_fehler2?>" style="width:75px;">
<input type="hidden" name="order_by_here" value="<?=$order_by_here?>" >

<input type="submit" name="aktion" value="auswahl">
</form>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr style="font-size:14px;font-family:verdana;background-color:#E8E8E8;">
		  	<th >Nummer</th>	
		  	<th >Bild</th>
			<!--<th>Produkt ID</th>-->
            <th >Artikelnummer</th>
			<th >
			
			<a href="http://www.krawatten-ties.com/_ties0021/aa_stats_products_purchased.php?anfangsdatum=<?=$anfangsdatum?>&enddatum=<?=$enddatum?>&aktion=auswahl&order_by_here=verkauefe" style="font-size:14px;color:blue;font-weight:bold;">Verk�ufe</a>
          
			
			
			</th>
			<th class="pageHeading"><a href="http://www.krawatten-ties.com/_ties0021/aa_stats_products_purchased.php?anfangsdatum=<?=$anfangsdatum?>&enddatum=<?=$enddatum?>&aktion=auswahl&order_by_here=products_quantity" style="font-size:14px;color:blue;font-weight:bold;">Lagerbestand</a></th>
			<!---<td class="pageHeading">Produkt ID</td>-->
			<th class="pageHeading"><a href="http://www.krawatten-ties.com/_ties0021/aa_stats_products_purchased.php?anfangsdatum=<?=$anfangsdatum?>&enddatum=<?=$enddatum?>&aktion=auswahl&order_by_here=bestellte_artikel" style="font-size:14px;color:blue;font-weight:bold;">Verkaufte Artikel</a></th>
			<th >Preis KRT</th>
			<!--<th class="pageHeading">Priorit�t</th>-->
			<th >Priorit�t</th>
			 
			<!--<td class="pageHeading">Datum</td>-->
			
			
          </tr>
<?php


?>

      <form action="http://www.krawatten-ties.com/_ties0021/aa_stats_products_purchased.php" method="POST">  

<input type="hidden" name="anfangsdatum" value="<?=$anfangsdatum?>"  > 
<input type="hidden" name="enddatum" value="<?=$enddatum?>" >

<input type="hidden" name="order_by_here" value="<?=$order_by_here?>" >
<?php


// Daten aus DB holen -------------------------
 
$zeile = 0;
while($export = mysql_fetch_array($query)){

$zeile ++;

$preis = $export[products_price];
$bruttopreis = $preis * 1.19;
$bruttopreis = number_format($bruttopreis, 2, '.', '');
$bruttopreis = $bruttopreis." &#8364;";
$priotest = $export[products_prioritaet];
//$priotest = $export[prio_test];
$id = $export[products_id];
$prioname = $id;

 if ($zeile % 2 == 0)
     {
         echo "<tr align='center' style='font-family:verdana;font-size:13px;background-color:#E8E8E8;'>";
     }
     else
     {
     		echo "<tr align='center' style='font-family:verdana;font-size:13px;'>";
     }
?>


 <!--<tr align="center" style="font-family:verdana;font-size:13px;">-->	
 		<td valign="center"><?=$zeile?></td>
		<td valign="center"><img src="http://www.krawatten-ties.com/images/<?=$export[products_image]?>" style="width:80px;height:90px;padding-bottom:3px;padding-top:3px;" /></td>
		<!--<td valign="top"><?=$export[products_id]?></td>-->
		<td valign="center"><?=$export[products_model]?></td>
				
	    <td valign="center"><?=$export[verkauefe]?></td>
	    <td valign="center"><?=$export[products_quantity]?></td>
				<!-- <input type="hidden" name="id" value="<?=$id?>">-->
			
		<td valign="center"><?=$export[bestellte_artikel]?></td>
		<td valign="center"><?=$bruttopreis?></td>
				 <!--<td valign="top"><?=$export[products_prioritaet]?></td>-->
		<td valign="center"><input type="text" name="<?=$prioname?>" value="<?=$priotest?>" style="width:40px;"></td>
				<!-- <td valign="top"><?=$export[date_purchased]?></td>-->	 
			     
	</tr>

<?php 
}

?>
<tr><td colspan="9" align="right" style="padding-right:25px;"><input type="submit" name="updateprio" value="aktualisieren"></td></tr>
</form>
</table>



    
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
