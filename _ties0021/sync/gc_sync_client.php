<?php 

/***************************************
*
* Dieses Tool erstellt für jeden
* externen Shop eine XML Datei mit allen
* Produkten
*
***************************************/

// Die Reihenfolge der Cron Parameter wird geändert:
// Aufruf durch ...._ties0021/sync/gc_sync_client.php 1 syncproducts
// Erster Parameter = 1 wenn das Script per Cron ausgeführt wird
// Zweiter Parameter = Anweisung welche Aktion gemacht werden soll

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include('includes/application_top.php');
require('includes/classes/custom/libs/JSON.php');

$aDBTablesToSync = array('address_format', 'orders_status', 'categories','categories_description', 'etc_relatedProducts', 'gc_server', 'gc_shops', 'languages', 'products_stock');

if($_GET['debug'] == '1') {
	
	if(file_exists('./sync/debug_'.$_GET['action'].'.log')  &&  is_writable('./sync/debug_'.$_GET['action'].'.log')){
			
		$logFS = filesize('./sync/debug_'.$_GET['action'].'.log');
		
		if($logFS > 5242880){
			unlink('./sync/debug_'.$_GET['action'].'.log');
		}
		
		if(@$fp = fopen('./sync/debug_'.$_GET['action'].'.log', 'a+')) {
			fwrite($fp, "RUN (".date('Y-m-d H:i:s').") \n");
			fwrite($fp, "PARAMETER DEBUG: ".$_GET['debug']."\n");
			fwrite($fp, "PARAMETER CRON: ".$_GET['cron']."\n");
			fwrite($fp, "PARAMETER ACTION: ".$_GET['action']."\n");
		}
		else {
			unset($_GET['debug']);
		}
	}
	else {
		unset($_GET['debug']);
	}		
}

/**************************************
* Functions
**************************************/

function check_and_decode_XMLData($aXmlVar = NULL){
	
	if(strtolower(CHARSET) == 'utf-8'){  // this shop has UTF-8 as its output (and database) character set, comes from the global configure.php
		return($aXmlVar);
	}
	else {
		return(utf8_decode($aXmlVar));
	}
}

function gatewayLogging($aShopID = NULL, $aStartOrEnd = 'start', $aHash = NULL){
	
	if(!$aStartOrEnd)   return(false);
	
	$gateway = 'http://gateway.krawatten-ties.com/gateway_extshop_syncjobs.php?shop_id='.$aShopID.'&action='.$_GET['action'].'&startOrEnd='.$aStartOrEnd;	
	
	if($aStartOrEnd == 'end'){
		
		if(!$aHash){
			return(false);
		}
		$gateway .= '&hash='.$aHash;
	}
	else {
		if(!$aShopID)   return(false);
	}
		
	$result = file_get_contents($gateway);
	
	if($aStartOrEnd == 'start'){
			
		list($suc, $hash) = explode(';', $result);
		return(array($suc, $hash));
	}	
	else {
		return(array($suc));
	}
}

function read_xml($filename, &$aShops) {
	
	global $fp, $rootdir;

	$aXML = array();
	
	foreach($aShops as $aShop) {
		
		list($logStartSuccess, $logStartHash) = gatewayLogging($aShop['shop_id'], 'start');		
		
		$logging[] = array('shop_id'=>$aShop['shop_id'], 'success'=>$logStartSuccess, 'hash'=>$logStartHash);
		
		$sXMLRemoteFile = $filename.'_'.strtoupper($aShop['shop_shortname']).'.xml';
		$sXMLLocalFile = 'sync/'.$filename.'_'.strtoupper($aShop['shop_shortname']).'.xml';
		
		if($fp) fwrite($fp, "READ XML ".$sXMLRemoteFile."\n");
		
		
		if(defined('SYNCHRO_COMMUNICATE_VIA_FILEGETCONTENTS')  &&  SYNCHRO_COMMUNICATE_VIA_FILEGETCONTENTS === true){
					
					$data = file_get_contents(SYNCHRO_DATA_URL.$sXMLRemoteFile);
					$fpWrite = fopen($sXMLLocalFile, 'w+');
					fwrite($fpWrite, $data);
					fclose($fpWrite);
		}
		else {					
					
					if(@!$ftp_connection_id = ftp_connect(FTP_REMOTE_HOST)){
						
						if($fp) fwrite($fp, "FTP CONNECT FEHLER - HOST: ".FTP_REMOTE_HOST."\n");
						continue;
						#die('FTP Connect Fehler mit '.$sXMLLocalFile);
						
					}
					
					if(@!$ftp_login_id = ftp_login($ftp_connection_id, FTP_REMOTE_USER, FTP_REMOTE_PASSWORD)) {
						
						ftp_close($ftp_connection_id);
						if($fp) fwrite($fp, "FTP LOGIN FEHLER - USER:".FTP_REMOTE_USER." PASSWORT: ".FTP_REMOTE_PASSWORD."\n");
						continue;
						#die('FTP Login Fehler mit '.$sXMLLocalFile);
						
					}
					
					
					if(@!ftp_get($ftp_connection_id, $sXMLLocalFile, FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile, FTP_BINARY)) {
						
						ftp_close($ftp_connection_id);
						if($fp) fwrite($fp, "FTP GET FEHLER - LOKAL: ".$sXMLLocalFile." REMOTE ".FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile."\n");
						continue;
						#die('FTP Get Fehler mit '.$sXMLLocalFile . ' und ' . FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile);
						
					}
					
					
					if(@!ftp_delete($ftp_connection_id, FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile)) {
						
						ftp_close($ftp_connection_id);
						if($fp) fwrite($fp, "FTP DELETE FEHLER - REMOTE: ".FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile."\n");
						continue;
						#die('FTP Delete Fehler mit ' . FTP_REMOTE_FOLDER_XML.$sXMLRemoteFile);
						
					}
					
					
					ftp_close($ftp_connection_id);
					
					if($fp) fwrite($fp, "FTP END ".$sXMLRemoteFile."\n");
					shell_exec('chmod 777 '.$sXMLRemoteFile);
		}		
		
		
		
		$aXML[$aShop['shop_id']] = simplexml_load_file($sXMLLocalFile, NULL, LIBXML_NOCDATA);		
	}
	
	return array($aXML, $logging);	
}

function install_db($aDBTable = NULL) {
	
	global $fp, $rootdir;
	
	if($fp) fwrite($fp, "\r\n__TXT_START__:\r\n");
	
	list($logStartSuccess, $logStartHash) = gatewayLogging('SERVER_'.SERVER_ID, 'start');	
	$logging[] = array('shop_id'=>SHOP_ID, 'success'=>$logStartSuccess, 'hash'=>$logStartHash);	
	
	$dbinput_external = file_get_contents('http://gateway.krawatten-ties.com/gateway_extshop_syncdb.php?dbtable='.$aDBTable);	

	if(!empty($dbinput_external)){
		
		$emptied = tep_db_query('TRUNCATE '.$aDBTable);
		
		if($emptied){
			$sqls = explode('>>>', $dbinput_external);
			foreach($sqls as $sql):				
				tep_db_query($sql);			
			endforeach;
		}
	}
		return array($logging);	
}

function show_products_from_xml(&$oXML) {

	$aExistingProductIds = get_existing_product_ids();
	
	$sProducts = '<table>';
	$bI = false;
	
		$sProducts .= '<tr class="tablegrey">';
			$sProducts .= '<th><input type="checkbox" onclick="toggleCheckboxes()"></th>';
			$sProducts .= '<th>Model</th>';
			$sProducts .= '<th>Name</th>';
			$sProducts .= '<th>Beschreibung</th>';
			$sProducts .= '<th>Seo Subhead</th>';
			$sProducts .= '<th>Seo Keywords</th>';
			$sProducts .= '<th>Preis</th>';
			$sProducts .= '<th>Bestand</th>';
			$sProducts .= '<th>Priorit&auml;t</th>';
		$sProducts .= '</tr>';
	
	foreach($oXML as $aProduct) {
		

		if(!in_array($aProduct->id, $aExistingProductIds)) {
			
			if($bI) {
				$sProducts .= '<tr class="tablegrey">';
				$bI = false;
			}
			else {
				$sProducts .= '<tr>';
				$bI = true;
			}
			
			$sProducts .= '<td><input type="checkbox" name="products[]" value="'.check_and_decode_XMLData($aProduct->id).'" /></td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->model).'</td>';
			$sProducts .= '<td>'.strip_tags(check_and_decode_XMLData($aProduct->name)).'</td>';
			$sProducts .= '<td>'.strip_tags(check_and_decode_XMLData($aProduct->description)).'</td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->seo_subhead).'</td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->seo_keywords).'</td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->price).'</td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->quantity).'</td>';
			$sProducts .= '<td>'.check_and_decode_XMLData($aProduct->priority).'</td>';
			$sProducts .= '</tr>';
			
		}
		
	}
	
	$sProducts .= '</table>';
	
	return $sProducts;
	
}

function write_products_to_db(&$aXML, &$aShops) {
	
	global $fp;
	
	foreach($aXML as $iShopID => $oXML) {
		
		if($fp)
		fwrite($fp, "PRODUCTS TO DB - SHOP ID ".$iShopID."\n");
		
		foreach($oXML as $aProduct) {
			
			$continue = false;
			
			if($_GET['cron'] == '1' && PRODUCTS_CRON_AUTOIMPORT == true) {
				$continue = true;			
			}		
			
			if($_GET['cron'] != '1' && (( is_array($_GET['products']) and in_array($aProduct->id, $_GET['products']) ) or (is_string($_GET['products']) and $aProduct->id == $_GET['products']))) {
				$continue = true;	
			}		
			
			if($continue) {
				if($fp) fwrite($fp, "IMPORT PRODUCT ".$aProduct->id."\n");
				
				// Wenn das Produkt schon mal importiert wurde
				if(get_existing_product($aProduct->id)) {
					
					if($fp) fwrite($fp, "PRODUCT EXISTS ".$aProduct->id."\n");
					
					// ... wird nur der Preis angepasst, da es ja aus einem andern Shop stammen könnte
					tep_db_query("UPDATE products SET ".$aShops[$iShopID]['products_price_fieldname']."='".check_and_decode_XMLData($aProduct->price)."' WHERE products_id='".$aProduct->id."'");
					
					if($fp) fwrite($fp, "CALL GATEWAY http://gateway.krawatten-ties.com/gateway_extshop_productsxml_success.php?products_id=".$aProduct->id."&language_id=".$aShops[$iShopID]['language_id']."\n");

					@$sResponse = readfile('http://gateway.krawatten-ties.com/gateway_extshop_productsxml_success.php?products_id='.$aProduct->id.'&language_id='.$aShops[$iShopID]['language_id']);
				}
				else {
					
					if($fp) fwrite($fp, "PRODUCT NEW ".$aProduct->id."\n");
					
					tep_db_query("REPLACE INTO products (products_id, products_quantity, products_model, ".$aShops[$iShopID]['products_price_fieldname'].", products_prioritaet, products_tax_class_id, products_status, products_date_added) VALUES ('".check_and_decode_XMLData($aProduct->id)."', '".check_and_decode_XMLData($aProduct->quantity)."', '".check_and_decode_XMLData($aProduct->model)."', '".check_and_decode_XMLData($aProduct->price)."', '".check_and_decode_XMLData($aProduct->priority)."', '1', '".PRODUCTS_STATUS_ON_IMPORT."', '".$aProduct->products_date_added."')");
					tep_db_query("REPLACE INTO products_to_categories (products_id, categories_id) VALUES ('".check_and_decode_XMLData($aProduct->id)."', '".DB_PRODUCT_BUCKET_ID."')");
					
					if(!empty($aProduct->category)) {
						
						foreach($aProduct->category as $category)
							tep_db_query("REPLACE INTO products_to_categories (products_id, categories_id) VALUES ('".check_and_decode_XMLData($aProduct->id)."', '".$category."')");
						
					}
					
					if($fp) fwrite($fp, "CALL GATEWAY http://gateway.krawatten-ties.com/gateway_extshop_productsxml_success.php?products_id=".$aProduct->id."&language_id=".$aShops[$iShopID]['language_id']."\n");
					
					@$sResponse = readfile('http://gateway.krawatten-ties.com/gateway_extshop_productsxml_success.php?products_id='.$aProduct->id.'&language_id='.$aShops[$iShopID]['language_id']);
					
				}
				
				tep_db_query("REPLACE INTO products_description (products_id, language_id, products_name, products_description, products_seo_subhead, products_seo_keywords) VALUES ('".check_and_decode_XMLData($aProduct->id)."', '".$aShops[$iShopID]['language_id']."', '".check_and_decode_XMLData(str_replace("'", "`", $aProduct->name))."', '".check_and_decode_XMLData(str_replace("'", "`", $aProduct->description))."', '".str_replace("'", "`", check_and_decode_XMLData($aProduct->seo_subhead))."', '".check_and_decode_XMLData($aProduct->seo_keywords)."')");
				
			}
			
		}
		
	}
	
}

function get_existing_product_ids() {
	
	$hQueryProductIds = tep_db_query("SELECT products_id FROM products");
	
	$aProductIds = array();
	
	while($aFetchProductIds = tep_db_fetch_array($hQueryProductIds))
		$aProductIds[] = $aFetchProductIds['products_id'];
	
	return $aProductIds;
	
}

function get_existing_product($iProductID) {
	
	$hQueryProduct = tep_db_query("SELECT products_id FROM products WHERE products_id='".$iProductID."'");
	
	if(tep_db_num_rows($hQueryProduct) > 0)
		return true;
	
	return false;
	
}

function check_existing_model($iProductMODEL, $iProductID) {
	
	$hQueryProduct = tep_db_query("SELECT products_id FROM products WHERE products_model='".$iProductMODEL."' AND products_id != ".$iProductID);
	
	if(tep_db_num_rows($hQueryProduct) > 0){
	
		list($prID) = tep_db_fetch_row($hQueryProduct);
		return ($prID);
	}
	
	return false;
	
}

function get_all_non_sync_orders_ids($iShopID) {
	
	
    global $conf_ordersStatusDelayedSync;
	
	if(!is_array($conf_ordersStatusDelayedSync)){
		 $conf_ordersStatusDelayedSync = array(6, 36);
	}
	//$hQueryOrdersIds = tep_db_query();
	
	$sql = "SELECT t1.orders_id, history1.date_added, history1.orders_status_id FROM ".TABLE_ORDERS." AS t1 ".
					"JOIN ".TABLE_ORDERS_STATUS." as t2 ON (t1.orders_status = t2.orders_status_id AND t2.language_id=".LANGUAGE_ID.")".
					"JOIN ".TABLE_ORDERS_STATUS_HISTORY." as history1 ON t1.orders_id = history1.orders_id ".
					"JOIN (".
						"SELECT history2.orders_id, MAX(history2.date_added) as max_date FROM ".TABLE_ORDERS_STATUS_HISTORY." as history2 GROUP BY history2.orders_id".						
					") as joined ON (joined.orders_id = history1.orders_id AND joined.max_date = history1.date_added) ".			   
			   "WHERE t1.shop_id = ".$iShopID." AND t2.orders_status_name != 'sync' AND t2.orders_status_id != ".ORDERS_STATUS_ID_STORNIERT." ".
			   				(($conf_ordersStatusDelayedSync) ? ' AND ((history1.orders_status_id NOT IN('.implode(',', $conf_ordersStatusDelayedSync).'))   OR   (history1.orders_status_id IN('.implode(',', $conf_ordersStatusDelayedSync).')  AND  history1.date_added <= \''.date('Y-m-d H:i:s', strtotime('-10 minutes', time())).'\'))' : '').' '.
			   "GROUP BY t1.orders_id";
		echo $sql;	
	$hQueryOrdersIds = tep_db_query($sql);
			
	while($aFetchOrdersIds = tep_db_fetch_array($hQueryOrdersIds))
		$aOrdersIds[] = $aFetchOrdersIds['orders_id'];
	
	return $aOrdersIds;
	
}

function sync_orders(&$aShops) {
	
	global $rootdir;

	
	foreach($aShops as $aShop) {

		
				
		list($logStartSuccess, $logStartHash) = gatewayLogging($aShop['shop_id'], 'start');	
		$logging[] = array('shop_id'=>$aShop['shop_id'], 'success'=>$logStartSuccess, 'hash'=>$logStartHash);
		
		$sFileDir  = './sync/';
		$sFileName = 'orders_'.strtoupper($aShop['shop_shortname']).'.xml';
		
		if($fp) fwrite($fp, "XML CREATE ".$sFileName."\n");
		
		
		
		if($aOrdersIds = get_all_non_sync_orders_ids($aShop['shop_id'])) {
			
			if(!is_file($sFileDir.$sFileName)) {
				
				$sXML  = '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
				$sXML .= '<all_orders>' . "\n";
			}
			else {
				$sXML = file_get_contents($sFileDir.$sFileName);
				$sXML = preg_replace('#</all_orders>#', '',$sXML);
			}
			
			foreach($aOrdersIds as $iOrdersId) {
				
				if($fp) fwrite($fp, "XML ADD ORDER ".$iOrdersId."\n");
				
				$sXML .= '<order id="'.$iOrdersId.'">' . "\n";
				
				$hQueryOrders = tep_db_query("SELECT * FROM orders WHERE orders_id='".$iOrdersId."'");
				
				$sXML .= '<orders>' . "\n";
				
				$aFetchOrders = tep_db_fetch_array($hQueryOrders);
				
				foreach($aFetchOrders as $sFetchOrdersKey => $sFetchOrdersValue)
					$sXML .= '<'.$sFetchOrdersKey.'><![CDATA[' . utf8_encode($sFetchOrdersValue) . ']]></'.$sFetchOrdersKey.'>' . "\n";
				
				$sXML .= '</orders>' . "\n";
				
				
				
				$hQueryProducts = tep_db_query("SELECT t1.*, IF(t2.products_attributes_id IS NOT NULL, 1, 0) as is_attribute_product FROM orders_products as t1 LEFT JOIN products_attributes as t2 ON t1.products_id = t2.products_id WHERE t1.orders_id='".$iOrdersId."' GROUP BY t1.products_id");
				
				$sXML .= '<orders_products>' . "\n";
				
				while($aFetchProducts = tep_db_fetch_array($hQueryProducts)) {
					
					$rowAttribute = array();
					if($aFetchProducts['is_attribute_product'] == '1'){
						unset($aFetchProducts['is_attribute_product']);
						$resAttribute = tep_db_query('SELECT products_options, products_options_values, options_values_price, price_prefix FROM orders_products_attributes WHERE orders_id = '.$iOrdersId.' AND orders_products_id = '.$aFetchProducts['orders_products_id']);
						$rowAttribute = tep_db_fetch_array($resAttribute);
					}
					
					$sXML .= '<product>' . "\n";
					
					foreach($aFetchProducts as $sFetchProductsKey => $sFetchProductsValue):
						$sXML .= '<'.$sFetchProductsKey.'><![CDATA[' . utf8_encode($sFetchProductsValue) . ']]></'.$sFetchProductsKey.'>' . "\n";
					endforeach;
					
					if(!empty($rowAttribute)){
						
						$sXML .= '<attribute>' . "\n";
						 
						foreach($rowAttribute as $k=>$v):
							$sXML .= '<'.$k.'><![CDATA[' . utf8_encode($v) . ']]></'.$k.'>' . "\n";
						endforeach; 
						
						$sXML .= '</attribute>' . "\n";
					}
					
					$sXML .= '</product>' . "\n";
					
				}
				
				$sXML .= '</orders_products>' . "\n";
				
				
				$hQueryTotal = tep_db_query("SELECT * FROM orders_total WHERE orders_id='".$iOrdersId."'");
				
				$sXML .= '<orders_total>' . "\n";
				
				while($aFetchTotal = tep_db_fetch_array($hQueryTotal)) {
					
					$sXML .= '<total>' . "\n";
					
					foreach($aFetchTotal as $sFetchTotalKey => $sFetchTotalValue)
						$sXML .= '<'.$sFetchTotalKey.'><![CDATA[' . utf8_encode($sFetchTotalValue) . ']]></'.$sFetchTotalKey.'>' . "\n";
					
					$sXML .= '</total>' . "\n";
					
				}
				
				$sXML .= '</orders_total>' . "\n";
				
				
				$hQueryHistory = tep_db_query("SELECT * FROM orders_status_history WHERE orders_id='".$iOrdersId."'");
				
				$sXML .= '<orders_status_history>' . "\n";
				
				while($aFetchHistory = tep_db_fetch_array($hQueryHistory)) {
					
					$sXML .= '<history>' . "\n";
					
					foreach($aFetchHistory as $sFetchHistoryKey => $sFetchHistoryValue)
						$sXML .= '<'.$sFetchHistoryKey.'><![CDATA[' . utf8_encode($sFetchHistoryValue) . ']]></'.$sFetchHistoryKey.'>' . "\n";
					
					$sXML .= '</history>' . "\n";
					
				}
				
				$sXML .= '</orders_status_history>' . "\n";
				
				$sXML .= '</order>' . "\n";
				
				//echo "UPDATE orders AS o, orders_status AS os SET o.orders_status=os.orders_status_id WHERE os.orders_status_name='sync' AND os.language_id='".LANGUAGE_ID."' AND o.orders_id='".$iOrdersId."'";
				//tep_db_query("UPDATE orders AS o, orders_status AS os SET o.orders_status=os.orders_status_id WHERE os.orders_status_name='sync' AND os.language_id='".LANGUAGE_ID."' AND o.orders_id='".$iOrdersId."'");
				
			}
			
			$sXML .= '</all_orders>' . "\n";
			
			if($hXMLFile = fopen($sFileDir.$sFileName, 'w')) {
				
				fwrite($hXMLFile, $sXML);
				fclose($hXMLFile);
				
			}
			else {
				
				die('Fopen Fehler mit '.$sFileDir.$sFileName);
				
			}
			
		}
		
	}

	return($logging);	
}

function sync_products_update(&$aXML, &$aShops) {
	
	global $fp;
	
	foreach($aXML as $iShopID => $oXML) {		

		foreach($oXML as $aProduct) {

			if(get_existing_product($aProduct->id)) {				

						if(array($aProduct->stock)){
							
							tep_db_query('DELETE FROM '.TABLE_PRODUCTS_STOCK.' WHERE products_id = '.$aProduct->id);
							foreach($aProduct->stock as $obj):								
								tep_db_query('INSERT INTO '.TABLE_PRODUCTS_STOCK.'(products_id, products_stock_attributes, products_stock_quantity) VALUES('.$aProduct->id.', \''.$obj->attributes.'\', '.$obj->quantity.')');
							endforeach;						
				}
				
				if($fp) fwrite($fp, "UPDATE PRODUCT ".$aProduct->id."\n");				
				
				if(false !== ($otherPrID = check_existing_model($aProduct->model, $aProduct->id))){
						
					tep_db_query("DELETE FROM products_to_categories WHERE products_id='".$otherPrID."'");
					tep_db_query("DELETE FROM products_description WHERE products_id='".$otherPrID."'");
					tep_db_query("DELETE FROM products WHERE products_id='".$otherPrID."'");
				}	
				
				tep_db_query("UPDATE products SET 
									products_quantity='".check_and_decode_XMLData($aProduct->quantity)."', 
									".$aShops[$iShopID]['products_price_fieldname']."='".check_and_decode_XMLData($aProduct->price)."', 
									products_prioritaet='".check_and_decode_XMLData($aProduct->priority)."', 
									products_status='".check_and_decode_XMLData($aProduct->status)."', 
									products_model='".check_and_decode_XMLData($aProduct->model)."' 
							WHERE products_id='".$aProduct->id."'");
				
				if($fp) fwrite($fp, "UPDATE PRODUCTSDESCRIPTION ".$aProduct->id."\n");
				
				$updatePrtextsSql = array();
				// also update the products texts if set (head, subhead, keywords, description)
				if(!empty($aProduct->seo_keywords)){					
					$updatePrtextsSql[] = "products_seo_keywords='".check_and_decode_XMLData(addslashes($aProduct->seo_keywords))."'";
				}
				if(!empty($aProduct->seo_subhead)){					
					$updatePrtextsSql[] = "products_seo_subhead='".check_and_decode_XMLData(addslashes($aProduct->seo_subhead))."'";
				}
				if(!empty($aProduct->name)){					
					$updatePrtextsSql[] = "products_name='".check_and_decode_XMLData(addslashes($aProduct->name))."'";
				}
				if(!empty($aProduct->description)){					
					$updatePrtextsSql[] = "products_description='".check_and_decode_XMLData(addslashes($aProduct->description))."'";
				}
				if(!empty($aProduct->extshops_translated)){					
					$updatePrtextsSql[] = "extshops_translated='".check_and_decode_XMLData($aProduct->extshops_translated)."'";
				}
				if(!empty($aProduct->extshops_translated_date)){					
					$updatePrtextsSql[] = "extshops_translated_date='".check_and_decode_XMLData($aProduct->extshops_translated_date)."'";
				}
		
				if(!empty($updatePrtextsSql)){
				   //tep_db_query("UPDATE products_description SET ".implode(', ', $updatePrtextsSql)." WHERE products_id='".$aProduct->id."' AND language_id = ".$aShops[$iShopID]['language_id']);				   
				   	   tep_db_query("REPLACE INTO products_description (products_id, language_id, products_name, products_description, products_seo_subhead, products_seo_keywords, extshops_translated, extshops_translated_date) VALUES ('".check_and_decode_XMLData($aProduct->id)."', '".$aShops[$iShopID]['language_id']."', '".mysql_real_escape_string(check_and_decode_XMLData(str_replace("'", "`", $aProduct->name)))."', '".mysql_real_escape_string(check_and_decode_XMLData(str_replace("'", "`", $aProduct->description)))."', '".mysql_real_escape_string(check_and_decode_XMLData(str_replace("'", "`", $aProduct->seo_subhead)))."', '".mysql_real_escape_string(check_and_decode_XMLData($aProduct->seo_keywords))."',  '".check_and_decode_XMLData($aProduct->extshops_translated)."', '".check_and_decode_XMLData($aProduct->extshops_translated_date)."')");
				}
				
				if($fp) fwrite($fp, "CLEAR CATEGORIES FROM PRODUCT ".$aProduct->id."\n");
				
				tep_db_query("DELETE FROM products_to_categories WHERE products_id='".$aProduct->id."'");
				
				foreach($aProduct->category as $iCategoryID) {
					
					if($fp) fwrite($fp, "INSERT CATEGORY ".$iCategoryID."\n");
					tep_db_query("REPLACE INTO products_to_categories (products_id, categories_id) VALUES ('".$aProduct->id."', '".$iCategoryID."')");
					
				}
				
			}
			else {
				if($fp) fwrite($fp, "PRODUCT DOESNT EXISTS ".$aProduct->id."\n");
			}
		}
		
	}
	
}

// Holt alle Daten der Shoops, die zu der ServerID passen
function get_shops_from_server_id($iServerID='') {
	
	if(empty($iServerID) or !is_numeric($iServerID))
		$iServerID = SERVER_ID;
	
	$aShop = array();
	
	$hQueryShops = tep_db_query("SELECT * FROM gc_shops WHERE shop_server='".$iServerID."'");
	
	while($aFetchShops = tep_db_fetch_array($hQueryShops))
		$aShops[$aFetchShops['shop_id']] = $aFetchShops;
	
	return $aShops;
	
}

/**************************************
* Operations
**************************************/

// Alle Shops holen, die zu der Server-ID dieses Backends gehören
$aShops = get_shops_from_server_id();


switch($_GET['action']) {

    case 'setOrderSynced':

        if(array_key_exists('orders_id', $_GET)){
            echo 'setting order synced:'.$_GET['orders_id'];
            tep_db_query("UPDATE orders SET orders_status = 28 WHERE orders_id = ".$_GET['orders_id']);
        }

        break;
	// Sync neuer Produkte
	case 'syncproducts':
		
		list($aXML, $logging) = read_xml('products', $aShops);

		write_products_to_db($aXML, $aShops);		

		foreach($logging as $loginfo):
			
			if($loginfo['success'] == '1'){
				list($logEndSuccess) = gatewayLogging($loginfo['shop_id'], 'end', $loginfo['hash']);
			}
		endforeach;
		break;
	
	// Sync der Bestellungen
	case 'syncorders':
		
		$logging = sync_orders($aShops);
		
		foreach($logging as $loginfo):
			
			if($loginfo['success'] == '1'){
				list($logEndSuccess) = gatewayLogging($loginfo['shop_id'], 'end', $loginfo['hash']);
			}
		endforeach;
		break;
	
	// Sync der Bestände usw...
	case 'syncproductsupdate':
		list($aXML, $logging)  = read_xml('products_update', $aShops);

		sync_products_update($aXML, $aShops);
		
		foreach($logging as $loginfo):
			
			if($loginfo['success'] == '1'){
				list($logEndSuccess) = gatewayLogging($loginfo['shop_id'], 'end', $loginfo['hash']);
			}
		endforeach;
		break;
	
	// Sync der DB Strucktur
	case 'syncdb':

		foreach($aDBTablesToSync as $dbtable):
			
			list($logging) = install_db($dbtable);
		endforeach;
		
		foreach($logging as $loginfo):
			
			if($loginfo['success'] == '1'){
				list($logEndSuccess) = gatewayLogging($loginfo['shop_id'], 'end', $loginfo['hash']);
			}
		endforeach;
		break;
	
}

/**************************************
* Output
**************************************/

// Wenn diese Datei nicht vom Cronjob angestoßen wurde gibt es eine Ausgabe für den Benutzer:
if(!isset($_GET['cron'])) {
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
        <title><?php echo TITLE; ?></title>
        <link rel="stylesheet" type="text/css" href="../includes/stylesheet.css">
        <style>
		.tablegrey {
			background:#F4F4F4;
		}
		</style>
        <script src="http://www.google.com/jsapi"></script>

		<script type="text/javascript">
            google.load("prototype", "1.6");
            google.load("scriptaculous", "1.8.2");
			
			function toggleCheckboxes() {
			// written by Daniel P 3/21/07
			// toggle all checkboxes found on the page
				var inputlist = document.getElementsByTagName("input");
				for (i = 0; i < inputlist.length; i++) {
				if ( inputlist[i].getAttribute("type") == 'checkbox' ) {	// look only at input elements that are checkboxes
						if (inputlist[i].checked)	inputlist[i].checked = false
						else								inputlist[i].checked = true;
					}
				}
			}

        </script>
    </head>
	<body bgcolor="#FFFFFF">
		<div id="container">
        
<?php include('includes/header.php'); ?>

<table border="0" width="100%" cellspacing="2" cellpadding="2">
    <tr>
    	<td width="<?php echo BOX_WIDTH; ?>" valign="top">
        
        	<table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">

				<?php require('includes/column_left.php'); ?>

    		</table>
            
		</td>
		<td width="100%" valign="top">
	
<h1>Datenbank Syncronisations-Tool CLIENT</h1>
<h2>Neue Produkte von KRT holen</h2>
<p>Hier k&ouml;nnen neue Produkte von Krawatten-Ties geholt werden.</p>

<form id="productsform" method="get" action="<?php echo $PHP_SELF; ?>">
<?php

list($xml) = read_xml('products', $aShops);
echo show_products_from_xml($xml);
 ?>
	<input type="hidden" name="action" value="syncproducts" />
	<input type="submit" value="Produkte kopieren" />
</form>

<h2>Bestand syncronisieren</h2>
<p>Hier wird der Bestand der vorhandenen Produkte mit Krawatten-Ties syncronisiert</p>

<form method="get" action="<?php echo $PHP_SELF; ?>">
	<input type="hidden" name="action" value="syncstock" />
	<input type="submit" value="Bestand aktualisieren" />
</form>

<h2>Bestellungen syncronisieren</h2>
<p>Hier werden die Bestellungen mit Krawatten-Ties syncronisiert</p>

<form method="get" action="<?php echo $PHP_SELF; ?>">
	<input type="hidden" name="action" value="syncorders" />
	<input type="submit" value="Bestellungen syncronisieren" />
</form>

		</td>

	</tr>
</table>

<?php require('includes/footer.php'); ?>

		</div>
	</body>
</html>

<?php } ?>


<?php 

include('includes/application_bottom.php'); 
if($fp)
	fclose($fp);

?>