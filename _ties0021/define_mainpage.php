<?php
/*
  DESCRIPTION:
  
  
*/

  require('includes/application_top.php');
  
  //wird zur Preisanzeige benötigt by Gurkcity Angebotsverwaltung 08.06.2006
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

header("Expires: -1");
header("Cache-Control: post-check=0, pre-check=0");
header("Pragma: no-cache");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

  require('includes/modules/top-angebote/update_mainpage.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<meta http-equiv="pragma" content="no-cache">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<style type="text/css">
<!--
.noactive {
	border: 2px solid #FF0000;
}
.active {
	border: 2px solid #FFFFFF;
}

-->
</style>

</head>
<body bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top">
	<table cellpadding="2" cellspacing="2" border="0">
		<tr>
			<td class="pageHeading">
			Startseite: Angebote	&auml;ndern		</td>
			<td><?
			 $aShops = getShopsFromServer(SERVER_ID);
			echo tep_draw_form('choose_shop', 'define_mainpage.php', NULL, 'GET');
			?>
			<input type="hidden" name="processorPath" value="_ties0021/" />
			<input type="hidden" name="do" value="start" /><select name="shop_id"><?
			
			foreach($aShops as $shop): ?>
				<option value="<?=$shop['shop_id']?>"><?=$shop['shop_shortname']?></option>
			<? endforeach;
			
			?></select><input type="submit" value="neu generieren"></form></td>
		</tr>
				<tr>
			<td class="pageHeading">
			Startseite: Angebote	anzeigen		</td>
			<td><?
			echo tep_draw_form('choose_shop', 'define_mainpage.php', NULL, 'GET');
			?><input type="hidden" name="do" value="show" /><select name="shop_id"><?
			$aShops = getShopsFromServer(SERVER_ID);
			
			foreach($aShops as $shop): ?>
				<option value="<?=$shop['shop_id']?>"><?=$shop['shop_shortname']?></option>
			<? endforeach;
			
			?></select><input type="submit" value="anzeige"></form></td>
		</tr>
		<tr>
		  <td>
<?php

if($_GET['do']  == 'start'  && $_GET['shop_id'])
{
	//Standardwerte setzen
	$specials_subtitle = 'Angebot';
	
	
	//Standardwerte holen, wenn ID übergeben wurde
	if(isset($_GET['specials_product_id']))
	{
		$sql = "SELECT p.products_model, p.products_image, p.products_price, p.products_tax_class_id, pd.products_description, pd.products_name
		FROM products AS p, products_description AS pd 
		WHERE pd.products_id = p.products_id 
		AND p.products_id = '".$_GET['specials_product_id']."'
		AND pd.language_id = '2'";
	
		$item_qry = tep_db_query($sql);
		$item = tep_db_fetch_array($item_qry);
		
		//Standard-Untertitel wird überschrieben
		$specials_subtitle = $item['products_name'];
		#$products_price_brutto = $currencies->format($item['products_price']);
		$products_price_brutto = $currencies->display_price($item['products_price'], tep_get_tax_rate($item['products_tax_class_id']));
		
		//Bild vom Ursprungsort kopieren
		$file     = DIR_FS_CATALOG."images/".$item['products_image'];
		$filecopy = DIR_FS_CATALOG.DIR_WS_IMAGES_SPECIALS.$item['products_image'];
		
		if (!copy($file, $filecopy)) {
   			print ("failed to copy $file...<br>\n");
		}
		else
		{
			print ("$file... erfolgreich kopiert<br>\n");
		}
		
		$source = $filecopy;
		$image_details = getimagesize("$source");
		$width_src = $image_details[0];
		$height_src = $image_details[1];
		
		//Bildgröße überschreiben
		$width = '125';
		$height = '143';
		
		
		$source_gr = imagecreatefromjpeg($source);
		#echo DIR_FS_CATALOG.DIR_WS_IMAGES_SPECIALS."wasserzeichen.png";
		$imgzeichen = imagecreatefrompng(DIR_FS_CATALOG.DIR_WS_IMAGES_SPECIALS."wasserzeichen_sm.png");
		
		
		
		// Bilder erzeugen
		$img = imagecreatetruecolor(500, 500);
		
		// Bild einfügen
		#imagecopy($img, $source_gr, 0, 0, 0, 0, $width, $height);
		imagecopyresampled($img, $source_gr, 0, 0, 0, 0, 500, 500,500,500);
		
		
		// Wasserzeichen einfügen
		imagecopy($img, $imgzeichen, 0, 0, 0, 0,500, 500);
		
		//Textbild erzeugen
		$black = ImageColorAllocate ($img, 0, 0, 0);
		$white = ImageColorAllocate ($img, 255, 255, 255);
		imagettftext ($img, 14, 0, 85, 175, $black, DIR_FS_CATALOG.DIR_WS_IMAGES_SPECIALS."arial.ttf",
					 $products_price_brutto);
		
		// Bild anzeigen
		imagejpeg($img, $filecopy);
		
		
		//Bild in DB eintragen
		//Datenbankeintrag
		$sql = "UPDATE specials_home SET specials_pic_url = '".$item['products_image']."' WHERE specials_id = '$specials_id'";
		#echo $sql;
		mysql_query($sql) or die(mysql_error());
		//Meldung ausgeben
		echo "<br>Datenbankeintrag/Aktualisierung erfolgreich!";
		
	}
	//Standardwerte setzen, wenn nichts eingegeben wurde
	//Standard-Untertitel wird überschrieben
	if($_GET['specials_subtitle'] != '')
	{
		$specials_subtitle = $_GET['specials_subtitle'];
	}
	
	
	
	$sql = "UPDATE specials_home SET 
	specials_subtitle = '".$specials_subtitle."',
	specials_product_id = '".$_GET['specials_product_id']."',
	specials_active = '".$_GET['specials_active']."'
	WHERE specials_id = '".$_SESSION['specials_id']."'
	";
	#echo $sql;
	mysql_query($sql) or die(mysql_error());
	
	//pic_upload_specials_home
	include('define_mainpage_picupload.inc.php');
}


if(isset($_GET['action']) && $_GET['action'] == 'edit')
{
	//Datensätze aus der DB holen
	$sql = "SELECT specials_id, specials_pic_url, specials_subtitle, specials_product_id, specials_active FROM specials_home WHERE specials_id= '".$_GET['specials_id']."'";
	#echo $sql;
	$specials_qry = mysql_query($sql);
	$specials= mysql_fetch_array($specials_qry);
	
	$_SESSION['specials_id'] = $_GET['specials_id'];
	
	//Edit-Form anzeigen
	include('define_mainpage_form.inc.php');
	
}
elseif($_GET['do'] == 'show'  &&  $_GET['shop_id'])
{

	//Datensätze aus der DB holen
	$sql = "SELECT specials_id, specials_pic_url, specials_subtitle, specials_product_id, specials_active FROM specials_home ORDER BY specials_id";
	#echo $sql;
	$specials_qry = mysql_query($sql);
	
	echo '<div style="border: solid 2px #CCCCCC; padding:10px;margin-bottom: 5px; text-align:center; width: 840px;"> ';
	
	$i = 0;
	while($specials= mysql_fetch_array($specials_qry))
	{
	
	echo '<div style="float:left; margin:2px"';
	if($specials['specials_active']=='0') echo ' class="noactive"';
	else echo  ' class="active"';
	echo '>';
	
	if($specials['specials_product_id']!='')
	{
		echo '<a href="../product_info.php?products_id='.$specials['specials_product_id'].'" target="_blank">';
		echo tep_image('../'.DIR_WS_IMAGES_SPECIALS.$specials['specials_pic_url'], $specials['specials_subtitle']);
		echo '</a>';
	} 
	else 
	{
		echo tep_image(DIR_WS_IMAGES_SPECIALS.$specials['specials_pic_url'], $specials['specials_subtitle']);
	}
	echo '<br>';
	echo '<a href="'.$PHP_SELF.'?specials_id='.$specials['specials_id'].'&amp;action=edit">bearbeiten &raquo;</a>';
	
	
	echo '</div>'."\n";
	$i++;
	if($i==5)
	{
		echo '<div style="clear:both;">';
		$i=0;
		
	}
	}
	echo '<div style="clear:both;">';
	echo '</div>';
}
?>
		  </td>
	    </tr>
	</table>
	
	</td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
