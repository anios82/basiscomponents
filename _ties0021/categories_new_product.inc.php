<?php


    $parameters = array('products_name' => '',
                       'products_description' => '',
                       'products_url' => '',
                       'products_seo_url' => '',
					   'products_seo_subhead' => '',
                       'products_id' => '',
                       'products_quantity' => '',
                       'products_model' => '',
                       'products_hemd_id' => '',
                       'products_weight' => '',
                       'products_date_added' => '',
                       'products_last_modified' => '',
                       'products_date_available' => '',
                       'products_status' => '',
                       'products_tax_class_id' => '',
                       'manufacturers_id' => '',
					   'products_sperrgut' => '',
					   'products_prioritaet' => '',
					   'products_serie' => '',
					   'products_del_nullbestand' => '',
					   'empf_VK_pay' => '',
		// ----------------#AA20080904----------------
					   'products_herstellerinfo' => '');

	foreach($aShops as $aShop) {
		$aShopParameters[$aShop['products_price_fieldname']] = '';
	}
	
	$parameters = array_merge($parameters, $aShopParameters);
	
    $pInfo = new objectInfo($parameters);
	
   
    if (isset($_GET['pID']) && empty($_POST)) {
    	
		/* properties for seo decription generator */	
		//$propertiesCats = $objPropertygenerator->getPropertiesCatsWithPatterns();
		//$propertiesKeywords = $objPropertygenerator->getKeywordsForProduct($_GET['pID']);
		
		// BOF MaxiDVD: Modified For Ultimate Images Pack!
		// ----------------#AA20080904---------------
		$aPriceFields = getPriceFieldsFromServerID();
		$sPriceFields = implode(', p.', $aPriceFields);
		
		$product_query = tep_db_query("select pd.products_name, pd.products_seo_url, pd.products_seo_subhead, pd.products_description, pd.products_url, pd.products_seo_keywords, pd.products_seo_subhead, pd.extshops_translated, p.products_id, p.products_quantity, p.products_model, p.".$sPriceFields.", p.products_hemd_id, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.manufacturers_id, products_prioritaet, products_serie, products_del_nullbestand, products_herstellerinfo from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$_GET['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '".LANGUAGE_ID."'");
		
		// EOF MaxiDVD: Modified For Ultimate Images Pack!
		$product = tep_db_fetch_array($product_query);
		
		$pInfo->objectInfo($product);
    } 
	elseif (tep_not_null($_POST)) {
		
		$pInfo->objectInfo($_POST);
		$products_name = $_POST['products_name'];
		$products_description = $_POST['products_description'];
		$products_url = $_POST['products_url'];
		$products_seo_url = $_POST['products_seo_url'];
		$products_seo_subhead = $_POST['products_seo_subhead'];
		
    }

    $manufacturers_array = array(array('id' => '', 'text' => TEXT_NONE));
    $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
    while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
      $manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
                                     'text' => $manufacturers['manufacturers_name']);
    }

    $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($tax_class = tep_db_fetch_array($tax_class_query)) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    #$languages = tep_get_languages();
	
	/*if($_GET['test'] == 'true'){
		
		echo '<pre>';
		print_r($languages);
		echo '</pre>';
	}*/

    if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
    switch ($pInfo->products_status) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
    }
?>
<link rel="stylesheet" type="text/css" href="includes/javascript/spiffyCal/spiffyCal_v2_1.css">


<script language="JavaScript" src="includes/javascript/spiffyCal/spiffyCal_v2_1.js"></script>
<script language="javascript" type="text/javascript"><!--
  var dateAvailable = new ctlSpiffyCalendarBox("dateAvailable", "new_product", "products_date_available","btnDate1","<?php echo $pInfo->products_date_available; ?>",scBTNMODE_CUSTOMBLUE);
//--></script>
<script language="javascript" type="text/javascript"><!--
var tax_rates = new Array();
<?php
    for ($i=0, $n=sizeof($tax_class_array); $i<$n; $i++) {
      if ($tax_class_array[$i]['id'] > 0) {
        echo 'tax_rates["' . $tax_class_array[$i]['id'] . '"] = ' . tep_get_tax_rate_value($tax_class_array[$i]['id']) . ';' . "\n";
      }
    }
?>

function doRound(x, places) {
  return Math.round(x * Math.pow(10, places)) / Math.pow(10, places);
}

function getTaxRate() {
	//by Gurkcity: fest auf 19% codiert
    return 19.00;
}

<?php
foreach($aPriceFields as $sPriceField) {
?>
function update_<?php echo $sPriceField; ?>_brutto() {
  var taxRate = getTaxRate();
  var grossValue = document.forms["new_product"].<?php echo $sPriceField; ?>.value;

  if (taxRate > 0) {
    grossValue = grossValue * ((taxRate / 100) + 1);
  }

  document.forms["new_product"].<?php echo $sPriceField; ?>_gross.value = doRound(grossValue, 4);
}

function update_<?php echo $sPriceField; ?>_netto() {
  var taxRate = getTaxRate();
  var netValue = document.forms["new_product"].<?php echo $sPriceField; ?>_gross.value;

  if (taxRate > 0) {
    netValue = netValue / ((taxRate / 100) + 1);
  }

  document.forms["new_product"].<?php echo $sPriceField; ?>.value = doRound(netValue, 4);
}
<?php
}
?>

//--></script>
<?php #AA20081002------------ ?>
<script language="javascript1.5" type="text/javascript">
function toggleVisibility(name1, name2) {
	if(document.new_product.catB2B.options[0].selected == true) document.getElementById(name2).style.visibility = 'hidden';
	else document.getElementById(name2).style.visibility = 'visible';
}
</script>

<script language="javascript" type="text/javascript">
<!--
function auswahl() {
 var f = document.forms[0]
 if (f.products_aktionsname.selectedIndex!=0){
   document.getElementById("tr-aktion-netto").style.display = "";
   document.getElementById("tr-aktion-netto").style.visibility = "visible";
   document.getElementById("tr-aktion-brutto").style.display = "";
   document.getElementById("tr-aktion-brutto").style.visibility = "visible";
 }
 else
 {
   document.getElementById("tr-aktion-netto").style.display = "none";
   document.getElementById("tr-aktion-netto").style.visibility = "hidden";
   document.getElementById("tr-aktion-brutto").style.display = "none";
   document.getElementById("tr-aktion-brutto").style.visibility = "hidden"; 
 }
}
-->
</script>
<?php

$sql = "SELECT * FROM gc_aktionsprogramme WHERE products_id = '".$_GET['pID']."'";
$aktions_query = tep_db_query($sql);
$aktion = tep_db_fetch_array($aktions_query);

?>


    <?php echo tep_draw_form('new_product', FILENAME_CATEGORIES, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=new_product_preview', 'post', 'enctype="multipart/form-data"'); ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo sprintf(TEXT_NEW_PRODUCT, tep_output_generated_category_path($current_category_id)); ?>
            <br /><a href="http://www.krawatten-ties.com/product_info.php?products_id=<?php echo $_GET['pID']; ?>" target="_blank">zum deutschen Produkt</a>
            </td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_STATUS; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('products_status', '1', $in_status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('products_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE; ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?><br><small>(YYYY-MM-DD)</small></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;'; ?><script language="javascript">dateAvailable.writeControl(); dateAvailable.dateFormat="yyyy-MM-dd";</script></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <!--<tr>
            <td class="main"><?php echo TEXT_PRODUCTS_MANUFACTURER; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('manufacturers_id', $manufacturers_array, $pInfo->manufacturers_id); ?></td>
          </tr>-->
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>

		  <tr>
		  	<td colspan='2' valign='top'>
		  		<div style='float: left; padding: 0px;'>
				<table cellpadding='0' cellpspacing='0'>
		  
<?php
    for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
?>
          <tr>
            <td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_NAME; ?></td>
            <td class="main"><?php echo tep_image('../images/layout/' . $aShops[$i]['shop_shortname'].'.gif', $aShops['shop_shortname']) . '&nbsp;' . tep_draw_input_field('products_name[' . $aShops[$i]['language_id'] . ']', (isset($products_name[$aShops[$i]['language_id']]) ? $products_name[$aShops[$i]['language_id']] : tep_get_products_name($pInfo->products_id, $aShops[$i]['language_id'])), 'size=\'50\''); ?></td>
          </tr>
<?php
    }
?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          
<?php
    for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
?>
      <tr>
        <td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_SEO_URL; ?></td>
        <td class="main"><?php echo tep_image('../images/layout/' . $aShops[$i]['shop_shortname'].'.gif', $aShops['shop_shortname']) . '&nbsp;' . tep_draw_input_field('products_seo_url[' . $aShops[$i]['language_id'] . ']', (isset($products_seo_url[$aShops[$i]['language_id']]) ? $products_seo_url[$aShops[$i]['language_id']] : tep_get_products_seo_url($pInfo->products_id, $aShops[$i]['language_id'])), 'size=\'50\''); ?></td>
       </tr>
<?php
    }
?>

 	<tr>
        <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>

<?php
    for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
?>
      <tr>
        <td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_SEO_SUBHEAD; ?></td>
        <td class="main"><?php echo tep_image('../images/layout/' . $aShops[$i]['shop_shortname'].'.gif', $aShops['shop_shortname']) . '&nbsp;' . tep_draw_input_field('products_seo_subhead[' . $aShops[$i]['language_id'] . ']', (isset($products_seo_subhead[$aShops[$i]['language_id']]) ? $products_seo_subhead[$aShops[$i]['language_id']] : tep_get_products_seo_subhead($pInfo->products_id, $aShops[$i]['language_id'])), 'size=\'50\''); ?></td>
       </tr>
<?php
    }
?>

 	<tr>
        <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>

<?php
    for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
?>
      <tr>
        <td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_SEO_KEYWORDS; ?></td>
        <td class="main"><?php echo tep_image('../images/layout/' . $aShops[$i]['shop_shortname'].'.gif', $aShops['shop_shortname']) . '&nbsp;';
		
		if(!isset($products_seo_keywords[$aShops[$i]['language_id']])) {
			$kwString = tep_get_products_seo_keywords($pInfo->products_id, $aShops[$i]['language_id']);
			$keywords = explode('|', $kwString);
		}
		
		for($c=0; $c<5; $c++):			
			echo tep_draw_input_field('products_seo_keywords[' . $aShops[$i]['language_id'] . '][]', (isset($products_seo_keywords[$aShops[$i]['language_id']][$c]) ? $products_seo_keywords[$aShops[$i]['language_id']][$c] : $keywords[$c]), 'size=\'15\'');
		endfor;
		
		?></td>		
       </tr>
<?php
    }
?>

	  		 </table></div>

	   </td>
	   
	   </tr>
	   <tr>
         <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
       </tr>
          <tr>

            <td class="main"><?php echo TEXT_PRODUCTS_TAX_CLASS; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_hidden_field('products_tax_class_id', 1, 'onchange="updateGross()"');
			#tep_draw_pull_down_menu('products_tax_class_id', $tax_class_array, 1, 'onchange="updateGross()"'); ?>19% Mwst.</td>
          </tr>
			
		<?php
			foreach($aPriceFields as $sShopShortname => $sPriceField) {
		?>
			
		  <tr bgcolor="#ebebff">
            <td class="main" colspan="2">Preis <?php echo $sShopShortname; ?></td>
		  <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_PRICE_NET; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field($sPriceField, $pInfo->$sPriceField, 'onKeyUp="update_'.$sPriceField.'_brutto()"'); ?> EUR</td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_PRICE_GROSS; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field($sPriceField.'_gross', $pInfo->$sPriceField, 'OnKeyUp="update_'.$sPriceField.'_netto()"'); ?> EUR</td>
          </tr>
			
		<?php
			}
		?>
<script language="javascript" type="text/javascript">

<?php
foreach($aPriceFields as $sPriceField) {
?>
update_<?php echo $sPriceField; ?>_brutto();
update_<?php echo $sPriceField; ?>_netto();
<?php
}
?>
</script>
<script language='Javascript'><!--
	
	function showPropertyContainer(ID, subID){
		
		var propCats = new Array();
		
		<?
		if($propertiesCats){
			$c = 0;
			foreach($propertiesCats as $catId=>$temp): ?>
				
				propCats[<?=$c?>] = '<?=$catId?>';
				
			<? $c++; endforeach;
		}
		?>
		
		// hide all blocks
		for(var i in propCats){
			
			divId = subID+'_'+propCats[i];
			document.getElementById(divId).style.display = 'none';
		}
				
		// show the block we want to see
		document.getElementById(ID).style.display = 'block';
	}
	
	function addPropertySentence(textareaID, sentence){
		
			document.getElementById(textareaID).value = sentence;
	}
	
--></script>
<?php
    for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
?>
          <tr>
            <td class="main" valign="top"><?php if ($i == 0) echo TEXT_PRODUCTS_DESCRIPTION; ?></td>
            <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="main" valign="top"><?php echo tep_image('../images/layout/' . $aShops[$i]['shop_shortname'].'.gif', $aShops['shop_shortname']); ?>&nbsp;</td>
                <td class="main" valign='top'>
                	<?php echo tep_draw_textarea_field('products_description[' . $aShops[$i]['language_id'] . ']', 'soft', '70', '15', (isset($products_description[$aShops[$i]['language_id']]) ? $products_description[$aShops[$i]['language_id']] : tep_get_products_description($pInfo->products_id, $aShops[$i]['language_id']))); ?>
                	<br /><br />
                	�bersetzungsstatus:&nbsp;&nbsp;<?
                	
                	$valueExtShopsTranslated = ($extshops_translated[$aShops[$i]['language_id']]) ? $extshops_translated[$aShops[$i]['language_id']] : tep_get_products_extshopsTranslated($pInfo->products_id, $aShops[$i]['language_id']);
                	?>
                	<select name="extshops_translated[<?=$aShops[$i]['language_id']?>]">
                		<option value="no"<?
                			if($valueExtShopsTranslated == 'no')   echo ' selected';
                		?>>Nein</option>
                		<option value="progress"<?
                			if($valueExtShopsTranslated == 'progress')   echo ' selected';
                		?>>in Bearbeitung</option>
                		<option value="done"<?
                			if($valueExtShopsTranslated == 'done')   echo ' selected';
                		?>>erledigt (fertig �bersetzt)</option>                	
                	</select>          
                	
                	</td>
			  
			  </tr>
            </table></td>
          </tr>
<?php
    }
?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_QUANTITY; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_quantity', $pInfo->products_quantity); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_MODEL; ?></td>
			
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_model', $pInfo->products_model); ?></td>
          </tr>
<?php //---------------#AA20080904-------------------- ?>
		  <tr>
			<td class="main"><?php echo TEXT_HERSTELLERINFO; ?></td>
			
			<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_herstellerinfo', $pInfo->products_herstellerinfo); ?></td>
			</tr>
<?php //--------Ende-------#AA20080904-------------------- ?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<!-- // BOF: MaxiDVD Added for Ulimited Images Pack! -->

          <tr>
           <td class="dataTableRow" valign="top"><span class="main"><?php echo TEXT_PRODUCTS_IMAGE_NOTE; ?></span></td>
           <td><?php echo tep_draw_separator('pixel_trans.gif', '24', '17" align="left') . tep_product_image($pInfo->products_model, 'sm', $pInfo->products_model, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="left" hspace="0" vspace="5"'); ?></td>
          </tr>
          <tr>
           <td class="dataTableRow" valign="top"><span class="main"><?php echo TEXT_PRODUCTS_IMAGE_MEDIUM; ?></span></td>
           <td><?php echo tep_draw_separator('pixel_trans.gif', '24', '17" align="left') . tep_product_image($pInfo->products_model, 'md', $pInfo->products_model, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="left" hspace="0" vspace="5"'); ?></td>
          </tr>
		  
		  <?php
		  /*
          <tr>
           <td class="dataTableRow" valign="top"><span class="main"><?php echo TEXT_PRODUCTS_IMAGE_LARGE; ?></span></td>
           <?php if ((HTML_AREA_WYSIWYG_DISABLE_JPSY == 'Disable') or (HTML_AREA_WYSIWYG_DISABLE == 'Disable')){ ?>
           <td class="dataTableRow" valign="top"><span class="smallText"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_file_field('products_image_lrg') . '<br>'; ?>
           <?php } else { ?>
           <td class="dataTableRow" valign="top"><span class="smallText"><?php echo '<table border="0" cellspacing="0" cellpadding="0"><tr><td class="main">' . tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp; </td><td class="dataTableRow">' . 
		   #tep_draw_textarea_field('products_image_lrg', 'soft', '70', '2', $pInfo->products_image_lrg)#by Gurkcity
		   tep_draw_input_field('products_image_lrg', $pInfo->products_image_lrg)
		    . tep_draw_hidden_field('products_previous_image_lrg', $pInfo->products_image_lrg) . '</td></tr></table>';
           } if (($_GET['pID']) && ($pInfo->products_image_lrg) != '')
           echo tep_draw_separator('pixel_trans.gif', '24', '17" align="left') . $pInfo->products_image_lrg . tep_image(DIR_WS_CATALOG_IMAGES . $pInfo->products_image_lrg, $pInfo->products_image_lrg, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="left" hspace="0" vspace="5"') . tep_draw_hidden_field('products_previous_image_lrg', $pInfo->products_image_lrg) . '<br>'. tep_draw_separator('pixel_trans.gif', '5', '15') . '&nbsp;<input type="checkbox" name="unlink_image_lrg" value="yes">' . TEXT_PRODUCTS_IMAGE_REMOVE_SHORT . '<br>' . tep_draw_separator('pixel_trans.gif', '5', '15') . '&nbsp;<input type="checkbox" name="delete_image_lrg" value="yes">' . TEXT_PRODUCTS_IMAGE_DELETE_SHORT . '<br>' . tep_draw_separator('pixel_trans.gif', '1', '42'); ?></span></td>
          </tr>
*/
?>
 
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>

          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <!--<tr>
            <td class="main">Hemd_ID</td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_hemd_id', $pInfo->products_hemd_id); ?></td>
          </tr>
          <tr>
            <td class="main">ManschettenKnopf_ID</td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_mknopf_id', $pInfo->products_mknopf_id); ?></td>
          </tr>-->
          <tr>
            <td class="main"><?php echo TEXT_PRODUCTS_PRIORITAET; ?></td>
            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_prioritaet', $pInfo->products_prioritaet); ?></td>
          </tr>
		  <tr>
            <td class="main">&nbsp;</td>
            <td class="main"><?php echo tep_draw_hidden_field('products_del_nullbestand', '1'); ?></td>
          </tr>
<?php
####  ProductCategories-Copy-Modul by Gurkcity 31.08.2007 BOF
?>		  
		  <tr>
            <td class="p2c" colspan="2"><b>Kategorien</b> (Produkt wird verlinkt in folgende Kategorien)</td>
          </tr>
		  <tr>
            <td class="p2c" colspan="2">
			
				<?php
				$product_cat_array = array();
				//Pr�fen, in welchen Kategorien das Produkt gelistet wird
				$sql = "SELECT * FROM `products_to_categories` WHERE `products_id` = " . (int)$_GET['pID'];
					$product_cat_query = tep_db_query($sql);
					
					while($product_cat = tep_db_fetch_array($product_cat_query))
					{
						$product_cat_array[] = $product_cat['categories_id'];
					}
					#go_dump($product_cat_array);
					
					
				
				//Kategoriengruppen definieren
				#CG20081027 mod. by Gurkcity: Kategoriegruppen dynamisch, bestimmte unerw�nschte Kategorien ausblenden
				$excludeCategoriesArray = array(
					'','Archiv','Perlen' //diese Kategorien werden ausgeschlossen f�r die Anzeige
				);
				
				$query = mysql_query("SELECT categories_group FROM categories GROUP BY categories_group ORDER BY categories_group");
				while($fetch = mysql_fetch_array($query)) {
					if(!in_array($fetch['categories_group'],$excludeCategoriesArray))
						{
							$categoriesGroupArray[] = $fetch['categories_group'];
						}
				}
				/*$categoriesGroupArray = array(
					'Streifen','Muster','Farben','Designer','Sondergr/Mikro','Sonstiges','Firmenkrawatten'
				);*/
				
				#CG20081028 by Gurkcity B2B-Gruppierung und dynamische Anzeige
				foreach($categoriesGroupArray AS $categoriesGroupValue)
				{
				
				
					echo '<div class="categoriesGroup';
					
					if($categoriesGroupValue == '~B2B') //neue Klasse setzen, wenn es um die B2B-Gruppe geht
					{
						echo 'B2B';
					}
					
					echo '">
					
							<b>'.$categoriesGroupValue.'</b><br>';
					
					
					
					if($categoriesGroupValue == '~B2B')
					{
						$old_parent = '';//initialisieren der parent_id
						$parentbg = '60';//Blauwert der Hintergrundfarbe (wird dynamisch gesetzt)
						
						$sql = "SELECT * FROM categories c, categories_description cd WHERE c.categories_id = cd.categories_id AND cd.language_id = '".LANGUAGE_ID."' AND categories_group = '".$categoriesGroupValue."' ORDER BY parent_id, sort_order, categories_name"; //Sortierung nach parent_id -> Gruppierung
						$cat_query = tep_db_query($sql);
						
						//1. div wird ge�ffnet
						echo '<div style="background:rgb(168, 180, '.$parentbg.');width=200px;float:left;overflow:hidden">';
						
						while ($cat = tep_db_fetch_array($cat_query))
						{			
						
						$new_parent = $cat['parent_id'];
						
						if($old_parent != $new_parent)//�berpr�fung ob new_parent = old_parent
						{
						$old_parent = $new_parent;//wenn nicht, dann old_parent neu setzen...
						$parentbg = $parentbg + 20; //...Hintergrundfarbe ver�ndern...
						
						$sql = "SELECT categories_name FROM categories_description WHERE categories_id = '".$cat['parent_id']."'";//...Name des parents ermitteln...
						$parent_query = tep_db_query($sql);
						$parent = mysql_fetch_array($parent_query);
						
						
						echo '</div>';//...div schlie�en...
						echo '<div style="background:rgb(168, 180, '.$parentbg.');width:200px;float:left;overflow:hidden;margin:5px">';//...und neuen div �ffnen
						echo '<b>'.$parent['categories_name'].'</b>';
						}
						?>
							<div class="checkboxCatB2B"><input type="checkbox" name="cat<?=$cat['categories_id']?>" value="1"<?php if(in_array($cat['categories_id'],$product_cat_array)) echo " checked"?>> <?=$cat['categories_name']?></div>
						<?php
						
						}
						
						echo '</div>';//letzter div wird geschlossen
					}
					else
					{	
						$sql = "SELECT * FROM categories c, categories_description cd WHERE c.categories_id = cd.categories_id AND cd.language_id = '".LANGUAGE_ID."' AND categories_group = '".$categoriesGroupValue."' ORDER BY parent_id, sort_order, categories_name";
						$cat_query = tep_db_query($sql);
						while ($cat = tep_db_fetch_array($cat_query))
						{			
						?>
							<div class="checkboxCat"><input type="checkbox" name="cat<?=$cat['categories_id']?>" value="1"<?php if(in_array($cat['categories_id'],$product_cat_array)) echo " checked"?>> <?=$cat['categories_name']?></div>
						<?php
						
						}
					}
						
					
					
					echo '<div style="clear:both">&nbsp;</div></div>';
					
				}
					?>


			
			</td>
          </tr>		  
		 
<?php
####  ProductCategories-Copy-Modul by Gurkcity 31.08.2007 EOF
?>		  		  
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main" align="right"><?php echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d'))) . tep_image_submit('button_preview.gif', IMAGE_PREVIEW) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
      </tr>
    </table></form>
<script language="javascript" type="text/javascript">
<!--
auswahl();
-->
</script>
<?php #20081002----------------------------------------------   ?>
<script language="javascript1.5" type="text/javascript">
function toggleVisibility(name1, name2) {
	if(document.new_product.catB2B.options[0].selected == true) document.getElementById(name2).style.visibility = 'hidden';
	else document.getElementById(name2).style.visibility = 'visible';
}
toggleVisibility('catB2B', 'catB2B2')
</script>