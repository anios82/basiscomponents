<?php

// Test

//trigger_error('Das ist noch nicht fertig!!', E_USER_ERROR);

require('includes/application_top.php');

$id = '';
$shop_id = '';
$title = '';
$content = '';
$teaser = '';
$image = '';
$image_file = '';
$meta_desc = '';
$meta_title = '';

$image_width = '250';

if($_POST['shop_id']=='1')
	$shop_shortname = 'KRT';
if($_POST['shop_id']=='2')
	$shop_shortname = 'KRH';
if($_POST['shop_id']=='4')
	$shop_shortname = 'LAC';
if($_POST['shop_id']=='5')
	$shop_shortname = 'KRC';
if($_POST['shop_id']=='7')
	$shop_shortname = 'PAY';
if($_POST['shop_id']=='11')
	$shop_shortname = 'STM';


if(!empty($_GET['show_shop_id'])) {
	$show_shop_id = " AND shop_id='".$_GET['show_shop_id']."'";
	$show_shop_id_method = '?show_shop_id='.$_GET['show_shop_id'];
}
else {
	$show_shop_id = "";
	$show_shop_id_method = '';
}

if(!empty($_FILES['image']['name'])) {

	$uploaddir = DIR_FS_DOCUMENT_ROOT.'/images/glossar/';
	
	$uploadfile = basename($_FILES['image']['name']);
	$uploadfile_shopshortname = basename($_FILES['image']['name'], ".jpg").'_'.$shop_shortname.'.jpg';
	
	move_uploaded_file($_FILES['image']['tmp_name'], $uploaddir.$uploadfile);
	
	list($width, $height) = getimagesize($uploaddir.$uploadfile);
	$new_width = $image_width;
	
	if($width > $image_width)
		$new_height = floor($height / ($width / $image_width));
	elseif($width < $image_width)
		$new_height = floor($height * ($image_width / $width));
	else
		$new_height = $height;

	$image_p = imagecreatetruecolor($new_width, $new_height);
	$image_n = imagecreatefromjpeg($uploaddir.$uploadfile);
	imagecopyresampled($image_p, $image_n, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
	
	$image_file = 'images/glossar/'.$uploadfile_shopshortname;
	imagejpeg($image_p, $uploaddir.$uploadfile_shopshortname , 90);
}
elseif(!empty($_POST['id'])) {
	$image_file = $_POST['image_file'];
}
else {
	$image_file = '';
}

if(isset($_GET['del']) and is_numeric($_GET['del'])) {
	tep_db_query("DELETE FROM gc_mensfashion_atoz WHERE id='".$_GET['del']."'");
}
elseif(isset($_GET['edit']) and is_numeric($_GET['edit'])) {
	$query = tep_db_query("SELECT * FROM gc_mensfashion_atoz WHERE id='".$_GET['edit']."' LIMIT 1");
	$fetch = tep_db_fetch_array($query);
	$id = $fetch['id'];
	$shop_id = $fetch['shop_id'];
	$title = $fetch['title'];
	$content = $fetch['content'];
	$teaser = $fetch['teaser'];
	$image_file = $fetch['image'];
	$meta_desc = $fetch['meta_desc'];
	$meta_title = $fetch['meta_title'];
}

if(isset($_POST['id'], $_POST['shop_id'], $_POST['title'], $_POST['content'], $_POST['teaser'], $_POST['meta_desc'], $_POST['meta_title']) and is_numeric($_POST['id'])) 
	tep_db_query("UPDATE gc_mensfashion_atoz SET shop_id='".$_POST['shop_id']."', title='".$_POST['title']."', content='".$_POST['content']."', teaser='".$_POST['teaser']."', image='".$image_file."', meta_desc='".$_POST['meta_desc']."', meta_title='".$_POST['meta_title']."' WHERE id='".$_POST['id']."'");
elseif(isset($_POST['shop_id'], $_POST['title'], $_POST['content'], $_POST['teaser'], $_POST['meta_desc'], $_POST['meta_title']) and empty($_POST['id']))
	tep_db_query("INSERT INTO gc_mensfashion_atoz (id, shop_id, title, content, teaser, image, meta_desc, meta_title) VALUES ('', '".$_POST['shop_id']."', '".$_POST['title']."', '".$_POST['content']."', '".$_POST['teaser']."', '".$image_file."', '".$_POST['meta_desc']."', '".$_POST['meta_title']."')");

if(isset($_GET['edit']) and is_numeric($_GET['edit'])) 
	$query = tep_db_query("SELECT * FROM gc_mensfashion_atoz WHERE id!='".$_GET['edit']."'".$show_shop_id." ORDER BY title");
else
	$query = tep_db_query("SELECT * FROM gc_mensfashion_atoz WHERE id!=''".$show_shop_id."ORDER BY title");
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
<title>Krawatten-Ties.com - Glossar</title>
</head>

<body>
<form method="get" action="<?php echo $PHP_SELF; ?>">
	<p><a href="index.php">zur&uuml;ck</a> | Zeige nur 
    	<select name="show_shop_id">
        	<option value=""<?php if(!isset($_GET['show_shop_id'])) echo ' selected="selected"'; ?>>alle</option>
            <option value="1"<?php if($_GET['show_shop_id']=='1') echo ' selected="selected"'; ?>>KRT</option>
            <option value="2"<?php if($_GET['show_shop_id']=='2') echo ' selected="selected"'; ?>>KRH</option>
            <option value="4"<?php if($_GET['show_shop_id']=='4') echo ' selected="selected"'; ?>>LAC</option>
            <option value="5"<?php if($_GET['show_shop_id']=='5') echo ' selected="selected"'; ?>>KRC</option>
            <option value="7"<?php if($_GET['show_shop_id']=='7') echo ' selected="selected"'; ?>>PAY</option>
            <option value="11"<?php if($_GET['show_shop_id']=='11') echo ' selected="selected"'; ?>>STM</option>
        </select>
       	<input type="submit" value="Zeigen" />
    </p>
</form>
<table border="1">
	<tr>
    	<th>ID</th>
        <th>Shop-ID</th>
        <th>Titel</th>
        <th>Inhalt</th>
        <th>Teaser</th>
        <th>Bild</th>
        <th>Meta Desc</th>
        <th>Meta Title</th>
        <th>&nbsp;</th>
    </tr>
    <tr>
    	<form action="<?php echo $PHP_SELF.$show_shop_id_method; ?>" method="post" enctype="multipart/form-data">
    	<td><input type="hidden" name="id" value="<?php echo $id; ?>" />
       	  <input type="hidden" name="image_file" value="<?php echo $image_file; ?>" /></td>
        <td>
        	<select name="shop_id">
                <option value="1"<?php if($shop_shortname=='KRT') echo ' selected="selected"'; ?>>KRT</option>
                <option value="2"<?php if($shop_shortname=='KRH') echo ' selected="selected"'; ?>>KRH</option>
                <option value="4"<?php if($shop_shortname=='LAC') echo ' selected="selected"'; ?>>LAC</option>
                <option value="5"<?php if($shop_shortname=='KRC') echo ' selected="selected"'; ?>>KRC</option>
                <option value="7"<?php if($shop_shortname=='PAY') echo ' selected="selected"'; ?>>PAY</option>
                <option value="11"<?php if($shop_shortname=='STM') echo ' selected="selected"'; ?>>STM</option>
            </select>
        </td>
        <td><input type="text" name="title" value="<?php echo $title; ?>" maxlength="50" style="width: 200px" /><br />
        	- wird als &lt;h1&gt; angezeigt<br />
       	  - Erster Buchstabe ist der Index<br />
            - Max. 50 Buchstaben</td>
        <td><textarea name="content" style="width: 300px; height: 100px;"><?php echo $content; ?></textarea></td>
        <td><textarea name="teaser" style="width: 300px; height: 100px;"><?php echo $teaser; ?></textarea></td>
        <td><input type="file" name="image" maxlength="50" /><br />
        	- nur jpg Bild
        </td>
        <td><textarea name="meta_desc" style="width: 300px; height: 100px;"><?php echo $meta_desc; ?></textarea></td>
        <td><input type="text" name="meta_title" value="<?php echo $meta_title; ?>" maxlength="50" style="width: 200px" /><br />
       	  - Titel im Brwserfenster</td>
        <td><input type="submit" value="Speichern" /></td>
      </form>
    </tr>
    <?php
		while($fetch = tep_db_fetch_array($query)) {
			
			switch($fetch['shop_id']) {
				case '1':
					$shop = 'KRT';
					break;
				case '2':
					$shop = 'KRH';
					break;
				case '4':
					$shop = 'LAC';
					break;
				case '5':
					$shop = 'KRC';
					break;
				case '7':
					$shop = 'PAY';
					break;
				case '11':
					$shop = 'STM';
					break;
			}
			
			echo '<tr>';
			echo '<td>'.$fetch['id'].'</td>';
			echo '<td>'.$fetch['shop_id'].'</td>';
			echo '<td>'.$fetch['title'].'</td>';
			echo '<td>'.$fetch['content'].'</td>';
			echo '<td>'.$fetch['teaser'].'</td>';
			echo '<td><img src="../'.$fetch['image'].'" width="100" height="80" /></td>';
			echo '<td>'.$fetch['meta_desc'].'</td>';
			echo '<td>'.$fetch['meta_title'].'</td>';
			echo '<td><a href="'.$PHP_SELF.'?edit='.$fetch['id'].'">Bearbeiten</a> | <a href="'.$PHP_SELF.'?del='.$fetch['id'].'">L&ouml;schen</a></td>';
			echo '</tr>';
		}
	?>
</table>
</body>
</html>
