<?php
/*
  $Id: customers.php,v 1.82 2003/06/30 13:54:14 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

/* todo
 *  blog settings hinterlegen
 * kko, per kleinere bildgröße? (previewPicWIdth übersteuern)
 * kk, per 2 produktbilder nebeneinander
 * jeder blog hat eigene admin_externe (zu viele einst. die aus der jew. configure.inc.php gebraucht werden, FS-pfad, DB für Produkte etc.)
 */
  
  if(!$_SESSION['admin_externe']['module_is_registered']){
    $languageSess = 'de';
    require('includes/application_top.php');
  }
  else {
    require('includes/configure.php');
    require('includes/functions/database.php');
    require('includes/database_tables.php');
	
    tep_db_connect() or die('Unable to connect to database server!');
  }
  
  require(DIR_WS_INCLUDES.'classes/custom/abstract/simple_dbobject.php5');
  require(DIR_WS_INCLUDES.'classes/custom/seo/seoBlogs.php5');
  require_once(DIR_WS_INCLUDES.'classes/custom/shops_main.php');
  require_once(DIR_WS_INCLUDES.'classes/custom/libs/JSON.php');
  require_once(DIR_WS_INCLUDES.'classes/custom/libs/Xml2Assoc.php5');
  require(DIR_WS_INCS.'seo/blogcms/functions.inc.php');
  
  $form = &$_POST['FORM'];
  
  $settings = array(
        'blogs' => array(
                              'krt'=>array('id'=>'krt', 'name'=>'Krawatten-Ties.com', 'server'=>'1und1', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten-ties.com/de/Fashion-Blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>1, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten-ties.com/admin_externe/'),
                              'krawatten'=>array('id'=>'krawatten', 'name'=>'Krawatten.com', 'server'=>'1und1', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten.com/de/Mode-Blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>5, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten.com/admin_externe/'),
                              'lacravate'=>array('id'=>'lacravate', 'name'=>'Lacravate.com', 'server'=>'OVH', 'language'=>'deutsch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.lacravate.com/de/Mode-Blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>4, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.lacravate.com/admin_externe/'),
                              'krh'=>array('id'=>'krh', 'name'=>'Krawatte-hemd.de', 'server'=>'1und1', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatte-hemd.de/de/Mode-Blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>2, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatte-hemd.de/admin_externe/'),
                              'per'=>array('id'=>'per', 'name'=>'Perlen-perlenketten.de', 'server'=>'1und1', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.perlen-perlenketten.de/de/Schmuck-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>6, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.perlen-perlenketten.de/admin_externe/'),
                              'krawattenknoten'=>array('id'=>'krawattenknoten', 'name'=>'Krawattenknoten.org', 'server'=>'1und1', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawattenknoten.org/de/Krawatten-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>0, 'noProducts'=>true, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawattenknoten.org/admin_externe/'),
                              'krawatte'=>array('id'=>'krawatte', 'name'=>'Krawatte.net', 'server'=>'HE', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatte.net/de/Herrenmode-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>21, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatte.net/admin_externe/'),
                              'krawattemobile'=>array('id'=>'krawattemobile', 'name'=>'Mobile.Krawatte.net', 'server'=>'HE', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'mobile' : 'mobile').'.krawatte.net/de/Herrenmode-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>21, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'mobile' : 'mobile').'.krawatte.net/admin_externe/'),
                               'krawattenforum'=>array('id'=>'krawattenforum', 'name'=>'Krawattenforum.com', 'server'=>'HE', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawattenforum.com/de/Blog/nid1-9/index.html', 'nodeId'=>'1-9', 'shopId'=>14, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawattenforum.com/admin_externe/'),
                             'krawattenbinden'=>array('id'=>'krawattenbinden', 'name'=>'Krawatten-binden.de', 'server'=>'HE', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten-binden.de/de/Klassische-Krawattenknoten/nid1-1/index.html', 'nodeId'=>'1-1', 'noProducts'=>true, 'furtherNodeIds'=>array('1-2'=>'Klassische Krawattenknoten', '1-3'=>'Moderne Krawattenknoten', '1-4'=>'Exotische Krawattenknoten'), 'shopId'=>14, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.krawatten-binden.de/admin_externe/'),
                             'etc'=>array('id'=>'etc', 'name'=>'Einstecktuch.com', 'server'=>'HE', 'language'=>'deutsch', 'type'=>'unique', 'url'=>'http://'.(($_testserver) ? 'etc.krawatte.net' : 'www.einstecktuch.com').'./de/Mode-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'noProducts'=>false, 'shopId'=>25, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'etc.krawatte.net' : 'www.einstecktuch.com').'/admin_externe/'),
                             'stm'=>array('id'=>'stm', 'name'=>'Strodas-mode.nl', 'server'=>'OVH', 'language'=>'niederl�ndisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.stropdas-mode.nl/nl/Stropdassen-mode-blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>11, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.stropdas-mode.nl/admin_externe/'),
                             'sto'=>array('id'=>'sto', 'name'=>'Strodas.org', 'server'=>'OVH', 'language'=>'niederl�ndisch', 'type'=>'translated', 'sources'=>array('krh'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.stropdas.org/nl/Stropdas-mode-blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>24, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'test' : 'www').'.stropdas.org/admin_externe/'),
                              'cro'=>array('id'=>'cro', 'name'=>'Cravate.org', 'server'=>'OVH', 'language'=>'franz�sisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravate.org/fr/Blog-de-mode/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>15, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravate.org/admin_externe/'),
                              'cor'=>array('id'=>'cor', 'name'=>'Corbatas.es', 'server'=>'OVH', 'language'=>'spanisch', 'type'=>'translated', 'sources'=>array('krh'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbatas.es/es/Blog-en-la-moda/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>17, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbatas.es/admin_externe/'),
                              'coe'=>array('id'=>'coe', 'name'=>'Corbata.es', 'server'=>'OVH', 'language'=>'spanisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.es/es/Blog-de-corbata/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>18, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.es/admin_externe/'),                  
                              'cam'=>array('id'=>'cam', 'name'=>'Cravatta.mobi', 'server'=>'OVH', 'language'=>'italienisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.mobi/it/Fashion-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>22, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.mobi/admin_externe/'),
                              'crn'=>array('id'=>'crn', 'name'=>'Cravate.net', 'server'=>'OVH', 'language'=>'franz�sisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravate.net/fr/Blog-de-mode/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>27, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravate.net/admin_externe/'),
                               'cvo'=>array('id'=>'cvo', 'name'=>'Cravatta.org', 'server'=>'OVH', 'language'=>'italienisch', 'type'=>'translated', 'sources'=>array('krawatten'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.org/it/Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>28, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.org/admin_externe/'),
                                'crm'=>array('id'=>'crm', 'name'=>'Cravates.mobi', 'server'=>'OVH', 'language'=>'franz�sisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravates.mobi/fr/Le-blog-mode/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>16, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravates.mobi/admin_externe/'),
                                'stl'=>array('id'=>'stl', 'name'=>'Strop-dassen.nl', 'server'=>'FF', 'language'=>'niederl�ndisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.strop-dassen.nl/nl/Modeblog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>30, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.strop-dassen.nl/admin_externe/'),
                                'stn'=>array('id'=>'stn', 'name'=>'Stropdassen.net', 'server'=>'FF', 'language'=>'niderl�ndisch', 'type'=>'translated', 'sources'=>array('krawatten'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.stropdassen.net/nl/Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>31, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.stropdassen.net/admin_externe/'),
                                 'con'=>array('id'=>'con', 'name'=>'Corbata.net', 'server'=>'OVH', 'language'=>'spanisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.net/es/Blog-moda/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>29, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.net/admin_externe/'),
                                   'cem'=>array('id'=>'cem', 'name'=>'Cravatte.mobi', 'server'=>'OVH', 'language'=>'italienisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatte.mobi/it/moda-blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>23, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatte.mobi/admin_externe/'),
                                 'coo'=>array('id'=>'coo', 'name'=>'Corbata.org', 'server'=>'OVH', 'language'=>'spanisch', 'type'=>'translated', 'sources'=>array('krawatten'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.org/es/Blog-de-moda/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>33, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.corbata.org/admin_externe/'),
                                  'gro'=>array('id'=>'gro', 'name'=>'Gravata.org', 'server'=>'GoDaddy', 'language'=>'portugiesisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.gravata.org/pt/Blog-de-moda/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>32, 'productsCols'=>3, 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.gravata.org/admin_externe/'),
                                  'neo'=>array('id'=>'neo', 'name'=>'Nekutai.org', 'server'=>'Onamae', 'language'=>'japanisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.nekutai.org/jp/burogu/nid1-1/index.html', 'nodeId'=>'1-1', 'utf8shop'=>true, 'shopId'=>37, 'productsCols'=>3, 'furtherNodeIds'=>array('1-2'=>'Krawattenknoten &amp; Co.', '1-3'=>'Dresscode', '1-4'=>'Krawatten Farben', '1-5'=>'Krawatten-Informationen', '1-6'=>'Figurtipps', '1-7'=>'Krawatten-Marken'), 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.nekutai.org/admin_externe/'),
                                 'mto'=>array('id'=>'mto', 'name'=>'Mens-Ties.org', 'server'=>'OVH', 'language'=>'englisch', 'type'=>'translated', 'sources'=>array('krawatten'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.mens-ties.org/en/Tie-Blog/nid1-2/index.html', 'nodeId'=>'1-2', 'shopId'=>36, 'productsCols'=>3, 'furtherNodeIds'=>array('1-2'=>'Krawattenknoten &amp; Co.', '1-3'=>'How to tie a tie', '1-4'=>'Dresscode', '1-5'=>'Tie Colours', '1-6'=>'Tie Care'), 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.mens-ties.org/admin_externe/'),
                                 'ntn'=>array('id'=>'ntn', 'name'=>'Necktie.net', 'server'=>'OVH', 'language'=>'englisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.necktie.net/en/Mensfashion-Blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>37, 'productsCols'=>3, 'furtherNodeIds'=>array(), 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.necktie.net/admin_externe/'),
                                 'nec'=>array('id'=>'nec', 'name'=>'Nekutai.com', 'server'=>'Onamae', 'language'=>'japanisch', 'type'=>'translated', 'sources'=>array('krt'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.nekutai.com/jp/blog/nid1-1/index.html', 'nodeId'=>'1-1', 'utf8shop'=>true, 'shopId'=>34, 'productsCols'=>3, 'furtherNodeIds'=>array(), 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.nekutai.com/admin_externe/'),
                                 'cai'=>array('id'=>'cai', 'name'=>'Cravatta.it', 'server'=>'OVH', 'language'=>'italienisch', 'type'=>'translated', 'sources'=>array('krh'), 'url'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.it/it/Cravatta-mode-blog/nid1-1/index.html', 'nodeId'=>'1-1', 'shopId'=>39, 'productsCols'=>3, 'furtherNodeIds'=>array(), 'adminUrl'=>'http://'.(($_testserver) ? 'www' : 'www').'.cravatta.it/admin_externe/'),
                                 'njn'=>array('id'=>'njn', 'name'=>'[nekutai].jp.net', 'server'=>'Onamae', 'language'=>'japanisch', 'type'=>'translated', 'sources'=>array('krawatten'), 'url'=>'http://xn--eckva2cwc.jp.net/jp/nekutai/nid1-1/index.html', 'nodeId'=>'1-1', 'utf8shop'=>true, 'shopId'=>41, 'productsCols'=>3, 'furtherNodeIds'=>array(), 'adminUrl'=>'http://xn--eckva2cwc.jp.net/admin_externe/'),
                                 'grm'=>array('id'=>'grm', 'name'=>'Gravata.mobi', 'server'=>'GoDaddy', 'language'=>'portugiesisch', 'type'=>'translated', 'sources'=>array('lacravate'), 'url'=>'http://www.gravata.mobi/pt/blogs-sobre-gravatas/nid1-2/index.html', 'nodeId'=>'1-2', 'utf8shop'=>false, 'shopId'=>40, 'productsCols'=>3, 'furtherNodeIds'=>array('1-3'=>'DRESSCODE', '1-4'=>'COLOURS', '1-5'=>'TIPS', '1-6'=>'BRANDS', '1-7'=>'KNOTS'), 'adminUrl'=>'http://www.gravata.mobi/admin_externe/'),
                                 
                                 
        ),
        
        'servers'=>array(
                              '1und1'=>array('id'=>'1und1', 'name'=>'1und1', 'exportGateway'=> 'http://gateway.krawatten-ties.com/gateway_extshop_createBlogxmlAdminExterne.php', 'exportFtpHost'=>'krawatten-ties.com', 'exportFtpUser'=>'u58312236', 'exportFtpPass'=>'Lmauf68Zstbr;', 'exportFtpFolder'=>'subd.gateway.krawatten-ties.com/dataExchange/blogXml'),
                              'HE'=>array('id'=>'HE', 'name'=>'Hosteurope'),
                              'OVH'=>array('id'=>'OVH', 'name'=>'OVH',  'exportGateway'=> 'http://www.lacravate.com/gateway_extshop_createBlogxmlAdminExterne.php', 'exportFtpHost'=>'188.165.248.35', 'exportFtpUser'=>'lacravat', 'exportFtpPass'=>'Bofzy42H', 'exportFtpFolder'=>'./www/dataExchange/blogXml'),
                                'FF'=>array('id'=>'FF', 'name'=>'Firstfind.nl'),
                                'GoDaddy'=>array('id'=>'GoDaddy', 'name'=>'GoDaddy.com'),
                                'Onamae'=>array('id'=>'Onamae', 'name'=>'Onamae.com'),
                                
        ),
        
        'previewPicWidth' => 220,
        'previewPicAllowedTypes' => array('jpg','gif'),
        'productsRows' => 4,
        'tempblogsBgcolor' => '#F9AAA2',
        'importDirectory' => DIR_FS_CATALOG.'admin_externe/dataExchange/blogXml',
  );
  
  $action = (isset($_GET['action']) ? $_GET['action'] : (isset($_POST['action']) ? $_POST['action'] : ''));
  $do = (isset($_GET['do']) ? $_GET['do'] : (isset($_POST['do']) ? $_POST['do'] : ''));
  $blogID = (isset($_GET['blogID']) ? $_GET['blogID'] : (isset($_POST['blogID']) ? $_POST['blogID'] : ''));
  $docID = (isset($_GET['docID']) ? $_GET['docID'] : (isset($_POST['docID']) ? $_POST['docID'] : ''));
  
   $_blog = $settings['blogs'][$blogID];
   $_server = $settings['servers'][$settings['blogs'][$blogID]['server']];
  
  $objBlogs = seoBlogsHandle::_getInstance($blogID, $settings['blogs'][$blogID]);
  $objShopsMain = new shops_main();
  
  $shopsPermit = range(1,100);
  $shopInfo = get_all_shops($shopsPermit);
  
  $languagesInfo = $objShopsMain->getAllLanguagesWithShopData();



  
 /* define heading title and inc module on action */
 switch($action){
  
  case 'create':
    $inc = 'create.inc.php';
    break;  
  
  case 'edit':
    $inc = 'edit.inc.php';
    break;  
    
  case 'ajax':
    $inc = 'ajax.inc.php';
    break;  
  
  case 'import':
    $inc = 'import.inc.php';
    break; 
    
  default: 
    $inc = 'index.inc.php';
    break;
 }

if($inc == 'ajax.inc.php'){
  require(DIR_WS_INCS.'seo/blogcms/'.$inc);
}
elseif(!$_SESSION['admin_externe']['module_is_registered']){

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="<? echo DIR_WS_INCS?>seo/linkbuilding/styles.css">
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript" src="includes/javascript/prototype.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("language", "1");
</script>
</head>
<body bgcolor="#FFFFFF" onLoad="SetFocus();">
<!-- header //-->
<?php 
if($inc != 'history.inc.php'  &&  $inc != 'writemessage.inc.php'){
  require(DIR_WS_INCLUDES . 'header.php'); 
}
?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php 
if($inc != '...'){
  require(DIR_WS_INCLUDES . 'column_left.php');
} ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">

  <table id='seo_linkbuilding' border="0" width="100%" cellspacing="0" cellpadding="2">
        
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><? echo HEADING_TITLE ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
     
     <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
     <tr>
        <td style='padding-left: 50px;'><?
          
       
      require(DIR_WS_INCS.'seo/blogcms/'.$inc);
      
    ?></td>
    </tr>
     
     
  </table>


  </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php 
if($inc != '...'){
   require(DIR_WS_INCLUDES . 'footer.php');
}
?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php');
}
else {  ?>  
  <style type="text/css"><?
  readfile(DIR_WS_BASISCOMP_BASE.'_ties0021/includes/stylesheet.css');
  readfile(DIR_WS_BASISCOMP_BASE.'_ties0021/includes/incs/seo/blogcms/styles.css');
  ?></style>
  <?
  require(DIR_WS_INCS.'seo/blogcms/'.$inc);
}
?>