<?php
//Stoppuhr Start
function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$parse1 = microtime_float();
/*
  $Id: categories.php,v 1.146 2003/07/11 14:40:27 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/


  require('includes/application_top.php');
  #require(DIR_WS_INCLUDES.'classes/custom/propertygenerator/propertygenerator.php');
  #require(DIR_WS_INCLUDES.'classes/custom/categories/categories.php');
  
  #$objPropertygenerator = new propertygenerator();
  $aShops = getShopsFromServer(SERVER_ID);
  
//Sortierung by Gurkcity 30.04.2007 BOF
if(!isset($_SESSION['order_by']))
{
		$_SESSION['order_by'] = "p.products_prioritaet";
}
else if(isset($_GET['order_by']))
{
	$_SESSION['order_by'] = $_GET['order_by'];
 # go_dump($_SESSION);
}
else
{
	$_SESSION['order_by'] = $_SESSION['order_by'];
}

//noch keine Session-Variable?
if(!isset($_SESSION['ascdesc_by']))
{
		$_SESSION['ascdesc_by'] = "DESC";
}
//Neues Sortierkriterium?
else if(isset($_GET['oldascdesc_by']))
{
	$_SESSION['ascdesc_by'] = ($_GET['oldascdesc_by']=='ASC'?'DESC':'ASC');
}
//Sortierkriterium durchgeschleift?
else if(isset($_GET['ascdesc_by']))
{
	$_SESSION['ascdesc_by'] = $_GET['ascdesc_by'];
}
//Sessionvariable bereits vorhanden!
else
{
	$_SESSION['ascdesc_by'] = $_SESSION['ascdesc_by'];
}	
 #go_dump($_SESSION);
//Sortierung by Gurkcity 30.04.2007 EOF  

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

// Ultimate SEO URLs BEGIN
// If the action will affect the cache entries
    if (eregi("(insert|update|setflag)", $action))
        include_once ('includes/reset_seo_cache.php');
// Ultimate SEO URLs END

  if (tep_not_null($action)) {
    switch ($action) {
      case 'setflag':
        if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
          /*if (isset($_GET['pID'])) {
            tep_set_product_status($_GET['pID'], $_GET['flag']);
          }*/
		  // BOF Enable - Disable Categories Contribution--------------------------------------

    if ($_GET['pID']) {
            tep_set_product_status($_GET['pID'], $_GET['flag']);
    }
    if ($_GET['cID']) {
            tep_set_categories_status($_GET['cID'], $_GET['flag']);
    }
// EOF Enable - Disable Categories Contribution----------------------------------

          if (USE_CACHE == 'true') {
            tep_reset_cache_block('categories');
            tep_reset_cache_block('also_purchased');
          }
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID']));
        break;
      case 'insert_category':
      case 'update_category':
        if (isset($_POST['categories_id'])) $categories_id = tep_db_prepare_input($_POST['categories_id']);
        $sort_order = tep_db_prepare_input($_POST['sort_order']);

        // BOF Enable - Disable Categories Contribution--------------------------------------
/*
        $sql_data_array = array('sort_order' => $sort_order);
*/
        $categories_status = tep_db_prepare_input($_POST['categories_status']);
        $sql_data_array = array('sort_order' => $sort_order, 'categories_status' => $categories_status);
// EOF Enable - Disable Categories Contribution--------------------------------------

        if ($action == 'insert_category') {
          $insert_sql_data = array('parent_id' => $current_category_id,
                                   'date_added' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

          tep_db_perform(TABLE_CATEGORIES, $sql_data_array);

          $categories_id = tep_db_insert_id();
        } elseif ($action == 'update_category') {
          $update_sql_data = array('last_modified' => 'now()');

          $sql_data_array = array_merge($sql_data_array, $update_sql_data);

          tep_db_perform(TABLE_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "'");
        }

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
          $categories_name_array = $_POST['categories_name'];
          $categories_seo_url_array = $_POST['categories_seo_url'];

          $language_id = $languages[$i]['id'];

          $sql_data_array = array('categories_name' => tep_db_prepare_input($categories_name_array[$language_id]),
                                  'categories_seo_url' => tep_db_prepare_input($categories_seo_url_array[$language_id]));

          if ($action == 'insert_category') {
            $insert_sql_data = array('categories_id' => $categories_id,
                                     'language_id' => $languages[$i]['id']);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
          } elseif ($action == 'update_category') {
            tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
          }
        }

        if ($categories_image = new upload('categories_image', DIR_FS_CATALOG_IMAGES)) {
          tep_db_query("update " . TABLE_CATEGORIES . " set categories_image = '" . tep_db_input($categories_image->filename) . "' where categories_id = '" . (int)$categories_id . "'");
        }
		
		tep_db_query("update " . TABLE_CATEGORIES . " set categories_group = '" . $_POST['categories_group'] . "' where categories_id = '" . (int)$categories_id . "'");
		
        if (USE_CACHE == 'true') {
          tep_reset_cache_block('categories');
          tep_reset_cache_block('also_purchased');
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&cID=' . $categories_id));
        break;
      case 'delete_category_confirm':
        if (isset($_POST['categories_id'])) {
          $categories_id = tep_db_prepare_input($_POST['categories_id']);

          $categories = tep_get_category_tree($categories_id, '', '0', '', true);
          $products = array();
          $products_delete = array();

          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
            $product_ids_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories[$i]['id'] . "'");

            while ($product_ids = tep_db_fetch_array($product_ids_query)) {
              $products[$product_ids['products_id']]['categories'][] = $categories[$i]['id'];
            }
          }

          reset($products);
          while (list($key, $value) = each($products)) {
            $category_ids = '';

            for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
              $category_ids .= "'" . (int)$value['categories'][$i] . "', ";
            }
            $category_ids = substr($category_ids, 0, -2);

            $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$key . "' and categories_id not in (" . $category_ids . ")");
            $check = tep_db_fetch_array($check_query);
            if ($check['total'] < '1') {
              $products_delete[$key] = $key;
            }
          }

// removing categories can be a lengthy process
          tep_set_time_limit(0);
          for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
            tep_remove_category($categories[$i]['id']);
          }

          reset($products_delete);
          while (list($key) = each($products_delete)) {
            tep_remove_product($key);
          }
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('categories');
          tep_reset_cache_block('also_purchased');
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath));
        break;
      case 'delete_product_confirm':
        if (isset($_POST['products_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {
          $product_id = tep_db_prepare_input($_POST['products_id']);
          $product_categories = $_POST['product_categories'];

          for ($i=0, $n=sizeof($product_categories); $i<$n; $i++) {
            tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "' and categories_id = '" . (int)$product_categories[$i] . "'");
          }

          $product_categories_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
          $product_categories = tep_db_fetch_array($product_categories_query);

          if ($product_categories['total'] == '0') {
            tep_remove_product($product_id);
          }
        }

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('categories');
          tep_reset_cache_block('also_purchased');
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath));
        break;
      case 'move_category_confirm':
        if (isset($_POST['categories_id']) && ($_POST['categories_id'] != $_POST['move_to_category_id'])) {
          $categories_id = tep_db_prepare_input($_POST['categories_id']);
          $new_parent_id = tep_db_prepare_input($_POST['move_to_category_id']);

          $path = explode('_', tep_get_generated_category_path_ids($new_parent_id));

          if (in_array($categories_id, $path)) {
            $messageStack->add_session(ERROR_CANNOT_MOVE_CATEGORY_TO_PARENT, 'error');

            tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&cID=' . $categories_id));
          } else {
            tep_db_query("update " . TABLE_CATEGORIES . " set parent_id = '" . (int)$new_parent_id . "', last_modified = now() where categories_id = '" . (int)$categories_id . "'");

            if (USE_CACHE == 'true') {
              tep_reset_cache_block('categories');
              tep_reset_cache_block('also_purchased');
            }

            tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $new_parent_id . '&cID=' . $categories_id));
          }
        }

        break;
      case 'move_product_confirm':
        $products_id = tep_db_prepare_input($_POST['products_id']);
        $new_parent_id = tep_db_prepare_input($_POST['move_to_category_id']);

        $duplicate_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$new_parent_id . "'");
        $duplicate_check = tep_db_fetch_array($duplicate_check_query);
        if ($duplicate_check['total'] < 1) tep_db_query("update " . TABLE_PRODUCTS_TO_CATEGORIES . " set categories_id = '" . (int)$new_parent_id . "' where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$current_category_id . "'");

        if (USE_CACHE == 'true') {
          tep_reset_cache_block('categories');
          tep_reset_cache_block('also_purchased');
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $new_parent_id . '&pID=' . $products_id));
        break;
      case 'insert_product':
      case 'update_product':
        if (isset($_POST['edit_x']) || isset($_POST['edit_y'])) {
          $action = 'new_product';
        } else {

          if (isset($_GET['pID'])) $products_id = tep_db_prepare_input($_GET['pID']);
          $products_date_available = tep_db_prepare_input($_POST['products_date_available']);

          $products_date_available = (date('Y-m-d') < $products_date_available) ? $products_date_available : 'null';
		  
		  //$sql_price_array[] = array();
		  
		  $aPriceFields = getPriceFieldsFromServerID();
		  foreach($aPriceFields as $sPriceFields) {
			$sql_price_array[$sPriceFields] = tep_db_prepare_input($_POST[$sPriceFields]);
		  }
		  
		  $sql_data_array = array('products_quantity' => tep_db_prepare_input($_POST['products_quantity']),
                                  'products_model' => tep_db_prepare_input($_POST['products_model']),
		  
		   						PRODUCTS_PRICE => tep_db_prepare_input($_POST[PRODUCTS_PRICE]),
		   
                                  'products_hemd_id' => tep_db_prepare_input($_POST['products_hemd_id']),
								  'products_date_available' => $products_date_available,
                                  'products_weight' => tep_db_prepare_input($_POST['products_weight']),
                                  'products_status' => tep_db_prepare_input($_POST['products_status']),
                                  'products_tax_class_id' => tep_db_prepare_input($_POST['products_tax_class_id']),
                                  'manufacturers_id' => tep_db_prepare_input($_POST['manufacturers_id']),
								  'products_prioritaet' => tep_db_prepare_input($_POST['products_prioritaet']),
								  'products_serie' => tep_db_prepare_input($_POST['products_serie']),
								  'products_del_nullbestand' => tep_db_prepare_input($_POST['products_del_nullbestand']),
					// ------------ #AA20080904 -------------------------
								  'products_herstellerinfo' => tep_db_prepare_input($_POST['products_herstellerinfo'])
					// -- Ende----- #AA20080904 -------------------------
								  ); 
			$sql_data_array = array_merge($sql_data_array, $sql_price_array);
//Aktionsprogramm by Gurkcity 25.06.2008 BOF

$sql = "DELETE FROM gc_aktionsprogramme WHERE products_id = '".$products_id."'";
mysql_query($sql);

if(tep_not_null($_POST['products_aktionsname']))
{
	
	$sql_data_array_aktion = array('aktionsname' => tep_db_prepare_input($_POST['products_aktionsname']),
                                  'products_id' => tep_db_prepare_input((int)$products_id),
                                  'aktionspreis_netto' => tep_db_prepare_input($_POST['products_price4_aktion']),
								  'aktionspreis_brutto' => tep_db_prepare_input($_POST['products_price4_gross_aktion'])
								  ); 
	tep_db_perform('gc_aktionsprogramme', $sql_data_array_aktion);								  
}

/* save keywords */
/*
$keywordsCleaned = $objPropertygenerator->deleteKeywordsForProduct($products_id);

if($keywordsCleaned){
	if($_POST['products_keywords']){

		foreach($_POST['products_keywords'] as $language_id=>$kws):
			
			foreach($kws as $kw):
				$objPropertygenerator->createKeyword($kw, $language_id, $products_id);
			endforeach;
		endforeach;
	}
}*/
/* end save keywords */

								  
//Aktionsprogramm by Gurkcity 25.06.2008 EOF								  								  



          if ($action == 'insert_product') {
            $insert_sql_data = array('products_date_added' => 'now()');

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_PRODUCTS, $sql_data_array);
            $products_id = tep_db_insert_id();

            tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$current_category_id . "')");
          } elseif ($action == 'update_product') {
            $update_sql_data = array('products_last_modified' => 'now()');

            $sql_data_array = array_merge($sql_data_array, $update_sql_data);

            tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
          }

          
/* #######  ProductCategories-Copy-Modul by Gurkcity 31.08.2007 BOF  ############*/  
		  
		  
		  ###############  Teil1: Produkte verlinken (ein Produkt in Kategorien kopieren)
		  
		  //erstmal alle Kategorien l�schen, die zum Produkt geh�ren
		  $sql = "DELETE FROM ".TABLE_PRODUCTS_TO_CATEGORIES." WHERE products_id = '" . (int)$products_id . "'";
		  tep_db_query($sql);
		  
		  $sql = "SELECT categories_id FROM ".TABLE_CATEGORIES;
		  $cat_qry = tep_db_query($sql);
		  
		  //alle Kategorien durchgehen und mit Postvariable vergleichen, bei �bereinstimmung Eintrag in Tabelle products_to_categories (damit wird Produkt verlinkt)
		  while ($cat = tep_db_fetch_array($cat_qry))
		  {
			  if(isset($_POST['cat'.$cat['categories_id']]) && $_POST['cat'.$cat['categories_id']] == '1' ) //z.B. $_POST['cat13'] == 1
			  {
			  		$catcopyid = $cat['categories_id'];
			  		$sql = "INSERT INTO " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) VALUES ('" . (int)$products_id . "', '" . (int)$catcopyid . "')";
			  		tep_db_query($sql);
			  }
		  }
		  
		  //B2B-Category erg�nzen
		  if(isset($_POST['catB2B'])) 
		  {
				$catcopyid = $_POST['catB2B'];
				$sql = "INSERT INTO " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) VALUES ('" . (int)$products_id . "', '" . (int)$catcopyid . "')";
				tep_db_query($sql);
		  }
		  //B2B-Category erg�nzen
		  if(isset($_POST['catB2B2']) and isset($_POST['catB2B']) and $_POST['catB2B'] != '0') 
		  {
				$catcopyid = $_POST['catB2B2'];
				$sql = "INSERT INTO " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) VALUES ('" . (int)$products_id . "', '" . (int)$catcopyid . "')";
				tep_db_query($sql);
		  }
	
          for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
            $language_id = $aShops[$i]['language_id'];

            $sql_data_array = array('products_name' => tep_db_prepare_input($_POST['products_name'][$language_id]),
							  'products_description' => tep_db_prepare_input($_POST['products_description'][$language_id]),
							  'products_url' => tep_db_prepare_input($_POST['products_url'][$language_id]),
							  'products_seo_url' => tep_db_prepare_input($_POST['products_seo_url'][$language_id]),
							  'products_seo_subhead' => tep_db_prepare_input($_POST['products_seo_subhead'][$language_id]),
							  'products_seo_keywords' => (($_POST['products_seo_keywords'][$language_id]) ? tep_db_prepare_input(implode('|', $_POST['products_seo_keywords'][$language_id])) : ''));
			

			//erst alles l�schen, dann neu einf�gen
			$sql = "DELETE FROM ".TABLE_PRODUCTS_DESCRIPTION." WHERE products_id = '".$products_id."' AND language_id = '".$language_id."'";
			tep_db_query($sql);
			
			$insert_sql_data = array('products_id' => $products_id,
                                       'language_id' => $language_id,
										'extshops_translated' => $_POST['extshops_translated'][$language_id]);

            $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

            tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array);
			
						
          }

          if (USE_CACHE == 'true') {
            tep_reset_cache_block('categories');
            tep_reset_cache_block('also_purchased');
          }
		  
		  if($_POST['catDupl21'] == '1' || $_POST['catDupl60'] == '1' || $_POST['catDupl61'] == '1' || $_POST['catDupl62'] == '1')
		  {
	
			//Array erzeugen, dass die zu duplizierenden Kategorien aufnimmt
			$catCopyArr = array();
			if(isset($_POST['catDupl21']) && $_POST['catDupl21'] == '1') $catCopyArr[] = '21';
			if(isset($_POST['catDupl60']) && $_POST['catDupl60'] == '1') $catCopyArr[] = '60';
			if(isset($_POST['catDupl61']) && $_POST['catDupl61'] == '1') $catCopyArr[] = '61';
			if(isset($_POST['catDupl62']) && $_POST['catDupl62'] == '1') $catCopyArr[] = '62';
			
			
			foreach($catCopyArr AS $catCopyArrValue)
			{
				switch($catCopyArrValue)
				{
					case 21:
						$products_model = tep_db_prepare_input($_POST['products_model']).'L';
						$products_quantity = tep_db_prepare_input($_POST['catDuplAnz21']);
						break;
					case 60:
						$products_model = tep_db_prepare_input($_POST['products_model']).'S';
						$products_quantity = tep_db_prepare_input($_POST['catDuplAnz60']);
						break;
					case 61:
						$products_model = tep_db_prepare_input($_POST['products_model']).'C';
						$products_quantity = tep_db_prepare_input($_POST['catDuplAnz61']);
						break;
					case 62:
						$products_model = tep_db_prepare_input($_POST['products_model']).'K';
						$products_quantity = tep_db_prepare_input($_POST['catDuplAnz62']);
						break;
				}
				
				//Eintr�ge in products
				$sql_data_array = array(                                  
								  'products_quantity' => $products_quantity,
								  'products_model' => $products_model,
								  'products_image' => '',
								  'products_image_med' => '',
								  'products_price' => tep_db_prepare_input($_POST['products_price']),
								  'products_date_added' => 'now()',
								  'products_prioritaet' => tep_db_prepare_input($_POST['products_prioritaet']),
								  'products_del_nullbestand' => '1',
								  'products_status' => '1',
								  'products_tax_class_id' => '1',
                                  'manufacturers_id' => tep_db_prepare_input($_POST['manufacturers_id']),
								  
								   PRODUCTS_PRICE => tep_db_prepare_input($_POST[PRODUCTS_PRICE]),
								  
								  'empf_VK_pay' => tep_db_prepare_input($_POST['empf_VK_pay']),
								  'products_qty_blocks' => '1',
								  'products_hemd_id' => tep_db_prepare_input($_POST['products_hemd_id']),
								  'products_mknopf_id' => tep_db_prepare_input($_POST['products_mknopf_id']),
								//--------- #AA20080904 -------------------------
								  'products_herstellerinfo' => tep_db_prepare_input($_POST['products_herstellerinfo'])
								 //----Ende----- #AA20080904 -------------------------						 
								  ); 
								  
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array,'insert');
            	$new_products_id = tep_db_insert_id();
				
				//Eintr�ge in products_description
				//Variablen werden noch vor der Abfrage initialisiert, da dies auch f�r Produkte zutrifft, die nicht nur dupliziert werden

				
				
				  for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
					$language_id = $aShops[$i]['language_id'];


					switch($catCopyArrValue)
					{
					case 21:
						$products_description = preg_replace('/#/',$descReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_description'][$language_id]));
						$products_name = preg_replace('/#/',$nameReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_name'][$language_id]));
						$products_url = tep_db_prepare_input($_POST['products_url'][$language_id]);
						$products_seo_url = tep_db_prepare_input($_POST['products_seo_url'][$language_id]);
						$products_seo_subhead = tep_db_prepare_input($_POST['products_seo_subhead'][$language_id]);
						$products_seo_keywords = tep_db_prepare_input($_POST['products_seo_keywords'][$language_id]);
						break;
					case 60:
					
					#DEBUG
					#echo $products_description = preg_replace('/#/',$descReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_description'][$language_id]));
					#echo tep_db_prepare_input($_POST['products_description']);
					#echo "<br />".$descReplace[$language_id][$catCopyArrValue];
					
						$products_description = preg_replace('/#/',$descReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_description'][$language_id]));
						$products_name = preg_replace('/#/',$nameReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_name'][$language_id]));
						$products_url = tep_db_prepare_input($_POST['products_url'][$language_id]);
						$products_seo_url = tep_db_prepare_input($_POST['products_seo_url'][$language_id]);
						$products_seo_subhead = tep_db_prepare_input($_POST['products_seo_subhead'][$language_id]);
						$products_seo_keywords = tep_db_prepare_input($_POST['products_seo_keywords'][$language_id]);
						break;
					case 61:
						$products_description = preg_replace('/#/',$descReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_description'][$language_id]));
						$products_name = preg_replace('/#/',$nameReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_name'][$language_id]));
						$products_url = tep_db_prepare_input($_POST['products_url'][$language_id]);
						$products_seo_url = tep_db_prepare_input($_POST['products_seo_url'][$language_id]);
						$products_seo_subhead = tep_db_prepare_input($_POST['products_seo_subhead'][$language_id]);
						$products_seo_keywords = tep_db_prepare_input($_POST['products_seo_keywords'][$language_id]);
						break;
					case 62:
						$products_description = preg_replace('/#/',$descReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_description'][$language_id]));
						$products_name = preg_replace('/#/',$nameReplace[$language_id][$catCopyArrValue],tep_db_prepare_input($_POST['products_name'][$language_id]));
						$products_url = tep_db_prepare_input($_POST['products_url'][$language_id]);
						$products_seo_url = tep_db_prepare_input($_POST['products_seo_url'][$language_id]);
						$products_seo_subhead = tep_db_prepare_input($_POST['products_seo_subhead'][$language_id]);
						$products_seo_keywords = tep_db_prepare_input($_POST['products_seo_keywords'][$language_id]);
						break;
					}

				 $sql_data_array = array('products_name' => $products_name,
							  'products_description' => $products_description,
							  'products_url' => $products_url,
							  'products_seo_url' => $products_seo_url,
							  'products_seo_subhead' => $products_seo_subhead,
							  'products_seo_keywords' => $products_seo_keywords);
	
				 $insert_sql_data = array('products_id' => $new_products_id,
										   'language_id' => $language_id);
	
				 $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

	
				tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array);
			

				
				
				
			}
			
			#echo '---------------------<br />yes';
			#go_dump($_POST);
			
			
			//Eintr�ge in products_to_categories
			
			switch($catCopyArrValue)
			{
				case 21:
					$sql_data_array = array(
							  'products_id' => $new_products_id,
							  'categories_id' => '21'							 
							  ); 
					break;
				case 60:
					$sql_data_array = array(
							  'products_id' => $new_products_id,
							  'categories_id' => '60'							 
							  ); 
					break;
				case 61:
					$sql_data_array = array(
							  'products_id' => $new_products_id,
							  'categories_id' => '61'							 
							  ); 
					break;
				case 62:
					$sql_data_array = array(
							  'products_id' => $new_products_id,
							  'categories_id' => '62'							 
							  ); 
					
					break;
			}
			tep_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array);
			
		  }
		  
		  }
		  
		  

/* #######  ProductCategories-Copy-Modul by Gurkcity 31.08.2007 EOF  ############*/  
		  
/*Serien-Transfer in Parsley-Krawatten.de // B2B-Shop by Gurkcity 14.08.2007 BOF */
		  
		  //Datenbank schlie�en
		  #tep_db_close();
		  
		  #go_dump($_POST);
		  
		 
		if($_POST['transferSerie'] == '1')
	 	{
			//Aufbau zur neuen Datenbank
			tep_db_connect(DB_SERVER_B2B, DB_SERVER_B2B_USERNAME, DB_SERVER_B2B_PASSWORD, DB_B2B_DATABASE, 'link_B2B') or die('Unable to connect to database server!');
			  
			
			
			//Kategorie_id holen, die zur Serie passt
			$b2bcat_id = '';
			$sql = "SELECT categories_id FROM categories_description WHERE categories_name = 'Serie ".tep_db_prepare_input($_POST['products_serie'])."' LIMIT 1";
			$cat_query = mysql_query($sql) or die(mysql_error());
			while($cat = mysql_fetch_array($cat_query))
			{
				$b2bcat_id = $cat['categories_id'];
			}
			
			//Update oder Neues Produkt?
			if($_POST['updatePAY'] == '1')
			{
				if($b2bcat_id != '')
				{
					//Eintr�ge in products
					$sql_data_array = array(
									  'products_quantity' => tep_db_prepare_input($_POST['catDuplAnzB2B']),
									  'products_last_modified' => 'now()',			 
									  ); 
					tep_db_perform(TABLE_PRODUCTS, $sql_data_array,'update',"products_model = '".tep_db_prepare_input($_POST['products_model'])."'",'link_B2B');
				}
			}
			else
			{
				
				if($b2bcat_id != '')
				{
					//Eintr�ge in products
					$sql_data_array = array(
									  'products_model' => tep_db_prepare_input($_POST['products_model']),
									  'products_quantity' => tep_db_prepare_input($_POST['catDuplAnzB2B']),
									  'products_ean' => '',
									  'products_shippingtime' => '1',
									  'manufacturers_id' => tep_db_prepare_input($_POST['manufacturers_id']),
									  'products_price' => tep_db_prepare_input($_POST['products_price']),
									  'products_date_added' => 'now()',
									  'products_status' => '0',
									  'products_tax_class_id' => '1',
									  'product_template' => 'default',
									  'options_template' => 'default'								 
									  ); 
					tep_db_perform(TABLE_PRODUCTS, $sql_data_array,'insert','','link_B2B');
					$b2b_products_id = tep_db_insert_id();
					
					//Eintr�ge in products_description
					$sql_data_array = array(
									  'products_id' => $b2b_products_id,
									  'language_id' => '2',
									  'products_name' => tep_db_prepare_input($_POST['products_name'][2]),
									  'products_description' => tep_db_prepare_input($_POST['products_description'][2])								 
									  ); 
					tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array,'insert','','link_B2B');
	
					
					//Eintr�ge in products_to_categories
					$sql_data_array = array(
									  'products_id' => $b2b_products_id,
									  'categories_id' => $b2bcat_id							 
									  ); 
					tep_db_perform(TABLE_PRODUCTS_TO_CATEGORIES, $sql_data_array,'insert','','link_B2B');
					
					//Bilder kopieren
					//Detail
					exec('cp -v \''.$DOCUMENT_ROOT.'/images/'.tep_db_prepare_input($_POST['products_image_med']).'\' \'/kunden/homepages/37/d91333445/htdocs/dom.parsley-krawatten.de/b2b/images/product_images/info_images/'.tep_db_prepare_input($_POST['products_model']).'.jpg\'',$output);
					//Thumb
					exec('cp -v \''.$DOCUMENT_ROOT.'/images/'.tep_db_prepare_input($_POST['products_image']).'\' \'/kunden/homepages/37/d91333445/htdocs/dom.parsley-krawatten.de/b2b/images/product_images/thumbnail_images/'.tep_db_prepare_input($_POST['products_model']).'.jpg\'',$output);

				}
			}//Ende Update oder neues Produkt
			
			#echo '---------------------<br />yes';
			#go_dump($_POST);
		  
			  //neue Datenbank schlie�en
			  tep_db_close('link_B2B');
		  
		  }
		  
		  
		  #exit;
		  
		  //Aufbau zur alten Datenbank
		  tep_db_connect() or die('Unable to connect to database server!');
		  
		  #exit;
		  
/*Serien-Transfer in Parsley-Krawatten.de // B2B-Shop by Gurkcity 14.08.2007 EOF */

          tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . '&pID=' . $products_id));
        }
        break;
      case 'copy_to_confirm':
        if (isset($_POST['products_id']) && isset($_POST['categories_id'])) {
          $products_id = tep_db_prepare_input($_POST['products_id']);
          $categories_id = tep_db_prepare_input($_POST['categories_id']);

          if ($_POST['copy_as'] == 'link') {
            if ($categories_id != $current_category_id) {
              $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$products_id . "' and categories_id = '" . (int)$categories_id . "'");
              $check = tep_db_fetch_array($check_query);
              if ($check['total'] < '1') {
                tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$categories_id . "')");
              }
            } else {
              $messageStack->add_session(ERROR_CANNOT_LINK_TO_SAME_CATEGORY, 'error');
            }
          } elseif ($_POST['copy_as'] == 'duplicate') {
// BOF MaxiDVD: Modified For Ultimate Images Pack!
	//  ------------#AA20080904--------------
            $product_query = tep_db_query("select products_quantity, products_model, ".PRODUCTS_PRICE.", products_hemd_id, products_mknopf_id, products_date_available, products_weight, products_tax_class_id, manufacturers_id, products_prioritaet, products_serie, products_del_nullbestand, products_herstellerinfo from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
            $product = tep_db_fetch_array($product_query);
            tep_db_query("insert into " . TABLE_PRODUCTS . " (products_quantity, products_model, ".PRODUCTS_PRICE.", products_hemd_id, products_mknopf_id, products_date_added, products_date_available, products_weight, products_status, products_tax_class_id, manufacturers_id, products_prioritaet, products_serie, products_del_nullbestand, products_herstellerinfo ) values ('" . tep_db_input($product['products_quantity']) . "', '" . tep_db_input($product['products_model']) . "_NEW', '" . tep_db_input($product[PRODUCTS_PRICE]) . "', '" . tep_db_input($product['products_hemd_id']) . "', '" . tep_db_input($product['products_mknopf_id']) . "',  now(), '" . tep_db_input($product['products_date_available']) . "', '" . tep_db_input($product['products_weight']) . "', '0', '" . (int)$product['products_tax_class_id'] . "', '" . (int)$product['manufacturers_id'] . "', '" . tep_db_input($product['products_prioritaet']) . "', '" . tep_db_input($product['products_serie']) . "', '" . tep_db_input($product['products_del_nullbestand']) . "', '" . tep_db_input($product['products_herstellerinfo']) . "')");
// BOF MaxiDVD: Modified For Ultimate Images Pack!
            $dup_products_id = tep_db_insert_id();

            $description_query = tep_db_query("select language_id, products_name, products_seo_url, products_seo_subhead, products_seo_keywords, products_description, products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$products_id . "'");
            while ($description = tep_db_fetch_array($description_query)) {
              tep_db_query("insert into " . TABLE_PRODUCTS_DESCRIPTION . " (products_id, language_id, products_name, products_seo_url, products_seo_subhead, products_seo_keywords, products_description, products_url, products_viewed) values ('" . (int)$dup_products_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['products_name']) . "', '" . tep_db_input($description['products_seo_url']) . "','" . tep_db_input($description['products_seo_subhead']) . "', '" . tep_db_input($description['products_seo_keywords']) . "', '" . tep_db_input($description['products_description']) . "', '" . tep_db_input($description['products_url']) . "', '0')");
            }

            tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$dup_products_id . "', '" . (int)$categories_id . "')");
            $products_id = $dup_products_id;
			
			#Produkt Bild Kopie hier rein:
			$product_model_image_sm = gc_getImageFilenameFromModel($product['products_model'], 'sm', '');
			$product_model_image_md = gc_getImageFilenameFromModel($product['products_model'], 'md', '');
			
			copy(DIR_FS_CATALOG.'images/ties/'.$product_model_image_sm, DIR_FS_CATALOG.'images/ties/'.$product['products_model'].'_NEW_sm.jpg');
			copy(DIR_FS_CATALOG.'images/ties/'.$product_model_image_md, DIR_FS_CATALOG.'images/ties/'.$product['products_model'].'_NEW_md.jpg');
			
          }

          if (USE_CACHE == 'true') {
            tep_reset_cache_block('categories');
            tep_reset_cache_block('also_purchased');
          }
        }

        tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $categories_id . '&pID=' . $products_id));
        break;

      case 'new_product_preview':
	    
		//modified by Gurkcity, eindeutige products_model 17.04.2008 BOF
		if(!isset($_GET['pID']))
		//insert
		{
			//Produkt wurde neu angelegt, Warnung ausgeben, wenn products_model schon existiert und nur zur�ck anbieten
			$sql = "select p.products_model FROM products p where p.products_model = '" . $_POST['products_model'] . "'";
			$product_model_query = tep_db_query($sql);
			if(tep_db_num_rows($product_model_query) > 0)
			{
				$no_insert = true;
				
			}
			
		}
		else
		//update
		{
			//Produkt wurde bearbeitet, Warnung ausgeben, wenn products_model schon existiert und nur zur�ck anbieten
			$sql = "select p.products_model FROM products p where p.products_model = '" . $_POST['products_model'] . "' AND p.products_id <> '".$_GET['pID']."'";
			$product_model_query = tep_db_query($sql);
			if(tep_db_num_rows($product_model_query) > 0)
			{
				$no_insert = true;
				
			}
		}
		//modified by Gurkcity, eindeutige products_model 17.04.2008 EOF
		
		
		if(!isset($no_insert) or $no_insert==false) {
		
			$query_model = tep_db_query("SELECT products_model FROM products WHERE products_id='".$_GET['pID']."'");
			$fetch_model = mysql_fetch_assoc($query_model);
			
			if($fetch_model['products_model'] != $_POST['products_model'] and is_file(DIR_FS_CATALOG.'images/ties/'.$fetch_model['products_model'].'_sm.jpg'))
				gcRenameProductImage($fetch_model['products_model'], $_POST['products_model']);
				
			if (in_array(substr($_POST['products_model'], -1), array('L', 'S', 'K', 'C')) and is_numeric(substr($model, -2)))
				$image_model = substr($_POST['products_model'], 0, -1);
			else
				$image_model = $_POST['products_model'];
			
			if(gcFileUpload('products_image', 'images/ties/'.$image_model.'_md.jpg')) {
				$imageSize =  getimagesize(DIR_FS_CATALOG.'images/ties/'.$image_model.'_md.jpg');
				if($imageSize[0] > $imageSize[1]) {
					meImageCutResampled('images/ties/'.$image_model.'_md.jpg', 'images/ties/'.$image_model.'_md.jpg', array(0,0,0,0), array(400, 272));
					meImageCutResampled('images/ties/'.$image_model.'_md.jpg', 'images/ties/'.$image_model.'_sm.jpg', array(0,0,0,0), array(160, 108));
				}
				else{
					meImageCutResampled('images/ties/'.$image_model.'_md.jpg', 'images/ties/'.$image_model.'_md.jpg', array(0,0,0,0), array(400, 569));
					meImageCutResampled('images/ties/'.$image_model.'_md.jpg', 'images/ties/'.$image_model.'_sm.jpg', array(0,0,400,450), array(160, 180));
				}
			}
		}
		
        break;
		
    }
  }


// check if the catalog image directory exists
  if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!is_writeable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
  } else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript" src="includes/javascript/gc_scroller.js"></script>


<script language="javascript" type="text/javascript"><!--
function popupWindow(url) {
  window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=100,height=100,screenX=150,screenY=150,top=150,left=150')
}
//--></script>

<script type="text/javascript" src="./tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	// General options
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",

	// Theme options
	theme_advanced_buttons1 : "bold,italic,underline,|,link,unlink,code",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_buttons4 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true

});
</script>

</head>
<body bgcolor="#FFFFFF" onLoad="ScrollWin.scroll('0')">
<div id="container">
<div id="spiffycalendar" class="text"></div>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top">
	
<?php

if ($action == 'new_product') {

	include('categories_new_product.inc.php');

}
elseif ($action == 'new_product_preview') {
	
	if($no_insert)
		echo "<div style=\"color:red;font:2em/2.5em monospace\">Warnung!!! Modell-Nr \"" . $_POST['products_model'] . "\" existiert bereits. Bitte �ndern!</div>";
	
	include('categories_new_product_preview.inc.php');

}
else {

	include('categories_catproduct_listing.inc.php');

}
 
?>

    </td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
<?php
//Stoppuhr2
#echo $parse1."<br;
$parse2 = microtime_float();

#go_dump($parse2);

$zeitdiff = round(($parse2 - $parse1),4);

echo "Parsetime: ".$zeitdiff." Sekunden";
?>
