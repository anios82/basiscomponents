<?php
    if (tep_not_null($_POST)) {
      $pInfo = new objectInfo($_POST);
      $products_name = $_POST['products_name'];
      $products_description = $_POST['products_description'];
      $products_url = $_POST['products_url'];
      $products_seo_url = $_POST['products_seo_url'];
	  $products_seo_subhead = $_POST['products_seo_subhead'];
	  $products_seo_keywords = $_POST['products_seo_keywords'];

	
    } else {
// BOF MaxiDVD: Modified For Ultimate Images Pack!
      $product_query = tep_db_query("select p.products_id, pd.language_id, pd.products_name, pd.products_seo_url, pd.products_seo_subhead, pd.products_seo_keywords, pd.products_description, pd.products_url, p.products_quantity, p.products_model, p.".PRODUCTS_PRICE.", p.products_hemd_id, p.products_weight, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status, p.manufacturers_id, p.products_sperrgut, p.products_prioritaet, p.products_del_nullbestand ,p.empf_VK_pay, products_herstellerinfo from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and p.products_id = '" . (int)$_GET['pID'] . "'");
// EOF MaxiDVD: Modified For Ultimate Images Pack!
      $product = tep_db_fetch_array($product_query);

      $pInfo = new objectInfo($product);
      
    }
	
	
	
	
	//modified by Gurkcity, categorie muss angegeben werden 17.04.2008 BOF
	//Ansonsten taucht Produkt nicht auf
	$cat = false;
		
	$sql = "SELECT * FROM categories c, categories_description cd WHERE c.categories_id = cd.categories_id AND cd.language_id = '".LANGUAGE_ID."' ORDER BY sort_order, categories_name";
	$cat_query = tep_db_query($sql);
	
	while ($cat = tep_db_fetch_array($cat_query))
	{
		#Vergleich mit Post-.Variable und Kategorien-ID, Abbruch der Bedingung, wenn true
		if(tep_not_null($_POST['cat'.$cat['categories_id']]))
		{
			$cat = true;
			break;
		}
	}
	
	//wenn keine Kategorie, dann insert sperren
	if($cat == false)
	{
		$no_insert = true;
		//Warnung
		echo "<span style=\"color:red;font:2em/2.5em monospace\">Warnung!!! Es wurde keine Kategorie ausgewählt. Bitte ändern!</span>";
	}
	
	//modified by Gurkcity, categorie muss angegeben werden 17.04.2008 EOF
	
	
	

    $form_action = (isset($_GET['pID'])) ? 'update_product' : 'insert_product';
	
	
    echo tep_draw_form($form_action, FILENAME_CATEGORIES, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"');

/* Re-Post all POST'ed variables */
      reset($_POST);
      while (list($key, $value) = each($_POST)) {
        if (!is_array($_POST[$key])) {
          echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
        }
      }
	  
      
      for ($i=0, $n=sizeof($aShops); $i<$n; $i++) {
        echo tep_draw_hidden_field('products_name[' . $aShops[$i]['language_id'] . ']', htmlspecialchars(stripslashes($products_name[$aShops[$i]['language_id']])));
        echo tep_draw_hidden_field('products_description[' . $aShops[$i]['language_id'] . ']', htmlspecialchars(stripslashes($products_description[$aShops[$i]['language_id']])));
        echo tep_draw_hidden_field('products_url[' . $aShops[$i]['language_id'] . ']', htmlspecialchars(stripslashes($products_url[$aShops[$i]['language_id']])));
        echo tep_draw_hidden_field('products_seo_url[' . $aShops[$i]['language_id'] . ']', htmlspecialchars(stripslashes($products_seo_url[$aShops[$i]['language_id']])));
		echo tep_draw_hidden_field('products_seo_subhead[' . $aShops[$i]['language_id'] . ']', htmlspecialchars(stripslashes($products_seo_subhead[$aShops[$i]['language_id']])));
    
	 	for($j=0; $j<5; $j++):	
	 		if($products_seo_keywords[$aShops[$i]['language_id']][$j]){
			   echo tep_draw_hidden_field('products_seo_keywords[' . $aShops[$i]['language_id'] . '][]', htmlspecialchars(stripslashes($products_seo_keywords[$aShops[$i]['language_id']][$j])));
			}
		endfor;
	  }
      echo tep_draw_hidden_field('products_image', stripslashes($products_image_name));
// BOF MaxiDVD: Added For Ultimate Images Pack!
      echo tep_draw_hidden_field('products_image_med', stripslashes($products_image_med_name));
// EOF MaxiDVD: Added For Ultimate Images Pack!
      echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="edit"') . '&nbsp;&nbsp;';

      
	  //modified by Gurkcity, eindeutige products_model 17.04.2008 BEO
	  //insert wird gesprerrt, wenn products_model schon vorhanden ist
	  //gilt ebenso f�r ungesetzte Kategorie
	if(isset($no_insert))
	{
		
	}
	else
	{

	  if (isset($_GET['pID'])) {

		//modified by Gurkcity 22.02.2007 Focus-Button setzen
        echo tep_image_submit('button_update.gif', IMAGE_UPDATE, 'id="focusbutton"');
		echo '<script type="text/javascript">
					document.'.$form_action.'.submit();
			  </script>';

      } else {
        echo tep_image_submit('button_insert.gif', IMAGE_INSERT);
		echo '<script type="text/javascript">
					document.'.$form_action.'.submit();
			  </script>';
      }
	}
	//modified by Gurkcity, eindeutige products_model 17.04.2008 EOF
	
	
      echo '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_CATEGORIES, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=new_product') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?></form>