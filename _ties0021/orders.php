<?php

/*
  $Id: orders.php,v 1.112 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

//Eingebaute stock-Korrektur-Funktion by Gurkcity: Stock-Update für Storno von Einzelprodukten und kompletter Order, modifiziert 14.11.2007

//Stock korrigieren
//im Fall von Bundles sind orders_product_id <> storno_id (=stornierte product_id)
//bei normalen Produkten sind beide gleich
function stock_update($orders_id, $orders_product_id, $storno_attr_value, $storno_anz_value, $storno_id)
{

                #echo $storno_anz_value . "/" . (int)$storno_id.'<br />';
                #CONCAT(pa.options_id,'-',pa.options_values_id) AS psa, => Verkettung der Werte!

                $sql = "SELECT CONCAT(pa.options_id,'-',pa.options_values_id) AS psa, po.products_options_name
                FROM products_attributes pa,
                        products_options po,
                        products_options_values pov,
                        orders_products_attributes opa,
                        orders_products op

                WHERE pov.language_id = '2'
                        AND pov.products_options_values_name = opa.products_options_values
                        AND po.products_options_name = opa.products_options
                        AND po.language_id = '2'
                        AND po.products_options_id = pa.options_id
                        AND pov.products_options_values_id = pa.options_values_id
                        AND pa.products_id = $storno_id
                        AND opa.orders_products_id = op.orders_products_id
                        AND op.products_id = $orders_product_id
                        AND op.orders_id = opa.orders_id
                        AND opa.orders_id = '".$orders_id."'
                        AND opa.products_options_values = '".$storno_attr_value."'";

                #echo $sql.'<br><br>';
                $stock_update_query = tep_db_query($sql);

                while ($stock_update = tep_db_fetch_array($stock_update_query))
                {

                tep_db_query("UPDATE " . TABLE_PRODUCTS_STOCK . " SET products_stock_quantity = products_stock_quantity +  " . $storno_anz_value . " WHERE products_id = '" . (int)$storno_id . "' AND products_stock_attributes = '".$stock_update['psa']."'");
                $output_text = "STOCK UPDATE ID(".$storno_id.") ".$stock_update['products_options_name']." ".$storno_attr_value." (".$stock_update['psa'].") +  " . $storno_anz_value . "
                ";
                }

          return $output_text;

}

  require('includes/application_top.php');

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $orders_statuses = array();
  $orders_status_array = array();
  $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by admin_order ASC");
  $orders_types = array();
  $orders_types_array = array();
  $orders_types_query = tep_db_query("select orders_type_id, orders_type_name from " . TABLE_ORDERS_TYPES . " where language_id = '" . (int)$languages_id . "'");

  while ($orders_status = tep_db_fetch_array($orders_status_query)) {
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                               'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
  }

  while ($orders_type = tep_db_fetch_array($orders_types_query)) {
    $orders_types[] = array('id' => $orders_type['orders_type_id'],
                               'text' => $orders_type['orders_type_name']);
    $orders_types_array[$orders_type['orders_type_id']] = $orders_type['orders_type_name'];
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (($action == 'edit' || $action == 'update_products' || $action == 'storno_all' || $action == 'firmenrechnung') && isset($_GET['oID'])) {
    $oID = tep_db_prepare_input($_GET['oID']);

    $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
    $order_exists = true;
    if (!tep_db_num_rows($orders_query)) {
      $order_exists = false;
      $messageStack->add(sprintf(ERROR_ORDER_DOES_NOT_EXIST, $oID), 'error');
    }
  }

  include(DIR_WS_CLASSES . 'order.php');

  $oID = tep_db_prepare_input($_GET['oID']);


  //Shop-ID abhängiger Store-Name by Gurkcity 05.03.2007 BOF
                $shop_query = tep_db_query("select o.shop_id, sh.shop_name, sh.shop_email1, sh.shop_sender1 FROM orders o, gc_shops sh WHERE o.shop_id = sh.shop_id AND o.orders_id = '".$oID."'");
        $shop = tep_db_fetch_array($shop_query);
                $shop_name = $shop['shop_name'];
                $shop_sender1 = $shop['shop_sender1'];
                $shop_email1 = $shop['shop_email1'];
  //Shop-ID abhängiger Store-Name by Gurkcity 05.03.2007 EOF




  if (tep_not_null($action)) {
    switch ($action) {
        #####################################################
        ##  BOF Update Products by Gurkcity                ##
        #####################################################
          case 'storno_all':

          #### Stornofunktion by Gurkcity 22.12.2006/04.12.2007 BOF ####





          // Kommentar initialisieren für orders_status_history
          $comment = '';

          #DEBUG-Info
          #go_dump($storno_anz);

          //Produkte ermitteln, die in einer Order vorhanden sind
          $sql = "SELECT * FROM orders_products WHERE orders_id = '".$oID."'";

          $orders_products_query = tep_db_query($sql);
          while($orders_products = tep_db_fetch_array($orders_products_query))
          {
                //wieviele Produkte wurden gekauft?
                $orders_products_quantity = $orders_products['products_quantity'];
                $products_id = $orders_products['products_id'];
                $orders_products_id = $orders_products['orders_products_id'];

                //products_ordered anpassen und Bestand in products korrigieren, sowie Produkt verfügbar setzen
                $sql = "UPDATE products
                                SET products_ordered = (products_ordered - ".$orders_products_quantity."),
                                        products_quantity = (products_quantity + ".$orders_products_quantity."),
                                        products_status = 1
                                WHERE products_id = ".$products_id;

                tep_db_query($sql);

                //Hat das Produkt Optionen?
            $sql = "SELECT * FROM orders_products_attributes WHERE orders_products_id = '".$orders_products_id."' AND orders_id = '".$oID."'";
            #echo $sql;
            $orders_products_attributes_query = tep_db_query($sql);
            while($orders_products_attributes = tep_db_fetch_array($orders_products_attributes_query))
                {
                        $comment .= stock_update($oID, $products_id, $orders_products_attributes['products_options_values'], $orders_products_quantity, $products_id);
                }

          }

          //georderte Produkte in der Rechnung(Order) auf Null setzen
          $sql = "UPDATE orders_products SET products_quantity = '0' WHERE orders_id = '".$oID."'";
          tep_db_query($sql);


         //Kommentar in Status-History eintragen
         $sql_data_array = array('orders_id' => $oID,
                                                                          'orders_status_id' => '5',
                                                                        'date_added' => 'now()',
                                                                        'customer_notified' => '0',
                                                                        'comments' => $comment);
         tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);

         //Status der Rechnung auf storniert setzen
         $sql = "UPDATE orders SET orders_status = '5' WHERE orders_id = '".$oID."'";
         tep_db_query($sql);


    //Orders_Total korrigieren
        $order = new order($oID);
        include('includes/modules/update_orders_total.php');

        //Weiterleitung auf Rechnungsseite, soll wiederholtes Laden verhindern
        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=edit'));



          #### Stornofunktion by Gurkcity 22.12.2006 EOF ####


          #echo "Rechnung komplett storniert!!!";
          break;


        #####################################################
        ##  BOF Update Products by Gurkcity                ##
        #####################################################
          case 'update_products':

          #### Stornofunktion by Gurkcity 22.12.2006/04.12.2007 BOF ####





          // Kommentar initialisieren für orders_status_history
          $comment = '';

          #DEBUG-Info
          #go_dump($storno_anz);

          //Anzahl abziehen in der Rechnung


          //Bundle Stückzahl abfragen, auf Krawatte und Hemd aufteilen, auf diese IDs die Funktion (unten in ein e Funktion packen) anwenden


          //Bestand korrigieren
          foreach($storno_anz AS $storno_id => $storno_attr)
          {
                        foreach($storno_attr AS $storno_attr_value => $storno_anz_value)
                        {

                                //Prüfen, ob Storno nicht zu negativen Beständen führt/Reload-Schutz
                                $sql = "SELECT products_quantity FROM orders_products WHERE products_id = '" . (int)$storno_id . "' AND orders_id = '".$oID."'";
                                $orders_products = tep_db_fetch_array(tep_db_query($sql));
                                $products_quantity = $orders_products['products_quantity'];

                                if($products_quantity - $storno_anz_value >= 0)
                                {

                                //Prüfen, ob es sich um ein Bundle handelt
                                  $sql = "SELECT * FROM products_bundles WHERE bundle_id = '" . (int)$storno_id . "'";
                                  $storno_qry = tep_db_query($sql);
                                  if(tep_db_num_rows($storno_qry) > 0)
                                  {

                                                $storno_bundle = tep_db_fetch_array($storno_qry);

                                                $comment .= "Storno Bundle:
                                                ";

                                                //Hemd hochsetzen
                                                tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $storno_anz_value . ", products_ordered = products_ordered - " . $storno_anz_value . ", products_status = '1' where products_id = '" . (int)$storno_bundle['hemd_id'] . "'");

                                                $comment .=  "BUNDLE(Hemd) PRODUCTS QUANTITY UPDATE: ID(".$storno_bundle['hemd_id'].") + " . $storno_anz_value . "
                                                ";

                                                //Krawatte hochsetzen
                                                tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $storno_anz_value . ", products_ordered = products_ordered - " . $storno_anz_value . ", products_status = '1' where products_id = '" . (int)$storno_bundle['krawatte_id'] . "'");

                                                $comment .=  "BUNDLE(Krawatte) PRODUCTS QUANTITY UPDATE: ID(".$storno_bundle['krawatte_id'].") + " . $storno_anz_value . "
                                                ";
                                  }

                                #$comment .= "Storno Products:
                                #";
                                //Tabelle products, products_quantity hochsetzen
                                tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $storno_anz_value . ", products_ordered = products_ordered - " . $storno_anz_value . ", products_status = '1' where products_id = '" . (int)$storno_id . "'");

                                #$comment .= "PRODUCTS QUANTITY UPDATE ID(".$storno_id.") + " . $storno_anz_value . "";
                        }

                        }
          }



          foreach($storno_anz AS $storno_id => $storno_attr)
          {
                        foreach($storno_attr AS $storno_attr_value => $storno_anz_value)
                        {

                                //Prüfen, ob Storno nicht zu negativen Beständen führt/Reload-Schutz
                                $sql = "SELECT products_quantity FROM orders_products WHERE products_id = '" . (int)$storno_id . "' AND orders_id = '".$oID."'";
                                $orders_products = tep_db_fetch_array(tep_db_query($sql));
                                $products_quantity = $orders_products['products_quantity'];

                                if($products_quantity - $storno_anz_value >= 0)
                                {


                                //Prüfen, ob es sich um ein Bundle handelt
                                  $sql = "SELECT * FROM products_bundles WHERE bundle_id = '" . (int)$storno_id . "'";
                                  $storno_qry = tep_db_query($sql);
                                  if(tep_db_num_rows($storno_qry) > 0)
                                  {

                                                $storno_bundle = tep_db_fetch_array($storno_qry);

                                                #go_dump($storno_bundle);

                                                $comment .=  "
                                                Storno Bundle Stock:
                                                ";

                                                //Hemd hochsetzen
                                                $comment .=  "Stock(Hemd".$storno_bundle['hemd_id']."):
                                                ";
                                                $comment .=  stock_update($oID, $storno_bundle['bundle_id'], $storno_attr_value, $storno_anz_value, $storno_bundle['hemd_id']);


                                                //Krawatte hochsetzen -> Krawatten haben keinen Stock, daher ausgeblendet
                                                #echo "Stock(Krawatte".$storno_bundle['krawatte_id']."):<br>";
                                                #echo stock_update($_GET['oID'], $storno_bundle['bundle_id'], $storno_attr_value, $storno_anz_value, $storno_bundle['krawatte_id']);

                                  }

                                #$comment .=  "
                                #Storno Products Stock:
                                #";
                                $comment .=  stock_update($oID, $storno_id, $storno_attr_value, $storno_anz_value, $storno_id);

                                }

                        }

          }

          /*
          $order_query = tep_db_query("select products_id, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$_GET['oID'] . "'");
      while ($order = tep_db_fetch_array($order_query)) {
        tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $order['products_quantity'] . ", products_ordered = products_ordered - " . $order['products_quantity'] . " where products_id = '" . (int)$order['products_id'] . "'");
      }
          **/


    #Bestellung ändern / Rechnung ändern

        //Tabelle orders_products aktualisieren
        foreach($storno_anz AS $storno_id => $storno_attr)
          {
                        foreach($storno_attr AS $storno_attr_value => $storno_anz_value)
                        {

                                        //Prüfen, ob das Produkt Attribute hat
                                        $sql = "SELECT * FROM orders_products_attributes AS opa, orders_products AS op  WHERE opa.orders_id = '".$oID."' AND opa.products_options_values = '" . $storno_attr_value . "' AND opa.orders_products_id = op.orders_products_id AND op.products_id = '".$storno_id."'";
                                        #echo "<br>";
                                        if(tep_db_num_rows(tep_db_query($sql)) > 0)
                                        {
                                                //wenn Attribute vorhanden, z.B. Größe 39, 40 o.ä.
                                                $orders_products_attributes = tep_db_fetch_array(tep_db_query($sql));
                                                $sql = "UPDATE orders_products SET products_quantity = products_quantity -  " . $storno_anz_value . " WHERE products_id = '" . (int)$storno_id . "' AND orders_id = '".$oID."' AND orders_products_id = '".$orders_products_attributes['orders_products_id']."'";
                                                #echo "<br>";
                                                tep_db_query($sql);
                                        }
                                        else
                                        {
                                                //keine Attribute vorhanden

                                                //Prüfen, ob Storno nicht zu negativen Beständen führt/Reload-Schutz
                                                $sql = "SELECT products_quantity, products_model FROM orders_products WHERE products_id = '" . (int)$storno_id . "' AND orders_id = '".$oID."'";
                                                #echo "<br>";
                                                $orders_products = tep_db_fetch_array(tep_db_query($sql));
                                                $products_quantity = $orders_products['products_quantity'];

                                                if($products_quantity - $storno_anz_value >= 0)
                                                {
                                                        $sql = "UPDATE orders_products SET products_quantity = products_quantity -  " . $storno_anz_value . " WHERE products_id = '" . (int)$storno_id . "' AND orders_id = '".$oID."'";
                                                        tep_db_query($sql);
                                                }
                                        }


                                        if($storno_anz_value > 0)
                                        {
                                                $comment .= $storno_anz_value." x Artikel ".$orders_products['products_model'] ." storniert
                                                ";
                                        }
                                        #$comment .=  "
                                        #Update Order (orders_products) okay!
                                        #";

                        }

          }

         //Kommentar in Status-History eintragen
         $sql_data_array = array('orders_id' => $oID,
                                                                          'orders_status_id' => '3',
                                                                        'date_added' => 'now()',
                                                                        'customer_notified' => '0',
                                                                        'comments' => $comment);
         tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);


    //Orders_Total korrigieren
        #tep_db_query("delete from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "'");
        #echo $sql = "SELECT * FROM " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$oID . "'";
        #$orders_total_query = tep_db_query($sql);
        #while($orders_total = tep_db_fetch_array($orders_total_query))
        #{
                ###
        #}

        $order = new order($oID);
        #go_dump($order->products);
        #go_dump($order2->products);


        include('includes/modules/update_orders_total.php');

        //Zwischensumme neu berechnen


        #echo $sql = "UPDATE " . TABLE_ORDERS_TOTAL . " SET value = '".$ot_subtotal."', text = '".$ot_subtotal_text."' WHERE class = 'ot_subtotal'";

        //Tax neu berechnen
        #echo $sql = "UPDATE " . TABLE_ORDERS_TOTAL . " SET value = '".$ot_tax."', text = '".$ot_tax_text."' WHERE class = 'ot_tax'";

        //Summe neu berechnen
        #echo $sql = "UPDATE " . TABLE_ORDERS_TOTAL . " SET value = '".$ot_total."', text = '".$ot_total_text."' WHERE class = 'ot_total'";


        //Product aktivieren, wenn Bestand = 0 war


          #### Stornofunktion by Gurkcity 22.12.2006 EOF ####


          break;
        #####################################################
        ##  EOF Update Products by Gurkcity                ##
        #####################################################
      case 'update_order':
        $oID = tep_db_prepare_input($_GET['oID']);
        $status = tep_db_prepare_input($_POST['status']);
                $statustypes = tep_db_prepare_input($_POST['statustypes']);
        $comments = tep_db_prepare_input($_POST['comments']);
                //Gutschein-Modul by Gurkcity 01.06.2007 BOF
                $coupons = tep_db_prepare_input($_POST['coupons']);
                //Gutschein-Modul by Gurkcity 01.06.2007 EOF

        $order_updated = false;
        $check_status_query = tep_db_query("select customers_name, customers_email_address, orders_status, date_purchased from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
        $check_status = tep_db_fetch_array($check_status_query);

                //Gutschein-Modul by Gurkcity 01.06.2007 BOF
        if ( ($check_status['orders_status'] != $status) || tep_not_null($comments) || tep_not_null($coupons) || tep_not_null($statustypes)) {
                //Gutschein-Modul by Gurkcity 01.06.2007 EOF
          tep_db_query("update " . TABLE_ORDERS . " set orders_status = '" . tep_db_input($status) . "', last_modified = now(), orders_type = '" . tep_db_input($statustypes) . "' where orders_id = '" . (int)$oID . "'");
          $customer_notified = '0';
          if (isset($_POST['notify']) && ($_POST['notify'] == 'on')) {
            $notify_comments = '';
                        ################################################
                        ##    BOF Auftragsbestätigung by Gurkcity     ##
                        ################################################
                        if($orders_status_array[$status]== FIELD_ORDERS_STATUS_AUFTRBEST){



                        $oID = tep_db_prepare_input($_GET['oID']);

                        $orders_query = tep_db_query("select orders_id, shop_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
                        $order_exists = true;
                        if (!tep_db_num_rows($orders_query)) {
                          $order_exists = false;
                          $messageStack->add(sprintf(ERROR_ORDER_DOES_NOT_EXIST, $oID), 'error');
                        }
                        #include(DIR_WS_CLASSES . 'order.php');

                        //Email-Text vorbereiten in auftragsbestaetigung.php

                                $notify_comments = "Order-Status: ".$orders_status_array[$status]."\n\n";
                                require('auftragsbestaetigung.php');
                                $notify_comments .= $email_order;


                        include('../'.DIR_WS_LANGUAGES . $language .'/conditions.php');
                        function unhtmlentities($string)
                                {
                                   $trans_tbl = get_html_translation_table(HTML_ENTITIES);
                                   $trans_tbl = array_flip($trans_tbl);
                                   return strtr($string, $trans_tbl);
                                }
                        $widerruf = "\n" . EMAIL_SEPARATOR . TEXT_EXTRACT_CONDITIONS . strip_tags(unhtmlentities(WIDERRUFSRECHT));
                        }
                        ################################################
                        ##    EOF Auftragsbestätigung by Gurkcity     ##
                        ################################################
            if (isset($_POST['notify_comments']) && ($_POST['notify_comments'] == 'on')) {
              $notify_comments .= sprintf(EMAIL_TEXT_COMMENTS_UPDATE, $comments) . "\n";
            }

            /*
                        * Betreff anpassen by Gurkcity 08.03.2006
                        */
                        if($orders_status_array[$status] == 'Versendet')
                        {
                                $subject = EMAIL_TEXT_SUBJECT_VERSENDET;
                                $statustext = EMAIL_TEXT_STATUS_UPDATE_VERSENDET . "\n\n";
                        }
                        else if($orders_status_array[$status] == 'Bestellung/ Auftrag storniert')
                        {
                                $subject = EMAIL_TEXT_SUBJECT_STORNIERT;
                                $statustext = EMAIL_TEXT_STATUS_UPDATE_STORNIERT . "\n\n";
                        }
                        else if($orders_status_array[$status] == 'In Bearbeitung')
                        {
                                $subject = EMAIL_TEXT_SUBJECT_BEARBEITUNG;
                                $statustext = EMAIL_TEXT_STATUS_UPDATE_BEARBEITUNG . "\n\n";
                        }
                        else
                        {
                                $subject = EMAIL_TEXT_SUBJECT;
                                $statustext = sprintf(EMAIL_TEXT_STATUS_UPDATE, $orders_status_array[$status]) . "\n\n";
                        }

                        /*
                        * Betreff Ende
                        */



                        $email =
                                        $shop_name . "\n" .
                                        EMAIL_SEPARATOR . "\n" .
                                        EMAIL_TEXT_ORDER_NUMBER . ' ' . $oID . "\n\n" .
                                        #EMAIL_TEXT_INVOICE_URL . ' ' . tep_catalog_href_link(FILENAME_CATALOG_ACCOUNT_HISTORY_INFO, 'order_id=' . $oID, 'SSL') . "\n" .
                                        EMAIL_TEXT_DATE_ORDERED . ' ' . tep_date_long($check_status['date_purchased']) . "\n\n\n" .

                                        $statustext;
                if (isset($comments) && $comments != '')
                {
                        $email.= $notify_comments;
                }
                        $email.= sprintf(EMAIL_TEXT_STATUS_UPDATE_NACHSATZ,$shop_name);

//Mahnungsmodul Teil 1 (E-Mail-Versand) by Gurkcity 25.10.2006 BOF
//ausserdem muss unter includes/languages/ die Datei mahnungstext.php eingebunden werden
//und unter functions/html_output muss  id="' . tep_output_string($name) . '" in tep_draw_selection_field ergänzt werden

                        if($orders_status_array[$status] == 'M1')
                        {

                                //Rechnungssumme ermitteln
                                $ot_query = tep_db_query("select text, value from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$oID . "' AND class = 'ot_total'");
                        $ot = tep_db_fetch_array($ot_query);
                                $order_total_value = strip_tags($ot['text']);

                                $subject = "Zahlungserinnerung";

                                //Variable $email wird jetzt mit dem Zahlungserinnerungstext überschrieben
$email = $shop_name."
------------------------------------------------------
<br>
Bestell-Nr.: ".$oID."<br>
Bestelldatum: ".tep_date_long($check_status['date_purchased'])."<br>
Rechnungssumme: ".$order_total_value."<br>
<br>

Zahlungserinnerung<br>
<br>
Ich hoffe, dass Sie mit meiner Lieferung zufrieden sind und ich Sie weiterhin als Kunden in meinem Internetshop bedienen darf.<br>
<br>
Bisher habe ich jedoch noch keinen Zahlungseingang festgestellt. Bitte überweisen Sie den oben genannten Betrag auf mein Konto:<br>
<br>
Dr. Ferdinand Pohl<br>
Deutsche Bank Hannover<br>
Kontonr. 021107800<br>
BLZ 25070024<br>
<br>
Sollte sich dieses Schreiben mit Ihrer Zahlung gekreuzt haben, so betrachten Sie es bitte als gegenstandslos. Sollte das Versehen auf meiner Seite liegen, so bitte ich kurz um Information, wann Sie welchen Betrag von welchem Kontoinhaber (wenn abweichend) überwiesen haben.<br>
<br>
Mit freundlichen Grüßen<br>
Dr. Ferdinand Pohl";
                        }



                        //Mail an Customer veschicken
                        tep_mail($check_status['customers_name'], $check_status['customers_email_address'], $subject, $email, $shop_sender1, $shop_email1);

                        //sepearate Mail an Store-Owner schicken
                        tep_mail($check_status['customers_name'], 'service@krawatten-ties.com', $subject, $email, $shop_sender1, $shop_email1);


            $customer_notified = '1';
          }

          tep_db_query("insert into " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments) values ('" . (int)$oID . "', '" . tep_db_input($status) . "', now(), '" . tep_db_input($customer_notified) . "', '" . tep_db_input($comments)  . "')");

          //Gutschein-Modul by Gurkcity 01.06.2007 BOF



                  if(tep_not_null($coupons))
                  {
                          //DB-Einträge coupons

                          //Coupons werden durch Leerzeichen getrennt, aufteilen in ein array
                          $coupons_array = explode(' ',$coupons);


                          //Array durchgehen und für jeden Couponcode Mail-Adresse und Name notieren und status auf eins setzen
                          //anhand Mail-Adresse wird der Gutscheincode später geprüft, es muss eine andere Mail-Adresse bei der Bestellung angegeben werden
                          foreach($coupons_array AS $coupons_code)
                          {
                                  $sql_data_array = array(
                                                                        'coupons_status' => '1',
                                                                        'coupons_name' => $check_status['customers_name']. '#' . date('d.m.Y'), //nur Hilfsinformation
                                                                        'coupons_email' => $check_status['customers_email_address'],
                                                                        'coupons_datetime' => 'now()');
                                  tep_db_perform('coupons', $sql_data_array, 'update', "coupons_code='".$coupons_code."'");

                                  //Eintrag in die Orders-History
                                  tep_db_query("insert into " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments) values ('" . (int)$oID . "', '" . tep_db_input($status) . "', now(), '0', '" . tep_db_input('Gutschein:'.$coupons_code)  . "')");

                                  //Es wird nun der eigene Gutschein mit der E-Mail-Adresse verknüpft
                                  //dazu muss der hash-Wert gleich sein
                                  $sql = "SELECT coupons_hash FROM coupons WHERE coupons_code='".$coupons_code."'";
                                  $couponsquery = tep_db_query($sql);
                                  while ($coupons = tep_db_fetch_array($couponsquery))
                                  {
                                                $sql_data_array = array(
                                                                        'coupons_status' => '0',//Coupons status wird erst auf eins gesetzt, wenn eine Bestellung mit dem verknüpften Gutschein erfolgt
                                                                        'coupons_name' => $check_status['customers_name']. '#' . date('d.m.Y') . 'KRT3000GUTSCHEINE', //nur Hilfsinformation
                                                                        'coupons_email' => $check_status['customers_email_address']);

                                                                        //echo "coupons_extra='".$coupons['coupons_hash']."'";
                                                                        //exit;
                                                //Abfrage, da ansonsten alle Gutscheincodes der Alt/Neukunden-Aktion überschrieben wird...das sollte das nächste mal besser programmiert werden...wein!
                                                //Fehler festgestellt am 25.09.2007, Datenbank wurde neu aufgespielt
                                                if(tep_not_null($coupons['coupons_hash']))
                                                {
                                                        tep_db_perform('coupons', $sql_data_array, 'update', "coupons_extra='".$coupons['coupons_hash']."'");//!!!
                                                }
                                  }
                          }

                  }

                  //Gutschein-Modul by Gurkcity 01.06.2007 EOF

                  $order_updated = true;
        }

        if ($order_updated == true) {
         $messageStack->add_session(SUCCESS_ORDER_UPDATED, 'success');
        } else {
          $messageStack->add_session(WARNING_ORDER_NOT_UPDATED, 'warning');
        }

        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=edit'));
        break;
      case 'deleteconfirm':
        $oID = tep_db_prepare_input($_GET['oID']);

        tep_remove_order($oID, $_POST['restock']);

        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))));
        break;

		
		
		
		
		
		
		// Hier wird zwischen Normaler Rechnung, Firmenrechnung und Ausl. Firmenrechnung unterschieden
		case 'firmenrechnung':
			
			switch($_POST['rechnung_type']) {
			
				// Normale rechnung (mit MwSt)
				case 'normal':
					$oID = tep_db_prepare_input($_GET['oID']);
					$order = new order($oID);
					
					foreach($order->products as $product) {
						$subtotal += ($product['final_price']*$product['qty']*1.19);
					}
					
					$shipping = 4.9;
					$moretotals = 0.00;
					
					$ot_tax = false;
					
					foreach($order->totals as $totals) {
						if($totals['class'] == 'ot_shipping' and (int)$totals['value'] == 0) $shipping = 0.0;
						if($totals['class'] == 'ot_tax') $ot_tax = true;
						if(strpos($totals['class'], 'custom')) {
							(float)$totals['value'] = round(((float)$totals['value']*1.19), 2);
							$moretotals += (float)$totals['value'];
							tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL." SET value='".$totals['value']."' , text='".number_format($totals['value'], 2, ',', '.')." &euro;'             WHERE orders_id='".$oID."' AND class='".$totals['class']."'");
						}
					}

					
					$total = $shipping + $subtotal + $moretotals;
					
					$tax = $total - ($total / 119 * 100);
					
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$subtotal."' , text='".number_format($subtotal, 2, ',', '.')." &euro;'      WHERE orders_id='".$oID."' AND class='ot_subtotal'");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$total."'    , text='<b>".number_format($total, 2, ',', '.')." &euro;</b>'  WHERE orders_id='".$oID."' AND class='ot_total'");
					if($ot_tax === true)
						tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$tax."'      , text='".number_format($tax, 2, ',', '.')." &euro;' , title='enth. 19% Mwst:'          WHERE orders_id='".$oID."' AND class='ot_tax'");
					else
						tep_db_query("INSERT INTO ".TABLE_ORDERS_TOTAL." (orders_total_id, orders_id, title, text, value, class, sort_order) VALUES ('', '".$oID."', 'enth. 19% Ust.:', '".number_format($tax, 2, ',', '.')." &euro;', ".$tax.", 'ot_tax', '3')");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$shipping."' , text='".number_format($shipping, 2, ',', '.')." &euro;'      WHERE orders_id='".$oID."' AND class='ot_shipping'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET netto='0'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET firma='0'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS_PRODUCTS." SET products_tax='19.0'   WHERE orders_id='".$oID."'");
					
					break;
					
				// Inland Firma Rechnung (mit MwSt aber zzgl)
				case 'inland':
					$oID = tep_db_prepare_input($_GET['oID']);
					$order = new order($oID);
					
					foreach($order->products as $product) {
						$subtotal += round((float)($product['final_price']*$product['qty']), 2);
					}
					
					$shipping = 0.0;
					$moretotals = 0.00;
					
					$ot_tax = false;
					
					foreach($order->totals as $totals) {
						if($totals['class'] == 'ot_shipping' and (int)$totals['value'] != 0)
							$shipping = 4.1176;
						if($totals['class'] == 'ot_tax') $ot_tax = true;
						if(strpos($totals['class'], 'custom')) {
							(float)$totals['value'] = round(((float)$totals['value']/1.19), 2);
							$moretotals += (float)$totals['value'];
							tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL." SET value='".$totals['value']."' , text='".number_format($totals['value'], 2, ',', '.')." &euro;' WHERE orders_id='".$oID."' AND class='".$totals['class']."'");
						}
					}
					
					$total = ($shipping + $subtotal + $moretotals)*1.19;
					
					$tax = $total - ($total / 119 * 100);
					
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$subtotal."' , text='".number_format($subtotal, 2, ',', '.')." &euro;'				WHERE orders_id='".$oID."' AND class='ot_subtotal'");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$total."'    , text='<b>".number_format($total, 2, ',', '.')." &euro;</b>'			WHERE orders_id='".$oID."' AND class='ot_total'");
					if($ot_tax === true)
						tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$tax."'      , text='".number_format($tax, 2, ',', '.')." &euro;', title='zuzgl. 19% Mwst:'	WHERE orders_id='".$oID."' AND class='ot_tax'");
					else
						tep_db_query("INSERT INTO ".TABLE_ORDERS_TOTAL." (orders_total_id, orders_id, title, text, value, class, sort_order) VALUES ('', '".$oID."', 'zuzgl. 19% Ust.:', '".number_format($tax, 2, ',', '.')." &euro;', ".$tax.", 'ot_tax', '3')");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$shipping."' , text='".number_format($shipping, 2, ',', '.')." &euro;'				WHERE orders_id='".$oID."' AND class='ot_shipping'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET firma='1'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET netto='0'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS_PRODUCTS." SET products_tax='19.0'    WHERE orders_id='".$oID."'");
					
					break;
					
				// Ausland Firma Rechnung (ohne MwSt)
				case 'ausland':
					$oID = tep_db_prepare_input($_GET['oID']);
					$order = new order($oID);
					
					foreach($order->products as $product) {
						$subtotal += round((float)($product['final_price']*$product['qty']), 2);
					}

					$shipping = 0.0;
					$moretotals = 0.00;

					foreach($order->totals as $totals) {
						if($totals['class'] == 'ot_shipping' and (int)$totals['value'] != 0) $shipping = 4.1176;
						if(strpos($totals['class'], 'custom')) {
							(float)$totals['value'] = round(((float)$totals['value']/1.19), 2);
							$moretotals += (float)$totals['value'];
							tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL." SET value='".$totals['value']."' , text='".number_format($totals['value'], 2, ',', '.')." &euro;'             WHERE orders_id='".$oID."' AND class='".$totals['class']."'");
						}
					}
					
					$total = $shipping + $subtotal + $moretotals;
					
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$subtotal."' , text='".number_format($subtotal, 2, ',', '.')." &euro;'             WHERE orders_id='".$oID."' AND class='ot_subtotal'");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$total."'    , text='<b>".number_format($total, 2, ',', '.')." &euro;</b>'         WHERE orders_id='".$oID."' AND class='ot_total'");
					tep_db_query("DELETE FROM ".TABLE_ORDERS_TOTAL." WHERE orders_id='".$oID."' AND class='ot_tax'");
					tep_db_query("UPDATE ".TABLE_ORDERS_TOTAL."    SET value='".$shipping."' , text='".number_format($shipping, 2, ',', '.')." &euro;'             WHERE orders_id='".$oID."' AND class='ot_shipping'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET netto='1'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS."          SET firma='0'             WHERE orders_id='".$oID."'");
					tep_db_query("UPDATE ".TABLE_ORDERS_PRODUCTS." SET products_tax='0.0'    WHERE orders_id='".$oID."'");
					
					break;
				}
			
			break;
		

    }
  }
$order = new order($oID);
	
		
$order_total_value = strip_tags($order->totals[sizeof($order->totals)-1]['text']);

include_once(DIR_FS_DOCUMENT_ROOT.'/includes/languages/'.LANGUAGE.'.php');
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript" src="includes/javascript/gc_scroller.js"></script>
<script type="text/javascript" src="prototype.js"></script>
</head>
<body bgcolor="#FFFFFF" onLoad="ScrollWin.scroll('0')">
<!-- header //-->
<?php
  require(DIR_WS_INCLUDES . 'header.php');
?>
<!-- header_eof //-->
<script type="text/javascript">
<?php
	if($order->info['netto'] == '1') echo 'var isnetto = true;';
	else echo 'var isnetto = false;';
	if($order->info['firma'] == '1') echo 'var isfirma = true;';
	else echo 'var isfirma = false;';
	
?>

	function setText(text) {
	 <?php
			 if($order->customer['gender'] == 'f') echo utf8_decode('anrede = "Sehr geehrte Frau '.$order->customer['name'].',\\n\\n";');
			 elseif($order->customer['gender'] == 'm') echo utf8_decode('anrede = "Sehr geehrter Herr '.$order->customer['name'].',\\n\\n";');
			else echo utf8_decode('anrede = "Sehr geehrte Damen und Herren,\\n\\n";');
	 ?>
	 blank = "";
	 artikelausverkauft = "<?php echo ORDERS_ARTIKELAUSVERKAUFT; ?>";
	 mustersendung ="<?php echo ORDERS_MUSTERSENDUNG; ?>";
	 vorkasse = "<?php echo ORDERS_VORKASSE; ?>";
	 sonderproduktion = "<?php echo ORDERS_SONDERPRODUKTION; ?>";
	 abwicklungretour = "<?php echo ORDERS_ABWICKLUNGSRETOUR; ?>";
	 retourrechnung = "<?php echo ORDERS_RETOURRECHNUNG; ?>";
	 portokostenerstattet = "<?php echo ORDERS_PORTOKOSTENERSTATTET; ?>";
	 kreditkartezusaetzlichabgebucht = "<?php echo ORDERS_KREDITKARTEZUSAETZLICHABGEBUCHT; ?>";
	 adressaenderung = "<?php echo ORDERS_ADRESSAENDERUNG; ?>";
	 storniert = "<?php echo ORDERS_STORNIERT; ?>";
	 kreditkartenerstattung = "<?php echo ORDERS_KREDITKARTENERSTATTUNG; ?>";
	 fehlendezahlung = "<?php echo ORDERS_FEHLENDEZAHLUNG; ?>";
	 muster = "<?php echo ORDERS_MUSTER; ?>";
	 fehlendeustid = "<?php echo ORDERS_FEHLENDEUSTID; ?>";
	 kreditkartenzahlung_fehlt = "<?php echo ORDERS_KREDITKARTENZAHLUNG_FEHLT; ?>";
	 paypal_fehlt = "<?php echo ORDERS_PAYPAL_FEHLT; ?>";
	 kreditkartenzahlung_pruefen = "<?php echo ORDERS_KREDITKARTENZAHLUNG_PRUEFEN; ?>";
	 paypal_pruefen = "<?php echo ORDERS_PAYPAL_PRUEFEN; ?>";
	 fehlerhafteware = "<?php echo ORDERS_FEHLERHAFTEWARE; ?>";

	 abrede = "<?php echo "\\n\\n".ORDERS_MFG."\\nDr. Ferdinand Pohl"; ?>";
	 abrede2 = "<?php echo "\\n\\n".ORDERS_MFG."\\ni.A. Christine Kielhorn"; ?>";

	 if($F('textversand') == 'blank') document.getElementById('comments').value = anrede + blank + abrede2;
	 if($F('textversand') == 'artikelausverkauft') document.getElementById('comments').value = anrede + artikelausverkauft + abrede;
	 if($F('textversand') == 'mustersendung') document.getElementById('comments').value = anrede + mustersendung + abrede;
	 if($F('textversand') == 'vorkasse') document.getElementById('comments').value = anrede + vorkasse + abrede;
	 if($F('textversand') == 'sonderproduktion') document.getElementById('comments').value = anrede + sonderproduktion + abrede;
	 if($F('textversand') == 'abwicklungretour') document.getElementById('comments').value = anrede + abwicklungretour + abrede;
	 if($F('textversand') == 'retourrechnung') document.getElementById('comments').value = anrede + retourrechnung + abrede;
	 if($F('textversand') == 'portokostenerstattet') document.getElementById('comments').value = anrede + portokostenerstattet + abrede;
	 if($F('textversand') == 'kreditkartezusaetzlichabgebucht') document.getElementById('comments').value = anrede + kreditkartezusaetzlichabgebucht + abrede;
	 if($F('textversand') == 'adressaenderung') document.getElementById('comments').value = anrede + adressaenderung + abrede;
	 if($F('textversand') == 'storniert') document.getElementById('comments').value = anrede + storniert + abrede;
	 if($F('textversand') == 'kreditkartenerstattung') document.getElementById('comments').value = anrede + kreditkartenerstattung + abrede;
	 if($F('textversand') == 'fehlendezahlung') document.getElementById('comments').value = anrede + fehlendezahlung + abrede;
	 if($F('textversand') == 'muster') document.getElementById('comments').value = anrede + muster + abrede;
	 if($F('textversand') == 'fehlendeustid') document.getElementById('comments').value = anrede + fehlendeustid + abrede;
	 if($F('textversand') == 'kreditkartenzahlung_fehlt') document.getElementById('comments').value = anrede + kreditkartenzahlung_fehlt + abrede;
	 if($F('textversand') == 'kreditkartenzahlung_pruefen') document.getElementById('comments').value = anrede + kreditkartenzahlung_pruefen + abrede;
	 if($F('textversand') == 'paypal_fehlt') document.getElementById('comments').value = anrede + paypal_fehlt + abrede;
	 if($F('textversand') == 'paypal_pruefen') document.getElementById('comments').value = anrede + paypal_pruefen + abrede;
	 if($F('textversand') == 'fehlerhafteware') document.getElementById('comments').value = anrede + fehlerhafteware + abrede;

	}

	function setRechnung() {
		document.getElementById('firmenrechnung').submit();
	}
						
</script>
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
        <?php //Klasse body_text hinzugefügt by Gurkcity 26.10.2006 !Achtung! auch im stylesheet Angabe für Klasse erstellen:
        //text-decoration:underline (dient zur Besseren Lesbarkeit der Links?>
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2" class="body_text">

<?php
  if (($action == 'edit' || $action == 'update_products' || $action == 'storno_all' || $action == 'firmenrechnung') && ($order_exists == true)) {


		// Eintrag in die Blacklist
		require_once(DIR_WS_INCLUDES.'/gc_blacklist.php');

	
?>
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeadingOrder">
                        <?php
                        echo '<strong><a href="orders.php?page='.$_GET['page'].'&oID='.($_GET['oID']-1).'&action=edit">&laquo;</a>&nbsp;&nbsp;'.$oID.'&nbsp;&nbsp;<a href="orders.php?page='.$_GET['page'].'&oID='.($_GET['oID']+1).'&action=edit">&raquo;</a></strong><br>'.
                                $order->info['date_purchased'].'<br>';
                        
						if($order->info['isKrawattenCompany']){
							echo '<img src="images/logo_rechnung_com.gif" width="170" height="38" />';
						}
						else {
							switch($order->info['shop_id']) {
	                                case '1': echo '<img src="images/logo_rechnung_krt.gif" width="170" height="38" />';break;
	                                case '2': echo '<img src="images/logo_rechnung_krh.gif" width="170" height="38" />';break;
	                                case '3': echo '<img src="images/logo_rechnung_tnt.gif" width="170" height="38" />';break;
	                                case '4': echo '<img src="images/logo_rechnung_lac.gif" width="170" height="38" />';break;
	                                case '5': echo '<img src="images/logo_rechnung_krc.gif" width="170" height="38" />';break;
	                                case '7': echo '<img src="images/logo_rechnung_pay.gif" width="170" height="38" />';break;
	                        }
						}
                        ?><br><br>
<?php echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action'))) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> '; ?></td>
                        <td class="orderHistory"><?php //echo orderHistory() ?></td>
                        <td class="blackList">
                        <?php

                        $gc_sequery = gc_sequery_show_admin($oID);
                        if(!empty($gc_sequery)) echo '<div style="font-size:12px;">'.$gc_sequery['searchengine'].'| &quot;'.$gc_sequery['query'].'&quot; | '.$gc_sequery['time'].' Minuten</div>';
                        //echo blacklistStatus();

                        ?>
            </td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="700" border="0" cellspacing="0" cellpadding="2" style="border:1px solid #C9C9C9;float:left">
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main" valign="top"><b><?php echo ENTRY_CUSTOMER; ?></b></td>
                  <td class="main">
                      <?php echo tep_address_format($order->customer['format_id'], $order->customer, 1, '', '<br />');?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo ENTRY_TELEPHONE_NUMBER; ?></b></td>
                <td class="main"><?php echo $order->customer['telephone']; ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo ENTRY_EMAIL_ADDRESS; ?></b></td>
                <td class="main"><?php echo '<a href="mailto:' . $order->customer['email_address'] . '">' . $order->customer['email_address'] . '</a>'; ?></td>
              </tr>
            </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main" valign="top"><b><?php echo ENTRY_SHIPPING_ADDRESS; ?></b></td>
                <td class="main">
                                <?php
                                //abweichende Lieferadresse gegenüber Rechnungsadresse farblich kennzeichnen by Gurkcity #CG20080819 BOF

                                #go_dump($order);
                                $customer_string = strtolower($order->customer['name']."/".$order->customer['company']."/".$order->customer['street_address']."/".$order->customer['postcode']."/".$order->customer['city']);
                                $delivery_string = strtolower($order->delivery['name']."/".$order->delivery['company']."/".$order->delivery['street_address']."/".$order->delivery['postcode']."/".$order->delivery['city']);

                                ?>
                                <div<?php if($customer_string != $delivery_string)
                                {
                                        echo ' style="font-weight:bold; color:#000099"';
                                }
                                ?>><?php

                                  //Zeitangaben aus mysql-Date extrahieren
                                  $dp = $order->info['date_purchased'];
                                  $Y = substr($dp,0,4);
                                  $m = substr($dp,5,2);
                                  $d = substr($dp,8,2);
                                  $h = substr($dp,11,2);
                                  $i = substr($dp,14,2);

                                  //Umstellung am 20.04.2007 21:30:00
                                  if(mktime($h,$i,0,$m,$d,$Y)<mktime(21,30,0,4,20,2007))
                                  {
                                          echo '<p>wie Rechnungsadresse</p>
                  <p>(evtl. abweichende Lieferadresse im Kommentarfeld)  </p>';
                                  }
                                  else
                                  {
                                          echo tep_address_format($order->customer['format_id'], $order->delivery, 1, '', '<br />');
                                  }
                                   ?></div>

                                   <?php
                                   #CG20080819 EOF
                                   ?>
                                   </td>
              </tr>
            </table></td>

          </tr>
        </table>

        <?php
                #edited by Gurkcity #CG20090203 08:52
                ?>
        <table cellpadding="0" cellmargin="0" style="float: left; width: 120px; display: inline; border-spacing:0; margin: 0 0 0 10px"><tbody><tr><td>
       <form method="post" target="_blank" action="http://www.krawatten-ties.com/_ties0021/gc_orders_multitool.php?page=1" name="rechnungkopieren">
                       <input type="image" src="includes/languages/stm/images/buttons/button_rechnungkopieren.gif"/>
                        <input type="hidden" value="<?=$_GET['oID']?>" name="printOid[]" id="printOid[]"/>
            <input type="hidden" value="copy_order" name="action"/>
       </form> </td></tr></tbody></table>



        </td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main">
                    <b><?php echo strip_tags($order->info['payment_method']); ?></b>
                    
					<?php
						
						echo tep_draw_form('firmenrechnung', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=firmenrechnung', 'post', 'id="firmenrechnung"');
						echo '<select name="rechnung_type" onchange="setRechnung();">';
						echo '<option value="normal"'.(($order->info['netto'] != '1' and $order->info['firma'] != '1') ? ' selected="selected"' : '').'>Normale Rechnung</option>';
						echo '<option value="inland"'.(($order->info['firma'] == '1') ? ' selected="selected"' : '').'>Firmenrechnung Inland (mit MwSt)</option>';
						echo '<option value="ausland"'.(($order->info['netto'] == '1') ? ' selected="selected"' : '').'>Firmenrechnung Ausland (ohne MwSt)</option>';
						echo '</select>';
						echo '</form> ';
						
						?>

            </td>
            <td class="main">&nbsp;</td>
          </tr>
<?php
    if (tep_not_null($order->info['cc_type']) || tep_not_null($order->info['cc_owner']) || tep_not_null($order->info['cc_number'])) {
?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_TYPE; ?></td>
            <td class="main"><?php echo $order->info['cc_type']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_OWNER; ?></td>
            <td class="main"><?php echo $order->info['cc_owner']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_NUMBER; ?></td>
            <td class="main"><?php echo $order->info['cc_number']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_EXPIRES; ?></td>
            <td class="main"><?php echo $order->info['cc_expires']; ?></td>
          </tr>
<?php
    }
?>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr class="dataTableHeadingRow">
            <!--<td class="dataTableHeadingContent">Storno</td>-->
                        <td class="dataTableHeadingContent" colspan="2"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRICE_EXCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRICE_INCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TOTAL_INCLUDING_TAX; ?></td>
          </tr>
<?php
	#go_dump($order->info);
	#go_dump($order->info);
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
      echo '          <tr class="dataTableRow">' . "\n";


          #### Stornofunktion by Gurkcity 22.12.2006 BOF ####

          /*if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
        for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {
          $last_attr_value = $order->products[$i]['attributes'][$j]['value'];
        }
      }

          echo '<td class="dataTableContent" valign="top" align="left">';

          $anz_values = array();
          for($anz = 0; $anz <= $order->products[$i]['qty']; $anz++)
          {
                  $anz_values[$anz] = array('id' => $anz, 'text' => $anz);
          }

      echo tep_draw_pull_down_menu('storno_anz['.$order->products[$i]['products_id'].']['.$last_attr_value.']', $anz_values);

          echo '</td>';*/

          #### Stornofunktion by Gurkcity 22.12.2006 EOF ####


      echo '            <td class="dataTableContent" valign="top" align="right">'.

                   ##### by Gurkcity ####
                   #. tep_draw_input_field('products_quantity'.$order->products[$i]['prodid'], $order->products[$i]['qty'], 'size="3"') .

                   $order->products[$i]['qty'].'&nbsp;x</td>' . "\n" .
           '            <td class="dataTableContent" valign="top">' . $order->products[$i]['name'];

      if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
        for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {
          echo '<br /><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
          if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
          echo '</i></small></nobr>';

        }
      }


      echo '            </td>' . "\n" .
           '            <td class="dataTableContent" valign="top">' . $order->products[$i]['model'] . '</td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top">' . tep_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .


           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</b><br />' .


                   #<a href="'.FILENAME_ORDERS_CHANGE.'?opi='.$order->products[$i]['prodid'].'&oID='.$oID.'&action=edit">Produkt &auml;ndern</a>

                   '</td>' . "\n";
      echo '          </tr>' . "\n";
    }
?>

                  <tr>
                    <td align="left" colspan="3">

                        <?php
                        //komplette Rechnung stornieren by Gurkcity 14.11.2007 BOF
                        //Artikel alle storniert? Dann Versandkosten/Ust./Summe stornieren
                        $sql = "SELECT SUM(products_quantity) AS summe_produkte FROM `orders_products` WHERE orders_id = '".$oID."'";
                        $summe = mysql_fetch_array(mysql_query($sql));
                        if($summe['summe_produkte'] > '0')
                        {

                                echo tep_draw_form('status', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=storno_all');

                                echo tep_image_submit('button_storno_order.gif', 'Rechnung komplett stornieren')  . "\n";

                                echo '</form>&nbsp;&nbsp;';

                        }
                        ?>


                        <?='<a href="' . tep_href_link(FILENAME_ORDERS_EDIT_V2, 'oID=' . $_GET['oID']) . '">' . tep_image_button('button_edit_order.gif', IMAGE_EDIT) . '</a>'?>

                        </td>
            <td align="right" colspan="6"><table border="0" cellspacing="0" cellpadding="2">
<?php
    for ($i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
      echo '              <tr>' . "\n" .
           '                <td align="right" class="mainsmaller">' . $order->totals[$i]['title'] . '</td>' . "\n" .
           '                <td align="right" class="mainsmaller">' . $order->totals[$i]['text'] . '</td>' . "\n" .
           '              </tr>' . "\n";
    }

?>
            </table>
<?php
//ORDER_TOTAL ändern BOF
//wird manuell auf die Werte gesetzt
?>
              <!--<a href="<?=tep_href_link('change_order_totals.php?&oID='.$oID.'&action=edit');?>">Versandkosten, Mwst. und Summe &auml;ndern</a>-->
<?php
//ORDER_TOTAL ändern BOF
?>
                          </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main"><table border="1" cellspacing="0" cellpadding="5">
          <tr>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_DATE_ADDED; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_CUSTOMER_NOTIFIED; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_STATUS; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_COMMENTS; ?></b></td>
          </tr>
<?php
#CG20080819 (by Gurkcity): Kundenkommentar kennzeichnen, um diesen später explizit sichtbar zu machen, neues DB Feld: ALTER TABLE `orders` ADD `order_comment` ENUM( '0', '1' ) NOT NULL DEFAULT '0'; //BOF;
    $orders_history_query = tep_db_query("select orders_status_id, date_added, customer_notified, comments from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . tep_db_input($oID) . "' order by date_added");
    if (tep_db_num_rows($orders_history_query)) {
      while ($orders_history = tep_db_fetch_array($orders_history_query)) {

                $commentstye = '';
                if($order->info['order_comment'] == '1') //Customer Comment
                {
                        $commentstye = ' style = "font-weight:bold;color:red"';
                }

                echo '          <tr>' . "\n" .
             '            <td class="mainsmaller" align="center">' . tep_datetime_short($orders_history['date_added']) . '</td>' . "\n" .
             '            <td class="mainsmaller" align="center">';
        if ($orders_history['customer_notified'] == '1') {
          echo tep_image(DIR_WS_ICONS . 'tick.gif', ICON_TICK) . "</td>\n";
        } else {
          echo tep_image(DIR_WS_ICONS . 'cross.gif', ICON_CROSS) . "</td>\n";
        }
        echo '            <td class="mainsmaller">' . $orders_status_array[$orders_history['orders_status_id']] . '</td>' . "\n" .
             '            <td class="mainsmaller"><div'.$commentstye.'>' . nl2br(tep_db_output($orders_history['comments'])) . '&nbsp;</div></td>' . "\n" .
             '          </tr>' . "\n";


#CG20080819 EOF



      }
    } else {
        echo '          <tr>' . "\n" .
             '            <td class="mainsmaller" colspan="5">' . TEXT_NO_ORDER_HISTORY . '</td>' . "\n" .
             '          </tr>' . "\n";
    }


?>
        </table></td>
      </tr>
      <tr>
        <td class="main"><br /><b><?php echo TABLE_HEADING_COMMENTS; ?></b>
        </td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
      </tr>
      <tr>
        
    <td class="main"> 
      <?php echo tep_draw_form('status', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=update_order', 'post', 'id="status"'); ?>
      <table width="1%" border="0">
        <tr> 
          <td> 
            <?php echo tep_draw_textarea_field('comments', 'soft', '95', '19', '', '', '', 'comments'); ?>
          </td>
          <td align="left" valign="top"><div align="left"><img src="./includes/languages/stm/images/buttons/button_text_versand.gif" onClick="setText();" style="cursor:pointer;" /> 
              <br>
              <select name="select" id="textversand">
                <option value="blank"></option>
                <option value="artikelausverkauft">Artikel ausverkauft</option>
                <option value="mustersendung">Mustersendung</option>
                <option value="vorkasse">Vorkasse</option>
                <option value="sonderproduktion">Sonderproduktion</option>
                <option value="abwicklungretour">Abwicklung Retour</option>
                <option value="retourrechnung">Retour Rechnung</option>
                <option value="portokostenerstattet">Portokosten erstattet</option>
                <option value="kreditkartezusaetzlichabgebucht">Kreditkarte zus&auml;tzlich 
                abgebucht</option>
                <option value="adressaenderung">Adress&auml;nderung</option>
                <option value="storniert">Storniert</option>
                <option value="kreditkartenerstattung">Kreditkartenerstattung</option>
				<option value="fehlendezahlung">Fehlende Zahlung</option>
				<option value="fehlerhafteware">Fehlerhafte Ware</option>
				<option value="muster">Muster</option>
				<option value="fehlendeustid">Fehlende UST-ID</option>
				<option value="kreditkartenzahlung_fehlt">Gescheiterte Kreditkarten-Zahlung</option>
				<option value="kreditkartenzahlung_pruefen">Kreditkarten-Zahlung pr&uuml;fen</option>
				<option value="paypal_fehlt">Gescheiterte Paypal-Zahlung</option>
				<option value="paypal_pruefen">Paypal-Zahlung pr&uuml;fen</option>				
              </select>
            </div></td>
        </tr>
      </table> 
    
                        <?php
                        //Gutschein-Modul by Gurkcity 01.06.2007 BOF
                        ?>

                        <br>
                        <strong>Gutscheine</strong> (Leerzeichen getrennt):<br>
                        <?php echo tep_draw_input_field('coupons','','style="width:470px"'); ?>


                        <?php
                        //Gutschein-Modul by Gurkcity 01.06.2007 BOF
                        ?>


                </td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="2">
              <tr>


                <td class="main">
					<b><?php echo ENTRY_STATUS; ?></b> <?php echo tep_draw_pull_down_menu('status', $orders_statuses, $order->info['orders_status']); ?></td>


                          </tr>
             <?php /*?> <tr>
                <td class="main"><b><?php echo ENTRY_STATUS_TYPE; ?></b> <?php echo tep_draw_pull_down_menu('statustypes', $orders_types, $order->info['orders_type']); ?>(Funktioniert noch nicht!!!)</td>


                          </tr><?php */?>
              <tr>
                <td class="main"><b><?php echo ENTRY_NOTIFY_CUSTOMER; ?></b> <?php echo tep_draw_checkbox_field('notify', '', false); ?></td>
                <td class="main"><b><?php echo ENTRY_NOTIFY_COMMENTS; ?></b> <?php echo tep_draw_checkbox_field('notify_comments', '', false); ?></td>
              </tr>
            </table></td>
            <td valign="top"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
          </tr>
        </table></td>
      </form></tr>
      <tr>
          <?php

         //Mahnung Link ausgeben #AA20080814

          if ($order->info['orders_status'] == 9){

                $mahnung_link = '<a href="' . tep_href_link('mahnungM2.php', 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_mahnungM2.gif', IMAGE_ORDERS_MAHNUNG) . '</a>';

                }

                elseif ($order->info['orders_status'] == 10){

                $mahnung_link = '<a href="' . tep_href_link('mahnungM3.php', 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_mahnungM3.gif', IMAGE_ORDERS_MAHNUNG) . '</a>';

                }

        elseif ($order->info['orders_status'] == 13){

                $mahnung_link = '<a href="' . tep_href_link('mahnungM4-incasso.php', 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_mahnungM4.gif', IMAGE_ORDERS_MAHNUNG) . '</a>';

                }
        else {
                $mahnung_link = '';
        }

          ?>

        <td colspan="2" align="right"><?php
                if($order->info['netto'] != '1' and $order->info['firma'] != '1') {
                	echo '<a href="' . tep_href_link(FILENAME_ORDERS_INVOICE, 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_invoice.gif', IMAGE_ORDERS_INVOICE, 'id="buttoninvoice"'.$visibility) . '</a>';
                }
                echo '
                <a href="' . tep_href_link('invoice_netto.php', 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_invoice_firma.gif', IMAGE_ORDERS_INVOICE) . '</a>
                <a href="' . tep_href_link(FILENAME_ORDERS_PACKINGSLIP, 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_packingslip.gif', IMAGE_ORDERS_PACKINGSLIP) . '</a>';
                //Direkt zum Multitool für Paketaufkleber wechseln #edited by Gurkcity #CG20090123 16:28
                ?>
       <form name="etiketten" action="http://www.stropdas-mode.nl/_ties0021/gc_orders_multitool.php?selected_box=customers&page=1" target="_blank" method="post">
                       <input type="image" src="includes/languages/stm/images/buttons/button_paketaufkleber.gif">
                        <input id="printOid[]" name="printOid[]" value="<?=$_GET['oID']?>" type="hidden">
            <input name="action" value="print" type="hidden">
       </form>
        <?php

                echo $mahnung_link.'

                <a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action'))) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td width="100%">

                <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?><?php
                        if(isset($_GET['artikelsuche'])) echo "<br><span class=\"messageStackError\">Filter: Alle Artikel oder Artikelnummern mit \"".$_GET['artikelsuche']."\"</span>";
                        else if(isset($_GET['suche'])) echo "<br><span class=\"messageStackError\">Filter: Suche in Bestellungen nach \"".$_GET['suche']."\"</span>";
                        else if(isset($_GET['suchekommentar'])) echo "<br><span class=\"messageStackError\">Filter: Suche in Kommentaren nach \"".$_GET['suchekommentar']."\"</span>";

                        if(isset($_GET['artikelsuche']) || isset($_GET['suche']) || isset($_GET['suchekommentar'])) echo "<br><a href=\"orders.php\">Alle Bestellungen zeigen</a>";
                        ?>
                        <br><br>

<?php echo '<a href="' . tep_href_link("gc_new_order.php") .'">' . tep_image_button('button_neue_bestellung.gif', 'neue Bestellung aufgeben') . '</a> '; ?>
<?php echo '<a href="' . tep_href_link("aa_rechnungen_null_mwst.php") .'">Bestellungen mit 0% MwSt</a> '; ?>
</td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td align="right">

                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
                                <?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get'); ?><?php echo 'Suche:&nbsp;' . tep_draw_input_field('suche', '', 'size="25"'); ?></form><?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get'); ?>&nbsp;<?php echo 'Kommentare suchen:&nbsp;' . tep_draw_input_field('suchekommentar', '', 'size="25"'); ?></form><br>
                                <?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get'); ?><?php echo '&nbsp;Artikel/nr:&nbsp;' . tep_draw_input_field('artikelsuche', '', 'size="25"'); ?></form>
                                </td>
                            <td class="smallText" align="right">
                            <?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get'); ?><?php echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('oID', '', 'size="12"') . tep_draw_hidden_field('action', 'edit'); ?></form><br>
<?php echo tep_draw_form('status', FILENAME_ORDERS, '', 'get'); ?><?php echo HEADING_TITLE_STATUS . ' ' . tep_draw_pull_down_menu('status', $orders_statuses, '', 'onChange="this.form.submit();"'); ?></form>
                                </td>
                          </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><div id="container">


<?php
    if (isset($_GET['cID'])) {
      $cID = tep_db_prepare_input($_GET['cID']);
          //, o.shop_id hinzugefügt by Gurkcity 05.03.2007 -> GC-Multishop-Modul
          #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
      $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.customers_id, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o left join (" . TABLE_ORDERS_TOTAL . " ot  on o.orders_id = ot.orders_id, " . TABLE_ORDERS_STATUS . " s ) where o.shop_id = ".SHOP_ID." AND o.customers_id = '" . (int)$cID . "' and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and ot.class = 'ot_total' order by orders_id DESC";
    } elseif (isset($_GET['status'])) {
      $status = tep_db_prepare_input($_GET['status']);
          #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
      $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o left join (" . TABLE_ORDERS_TOTAL . " ot  on  o.orders_id = ot.orders_id, " . TABLE_ORDERS_STATUS . " s ) where o.shop_id = ".SHOP_ID." AND o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and s.orders_status_id = '" . (int)$status . "' and ot.class = 'ot_total' order by o.orders_id DESC";


        //erweiterte Suche by Gurkcity 06.11.2007
        } elseif (isset($_GET['suche'])) {
      $suche = tep_db_prepare_input($_GET['suche']);


          #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
          $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o left join (" . TABLE_ORDERS_TOTAL . " ot  on  o.orders_id = ot.orders_id, " . TABLE_ORDERS_STATUS . " s, " . TABLE_ORDERS_STATUS_HISTORY . " osh ) where o.shop_id = ".SHOP_ID." AND osh.orders_id = o.orders_id and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and ot.class='ot_total' ";



          $orders_query_raw .= "and (";
          $orders_query_raw .= "o.customers_name like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_company like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_street_address like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_city like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_country like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_telephone like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_email_address like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.customers_postcode like '%" . $suche . "%' ";

          $orders_query_raw .= "OR o.delivery_name like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.delivery_company like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.delivery_street_address like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.delivery_city like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.delivery_country like '%" . $suche . "%' ";
          $orders_query_raw .= "OR o.delivery_postcode like '%" . $suche . "%' ";

          //Gesamtwert durchsuchen
          $orders_query_raw .= "OR ot.text like '%" . $suche . "%' ";

          //Kommentare in osh durchsuchen
          #$orders_query_raw .= "OR osh.comments like '%" . $suche . "%' ";

          //Datum in orders durchsuchen
          $orders_query_raw .= "OR DATE_FORMAT(o.date_purchased,\"%d.%m.%Y\") like '%" . $suche . "%' ";


          //Subquery ot.text ausserhalb von ot.class = 'ot_total'
          /*
          1&1 unterstützt in MySQL-Version 4.0.25 keine Subqueries

          $orders_query_raw .= "OR orders_id = (SELECT orders_id FROM " . TABLE_ORDERS_STATUS_HISTORY . " WHERE comments like '%" . $suche . "%') ";

          */

          $orders_query_raw .= ") ";

          $orders_query_raw .= "group by o.orders_id order by o.orders_id DESC";


          #echo $orders_query_raw;
          #exit;

    } elseif (isset($_GET['suchekommentar'])) {
      $suchekommentar = tep_db_prepare_input($_GET['suchekommentar']);


          #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
          $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o left join (" . TABLE_ORDERS_TOTAL . " ot  on  o.orders_id = ot.orders_id, " . TABLE_ORDERS_STATUS . " s, " . TABLE_ORDERS_STATUS_HISTORY . " osh ) where o.shop_id = ".SHOP_ID." AND osh.orders_id = o.orders_id and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' ";



          $orders_query_raw .= "and (";


          //Kommentare in osh durchsuchen
          $orders_query_raw .= "osh.comments like '%" . $suchekommentar . "%' ";


          //Subquery ot.text ausserhalb von ot.class = 'ot_total'
          /*
          1&1 unterstützt in MySQL-Version 4.0.25 keine Subqueries

          $orders_query_raw .= "OR orders_id = (SELECT orders_id FROM " . TABLE_ORDERS_STATUS_HISTORY . " WHERE comments like '%" . $suche . "%') ";

          */

          $orders_query_raw .= ") ";

          $orders_query_raw .= "and ot.class = 'ot_total' group by o.orders_id order by o.orders_id DESC";


          #echo $orders_query_raw;
          #exit;

    }
        elseif (isset($_GET['artikelsuche'])) {
      $suche = tep_db_prepare_input($_GET['artikelsuche']);

          #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
          $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o left join (" . TABLE_ORDERS_TOTAL . " ot  on o.orders_id = ot.orders_id, " . TABLE_ORDERS_STATUS . " s, " . TABLE_ORDERS_PRODUCTS . " op ) where o.shop_id = ".SHOP_ID." AND op.orders_id = o.orders_id and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' ";


          $orders_query_raw .= "and (";


          //Artikelnr durchsuchen
          $orders_query_raw .= "op.products_model like '%" . $suche . "%' ";

          //Artikelnr durchsuchen
          $orders_query_raw .= "OR op.products_name like '%" . $suche . "%' ";


          $orders_query_raw .= ") ";

          $orders_query_raw .= "and ot.class = 'ot_total' group by o.orders_id order by o.orders_id DESC";


          #echo $orders_query_raw;
          #exit;

    } else {
      #CG20080819: order_comment added, #CG20081016: order_aktionsprogramm added
          $orders_query_raw = "select o.orders_id, o.shop_id, o.customers_id, o.customers_name, o.customers_company, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total, order_comment, order_aktionsprogramm from " . TABLE_ORDERS . " o  left join  " . TABLE_ORDERS_TOTAL . " AS ot on (o.orders_id = ot.orders_id ) , " . TABLE_ORDERS_STATUS . " s  where o.shop_id = ".SHOP_ID." AND o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and ot.class = 'ot_total' order by o.orders_id DESC";
    }

        //, o.shop_id hinzugefügt by Gurkcity 05.03.2007 -> GC-Multishop-Modul Ende
    $orders_split = new splitPageResultsOrder($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $orders_query_raw, $orders_query_numrows);

        #go_dump($orders_query_raw);
    $orders_query = tep_db_query($orders_query_raw);

        //query liefert keine Ergebnisse, es handelt sich aber um eine Suche, dann Untertabellen abfragen:
        //by Gurkcity 08.11.2007

        /*if(mysql_num_rows($orders_query) < 1 && isset($_GET['suche']))
        {
                //in orders_status_history Kommentaren suchen
                $orders_subquery_raw = "SELECT orders_id FROM " . TABLE_ORDERS_STATUS_HISTORY . " WHERE comments like '%" . $suche . "%' GROUP BY orders_id";
                #$orders_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $orders_query_raw, $orders_query_numrows);
                echo $orders_subquery_raw;
            $orders_subquery = tep_db_query($orders_subquery_raw);
                echo mysql_num_rows($orders_subquery);

        }*/
?>
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_split->display_count($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_split->display_links($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
                  </tr>
                </table>


                        <?php
                        //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 BOF 'CG20081126
                        echo tep_draw_form('etiketten', 'gc_orders_multitool.php', tep_get_all_get_params(array('action')));
                        //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 EOF
                        ?>
                        <script type="text/javascript" language="javascript">
                                function selectallboxes()
                                {

                                        var arrboxen = document.getElementsByName("printOid[]");
                                        for (i = 0; i < arrboxen.length; i++)
                                        {
                                                arrboxen[i].checked = true ;
                                        }

                                }
                        </script>

                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                          <?php
                                //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 BOF
                echo '<td class="dataTableHeadingContent">'.tep_image_submit('button_orders_druck.gif','druck','name="action" value="print"').'&nbsp;&nbsp;&nbsp;&nbsp;</td>';
                                //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 EOF
                                ?>
                                <td class="dataTableHeadingContent">
                                  <?php
                                        //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 BOF
                            echo tep_image_submit('button_orders_deliver.gif','deliver','name="action" value="deliver"').'&nbsp;&nbsp; ';
		echo tep_image_submit('button_orders_deliver_TM.gif','deliver_tm','name="action" value="deliver_TM"').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
				echo tep_image_submit('testbutton.gif','deliver_tm_TEST','name="action" value="deliver_TM_paketscheine"').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
		echo tep_image_submit('button_orders_ups.gif','email_versand_ups','name="action" value="email_versand_ups"').'&nbsp; ';
                            echo tep_image_submit('button_orders_post.gif','email_versand_post','name="action" value="email_versand_post"').'&nbsp; ';
		echo tep_image_submit('button_orders_TM.gif','email_versand_TM','name="action" value="email_versand_TM"').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ';
                                        #echo tep_image_submit('button_orders_copyorder.gif','copy orders','name="action" value="copy_order"').'&nbsp;&nbsp; ';
                            // create_picking() By Gurkcity 2009
                                        #edited by Gurkcity #CG20090123 16:54
                            echo tep_image_submit('button_orders_picking.gif','create picking','name="action" value="create_picking"').'&nbsp;&nbsp; ';
                            echo tep_image_submit('button_orders_selectall.gif','selectall','onclick="selectallboxes(); return false;"').'&nbsp;&nbsp; ';
                                        //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 EOF
                                        ?>
                                </td>
                                <td class="dataTableHeadingContent">Firma</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    while ($orders = tep_db_fetch_array($orders_query)) {
        #if ($orders['customers_id']=='0') $orders['customers_name'] = '<b>!!</b> ' . $orders['customers_name'];
    if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
        $oInfo = new objectInfo($orders);
      }

      
      if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) {
			echo '<tr id="defaultSelected"';
			echo ' class="dataTableRowSelected"';
			echo ' onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onDblClick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '\'"><a name="anchor0">' . "\n";
      } else {
			echo '<tr';
			echo ' class="dataTableRow"';
			echo ' onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onDblClick="document.location.href=\'' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '\'">' . "\n";
      }
?>
                <?php
                                //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 BOF
                                ?>
                                <td class="dataTableContent"><?=tep_draw_checkbox_field('printOid[]', $orders['orders_id'])?></td>
                                <?php
                                //Multitool-Modul für Paketaufkleber, Versandemails, CSV-Listen (Spediteure) by Gurkcity 26.11.2008 EOF
                                ?>
                                <td class="dataTableContent"><?php
                                #by Gurkcity 23.10.2006
                                #Order-Id eingefügt
                                echo $orders['orders_id'];

                                #by Gurkcity 05.03.2007
                                #Shop-Id eingefügt
                                #echo '/'.$orders['shop_id'];

                                ?><?php
                                #CG20080819: order_comment added: Sichtbarmachung in Liste BOF
                                if($orders['order_comment']=='1')
                                {
                                        echo '<span style="font-weight:bold;color:red;font-size:larger">&nbsp;!</span>';
                                }
                                #CG20080819: order_comment added EOF

                                //Name des Kunden anzeigen
                                echo '&nbsp;'.$orders['customers_name'];

                                #CG20081016: order_aktionsprogramm added: Sichtbarmachung in Liste BOF
                                if($orders['order_aktionsprogramm']=='1')
                                {
                                        echo '<span style="font-weight:bold;color:red;font-size:larger">&nbsp;<a href="http://www.krawatten-ties.com/_ties0021/_wiki/bin/view/Main/AktionsProgramme" target="_blank">' . tep_image(DIR_WS_IMAGES . 'icon_aktionsprogramm_gourmet.gif', 'Gourmet Aktionsprogramm') . '</a></span>';
                                }
                                #CG20081016: order_aktionsprogramm added EOF


                                #echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $orders['customers_name']; ?></td>
                                <td class="dataTableContent" align="left"><?php echo strip_tags($orders['customers_company']); ?></td>
                <td class="dataTableContent" align="right"><?php echo strip_tags($orders['order_total']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
                <td class="dataTableContent" align="right"><?php echo $orders['orders_status_name']; ?></td>
                <td class="dataTableContent" align="right"><?php if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              </table>
                        </form>

                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_split->display_count($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_split->display_links($orders_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></div></td>

          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>