<?php
/*
  $Id: filenames.php,v 1.1 2003/06/20 00:18:30 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// define the filenames used in the project
  define('FILENAME_BACKUP', 'backup.php');
  define('FILENAME_BANNER_MANAGER', 'banner_manager.php');
  define('FILENAME_BANNER_STATISTICS', 'banner_statistics.php');
  define('FILENAME_CACHE', 'cache.php');
  define('FILENAME_CATALOG_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
  define('FILENAME_CATEGORIES', 'categories.php');
  define('FILENAME_CONFIGURATION', 'configuration.php');
  define('FILENAME_COUNTRIES', 'countries.php');
  define('FILENAME_CURRENCIES', 'currencies.php');
  define('FILENAME_CUSTOMERS', 'customers.php');
  define('FILENAME_SALESAGENTS', 'salesagents.php');
  define('FILENAME_DEFAULT', 'index.php');
  define('FILENAME_DEFINE_LANGUAGE', 'define_language.php');
  define('FILENAME_DEFINE_MAINPAGE', 'define_mainpage.php');  // MaxiDVD Added WYSIWYG HTML Area
  define('FILENAME_FILE_MANAGER', 'file_manager.php');
  define('FILENAME_GEO_ZONES', 'geo_zones.php');
  define('FILENAME_LANGUAGES', 'languages.php');
  define('FILENAME_MAIL', 'mail.php');
  define('FILENAME_MANUFACTURERS', 'manufacturers.php');
  define('FILENAME_MODULES', 'modules.php');
  define('FILENAME_NEWSLETTERS', 'newsletters.php');
  define('FILENAME_ORDERS', 'orders.php');
  define('FILENAME_ORDERS_CHANGE', 'change_table_order_products.php');//by Gurkcity Order-Products �ndern
  define('FILENAME_ORDERS_CONFIRMATION', 'auftragsbestaetigung.php');
  define('FILENAME_ORDERS_INVOICE', 'invoice.php');
  define('FILENAME_ORDERS_PACKINGSLIP', 'packingslip.php');
  define('FILENAME_ORDERS_STATUS', 'orders_status.php');
  define('FILENAME_PHPOPENTRACKER', 'simple_report/');
  define('FILENAME_POPUP_IMAGE', 'popup_image.php');
  define('FILENAME_PRODUCTS_ATTRIBUTES', 'products_attributes.php');
  define('FILENAME_PRODUCTS_EXPECTED', 'products_expected.php');
  define('FILENAME_REVIEWS', 'reviews.php');
  define('FILENAME_SERVER_INFO', 'server_info.php');
  define('FILENAME_SHIPPING_MODULES', 'shipping_modules.php');
  define('FILENAME_SPECIALS', 'specials.php');
  define('FILENAME_STATS_CUSTOMERS', 'stats_customers.php');
  define('FILENAME_STATS_PRODUCTS_PURCHASED', 'stats_products_purchased.php');
  define('FILENAME_AA_STATS_PRODUCTS_PURCHASED', 'aa_stats_products_purchased.php');
  define('FILENAME_AA_STATS_PRODUCTS_NOT_PURCHASED', 'aa_stats_products_not_purchased.php');
  define('FILENAME_TURNOVEREXPORT_DATEV', 'export_turnovers_datev.php');
  define('FILENAME_PROJECTLIST', 'projects.php');
  define('FILENAME_WORKINGSCHEDULES', 'workingschedules.php');
  define('FILENAME_PROJECTSANDSCHEDULES', 'projects_and_schedules.php');
  define('FILENAME_SEO_GENERATOR_PROPERTIES', 'seo_propertygenerator_properties.php');
  define('FILENAME_SEO_MULTIPLY', 'seo_multiply.php');
  define('FILENAME_SEO_MULTIPLY_SYNONYMS_AJAX', 'seo_multiply_synonyms_ajax.php');
  define('FILENAME_SEO_TEXTS_SUCCESSANALYSE', 'seo_texts_successanalyse.php');
  define('FILENAME_SEO_REPORTS', 'seo_reports.php');
  
  define('FILENAME_STATS_PRODUCTS_VIEWED', 'stats_products_viewed.php');
  define('FILENAME_TAX_CLASSES', 'tax_classes.php');
  define('FILENAME_TAX_RATES', 'tax_rates.php');
  define('FILENAME_WHOS_ONLINE', 'whos_online.php');
  define('FILENAME_ZONES', 'zones.php');
  // order editor
  define('FILENAME_ORDERS_EDIT', 'edit_orders.php');
  define('FILENAME_ORDERS_EDIT_ADD_PRODUCT', 'edit_orders_add_product.php');
  define('FILENAME_ORDERS_EDIT_AJAX', 'edit_orders_ajax.php');
  define('FILENAME_ORDERS_EDIT_V2', 'edit_orders_v2.php');
  define('FILENAME_ORDERS_EDIT_ADD_PRODUCT_V2', 'edit_orders_add_product_v2.php');
  define('FILENAME_ORDERS_EDIT_AJAX_V2', 'edit_orders_ajax_v2.php');
  // end order editor
  define('FILENAME_STATS_TIES_TOP100', 'stats_ties_top100.php');
  define('FILENAME_CATALOG_VENDORORDERS', 'catalog_vendororders.php');
  define('FILENAME_PRINT_FRAME', 'print_frame.php');
  define('FILENAME_SEO_FILL_PRODUCTS_TEXTS', 'seo_fill_products_texts.php');
?>
