<?php
/*
  $Id: localization.php,v 1.16 2003/07/09 01:18:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
?>
<!-- salesagents //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_SALESAGENTS,
                     'link'  => tep_href_link(FILENAME_SALESAGENTS, 'selected_box=salesagents'));

  if ($selected_box == 'salesagents') {
    $contents[] = array('text'  => '<a href="' . tep_href_link(FILENAME_SALESAGENTS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SALESAGENTS_SUMMARY . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SALESAGENTS, 'action=create', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SALESAGENTS_CREATE . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SALESAGENTS, 'action=accounting', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SALESAGENTS_ACCOUNTING . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SALESAGENTS, 'action=messages', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SALESAGENTS_MESSAGES . '</a><br>');
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- localization_eof //-->
