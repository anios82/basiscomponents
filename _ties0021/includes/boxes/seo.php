<?php
/*
  $Id: catalog.php,v 1.21 2003/07/09 01:18:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
?>
<!-- catalog //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_SEO,
                     'link'  => tep_href_link(FILENAME_SEO_TEXTS_SUCCESSANALYSE, 'selected_box=seo'));

  if ($selected_box == 'seo') {
    $contents[] = array('text'  => '<a href="' . tep_href_link(FILENAME_SEO_TEXTS_SUCCESSANALYSE, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_TEXTS_SUCCESSANALYSE . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SEO_GENERATOR_PROPERTIES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_GENERATOR_PROPERTIES . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SEO_MULTIPLY, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_GENERATOR_MULTIPLY . '</a><br>'.
												'<a href="' . tep_href_link(FILENAME_SEO_FILL_PRODUCTS_TEXTS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_FILL_PRODUCTS_TEXTS . '</a><br>');
                                  
                                   // MaxiDVD Added Line For WYSIWYG HTML Area: EOF
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- catalog_eof //-->
