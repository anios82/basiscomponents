<?
$_edit = ($docID);


if($_edit){
  $objDoc = new blogDocument($docID, seoBlogsHandle::getBlogDb());
  $objDoc->load();
  
  if(!$objDoc->isLoaded()){
    _errormsg('Dokument konnte nicht geladen werden..');
    return;
  }
  
  $extractedContent = blogDocument::extractContent($objDoc->getProperty('content'));
  $contentPure = $extractedContent['content'];
  $linkstoDocs = $extractedContent['linkstoDocs'];
}
else {
  $objDoc = new blogDocument(NULL, seoBlogsHandle::getBlogDb());
}

$tmpProductsPre = (($form['products']) ? $form['products'] : $objDoc->getProperty('custom_1'));
if(!empty($tmpProductsPre)){
  foreach($tmp = explode('|', $tmpProductsPre) as $v):                    
          if($v)   $preSelProducts[] = $v;  // turn a|b|c||||  into a|b|c
  endforeach;
}

if($do == 'fromFormSave'){
		
        if(!$form['title']){
          
          $_formErrors['title'] = 'Bitte Titel angeben..';
        }
		 else{
          $form['title'] = stripslashes($form['title']);
        }
        
        if(!$form['teaser']){
          
          $_formErrors['teaser'] = 'Bitte Teaser angeben..';
        }
		 else{
          $form['teaser'] = stripslashes($form['teaser']);
        }
        
        if(!$form['content']){
          
          $_formErrors['content'] = 'Bitte Inhalt angeben..';
        }
        else{
          $form['content'] = stripslashes($form['content']);
        }
        
        if(!$_edit  ||  ($_edit  &&  $form['updatePrevPic'] == 'on')){
          
          if(!$_FILES['prevPic']['tmp_name'])
          $_formErrors['prevPic'] = 'Bitte Vorschaubild angeben..';
        }
        
        // parse for dead links
        $deadlinks = blogDocument::parseDeadLinks($form['content']);
        
        if(!empty($deadlinks)  &&  !$_POST['Force_Submit']){
          
          $_formErrors['deadlinks'] = 'Es sind tote (nicht funktionierende) Links im Inhalt enthalten:<br />';
          foreach($deadlinks as $linktext=>$link):
              $_formErrors['deadlinks'] .= '<br />Linktext: '.$linktext.', Link: '.$link;
          endforeach;
          $_formErrors['deadlinks'] .= '<br /><br />Bitte mit dem Link-Inserter des Editors korrigieren (mögliche Ursache: der Blog wurde von einem anderen Blog importiert zum Übersetzen, und
          es sind noch falsche Links enthalten, dann bitte auf die entsprechenden Links des aktuellen Blogs umstellen)..<br /><br />Dennoch speicher: dann den "Zwingen-Speichern" unter dem Dokument Button klicken"!';
          
          $showForceSubmitBtn = true;
        }
       
        if(empty($_formErrors)){
              
			  $objDoc->setProperty('created', date('Y-m-d H:i:s'));
			
              if(!$_edit){      
                
                // author
                $authorID = seoBlogsHandle::getDefaultAuthor($blogID);
                $objDoc->setProperty('authorId', $authorID);
                $objDoc->setProperty('projectId', $blogID);
              }
              else {
                
                 if($form['projectId']  == $blogID  ||  $form['projectId'] == $blogID.'_tmp'){
                  $objDoc->setProperty('projectId', $form['projectId']);
                 }
              }

              $objDoc->setProperty('title', mysql_real_escape_string(blogDocument::convertIllegalFrenchCharacters($form['title'])));
              $objDoc->setProperty('teaser', mysql_real_escape_string(blogDocument::convertIllegalFrenchCharacters($form['teaser'])));

              
              // create content
              $content = $objDoc->createContent(blogDocument::convertIllegalFrenchCharacters(str_replace("'", "`", $form['content'])), $form['linktoDocs']);
              
              // nodeID
              if(!$_edit){
                 $nodeId = ($form['nodeId']) ? $form['nodeId']  : $_blog['nodeId'];
			  }
              
              // products
              if($preSelProducts){
                $objDoc->setProperty('custom_1', implode('|', $preSelProducts));
              }
      
              $objDoc->setProperty('content', $content);
              $objDoc->setProperty('nodeId', $nodeId);
              
              
              if(!$_edit  ||  ($_edit  &&  $form['updatePrevPic'] == 'on')){
                
                      $prevPicSaved = seoBlogsHandle::savePrevPic($_FILES['prevPic'], $settings['previewPicAllowedTypes'], (($_blog['prevPicWidth']) ? $_blog['prevPicWidth'] : $settings['previewPicWidth']));
  					  
                      if($prevPicSaved[0] === false){
                        
                        switch($prevPicSaved[1]):
                          
                          case 'filetype':
                            _errormsg('Falscher Dateityp für Bilderupload..');
                            break;
                          case 'upload':
                             _errormsg('Fehler im Bilderupload..');
                            break;
                          case 'filemove':
                             _errormsg('Bild konnte nicht verschoben werden..');
                            break;                      
                        endswitch;
                    }    
                    else {
                        $objDoc->setProperty('pictures', '["/'.seoBlogsHandle::imagesWebPath.'/'.$_FILES['prevPic']['name'].'"]');  // Json format       
                    }
              }              


                            
              if($_edit){
                  $objDoc->setProperty('modified', date('Y-m-d H:i:s'));
                  $saved = $objDoc->save();                  
              }
              else {
                  $saved = $objDoc->create();
              }
              
              if($saved){
                
                    if(!$_edit  &&  !empty($form['linkinDocs'])){
                      
                      foreach($form['linkinDocs'] as $docID):
                        
                        if($objDoc->addLinkInRelatedBlog($docID)){
                          _successmsg('Verlinkung in Blog '.$docID.' erfolgreich erstellt..<br />');
                        }
                        else {
                          _successmsg('Verlinkung in Blog '.$docID.' konnte nicht erstellt werden..<br />');
                        } 
                      endforeach; ?>
                      <br /><?
                    }
                                                                                  
                    _successmsg('Dokument erfolgreich erstellt..<br /><br />', true);
                    ?><a href="<?=$_blog['url']?>" target="_blank">&raquo; Blog im Web anschauen</a><br /><br />
                    <a href="<?=$_baseurl.(($_blog['utf8shop']) ? '&utf8_encoded=true': '')?>">&laquo; Zurück zur Startseite</a><?
                   return;
              }
             else {
                _errormsg('Dokument konnte nicht erstellt werden..');
                return;
             }
        }
     }

?><script type="text/javascript" src="/admin_externe/ckeditor/ckeditor.js"></script>
<script language="Javascript"><!--

    function productChooser(){
      
      this.currentpos;
      this.productlist = new Array(<?=($settings['productsRows'] * $_blog['productsCols'])?>);
    }
    
    productChooser.prototype.selectMatrix = function(pos){
      

      this.currentpos = pos;
      
      for(i=1; i<=this.productlist.length; i++){

          if(i != pos){
            $('productsChooser_'+i).setStyle({
                          border: '1px #D0D0D0 solid'
                        });
          }
      }      

       $('productsChooser_'+pos).setStyle({
                        border: '1px #FF0000 solid'
                      });
    }
    
    productChooser.prototype.chooseProduct = function(prid){
      
      if(this.currentpos >= 1){
        this.productlist[(this.currentpos-1)] = prid;
        $('productsChooser_'+this.currentpos).update($('product_'+prid).innerHTML);  
        
        $('productsHelpHiddenField').value = this.productlist.join('|');
      }
    }
    
    productChooser.prototype.preChooseProduct = function(pos, prid){  // automatic call if productsString is set onLoad()
        
       if($('productsChooser_'+pos) != undefined  &&  $('product_'+prid) != undefined){
          this.productlist[(pos-1)] = prid;
          $('productsChooser_'+pos).update($('product_'+prid).innerHTML);  
       }
       
       $('productsHelpHiddenField').value = this.productlist.join('|');
    }
    
    var objProductChooser = new productChooser();
    
--></script><div id="blogcms_content">
<? require('bloginfo.inc.php'); ?>
<h1>Neuen Blogtext erstellen</h1><?

if(!empty($_formErrors)){
    _errormsg('Es liegen Eingabefehler vor (siehe unten)..<br /><br /><br />', true);
}
?>

<? _form('create', 'fromFormSave', true, (($_blog['utf8shop'] === true) ? 'accept-charset="UTF-8" ' : '')) ?>
<?=tep_draw_hidden_field('FORM[products]', (($form['products']) ? $form['products'] : $objDoc->getProperty('custom_1')), 'id="productsHelpHiddenField"');?><?
if($_edit){
  echo tep_draw_hidden_field('docID', $docID);
}
if($_blog['utf8shop'] === true){
  echo tep_draw_hidden_field('utf8_encoded', 'true');
} ?>

<?
if($_edit){ ?>
    
    Status:<br />
    <? 
    if($objDoc->getProperty('projectId') == $blogID.'_tmp'){ ?>
      <div id="statusTmp">Tempor�r&nbsp;&nbsp;      
      <? _select('FORM[projectId]', $blogID.'_tmp', array('Tempor�r'=>$blogID.'_tmp', 'Freigegeben'=>$blogID)); ?>
      </div>
    <? } 
    else { ?>      
      <div id="statusValid">Freigegeben&nbsp;&nbsp;
      <? _select('FORM[projectId]', $blogID, array('Freigegeben'=>$blogID, 'Tempor�r'=>$blogID.'_tmp')); ?></div>
    <? } ?>
    <br /><br />
<? }
?>

  Titel:<br />
  <div class="inputCon"><?
    _input('FORM[title]', (($form['title']) ? $form['title'] : $objDoc->getProperty('title')), 30, 100);
    if($_formErrors['title']) {
      echo _errormsg('<br />'.$_formErrors['title']);
    }
  ?></div>
  
  <br /><br />
  Teaser:<br />
  <div class="inputCon"><?
    _textarea('FORM[teaser]', (($form['teaser']) ? $form['teaser'] : $objDoc->getProperty('teaser')), 50, 3);
     if($_formErrors['teaser']) {
      echo _errormsg('<br />'.$_formErrors['teaser']);
    }
  ?></div>
  
  <?
  if($_blog['nodeIdNews']  ||  $_blog['furtherNodeIds']){ ?>
    <br /><br />
    Blogverzeichnis:<br />
    <div class="inputCon"><?
    
    $nodes =  array('Hauptblog'=>$_blog['nodeId']);
    
    if($_blog['nodeIdNews']){
       $nodes['News'] = $_blog['nodeIdNews'];
    }
    if($_blog['furtherNodeIds']){
      foreach($_blog['furtherNodeIds'] as $nodeID=>$nodeTitle):
        $nodes[$nodeTitle] = $nodeID;
      endforeach;
    }
    
      _select('FORM[nodeId]', (($form['nodeId']) ? $form['nodeId'] : $objDoc->getProperty('nodeId')), $nodes);
    ?></div>
  <? } ?>
  
  <br /><br />
  Inhalt (Falls im nächsten Punkt zu verlinkende Blogartikel angeklickt werden, so empfielt sich den Blogcontent mit einem Satz abzuschließen wie "Hier lesen Sie im Blog weiter .." o.ä.):<br />
  <div class="inputCon"><?
    _textarea('FORM[content]', (($form['content']) ? $form['content'] : $contentPure), 100, 30, 'editorContent');
     if($_formErrors['content']) {
      echo _errormsg('<br />'.$_formErrors['content']);
    }
    if($_formErrors['deadlinks']) {
      echo _errormsg('<br />'.$_formErrors['deadlinks']);
    }
  ?></div>
  
  <script type="text/javascript">
  window.onload = function()
  {
    CKEDITOR.replace( 'editorContent' );
  };
</script>
  
  <?  
  $docs = seoBlogsHandle::getAllDocuments($blogID);  
  
  if(!empty($docs)){ ?>
    
    <br /><br />
    Welche Blogartikel (dieses Blogs) sollen in diesem Blogartikel verlinkt werden? (3 auswählen)<br />
    <div class="inputCon"><span class="relatedDoc"><?
    
      foreach($docs as $doc): ?>
        <input type="checkbox" name="FORM[linktoDocs][]" value="<?=$doc->getProperty('id')?>"<?
          
          if($form['linktoDocs']){
             if(in_array($doc->getProperty('id'), $form['linktoDocs'])){
                echo ' checked';
             }
          }
          elseif(is_array($linkstoDocs) && in_array($doc->getProperty('id'), $linkstoDocs)){
            echo ' checked';
          }
        ?> />&nbsp;<?=$doc->getProperty('title')?> &nbsp; 
      <? endforeach;
    ?></span> 
    </div>
  <? }
  ?>
  
    <?    
  if(!$_edit && !empty($docs)){ ?>
    
    <br /><br />
    In welchen Blogartikeln (dieses Blogs) soll dieser Blogartikel verlinkt werden? (2-4 auswählen)<br />
    <div class="inputCon"><span class="relatedDoc"><?
    
      foreach($docs as $doc): ?>
        <input type="checkbox" name="FORM[linkinDocs][]" value="<?=$doc->getProperty('id')?>"<?
          
          if($form['linkinDocs']){
             if(in_array($doc->getProperty('id'), $form['linkinDocs'])){
                echo ' checked';
             }
          }
        ?> />&nbsp;<?=$doc->getProperty('title')?> &nbsp; 
      <? endforeach;
    ?></span> 
    </div>
  <? }
  ?>
  
  <?

  if(!$_blog['noProducts']){
  tep_db_connect();

  $products = _getRandomProductsForShop($shopInfo[$_blog['shopId']]['language_id'], $shopInfo[$_blog['shopId']]['products_price_fieldname'], $preSelProducts);

   if(!empty($products)){ ?>
    
   <br /><br />
   Welche Produkte sollen im Blogtext verlinkt werden (mit Bild)? (6-12 auswählen)<br /><br />
   
   <div id="products_left">
       1. Position in Matrix anklicken:<br />
       <div id="productsChooser"><?
          
          for($r=1; $r<=$settings['productsRows']; $r++):
            for($c=1; $c<=$_blog['productsCols']; $c++): ?>
              
              <div id="productsChooser_<?=((($r-1) * $_blog['productsCols'])+$c)?>" class="pic" onClick="Javascript: objProductChooser.selectMatrix(<?=((($r-1) * $_blog['productsCols'])+$c)?>);"></div>
              
            <? endfor;
            ?><div style="clear: both;"></div><? 
          endfor; 
       ?></div>
   </div>
   <div id="products_right">
   2. Produkt auswählen (750 zufällig gewählte Produkte des Shops):<br />
    <div class="productsCon"><span class="relatedDoc"><?
      $tmp=1;
      foreach($products as $product): 
        
        $img = tep_product_image($product['model'], 'sm', $product['name'], 80, 90);
        if(!strstr($img, '/images')){
          $imageStr = str_replace('images/', '/images/', $img);
        }
        else {
          $imageStr = $img;
        }
               
        if(!$imageStr)   continue;
        ?>
        <div id="product_<?=$product['id']?>" class="product" onClick="Javascript: objProductChooser.chooseProduct(<?=$product['id']?>);"><?=$imageStr?></div> 
      <? 
        $tmp++;
      endforeach;
    ?></span> 
    </div>
  <? }
  }
  ?>
  </div>
  
  <div style="clear: both";></div>
  <br /><br />
  Vorschaubild (nur Jpg/Gif, wird automatisch auf <?=(($_blog['previewPicWidth']) ? $_blog['previewPicWidth'] : $settings['previewPicWIdth'])?>px Breite skaliert):<br />
  <div class="inputCon">
    <input type="file" name="prevPic" /><?
     if($_edit){ 
            
            $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);

            $pictures = $json->decode($objDoc->getProperty('pictures'));
			?>
              &nbsp;&nbsp;<input type="checkbox" name="FORM[updatePrevPic]" value="on" /> Bild ändern<?
              $blogShema = parse_url($_blog['url']); 
              
			  if(!empty($pictures)){ ?>
              	<br /><br /><img src="http://<?=$blogShema['host']?>/<?=$pictures[0]?>" /><?
			  }
     }
     
     if($_formErrors['prevPic']) {
      echo _errormsg('<br />'.$_formErrors['prevPic']);
    } ?>
  </div>
  
  <br /><br />
  <input type="submit" value="Blogartikel speichern" class="save" /><?
  if($showForceSubmitBtn){ ?>
    &nbsp;&nbsp;<input type="submit" name="Force Submit" value="Speichern erzwingen" class="save" />
  <? } ?>
</form><?

if($preSelProducts){ ?>
  <script language="Javascript"><!--      
    <?     
    
    foreach($preSelProducts as $c=>$prID): ?>
      objProductChooser.preChooseProduct(<?=($c+1)?>, <?=$prID?>);
    <? endforeach; ?>    
      
  --></script>
<? }

?>
</div>