<?
$sources = $_blog['sources'];

if($do == 'fromForm'){
  
  foreach($sources as $src):
    
      if(!($gateway = $settings['servers'][$settings['blogs'][$src]['server']]['exportGateway'])){
        _errormsg('Kein Gateway gefunden (zu '.$src.')..<br />');
        continue;
      }
         
      $gateway .= '?srcBlogID='.$src.'&dstBlogID='.$blogID.(($_testserver) ? '&testserver=true' : '');

      // get docIDs already imported
      $alreadyImported = $objBlogs->import_getImportedDocIDsForSrc($src);
      
      if(!empty($alreadyImported)){
        $gateway .= '&excludeDocIDs='.implode(';', $alreadyImported);
      }

      $exportResult = explode(';', file_get_contents($gateway));
      
      if($exportResult[0] == '1'){
        _successmsg('Export-Datei ('.$src.') erfolgreich extern erstellt mit '.$exportResult[1].' zu importierenden Blogartikeln..<br />');
        $successExports[$src] = array('src'=>$krt, 'num'=>$exportResult[1]);
      }
      else {
         _errormsg('Export-Datei ('.$src.') konnte nicht erstellt werden ('.$exportResult[1].')..<br />');
      }
      
  endforeach;
  
  if(!empty($successExports)){
    $do = 'toFormStartImport';
  }
}
elseif($do == 'fromFormStartImport' && $src = $form['src']){
  
  $ftpH = $settings['servers'][$settings['blogs'][$src]['server']]['exportFtpHost'];
  $ftpU = $settings['servers'][$settings['blogs'][$src]['server']]['exportFtpUser'];
  $ftpP = $settings['servers'][$settings['blogs'][$src]['server']]['exportFtpPass'];
  $ftpF = $settings['servers'][$settings['blogs'][$src]['server']]['exportFtpFolder'];
  
  $ftpCon = ftp_connect($ftpH);
  
  if(!is_resource($ftpCon)){
     _errormsg('FTP-Connect zum Quellserver fehlgeschlagen..<br />');
  }
  else{
  
    if(!ftp_login($ftpCon, $ftpU, $ftpP)){
      _errormsg('FTP-Login zum Quellserver fehlgeschlagen..<br />');
    }
    else {
      //die($settings['importDirectory'].'/'.$src.'_'.$blogID.'.xml');
      if(!ftp_get($ftpCon, $settings['importDirectory'].'/'.$src.'_'.$blogID.'.xml', $ftpF.'/'.$src.'_'.$blogID.'.xml', FTP_ASCII)){
        _errormsg('FTP-Transfer fehlgeschlagen..<br />');
      }
      else {
        $numDocsAdded = $objBlogs->import_intoDB($settings['importDirectory'].'/'.$src.'_'.$blogID.'.xml', $settings['blogs'], $src, $ftpCon);
        
        if($numDocsAdded > 0){
          _successmsg($numDocsAdded.' Blogartikel erfolgreich importiert..<br />');
        }
        else{
          _errormsg('Es wurden keine Blogartikel importiert..<br />');
        }
      }
    }
  }  
  
  if(is_resource($ftpCon)){
    ftp_close($ftpCon);
  }
}

?><div id="blogcms_content">
<? require('bloginfo.inc.php'); ?>
<h1>Blogtexte importieren</h1>

<?
if(!$do){
    if($_blog['type'] == 'translated'  &&  $sources){ ?>
      <? _form('import', 'fromForm', true) ?>
      
      <font>Laut Konfiguration werden Dokumente von folgenden Blogs importiert:</font><br /><br />
      <?
      foreach($sources as $src): ?>
        <li><?=$settings['blogs'][$src]['name']?> (<?=$settings['blogs'][$src]['id']?>)</li>
      <? endforeach;
      ?><br /><br />
      <input type="submit" value="Extern Blog-Importdatei erstellen" class="import" />
      </form><?
    }
    else {
      _errormsg('F�r diesen Blog ist kein Import vorgesehen.', true);
    }
}
elseif($do == 'toFormStartImport'){
  
  if($successExports){ ?>
  
    <font>Export-Blogdateien erfolgreich extern erstellt.. Bereit zum Import..</font><br /><br />
    <? foreach($successExports as $src=>$data): ?>
    
      <? _form('import', 'fromFormStartImport', true, 'target="_blank"') ?>
      <input type="hidden" name="FORM[src]" value="<?=$src?>" />
      <input type="submit" value="Import von <?=$settings['blogs'][$src]['name']?> mit <?=$data['num']?> Artikeln starten" class="import" />
      </form><br /><br /><?
      endforeach;
  }
}?>

</div>