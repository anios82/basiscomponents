<?
if($do == 'delete'  &&  $_GET['docID']){
  
  $objDoc = new blogDocument($_GET['docID'], seoBlogsHandle::getBlogDb());
  
  if(!$objDoc->load()){
    _errormsg('Dokument konnte nicht geladen werden..<br />');
  }
  else {
     if(!$objDoc->delete()){
      _errormsg('Dokument konnte nicht gel&ouml;scht werden..<br />');
    }
    else {
      _successmsg('Dokument erfolgreich gel&ouml;scht..<br />');
    }
  }
}
elseif($do == 'settmp'  &&  $_GET['docID']){
  
  $objDoc = new blogDocument($_GET['docID'], seoBlogsHandle::getBlogDb());
  
  if(!$objDoc->load()){
    _errormsg('Dokument konnte nicht geladen werden..<br />');
  }
  else {
    
    $objDoc->setProperty('projectId', $blogID.'_tmp');
    
     if(!$objDoc->save()){
      _errormsg('Dokument konnte nicht ge&auml;ndert werden..<br />');
    }
    else {
      _successmsg('Dokument erfolgreich ge&auml;ndert..<br />');
    }
  }
}
elseif($do == 'setvalid'  &&  $_GET['docID']){
  
  $objDoc = new blogDocument($_GET['docID'], seoBlogsHandle::getBlogDb());
  
  if(!$objDoc->load()){
    _errormsg('Dokument konnte nicht geladen werden..<br />');
  }
  else {
    
    $objDoc->setProperty('projectId', $blogID);
    
     if(!$objDoc->save()){
      _errormsg('Dokument konnte nicht ge&auml;ndert werden..<br />');
    }
    else {
      _successmsg('Dokument erfolgreich ge&auml;ndert..<br />');
    }
  }
}


$docs = array_merge(seoBlogsHandle::getAllDocuments($blogID.'_tmp'), seoBlogsHandle::getAllDocuments($blogID));
?><div id="blogcms_content">
<? require('bloginfo.inc.php'); ?>
<h1>Blogartikel editieren / freigeben / l&ouml;schen</h1>
<br /><br /><?  
if(empty($docs)){
  _errormsg('Es liegen noch keine Blogdokumente vor..');
  return;
} ?>

<table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="dataTableHeadingRow">
              <td class="dataTableHeadingContent" width="250">Titel</td>
              <td class="dataTableHeadingContent" width="100">Erstelldatum</td>
              <td class="dataTableHeadingContent" width="80">Status</td>
              <td class="dataTableHeadingContent" width="250">&nbsp;</td>
            </tr><?
            
            foreach($docs as $obj):               
                $status = ($blogID == $obj->getProperty('projectId')) ? 'valid' : 'temp';
                ?>              
                  <tr class="dataTableRow">
                      <td class="dataTableContent"<?
                          if($status == 'temp') echo ' style="background-color: '.$settings['tempblogsBgcolor'].';"';
                      ?>><?=$obj->getProperty('title')?></td>
                      <td class="dataTableContent"<?
                          if($status == 'temp') echo ' style="background-color: '.$settings['tempblogsBgcolor'].';"';
                      ?>><?
                          $created = date('d.m.Y H:i', strtotime($obj->getProperty('created')));
                          echo $created;
                      ?></td>
                      <td class="dataTableContent"<?
                          if($status == 'temp') echo ' style="background-color: '.$settings['tempblogsBgcolor'].';"';
                      ?>><?=(($status == 'temp') ? '<i>Tempor&auml;r</i>' : 'Freigegeben')?></td>                      
                      <td class="dataTableContent"<?
                          if($status == 'temp') echo ' style="background-color: '.$settings['tempblogsBgcolor'].';"';
                      ?>><a href="<?   
                         echo $_baseurl;
                        ?>&module=blogcms&action=create&docID=<?=$obj->getProperty('id')?>&blogID=<?=$blogID?><?=(($_blog['utf8shop'] === true) ? '&utf8_encoded=true' : '')?>">&raquo; Editieren<?=(($_blog['type'] == 'translated') ? '/&uuml;bersetzen': '')?></a>&nbsp;&nbsp;&nbsp;
                        <a href="<?   
                          echo $_baseurl;
                        ?>&module=blogcms&action=edit&do=delete&docID=<?=$obj->getProperty('id')?>&blogID=<?=$blogID?>">&raquo; Blog l&ouml;schen</a>&nbsp;&nbsp;&nbsp;<?
                        if($status == 'valid'){ ?>                          
                          <a href="<?   
                          echo $_baseurl;
                        ?>&module=blogcms&action=edit&do=settmp&docID=<?=$obj->getProperty('id')?>&blogID=<?=$blogID?>">&raquo; Pausieren</a>
                        <? }
                        elseif($status == 'temp'){ ?>                          
                          <a href="<?   
                          echo $_baseurl;
                        ?>&module=blogcms&action=edit&do=setvalid&docID=<?=$obj->getProperty('id')?>&blogID=<?=$blogID?>">&raquo; Freigeben</a>
                        <? } ?>
                        </td>
                  </tr>
             <?  endforeach; ?>
             </table></td>
         </tr>
</table>
</div>