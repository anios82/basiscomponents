<?
  function _errormsg($aText = NULL, $aStrong = false){ ?>
    
    <span class="messageStackError"<?=(($aStrong) ? ' style="font-size: 18px; font-weight: bold;"' : '')?>><?=$aText?></span>
  <? }
  
  function _successmsg($aText = NULL, $aStrong = false){ ?>
    
    <span class="messageStackSuccess"<?=(($aStrong) ? ' style="font-size: 18px; font-weight: bold;"' : '')?>><?=$aText?></span>
  <? }
  
  function _form($aAction = NULL, $aDo = NULL, $aMultipart = false, $aParams = ''){
        
          global $module, $blogID;
           
          echo tep_draw_form('tool_tradeshow_marketing', $_SERVER['PHP_SELF'], 'POST', $aParams.(($aMultipart) ? 'enctype=\'multipart/form-data\'': ''));
          echo tep_draw_hidden_field('FORM[module]', $module);
          echo tep_draw_hidden_field('blogID', $blogID);
          echo tep_draw_hidden_field('action', $aAction);
          echo tep_draw_hidden_field('do', $aDo);
  }
  
  function _input($aName = NULL, $aValue = NULL, $aSize = NULL, $aMaxlength = NULL){
    
    echo '<input name="'.$aName.'" value="'.$aValue.'" size="'.$aSize.'" maxlength="'.$aMaxlength.'" />';
  }
  
  function _textarea($aName = NULL, $aValue = NULL, $aCols = NULL, $aRows = NULL,$aId = NULL){
    
    echo '<textarea name="'.$aName.'" cols="'.$aCols.'" rows="'.$aRows.'" id="'.$aId.'">'.$aValue.'</textarea>';
  }
  
  function _select($aName = NULL, $aValue = NULL, $aOptions = NULL){
    
    echo '<select name="'.$aName.'">';
    foreach($aOptions as $label=>$value){
      
      echo '<option value="'.$value.'"'.(($value == $aValue) ? 'selected' : '').'>'.$label.'</option>';
    }    
    echo '</select>';
  }
  
  
  function sortProductsByModel($a, $b){
  
    if($a['model'] > $b['model']){
      return(-1);
    }elseif($a['model'] < $b['model']){
      return(1);
    }
    else return(0);     
  }
  
  function _getRandomProductsForShop($aLangID = NULL, $aPriceField = NULL, $mustGetProducts = array()){
    
    if(!$aLangID  ||  !$aPriceField)   return(false);
    
	global $db_link;
    $sql = 'SELECT t1.products_model, t1.products_id,  t2.products_name FROM products as t1 '.
                  'LEFT JOIN products_description as t2 ON(t1.products_id = t2.products_id AND t2.language_id = '.$aLangID.') '.
           		// 'JOIN products_to_categories as t3 ON (t2.products_id = t3.products_id AND t3.categories_id = 507) '.
                  'WHERE t1.products_status = 1 AND t1.'.$aPriceField.' > 0 ORDER BY RAND() LIMIT 750';

     $res = tep_db_query($sql);
     
     while(list($model, $id, $name) = tep_db_fetch_row($res)):
       
       $ret[$model] = array('id' => $id, 'model'=>$model, 'name'=>$name);          
     endwhile;
     
     if(!empty($mustGetProducts)){
       
       $sql = 'SELECT t1.products_model, t1.products_id,  t2.products_name FROM products as t1 '.
                    'LEFT JOIN products_description as t2 ON(t1.products_id = t2.products_id AND t2.language_id = '.$aLangID.') '.
                    'WHERE t1.products_id IN('.implode(',', $mustGetProducts).')';
                    
       $res = tep_db_query($sql);
       
       while(list($model, $id, $name) = tep_db_fetch_row($res)):
         
         $ret[$model] = array('id' => $id, 'model'=>$model, 'name'=>$name);          
       endwhile;
     }

    usort($ret, 'sortProductsByModel'); 
     
     return($ret);
  }
?>