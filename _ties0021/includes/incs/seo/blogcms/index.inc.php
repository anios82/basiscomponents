<div id="blogcms_content">

<h1>Installierte Blogs</h1>

<table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" width="100">Server</td>
                <td class="dataTableHeadingContent" width="100">Sprache</td>
                <td class="dataTableHeadingContent" width="290">Projekt (ID)</td>
                <td class="dataTableHeadingContent" width="140">Typ</td>
                <td class="dataTableHeadingContent" width="400">&nbsp;</td>
              </tr>
              <?
              foreach($settings['blogs'] as $blogId=>$blogSet):

                            $requServer= parse_url($blogSet['adminUrl']); 
                            $tmp = explode('.', $requServer['host']);   
                            $requServerHostBase =  $tmp[(count($tmp)-2)].'.'.$tmp[(count($tmp)-1)];// 1und1 will have lacravate.com (without www) in server_name, OVH has www.corbatas.es with www

                            $tmp = explode('.', $_SERVER['SERVER_NAME']);   
                            $requCurrentHostBase =  $tmp[(count($tmp)-2)].'.'.$tmp[(count($tmp)-1)];// 1und1 will have lacravate.com (without www) in server_name
                            ?>
              
                    <tr class="dataTableRow">
                        <td class="dataTableContent"><?=$settings['servers'][$blogSet['server']]['name']?></td>
                        <td class="dataTableContent"><?=$blogSet['language']?></td>
                        <td class="dataTableContent"><?=$blogSet['name']?> (<?=$blogId?>) &nbsp;<a href="<?=$blogSet['url']?>" target="_blank">&raquo; aufrufen</a></td>
                        <td class="dataTableContent"><?
                            if($blogSet['type'] == 'unique'){
                              echo 'eigenst&auml;ndig';
                            }
                            elseif($blogSet['type'] == 'translated'){
                              echo '&uuml;bersetzt von ';
                              echo implode(', ', $blogSet['sources']);
                            }                        
                        ?></td>
                        <td class="dataTableContent"><a href="<?   
                            
                            
                            $baseurl = ($requCurrentHostBase != $requServerHostBase) ? $blogSet['adminUrl'].'?auth='.session_id() : $_SERVER['PHP_SELF'].'?internal=true';
                            echo $baseurl;
                        ?>&module=blogcms&action=create&blogID=<?=$blogId?><?=(($blogSet['utf8shop'] === true) ? '&utf8_encoded=true' : '')?>">&raquo; Blog erstellen</a>&nbsp;&nbsp;&nbsp;
                        <a href="<?   
                        
                            $baseurl = ($requCurrentHostBase != $requServerHostBase) ? $blogSet['adminUrl'].'?auth='.session_id() : $_SERVER['PHP_SELF'].'?internal=true';
                            echo $baseurl;
                        ?>&module=blogcms&action=edit&blogID=<?=$blogId?><?=(($blogSet['utf8shop'] === true) ? '&utf8_encoded='.$blogSet['utf8shop'] : '')?>">&raquo; Blog editieren<?=(($blogSet['type'] == 'translated') ? '/&uuml;bersetzen': '')?></a>&nbsp;&nbsp;&nbsp;<?
                        if($blogSet['type'] == 'translated'){ ?>                          
                          <a href="<?      
                                             
                            $baseurl = ($requCurrentHostBase != $requServerHostBase) ? $blogSet['adminUrl'].'?auth='.session_id() : $_SERVER['PHP_SELF'].'?internal=true';
                            echo $baseurl;
                        ?>&module=blogcms&action=import&blogID=<?=$blogId?>">&raquo; Blogs importieren</a>
                       <?  }                        
                        ?></td>
                    </tr>
              <? endforeach;
              ?>              
              </table></td>
           </tr>
</table>
</div>