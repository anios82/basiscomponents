function SetFocus() {
  if (document.forms.length > 0) {
    var field = document.forms[0];
    for (i=0; i<field.length; i++) {
      if ( (field.elements[i].type != "image") &&
           (field.elements[i].type != "hidden") &&
           (field.elements[i].type != "reset") &&
           (field.elements[i].type != "submit") ) {

        document.forms[0].elements[i].focus();

        if ( (field.elements[i].type == "text") ||
             (field.elements[i].type == "password") )
          document.forms[0].elements[i].select();

        break;
      }
    }
  }
}

function rowOverEffect(object) {
  if (object.className == 'dataTableRow') object.className = 'dataTableRowOver';
  if (object.className == 'dataTableRowShop2') object.className = 'dataTableRowOverShop2';
  if (object.className == 'dataTableRowShop3') object.className = 'dataTableRowOverShop3';
  if (object.className == 'dataTableRowShop4') object.className = 'dataTableRowOverShop4';
  if (object.className == 'dataTableRowShop5') object.className = 'dataTableRowOverShop5';
  if (object.className == 'dataTableRowShop11') object.className = 'dataTableRowOverShop11';
}

function rowOutEffect(object) {
  if (object.className == 'dataTableRowOver') object.className = 'dataTableRow';
  if (object.className == 'dataTableRowOverShop2') object.className = 'dataTableRowShop2';
  if (object.className == 'dataTableRowOverShop3') object.className = 'dataTableRowShop3';
  if (object.className == 'dataTableRowOverShop4') object.className = 'dataTableRowShop4';
  if (object.className == 'dataTableRowOverShop5') object.className = 'dataTableRowShop5';
  if (object.className == 'dataTableRowOverShop11') object.className = 'dataTableRowShop11';
}
