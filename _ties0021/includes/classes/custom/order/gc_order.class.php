<?php

/***************************************
* Class Order
***************************************/
class GC_Order {

	/***************************************
	* 
	***************************************/
	function GC_Order($iOrderId) {
		
		if(!empty($iOrderId))
			$this->iOrderId = $iOrderId;
		
		$this->getOrder();
		$this->getBilling();
		$this->getProducts();
		$this->getTotalsCustom();
		$this->getShipping();
		
		$this->calcSubtotal();
		$this->calcSubSubtotal();
		$this->calcTax();
		$this->calcTotal();
		
	}

	/***************************************
	* 
	***************************************/
	function calculateOrder() {
		
		$this->getOrder();
		$this->getBilling();
		$this->getProducts();
		$this->getTotalsCustom();
		$this->getShipping();
		
		$this->calcProducts();
		$this->calcSubtotal();
		$this->calcShipping();
		$this->calcSubSubtotal();
		$this->calcTax();
		$this->calcTotal();
			
		tep_db_query("DELETE FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='ot_total'");
		tep_db_query("INSERT INTO orders_total SET orders_id='".$this->iOrderId."', title='<b>Totaal:</b>', text='&euro; ".number_format($this->fTotal, 2, ',', '.')."', value='".$this->fTotal."', class='ot_total', sort_order='7'");
		
		

	}
	
	/***************************************
	* 
	***************************************/
	function getOrder() {
		
		$hQueryOrder = tep_db_query("SELECT * FROM orders WHERE orders_id='".$this->iOrderId."'");
		$this->aOrder =  tep_db_fetch_array($hQueryOrder);
			
	}
	
	/***************************************
	* 
	***************************************/
	function getProducts() {
		
		$this->aProducts = array();
		
		$hQueryProducts = tep_db_query("SELECT op.products_id, op.products_model, op.products_name, op.products_price, op.final_price, op.products_tax, op.products_quantity FROM orders_products AS op WHERE op.orders_id='".$this->iOrderId."'");
		while($aFetchProducts = tep_db_fetch_array($hQueryProducts))
			$this->aProducts[] = $aFetchProducts;
		
	}
	
	/***************************************
	* 
	***************************************/
	function getTotalsCustom() {
	
		$this->aTotalsCustom = array();
	
		$hQueryTotalsCustom = tep_db_query("SELECT * FROM orders_total WHERE orders_id='".$this->iOrderId."' and SUBSTR(class, 1, 9)='ot_custom'");
		while($aFetchTotalsCustom = tep_db_fetch_array($hQueryTotalsCustom))
			$this->aTotalsCustom[] = $aFetchTotalsCustom;
			
	}
	
	/***************************************
	* 
	***************************************/
	function getBilling() {
	
		$hQueryBilling = tep_db_query("SELECT firma, netto FROM orders WHERE orders_id='".$this->iOrderId."'");
		$aFetchBilling = tep_db_fetch_array($hQueryBilling);
		
		if($aFetchBilling['netto']=='0' and $aFetchBilling['firma']=='0')
			$this->sBilling = 'normal';
		elseif($aFetchBilling['netto']=='1')
			$this->sBilling = 'ausland';
		elseif($aFetchBilling['firma']=='1')
			$this->sBilling = 'inland';
		
	}
	
	/***************************************
	* 
	***************************************/
	function getShipping() {
	
		$hQueryShipping = tep_db_query("SELECT value, text, title FROM orders_total WHERE class='ot_shipping' AND orders_id='".$this->iOrderId."'");
		$this->fShipping = tep_db_fetch_array($hQueryShipping);
		
	}
	
	/***************************************
	* 
	***************************************/
	function getTax() {
	
		$hQueryTax = tep_db_query("SELECT value FROM orders_total WHERE class='ot_tax' AND orders_id='".$this->iOrderId."'");
		if(tep_db_num_rows($hQueryTax)) {
			$aFetchTax = tep_db_fetch_array($hQueryTax);
			$this->fTax = $aFetchTax['value'];
		}
		else {
			$this->fTax = 0.0;
		}
	
	}
	
	/***************************************
	* 
	***************************************/
	function getSubtotal() {
	
		$this->fSubtotal = 0.0;
			
		if(!empty($this->aProducts)) {
			foreach($this->aProducts as $aProduct) {
				
				$this->fSubtotal += ( (float)$aProduct['products_price'] + ( ((float)$aProduct['products_price'] / 100) * (float)$aProduct['products_tax'])) * $aProduct['products_quantity'];
				
			}
		}
		
	}
	
	/***************************************
	* 
	***************************************/
	function getSubSubtotal() {
		
		$this->fSubSubtotal = (float)$this->fSubtotal + (float)$this->fShipping['value'];
		
		if(!empty($this->aTotalsCustom)) {
			foreach($this->aTotalsCustom as $aTotalCustom) {
				
				$this->fSubSubtotal += (float)$aTotalCustom['value'];
				
			}
		}
		
	}
	
	/***************************************
	* 
	***************************************/
	function getZoneId() {

		$hQueryZoneId = tep_db_query("SELECT z2gz.geo_zone_id FROM zones_to_geo_zones AS z2gz, countries AS c WHERE c.countries_id=z2gz.zone_country_id AND c.countries_name='".$this->aOrder['delivery_country']."'");
		$aFetchZoneId = tep_db_fetch_array($hQueryZoneId);
		$this->iZoneId = $aFetchZoneId['geo_zone_id'];

	}
	
	/***************************************
	* 
	***************************************/
	function calcProducts() {
	
		if($this->sBilling=='normal')  tep_db_query("UPDATE orders_products SET products_tax='19.0' WHERE orders_id='".$this->iOrderId."'"); 
		if($this->sBilling=='inland')  tep_db_query("UPDATE orders_products SET products_tax='0.00' WHERE orders_id='".$this->iOrderId."'"); 
		if($this->sBilling=='ausland') tep_db_query("UPDATE orders_products SET products_tax='0.00' WHERE orders_id='".$this->iOrderId."'");
		
		$this->getProducts();
		
	}
	
	/***************************************
	* 
	***************************************/
	function calcShipping() {
		
		if($this->fShipping['title'] != 'Geen verzendkosten:') {
		
			$this->getZoneId();
			
			if($this->iZoneId == '1') {
				if($this->fSubtotal >= 100.0)
					tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(0.0, 2, ',', '.')."', value='0.0', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				else {
					if($this->sBilling=='normal')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.9, 2, ',', '.')."', value='4.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='inland')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.1176, 2, ',', '.')."', value='4.1176', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.1176, 2, ',', '.')."', value='4.1176', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				}
			}
			if($this->iZoneId == '2') {
				if($this->fSubtotal >= 100.0)
					tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro;".number_format(0.0, 2, ',', '.')."', value='0.0', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				else {
					if($this->sBilling=='normal')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(7.9, 2, ',', '.')."', value='7.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='inland')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(6.6387, 2, ',', '.')."', value='6.6387', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(6.6387, 2, ',', '.')."', value='6.6387', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				}
			}
			if($this->iZoneId == '3') {
			
				if($this->sBilling=='normal') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(9.9, 2, ',', '.')."', value='9.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				if($this->sBilling=='inland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(8.3193, 2, ',', '.')."', value='8.3193', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(8.3193, 2, ',', '.')."', value='8.3193', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
			
			}
			
		}
		
		$this->getShipping();
		
	}
	
	/***************************************
	* 
	***************************************/
	function calcTax() {
		
		tep_db_query("DELETE FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='ot_tax'");
		
		if($this->sBilling=='normal')  tep_db_query("INSERT INTO orders_total SET title='19 % BTW:', text='".number_format((($this->fSubSubtotal/119)*19), 2, ',', '.')." &euro;', value='".(($this->fSubSubtotal/119)*19)."', orders_id='".$this->iOrderId."', class='ot_tax', sort_order='6'");
		if($this->sBilling=='inland')  tep_db_query("INSERT INTO orders_total SET title='19 % BTW:', text='".number_format((($this->fSubSubtotal/100)*19), 2, ',', '.')." &euro;', value='".(($this->fSubSubtotal/100)*19)."', orders_id='".$this->iOrderId."', class='ot_tax', sort_order='6'");
		if($this->sBilling=='ausland') { }
		
		$this->getTax();
		
	}
	
	/***************************************
	* 
	***************************************/
	function calcSubtotal() {
		
		$this->getSubtotal();
		
		tep_db_query("DELETE FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='ot_subtotal'");
		
		tep_db_query("INSERT INTO orders_total SET title='Subtotaal:', text='&euro; ".number_format($this->fSubtotal, 2, ',', '.')."', value='".($this->fSubtotal)."', orders_id='".$this->iOrderId."', class='ot_subtotal', sort_order='1'");
		
	}
	
	/***************************************
	* 
	***************************************/
	function calcSubSubtotal() {
		
		$this->getSubSubtotal();
		
	}
	
	/***************************************
	* 
	***************************************/
	function calcTotal() {
		
		if($this->sBilling=='normal')  $this->fTotal = $this->fSubSubtotal ;
		if($this->sBilling=='inland')  $this->fTotal = $this->fSubSubtotal + $this->fTax;
		if($this->sBilling=='ausland') $this->fTotal = $this->fSubSubtotal + $this->fTax;
		
	}

	
	/***************************************
	* 
	***************************************/
	function setCountry($iCountryId) {
	
		tep_db_query("UPDATE orders AS o, countries AS c  SET o.delivery_country = c.countries_name WHERE c.countries_id='".$iCountryId."' and o.orders_id='".$this->iOrderId."'");
		
		$this->calculateOrder();
		
	}
	
	/***************************************
	* 
	***************************************/
	function setBilling($sBilling) {
		if($sBilling=='normal') {
			tep_db_query("UPDATE orders SET netto='0', firma='0' WHERE orders_id='".$this->iOrderId."'");
			if($this->sBilling=='ausland' or $this->sBilling=='inland')
				tep_db_query("UPDATE orders_total SET value=value*1.19, text=CONCAT( '&euro;', REPLACE( REPLACE( REPLACE( FORMAT( (value*1.19), 2), '.', '|'), ',', '.'), '|', ',')) WHERE orders_id='".$this->iOrderId."' AND SUBSTR(class, 1, 9)='ot_custom'");
		}
		if($sBilling=='inland') {
			tep_db_query("UPDATE orders SET netto='0', firma='1' WHERE orders_id='".$this->iOrderId."'");
			if($this->sBilling=='normal')
				tep_db_query("UPDATE orders_total SET value=value/1.19, text=CONCAT( '&euro;', REPLACE( REPLACE( REPLACE( FORMAT( (value/1.19), 2), '.', '|'), ',', '.'), '|', ',')) WHERE orders_id='".$this->iOrderId."' AND SUBSTR(class, 1, 9)='ot_custom'");
		}
		if($sBilling=='ausland') {
			tep_db_query("UPDATE orders SET netto='1', firma='0' WHERE orders_id='".$this->iOrderId."'");
			if($this->sBilling=='normal')
				tep_db_query("UPDATE orders_total SET value=value/1.19, text=CONCAT( '&euro;', REPLACE( REPLACE( REPLACE( FORMAT( (value/1.19), 2), '.', '|'), ',', '.'), '|', ',')) WHERE orders_id='".$this->iOrderId."' AND SUBSTR(class, 1, 9)='ot_custom'");
		}
		
		$this->calculateOrder();
		
	}
	
	/***************************************
	* 
	***************************************/
	function setShipping($bFree=false) {
		
		if($bFree === true)
			tep_db_query("UPDATE orders_total SET title='Geen verzendkosten:', text='&euro; 0,00', value='0.0' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
			
		else {
		
			$this->getZoneId();
			
			if($this->iZoneId == '1') {
				if($this->fSubtotal >= 100)
					$this->fShipping = 0.0;
				else {
					if($this->sBilling=='normal')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.9, 2, ',', '.')."', value='4.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='inland')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.1176, 2, ',', '.')."', value='4.1176', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(4.1176, 2, ',', '.')."', value='4.1176', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				}
			}
			if($this->iZoneId == '2') {
				if($this->fSubtotal >= 100)
					$this->fShipping = 0.0;
				else {
					if($this->sBilling=='normal')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(7.9, 2, ',', '.')."', value='7.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='inland')  tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(6.6387, 2, ',', '.')."', value='6.6387', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
					if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze (vanaf 100 &euro; gratis):', text='&euro; ".number_format(6.6387, 2, ',', '.')."', value='6.6387', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				}
			}
			if($this->iZoneId == '3') {
				if($this->sBilling=='normal') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(9.9, 2, ',', '.')."', value='9.9', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				if($this->sBilling=='inland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(8.3193, 2, ',', '.')."', value='8.3193', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
				if($this->sBilling=='ausland') tep_db_query("UPDATE orders_total SET title='Pakket verzendwijze:', text='&euro; ".number_format(8.3193, 2, ',', '.')."', value='8.3193', sort_order='2' WHERE orders_id='".$this->iOrderId."' AND class='ot_shipping'");
			}
			
		}
		
		$this->calculateOrder();
		
	}
	
	/***************************************
	* 
	***************************************/
	function deleteProduct($iProductId) {
		
		tep_db_query("DELETE FROM orders_products WHERE orders_products_id='".$iProductId."'");
		tep_db_query("DELETE FROM orders_products_attributes WHERE orders_products_id='".$iProductId."'");
		
		$this->calculateOrder();
		
	}
	
	/***************************************
	* 
	***************************************/
	function addProduct($aProducts) {
		
		/*
		foreach($aProducts as $aProduct) {
			$hQueryProduct = tep_db_query("SELECT products_id FROM orders_products WHERE products_model='".$aProduct['model']."'");
			if(tep_db_num_rows($hQueryProduct)) {
				tep_db_query("UPDATE orders_products SET products_quantity=products_quantity+".$aProduct['qty']." WHERE orders_id='".$this->iOrderId."'");
			}
			else {
				tep_db_query("INSERT INTO orders_products (orders_id, products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity) VALUES (".$this->iOrderId.", products_id, products_model, products_name, products_price, final_price, products_tax, products_quantity)");
			}
		}
		
		$this->calculateOrder();
		
		*/
		
	}
	
	/***************************************
	* 
	***************************************/
	function updateQty($iProductId, $iProductQty) {
	
		tep_db_query("UPDATE orders_products SET products_quantity='".$iProductQty."' WHERE orders_products_id='".$iProductId."'");
		
		$this->calculateOrder();
	
	}
	
	/***************************************
	* 
	***************************************/
	function updatePrice($iProductId, $iProductPrice) {
	
		tep_db_query("UPDATE orders_products SET products_price='".$iProductPrice."', final_price='".$iProductPrice."' WHERE orders_products_id='".$iProductId."'");
		
		$this->calculateOrder();
	
	}
	
	/***************************************
	* 
	***************************************/
	function updateName($iProductId, $iProductName) {
	
		tep_db_query("UPDATE orders_products SET products_name='".$iProductName."' WHERE orders_products_id='".$iProductId."'");
		
		$this->calculateOrder();
	
	}
	
	/***************************************
	* 
	***************************************/
	function updateTotalsTitle($sTotalId, $sTotalName, $sTotalTitle) {
		
		$hQueryTotals = tep_db_query("SELECT * FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='".$sTotalId."'");
		if(tep_db_num_rows($hQueryTotals) == 0) {
				tep_db_query("INSERT INTO orders_total (orders_id, title, text, value, class, sort_order) VALUES ('".$this->iOrderId."', '".$sTotalTitle."', '&euro; 0,00', '0.00', '".$sTotalId."', '3')");
		}
		else {
			if(!empty($sTotalTitle)) {
				tep_db_query("UPDATE orders_total SET title='".$sTotalTitle."' WHERE orders_id='".$this->iOrderId."' AND class='".$sTotalId."'");
			}
			else {
				tep_db_query("DELETE FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='".$sTotalId."'");
				$hQueryTotalsCustom = tep_db_query("SELECT * FROM orders_total WHERE orders_id='".$this->iOrderId."' AND SUBSTR(class, 1, 9) = 'ot_custom' ORDER BY class DESC");
				$iTotalsCustom = 1;
				while($aFetchTotalsCustom = tep_db_fetch_array($hQueryTotalsCustom)) {
					tep_db_query("UPDATE orders_total SET class='ot_custom" . $iTotalsCustom++ . "' WHERE orders_total_id='".$aFetchTotalsCustom['orders_total_id']."'");
				}
			}
		}
		
		$this->calculateOrder();
			
	}
	
	/***************************************
	* 
	***************************************/
	function updateTotalsValue($sTotalId, $sTotalName, $sTotalValue) {
		
		$hQueryTotals = tep_db_query("SELECT * FROM orders_total WHERE orders_id='".$this->iOrderId."' AND class='".$sTotalId."'");
		if(tep_db_num_rows($hQueryTotals) == 0)
			return NULL;
		else
			tep_db_query("UPDATE orders_total SET value='".$sTotalValue."', text='&euro; ".number_format($sTotalValue, 2, ',', '.')."' WHERE orders_id='".$this->iOrderId."' AND class='".$sTotalId."'");
			
		$this->calculateOrder();
			
	}
	
}
/***************************************
* End  Class Order
***************************************/

?>