<?

class categories {
	
	var $langId;
	
	function categories($aLangId = NULL){
		
		if($aLangId){
			$this->langId = $aLangId;
		}
	}
	
	
	function getSubcatsForCat($aCatId = NULL){
		
		global $languages_id;
		
		if(!$aCatId)   return(false);
		
		 $sql = 'SELECT t1.categories_id, t1.parent_id as parentid, t1.sort_order as sortorder, t2.categories_name as name, t2.categories_textattributes as textattributes, t2.categories_default_products_id as default_productid '.
					'FROM '.TABLE_CATEGORIES.' as t1 '.
					'LEFT JOIN '.TABLE_CATEGORIES_DESCRIPTION.' as t2 '.
						'ON (t1.categories_id = t2.categories_id AND t2.language_id = \''.(($this->langId) ? $this->langId : $languages_id).'\')  '.
					'WHERE t1.categories_status = 1 AND t1.parent_id = \''.$aCatId.'\' '.
					'ORDER BY t1.sort_order ASC';

		  $result = tep_db_query($sql);
		  
		  $ret = array();
		  while($data = tep_db_fetch_array($result)):
		  	
		  	  $item = new category_item($data['categories_id'], $this->langId);
			  $item->loadArray($data);
			  if($item->isLoaded()){
			  	$ret[] = $item;
			  }
		  endwhile;
		  
		  return($ret);
	}
}



class category_item {
	
	var $id;
	var $parentid;
	var $name;
	var $sortorder;
	var $textattributes = array();
	var $default_productid;
	var $langId;
	
	var $propertyRequ = array('id', 'name');
	var $isLoaded = false;	
	
	function isLoaded(){
		
		return($this->isLoaded);
	} 
	
	function category_item($aId = NULL, $aLangId = NULL){
		
		$this->id = $aId;
		
		if($aLangId){
			$this->langId = $aLangId;
		}
	}	
	
	function setParentId($aParentId = NULL){
		
		$this->parentid = $aParentId;
	}
	
	function setName($aName = NULL){
		
		$this->name = $aName;
	}
	
	function setTextattributes($aTextattributes = NULL){
		
		$this->textattributes = $aTextattributes;
	}
	
	function load(){

		if(!$this->id)   return(false);
		
		if($this->isLoaded())   return(true);
				
		$dbArr = $this->getDBArrayById($this->id);
		
		if(is_array($dbArr)){
			return($this->loadArray($dbArr));
		}
		
		return(false);		
	}
	
	function getDBArrayById($aId = NULL){
		
		global $languages_id;
		
		if($aId){
			 $sql = 'SELECT t1.categories_id, t1.parent_id as parentid, t1.sort_order as sortorder, t2.categories_name as name, t2.categories_textattributes as textattributes, t2.categories_default_products_id as default_productid '.
						'FROM '.TABLE_CATEGORIES.' as t1 '.
						'LEFT JOIN '.TABLE_CATEGORIES_DESCRIPTION.' as t2 '.
							'ON (t1.categories_id = t2.categories_id AND t2.language_id = \''.(($this->langId) ? $this->langId : $languages_id).'\')  '.
						'WHERE t1.categories_status = 1 AND t1.categories_id = \''.$aId.'\'';

			  $result = tep_db_query($sql);
			  $data = tep_db_fetch_array($result);
			  return($data);
		}		
		
		return(false);
	}
	
	function loadArray($aProperties = NULL){
		
		if(!is_array($aProperties))   return(false);
		
		if($this->isLoaded())   return(true);

		return($this->setLoadedProperties($aProperties));
	}
	
	function setLoadedProperties($aProperties = NULL){
		
		if(!$aProperties)   return(false);
		
		if(isset($aProperties['parentid']))   $this->parentid = $aProperties['parentid'];		
		if(isset($aProperties['name']))   $this->name = $aProperties['name'];
		if(isset($aProperties['sortorder']))   $this->sortorder = $aProperties['sortorder'];
		if(isset($aProperties['textattributes'])){
			
			$this->textattributes = explode("\r\n", $aProperties['textattributes']);
		}   		
		if(isset($aProperties['default_productid']))   $this->default_productid = $aProperties['default_productid'];
		
		$this->isLoaded = ($this->checkPropertyRequirements());
		
		return($this->isLoaded);
	}
	
	function checkPropertyRequirements(){
		
		foreach($this->propertyRequ as $prop){
			if($this->getProperty($prop) === false)   return(false);	
		}
		
		return(true);
	}
	
	function getProperty($aProp = NULL){
		
		if(!$aProp)   return(false);
		
		if(isset($this->$aProp)){ // value can be '0' so must check on is set
			return($this->$aProp);
		}
		
		return(false);		
	}	
	
	function saveDefaultProductid($aDefPrId = null){
		
		if(!$aDefPrId)   return(false);
		
		return(tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, array('categories_default_products_id '=>$aDefPrId), 'update', 'categories_id = \''.$this->id.'\' AND language_id = \''.$this->langId.'\''));
	}
}

?>