<?php
/*
  $Id: database_tables.php,v 1.1 2003/06/20 00:18:30 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// define the database table names used in the project
  define('TABLE_ADDRESS_BOOK', 'address_book');
  define('TABLE_ADDRESS_FORMAT', 'address_format');
  define('TABLE_BANNERS', 'banners');
  define('TABLE_BANNERS_HISTORY', 'banners_history');
  define('TABLE_CATEGORIES', 'categories');
  define('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
  define('TABLE_CONFIGURATION', 'configuration');
  define('TABLE_CONFIGURATION_GROUP', 'configuration_group');
  define('TABLE_COUNTRIES', 'countries');
  define('TABLE_CURRENCIES', 'currencies');
  define('TABLE_CUSTOMERS', 'customers');
  define('TABLE_CUSTOMERS_BASKET', 'customers_basket');
  define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES', 'customers_basket_attributes');
  define('TABLE_CUSTOMERS_INFO', 'customers_info');
  define('TABLE_LANGUAGES', 'languages');
  define('TABLE_MANUFACTURERS', 'manufacturers');
  define('TABLE_MANUFACTURERS_INFO', 'manufacturers_info');
  define('TABLE_NEWSLETTERS', 'newsletters');
  define('TABLE_ORDERS', 'orders');
  define('TABLE_ORDERS_PRODUCTS', 'orders_products');
  define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');
  define('TABLE_ORDERS_PRODUCTS_DOWNLOAD', 'orders_products_download');
  define('TABLE_ORDERS_STATUS', 'orders_status');
  define('TABLE_ORDERS_TYPES', 'orders_types');
  define('FIELD_ORDERS_STATUS_AUFTRBEST', 'Auftragsbest�tigung');
  define('TABLE_ORDERS_STATUS_HISTORY', 'orders_status_history');
  define('TABLE_ORDERS_TOTAL', 'orders_total');
  define('TABLE_PRODUCTS', 'products');
  define('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
  define('TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD', 'products_attributes_download');
  define('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
  define('TABLE_PRODUCTS_NOTIFICATIONS', 'products_notifications');
  define('TABLE_PRODUCTS_OPTIONS', 'products_options');
  define('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
  define('TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS', 'products_options_values_to_products_options');
  define('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
  define('TABLE_REVIEWS', 'reviews');
  define('TABLE_REVIEWS_DESCRIPTION', 'reviews_description');
  define('TABLE_SESSIONS', 'sessions');
  define('TABLE_SPECIALS', 'specials');
  define('TABLE_TAX_CLASS', 'tax_class');
  define('TABLE_TAX_RATES', 'tax_rates');
  define('TABLE_GEO_ZONES', 'geo_zones');
  define('TABLE_ZONES_TO_GEO_ZONES', 'zones_to_geo_zones');
  define('TABLE_WHOS_ONLINE', 'whos_online');
  define('TABLE_ZONES', 'zones');  
  define('TABLE_PRODUCTS_STOCK', 'products_stock');
  define('TABLE_SALESAGENTS', 'salesagents');  
  define('TABLE_SALESAGENTS_MESSAGES', 'salesagents_messages');  
  define('TABLE_CONTENT_TEXTS', 'content_texts');  
  define('TABLE_CONTENT_CATS', 'content_cats');  
  define('TABLE_CONTENT_KEYWORDS', 'content_keywords');   
  define('TABLE_CONTENT_VISITS', 'content_visits');  
  define('TABLE_PROJECTS', 'projects');  
  define('TABLE_PROJECTS_TASKS', 'projects_tasks');  
  define('TABLE_PROPERTIES_CATS', 'products_properties_cats');  
  define('TABLE_PROPERTIES_KOMBINATIONS', 'products_properties_kombinations');  
  define('TABLE_PROPERTIES_PATTERNS', 'products_properties_patterns');  
  define('TABLE_PROPERTIES_VALUES', 'products_properties_values');  
  define('TABLE_SALESAGENTS', 'salesagents'); 
  define('TABLE_SALESAGENTS_MESSAGES', 'salesagents_messages'); 
  define('TABLE_SALESAGENTS_COMMISSIONS', 'salesagents_commissions'); 
  define('TABLE_STATS_TIES_TOP100', 'stats_ties_top100'); 
  define('TABLE_VENDORORDERS', 'vendororders');
  define('TABLE_VENDORORDERS_ITEMS', 'vendororders_items'); 
  define('TABLE_VENDORORDERS_ITEMS_SIZES', 'vendororders_items_sizes');
  define('TABLE_SEO_TRANSLATESHOPTIES_PRODUCTS', 'seo_translateshopties_products');
  define('TABLE_WORKINGSCHEDULES', 'workingschedules'); 
  define('TABLE_SEO_MULTIPLY_SYNONYMS', 'seo_multiply_synonyms'); 
  define('TABLE_SEO_MULTIPLY_SYNONYMS_CATS', 'seo_multiply_synonyms_cats'); 
  define('TABLE_EXTSHOPS_PRODUCTS_IMPORTED', 'extshops_products_imported');  
  define('TABLE_EXTSHOPS_PRODUCTSEXPORT_CONFIG', 'extshops_productsexport_config');  
  define('TABLE_FIRSTNEWS_GROUPS', 'fn_groups');
  define('TABLE_FIRSTNEWS_ENTRIES', 'fn_entries');
  define('TABLE_NEWSLETTER_ENTRIES', 'newsletter_entries');
  define('TABLE_NEWSLETTER_GROUPS', 'newsletter_groups');
  define('TABLE_NEWSLETTER_DRAGGEDOUT', 'newsletter_draggedout');
  define('TABLE_NEWSLETTER_SUCCESS', 'newsletter_success');
  define('TABLE_NEWSLETTER_CAMPAIGNS', 'newsletter_campaigns');
  define('TABLE_NEWSLETTER_CAMPAIGNS_PRODUCTS', 'newsletter_campaigns_products');
  define('TABLE_NEWSLETTER_CAMPAIGNS_PRODUCTS_BLOCKS', 'newsletter_campaigns_products_blocks');
  define('TABLE_TRADESHOW_MARKETING_ENTRIES', 'tradeshow_marketing_entries');
  define('TABLE_TRADESHOW_MARKETING_TRADESHOWS', 'tradeshow_marketing_tradeshows');
  define('TABLE_TRADESHOW_MARKETING_OFFERS', 'tradeshow_marketing_offers');
  define('TABLE_TRADESHOW_MARKETING_OFFERS_PRICES', 'tradeshow_marketing_offers_prices');
  define('TABLE_TRADESHOW_MARKETING_ENTRIES_OFFERS', 'tradeshow_marketing_entries_offers');
  define('TABLE_TRADESHOW_MARKETING_TEMPLATES', 'tradeshow_marketing_templates');
  define('TABLE_TRADESHOW_MARKETING_CONFIG', 'tradeshow_marketing_config');
  define('TABLE_TRADESHOW_MARKETING_MASSFORWARDING', 'tradeshow_marketing_massforwarding');
  define('TABLE_KKO_DOWNLOAD', 'kko_download');
  define('TABLE_KKO_DOWNLOAD_BACKLINKS', 'kko_download_backlinks');  
  define('TABLE_CLICK_CAMPAIGNS', 'click_campaigns');  
  define('TABLE_CLICK_SUCCESS', 'click_success');
  define('TABLE_CLICK_SUCCESS', 'click_success');
  define('TABLE_SEO_TRAFFICKEYWORDS', 'seo_traffickeywords');
  define('TABLE_SEO_TRAFFICKEYWORDS_TASKS', 'seo_traffickeywords_tasks');
  define('TABLE_SEO_REPORTS', 'seo_reports');
  define('TABLE_SEO_LINKBUILDING_URLS', 'seo_linkbuilding_urls');
  define('TABLE_SEO_LINKBUILDING_LINKS', 'seo_linkbuilding_links');
  define('TABLE_SEO_LINKBUILDING_TODO', 'seo_linkbuilding_todo');
  define('TABLE_SEO_LINKBUILDING_USERS', 'seo_linkbuilding_users');
  define('TABLE_SEO_LINKBUILDING_MESSAGES', 'seo_linkbuilding_messages');
  define('TABLE_SEO_LINKBUILDING_RANKS', 'seo_linkbuilding_ranks');
?>