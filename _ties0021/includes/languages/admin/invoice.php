<?php
/*
  $Id: invoice.php,v 1.1 2002/06/11 18:17:59 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TABLE_HEADING_COMMENTS', 'Comments');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Price (ex)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Price (inc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total (ex)');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (inc)');

define('ENTRY_SOLD_TO', 'SOLD TO:');
define('ENTRY_SHIP_TO', 'SHIP TO:');
define('ENTRY_PAYMENT_METHOD', 'Payment Method:');
define('ENTRY_SUB_TOTAL', 'Sub-Total:');
define('ENTRY_TAX', 'Tax:');
define('ENTRY_SHIPPING', 'Shipping:');
define('ENTRY_TOTAL', 'Total:');
define('ENTRY_INVOICE_COSTUMER_EMAIL', 'eMail:'); 
define('ENTRY_INVOICE_COSTUMER_FON', 'Tel:'); 
define('ENTRY_INVOICE_COSTUMER_ID', 'customer-ID:'); 
define('ENTRY_INVOICE_DATE_PURCHASED', 'Date:'); 
define('ENTRY_INVOICE_ORDER_ID', 'Order ID:'); 
define('ENTRY_INVOICE_CUSTOMER_ACCOUNT', 'Customer Account'); 
define('ENTRY_INVOICE_BANKTRANSFER', '
<br>Bank-Account:<br>
Deutsche Bank Hannover<br>
Kto: 021107800<br>
BLZ: 25070024<br>
IBAN: DE30250700240021107800<br>
BIC bzw. Swiftcode: DEUTDEDBHAN');
?>
