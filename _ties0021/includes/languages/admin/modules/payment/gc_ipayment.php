<?php
/*
  $Id: ipayment.php,v 1.7 2003/07/11 09:04:23 jan0815 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_TITLE', 'Credit Card (<a href="http://www.ipayment.de/" target="_blank">ipayment</a>)');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_DESCRIPTION', 'With ipayment we accept the following payment methods:<br />
'.tep_image(DIR_WS_IMAGES.'layout/ipaymentlogos.jpg').'<br />
<br />');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CONFIRMATION', 'With ipayment we accept the following payment methods:<br />
'.tep_image(DIR_WS_IMAGES.'layout/ipaymentlogos.jpg').'<br />
<br />
After pressing order you will be redirected to our ipayment system.');
  define('GC_IPAYMENT_ERROR_HEADING', 'Folgender Fehler wurde von iPayment w&auml;hrend des Prozesses gemeldet:');
  define('GC_IPAYMENT_ERROR_MESSAGE', 'Bitte kontrollieren Sie die Daten Ihrer Kreditkarte!');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CREDIT_CARD_OWNER', 'Kreditkarteninhaber');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CREDIT_CARD_NUMBER', 'Kreditkarten-Nr.:');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CREDIT_CARD_EXPIRES', 'G&uuml;ltig bis:');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CREDIT_CARD_CHECKNUMBER', 'Karten-Pr&uuml;fnummer');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_CREDIT_CARD_CHECKNUMBER_LOCATION', '(Auf der Kartenr&uuml;ckseite im Unterschriftsfeld)');

  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_JS_CC_OWNER', '* Der Name des Kreditkarteninhabers mss mindestens aus  ' . CC_OWNER_MIN_LENGTH . ' Zeichen bestehen.\n');
  define('MODULE_PAYMENT_GC_IPAYMENT_TEXT_JS_CC_NUMBER', '* Die \'Kreditkarten-Nr.\' muss mindestens aus ' . CC_NUMBER_MIN_LENGTH . ' Zahlen bestehen.\n');
?>
