<?php
/*
  $Id: ot_shipping.php,v 1.5 2003/07/11 09:04:23 jan0815 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'shipping costs');
  define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'shipping costs for one order');

  define('FREE_SHIPPING_TITLE', 'free of shipping costs');
  define('FREE_SHIPPING_DESCRIPTION', 'free of shipping costs for an order value above %s');
?>
