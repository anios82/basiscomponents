<?php
/*
  $Id: ot_gv.php,v 1.0 2002/04/03 23:09:49 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_COUPON_TITLE', 'Coupon');
  define('MODULE_COUPON_DESCRIPTION', 'Coupon-codes');
  define('DISC_SHIPPING_NOT_INCLUDED', ' [without shipping]');
  define('DISC_TAX_NOT_INCLUDED', ' [without taxes]');
?>