<?php
/*
  $Id: table.php,v 1.7 2003/07/11 09:04:23 jan0815 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_RUSH_TEXT_TITLE', 'Rush shipping');
define('MODULE_SHIPPING_RUSH_TEXT_DESCRIPTION', 'Rush Shipping');
define('MODULE_SHIPPING_RUSH_TEXT_WAY', 'Next Day Guaranteed - only continental US');
define('MODULE_SHIPPING_RUSH_TEXT_WEIGHT', 'Weight');
define('MODULE_SHIPPING_RUSH_TEXT_AMOUNT', 'Quantity');
?>
