<?php
/*
  $Id: table.php,v 1.7 2003/07/11 09:04:23 jan0815 Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_SHIPPING_TABLE2_TEXT_TITLE', 'Shipping costs');
define('MODULE_SHIPPING_TABLE2_TEXT_DESCRIPTION', 'Shipping costs');
define('MODULE_SHIPPING_TABLE2_TEXT_WAY', 'Free shipping for orders above $100.00');
define('MODULE_SHIPPING_TABLE2_TEXT_WEIGHT', 'Weight');
define('MODULE_SHIPPING_TABLE2_TEXT_AMOUNT', 'Quantity');
?>
