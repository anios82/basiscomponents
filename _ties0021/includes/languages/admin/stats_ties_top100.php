<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('LINK_PRINTVERSION', 'Druckversion');

define('TABLE_HEADING_RANK', 'Rang');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Art.-Nr.');
define('TABLE_HEADING_PICTURE', 'Bild');
define('TABLE_HEADING_NUMBERSELLINGS', 'Anzahl Verk&auml;ufe');
define('TABLE_HEADING_PRICEBUY', 'EK');
define('TABLE_HEADING_PRICESELL', 'VK');
define('TABLE_HEADING_PRICESELL_NOTAX', 'VK o. Mwst');
define('TABLE_HEADING_MARGE', 'Marge');
define('TABLE_HEADING_POINTS', 'Punkte');
define('TABLE_HEADING_RANKUSA', 'Rang USA');
?>