<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('TABLE_HEADING_SHOP', 'Shop');
define('TABLE_HEADING_CONTENTID', 'Content-ID');
define('TABLE_HEADING_CONTENTNAME', 'Content-Name');
define('TABLE_HEADING_CONTENTURL', 'Content-Url');
define('TABLE_HEADING_VISITORSGOOGLE', 'Besucher-Google');
define('TABLE_HEADING_VISITORSYAHOO', 'Besucher-Yahoo');
define('TABLE_HEADING_VISITORSBING', 'Besucher-Bing');
define('TABLE_HEADING_VISITORSALTAVISTA', 'Besucher-Altavista');

?>