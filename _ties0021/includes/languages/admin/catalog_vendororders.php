<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('TABLE_HEADING_ORDERID', 'Bestell-ID');
define('TABLE_HEADING_CREATOR', 'Erfasser');
define('TABLE_HEADING_CREATED', 'Erfassungs-Datum');
define('TABLE_HEADING_VENDOR', 'Hersteller');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_STATUSMODIFIED', 'Letzte Status-&Auml;nderung');
define('TABLE_HEADING_ACTIONS', 'Aktionen');
define('TABLE_HEADING_NUMBERS', 'Anz. Produkte / Anz. Variationen / Summe Artikel');
define('TABLE_HEADING_ORDERDATE', 'Bestell-Datum');
define('TABLE_HEADING_DELIVERYDATE_PLANNED', 'geplantes Liefer-Datum');

/* module new.inc.php */
define('TABLE_HEADING_EDIT_NEW_INTRO', 'Folgende Felder bitte ausf&uuml;llen:');
define('TABLE_HEADING_EDIT_NEW_ADDPRODUCTS', 'Artikel hinzuf&uuml;gen / editieren:');

/* module choose_product.inc.php */
define('CHOOSE_PRODUCT_ALL_CATEGORIES', 'Alle Kategorien anzeigen');
define('CHOOSE_PRODUCT_SELECT_PRODUCT', 'Produkt ausw&auml;hlen');
define('CHOOSE_PRODUCT_PRODUCT_NOT_FOUND', 'Keine Produkte gefunden');
define('CHOOSE_PRODUCT_BUTTON_SELECT_PRODUCT', 'Produkt ausw&auml;hlen');
define('CHOOSE_PRODUCT_BUTTON_ADD_PRODUCT', 'Produkt hinzuf&uuml;gen');
define('CHOOSE_PRODUCT_STEP_1', 'Kategorie ausw&auml;hlen:');
define('CHOOSE_PRODUCT_PRODUCT_SEARCH', 'Suchen:');
define('CHOOSE_PRODUCT_SELECT_CATEGORY', 'Ausw&auml;hlen');
define('CHOOSE_PRODUCT_STEP_2', 'Suchergebnisse:');
define('CHOOSE_PRODUCT_ADDING_TITLE', '(1) Bestehende Produkt(e) zu Bestellung #%s hinzuf&uuml;gen');
define('CHOOSE_PRODUCT_ADDING_TITLE_NEW', '(2) Ein neu-entworfenes Pr. zu Bestellung #%s hinzuf&uuml;gen');


?>