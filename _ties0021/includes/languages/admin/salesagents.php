<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('TABLE_HEADING_ID', 'Vertreter-Nr.');
define('TABLE_HEADING_FIRSTNAME', 'Vorname');
define('TABLE_HEADING_LASTNAME', 'Nachname');
define('TABLE_HEADING_CREATED', 'Angelegt wann');
define('TABLE_HEADING_EMAIL', 'Email');
define('TABLE_HEADING_VALIDATED', 'Account validiert');
define('TABLE_HEADING_LOCKED', 'Account gesperrt');
define('TABLE_HEADING_NUMCUSTOMERS', 'Anzahl Kunden');
define('TABLE_HEADING_ACCOUNTING_TURNOVER', 'Umsatz (Mwst.-frei)');
define('TABLE_HEADING_ACCOUNTING_COMMISSION', 'Provision');
define('TABLE_HEADING_ACCOUNTING_OWNCOMMISSION', 'Eigen-Provision');
define('TABLE_HEADING_ACCOUNTING_HVCOMMISSION', 'HV-Provision');
define('TABLE_HEADING_ACCOUNTING_TAX', 'Mwst. 19%');
define('TABLE_HEADING_ACCOUNTING_COMMISSION_GROSS', 'Provision Brutto');
define('LINK_PRINTVERSION', 'Druckversion');
?>