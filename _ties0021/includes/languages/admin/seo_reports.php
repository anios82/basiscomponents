<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('TABLE_HEADING_SHOP', 'Shop');
define('TABLE_HEADING_REPORT_SCREENSHOTS', 'Report-Screenshots');
?>