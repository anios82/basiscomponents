<?php
/*
  $Id: modules.php,v 1.9 2003/05/28 14:07:38 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
/*
define('HEADING_TITLE_MODULES_PAYMENT', 'Zahlungsweisen');
define('HEADING_TITLE_MODULES_SHIPPING', 'Versandarten');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Modul Zusammenfassung einer Bestellung');
*/

define('TABLE_HEADING_NAME', 'Projekt-Name');
define('TABLE_HEADING_AUTHOR', 'Projekt-Verantwortlicher');
define('TABLE_HEADING_TASK', 'Aufgabe');
define('TABLE_HEADING_TASKCREATED', 'Angelegt wann');
define('TABLE_HEADING_TASKMODIFIED', 'Letzte Aktualisierung');
define('TABLE_HEADING_TASKAUTHOR', 'Verantwortlich');
define('TABLE_HEADING_TASKPRIORITY', 'Priorit&auml;t');
define('TABLE_HEADING_TASKSTATUS', 'Status');
?>