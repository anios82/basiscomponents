<?php
/*
 Basis-Shop V1.1
*/
// Dateinamen zwingend erforderlich
// werden durch Basisshop aktualisiert
// alle Texte in der jeweiligen Sprache sind direkt in der Datei includes/language/domainkuerzel.php abgelegt
// alle shopspezifischen Variablen in der includes/configure.php
// �nderungen des Dateinamens nur in .htaccess erlaubt

define('FILENAME_ALL_PRODUCTS', 'all_products.php');
define('FILENAME_ALL_PRODUCTS_OOS','all_products_out_of_stock.php');
define('FILENAME_CHECKOUT', 'checkout.php');
define('FILENAME_CHECKOUT_CONFIRMATION', 'checkout_confirmation.php');
define('FILENAME_CHECKOUT_PAYMENT', 'checkout_payment.php');
define('FILENAME_CHECKOUT_PAYPAL', 'checkout_paypal.php');
define('FILENAME_CHECKOUT_PAYPAL_ERROR', 'checkout_paypal_error.php');
define('FILENAME_CHECKOUT_SOFORTUE_ERROR', 'checkout_sofortue_error.php');
define('FILENAME_CHECKOUT_PROCESS', 'checkout_process.php');
define('FILENAME_CHECKOUT_SHIPPING', 'checkout_shipping.php');
define('FILENAME_CHECKOUT_SUCCESS', 'checkout_success.php');
define('FILENAME_CONTACT_US', 'contact_us.php');
define('FILENAME_COOKIE_USAGE', 'cookie_usage.php');
define('FILENAME_CREATE_ACCOUNT', 'create_account.php');
define('FILENAME_INDEX_COLUMN_LIST', 'index_column_list.php');
define('FILENAME_MENS_FASHION_AZ', 'mens-fashion-a-z.php');
define('FILENAME_PRODUCT_INFO', 'product_info.php');
define('FILENAME_PRODUCT_INFO_MOBILE', 'product_info_mobile.php');
define('FILENAME_PRODUCT_SEARCH', 'product_search.php');
define('FILENAME_SHOPPING_CART', 'shopping_cart.php');
define('FILENAME_SSL_CHECK', 'ssl_check.php');

//in anderen Ordnern
define('FILENAME_PRODUCT_LISTING', 'product_listing.php');
define('FILENAME_NEWSLETTER', 'newsletter/index.php');

// Dateinamen optional
// werden durch Basisshop aktualisiert
// Hier bestehen eventuel Referenszen/Variablenaufrufe innerhalb der OsCommerce? Funktionen
// alle Texte in der jeweiligen Sprache sind direkt in der Datei includes/language/domainkuerzel.php abgelegt
define('FILENAME_ACCOUNT', 'account.php');
define('FILENAME_CHECKOUT_PAYMENT_ADDRESS', 'checkout_payment_address.php');
define('FILENAME_CHECKOUT_SHIPPING_ADDRESS', 'checkout_shipping_address.php');
define('FILENAME_ACCOUNT_EDIT', 'account_edit.php');
define('FILENAME_ACCOUNT_HISTORY', 'account_history.php');
define('FILENAME_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
define('FILENAME_ACCOUNT_NEWSLETTERS', 'account_newsletters.php');
define('FILENAME_ACCOUNT_NOTIFICATIONS', 'account_notifications.php');
define('FILENAME_ACCOUNT_PASSWORD', 'account_password.php');
define('FILENAME_ADVANCED_SEARCH', 'advanced_search.php');
define('FILENAME_ADVANCED_SEARCH_RESULT', 'advanced_search_result.php');
define('FILENAME_ALSO_PURCHASED_PRODUCTS', 'also_purchased_products.php');
define('FILENAME_CREATE_ACCOUNT_SUCCESS', 'create_account_success.php');
define('FILENAME_LOGIN', 'login.php');
define('FILENAME_LOGOFF', 'logoff.php');
define('FILENAME_PASSWORD_FORGOTTEN', 'password_forgotten.php');
define('FILENAME_PRODUCT_REVIEWS', 'product_reviews.php');
define('FILENAME_DOWNLOAD', 'download.php');
define('FILENAME_PRODUCTS_NEW', 'products_new.php');
define('FILENAME_REDIRECT', 'redirect.php');
define('FILENAME_SPECIALS', 'specials.php');

// Dateinamen optional
// werden nicht durch Basisshop aktualisiert
// individuelle URLs m�glich
// Definitionen l�schen / Vorlagen in configure.php
// alle Texte in der jeweiligen Sprache sind direkt in den Dateien abgelegt
define('FILENAME_DEFINE_MAINPAGE', 'mainpage.php'); // MaxiDVD? Added WYSIWYG HTML Area
define('FILENAME_INFO_SHOPPING_CART', 'info_shopping_cart.php');
define('FILENAME_JOIN_CLUB1', 'join_club1.php');
define('FILENAME_NEW_PRODUCTS', 'new_products.php');
define('FILENAME_MESSEN', 'messetermine.php');
define('FILENAME_POPUP_IMAGE', 'popup_image.php');
define('FILENAME_POPUP_SEARCH_HELP', 'popup_search_help.php');
define('FILENAME_PRODUCT_REVIEWS_INFO', 'product_reviews_info.php');
define('FILENAME_PRODUCT_REVIEWS_WRITE', 'product_reviews_write.php');
define('FILENAME_REVIEWS', 'reviews.php');
define('FILENAME_TELL_A_FRIEND', 'tell_a_friend.php');
define('FILENAME_UPCOMING_PRODUCTS', 'upcoming_products.php');
define('FILENAME_PWA_PWA_LOGIN', 'login_pwa.php');
define('FILENAME_PWA_ACC_LOGIN', 'login_acc.php');
define('FILENAME_CHECKOUT', 'Order_Info.php');
define('FILENAME_IMPRESSUM_INC', 'impressum.inc.php');
define('FILENAME_FIRMENKRAWATTEN_INC', 'firmenkrawatten.inc.php');
define('FILENAME_SHIPPING_INC', 'shipping.inc.php');
define('FILENAME_CONDITIONS_INC', 'conditions.inc.php');
define('FILENAME_CONDITIONS_PRINT', 'conditions_print.php');
define('FILENAME_PRIVACY_INC', 'privacy.inc.php');
define('FILENAME_HISTORY', 'krawatten-geschichte.php');
define('FILENAME_HISTORY_INC', 'krawatten-geschichte.inc.php');
define('FILENAME_MATERIAL', 'krawatten-material-seide.php');
define('FILENAME_MATERIAL_INC', 'krawatten-material-seide.inc.php');
define('FILENAME_VERARBEITUNG', 'krawatten-verarbeitung.php');
define('FILENAME_VERARBEITUNG_INC', 'krawatten-verarbeitung.inc.php');
define('FILENAME_KNOTEN', 'krawatten-knoten.php');
define('FILENAME_KNOTEN_INC', 'krawatten-knoten.inc.php');
define('FILENAME_KOMBINATIONEN', 'krawatten-kombinieren.php');
define('FILENAME_KOMBINATIONEN_INC', 'krawatten-kombinieren.inc.php');
define('FILENAME_AUSSAGEN', 'krawatten-code-vestimentaire.php');
define('FILENAME_AUSSAGEN_INC', 'krawatten-dresscode.inc.php');
define('FILENAME_PFLEGE', 'krawatten-pflege.php');
define('FILENAME_PFLEGE_INC', 'krawatten-pflege.inc.php');
define('FILENAME_HEMDEN_KAUF', 'hemden-kauf-oberhemden.php');
define('FILENAME_HEMDEN_MODE', 'hemden-mode-businesshemden.php');
define('FILENAME_HEMDEN_PFLEGE', 'hemden-pflege.php');
define('FILENAME_CHECKOUT_CONDITIONS', 'checkout_conditions.htm'); // These are the conditions displayed at the checkout payment page
define('FILENAME_CONDITIONS_DOWNLOAD', 'conditions.pdf'); // These are the conditions which can be download by the customer at the checkout payment page
define('FILENAME_FIRMENKRAWATTEN', 'firmenkrawatten.php'); 

define('FILENAME_SITEMAP', 'sitemap.php');
define('FILENAME_CHECKOUT_INPUTMASK', 'checkout.php?jump_to_form=true');


?>