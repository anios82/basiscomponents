<?php
/*
  $Id: html_output.php,v 1.56 2003/07/09 01:15:48 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

////
// The HTML href link wrapp er function
  function tep_href_link($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true) {

      global $seo_urls;
      if (!is_object($seo_urls)) {
          if (!class_exists('SEO_URL')) {
              include_once(DIR_WS_CLASSES . 'seo.class.php');
          }
          global $languages_id;
          $seo_urls = new SEO_URL($languages_id);
      }
	  
	  #echo $add_session_id. " <---  Added SessionID \n";
	  
	  $link = $seo_urls->href_link($page, $parameters, $connection, $add_session_id);
	  
	  
	  #echo $link." <---  LinkSEO \n";
	  return $link;
	  
/*	  
    global $request_type, $session_started, $SID;
//================================================================
//SID Killer v1.2 (Feb 20th 2003) by Gurkcity 11.08.2006
	global $kill_sid, $_GET;
//================================================================	

    if (!tep_not_null($page)) {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><b>Error!</b></font><br /><br /><b>Unable to determine the page link!<br /><br />');
    }

    if ($connection == 'NONSSL') {
      $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == true) {
        $link = HTTPS_SERVER . DIR_WS_HTTPS_CATALOG;
      } else {
        $link = HTTP_SERVER . DIR_WS_HTTP_CATALOG;
      }
    } else {
      die('</td></tr></table></td></tr></table><br /><br /><font color="#ff0000"><b>Error!</b></font><br /><br /><b>Unable to determine connection method on a link!<br /><br />Known methods: NONSSL SSL</b><br /><br />');
    }
//================================================================
//SID Killer v1.2 (Feb 20th 2003)
//ge�ndert by Gurkcity	08.08.2006 -> nur wenn Sprache gesetzt ist, diese auch setzen
//r�ckg�ngig gemacht, da sid nicht �bertragen wird

if ($_GET['language'] && $_SESSION['kill_sid']) {
      $l = ereg('[&\?/]?language[=/][a-z][a-z]', $parameters, $m);
      if ($l) {
        $parameters = ereg_replace("[&\?/]?language[=/][a-z][a-z]", "", $parameters);
        $_GET['language'] = substr($m[0],-2);
      }
      if (tep_not_null($parameters)) {
        $parameters .= "&language=" . $_GET['language'];
      } else {
        $parameters = "language=" . $_GET['language'];
      }
    }
   
//================================================================

//================================================================
//Startlink index.php auf Domainnamen leiten
//#by Gurkcity 14.09.2006
if($page == 'index.php' && !tep_not_null($parameters))
{
	$page = '';
}

    if (tep_not_null($parameters)) {
      $link .= $page . '?' . tep_output_string($parameters);
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

// Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
    if ( ($add_session_id == true) && ($session_started == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
      if (tep_not_null($SID)) {
        $_sid = $SID;
      } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
        if (HTTP_COOKIE_DOMAIN != HTTPS_COOKIE_DOMAIN) {
          $_sid = tep_session_name() . '=' . tep_session_id();
        }
      }
    }

    if ( (SEARCH_ENGINE_FRIENDLY_URLS == 'true') && ($search_engine_safe == true) ) {
      while (strstr($link, '&&')) $link = str_replace('&&', '&', $link);

      $link = str_replace('?', '/', $link);
      $link = str_replace('&', '/', $link);
      $link = str_replace('=', '/', $link);

      $separator = '?';
    }
//================================================================
//SID Killer v1.2 (Feb 20th 2003)
/*
    if (isset($_sid)) {
      $link .= $separator . $_sid;
    }
	

	if (isset($_sid) && ( !$_SESSION['kill_sid'] ) ) {
      $link .= $separator . $_sid;
    }
//================================================================

    return $link;
*/
  }

////
// The HTML image wrapper function
function tep_image($src, $alt = '', $width = '', $height = '', $parameters = '') {

	if ( (empty($src) || ($src == DIR_WS_IMAGES)) && (IMAGE_REQUIRED == 'false') ) {
		return false;
	}

	// alt is added to the img tag even if it is null to prevent browsers from outputting
	// the image filename as default
	$image = '<img src="' . tep_output_string($src) . '" border="0" alt="' . tep_output_string(str_replace(' & ', ' und ',$alt)) . '"';
	
	if (tep_not_null($alt)) {
		$image .= ' title=" ' . tep_output_string(str_replace(' & ', ' und ',$alt)) . ' "';
	}

	if (tep_not_null($width) && tep_not_null($height)) {
		$image .= ' width="' . tep_output_string($width) . '" height="' . tep_output_string($height) . '"';
	}

	if (tep_not_null($parameters))
		$image .= ' ' . $parameters;

	$image .= ' />';

	return $image;
}

 // test
 function checkLocalOrExternalImage($image_string, $id, $aResizeFromSize){
		
		//echo '<br>'.DIR_FS_CATALOG.$image_string.'<br>'; ttest

     if(!file_exists(DIR_FS_CATALOG.$image_string)  ||  filesize(DIR_FS_CATALOG.$image_string) < 2000
         || strtotime('-1 day') > filemtime(DIR_FS_CATALOG.$image_string)) {

         $ftpResult = gc_image_live_syncro($image_string, SHOP_SHORTNAME, $id, $aResizeFromSize);
			
			if($ftpResult['found'] === true){
				$image_string = $ftpResult['img']['name'];
			}
			else {
                if(!file_exists(DIR_FS_CATALOG.$image_string)){
                    return(array('image_string'=>NULL, 'result'=>false));
                }
			}
		}

		return(array('image_string'=>$image_string, 'result'=>true));
 }
 
 function getBlankoImage($id = NULL, $aTargetImgSize = NULL){
 	
	  global $aExtraImageCategories;
	  
	  $image_string = 'images/product_blank_'.$aTargetImgSize.'.jpg';

	  if($id){
  		  	$res = tep_db_query('SELECT categories_id FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' WHERE products_id = '.$id);
		  	while(list($c) = tep_db_fetch_row($res)):
				$cats[] = $c;
			endwhile;
			
			if(array_intersect($cats, $aExtraImageCategories)){
				$isSpecialSize = true;
			}
	  }
	  elseif(in_array($_GET['cPath'], $aExtraImageCategories)){
	    	$isSpecialSize = true;
	  }
	  
	  if($targetImgSize == 'sm'){
	  		
			$width = SMALL_IMAGE_WIDTH;
			if($isSpecialSize){
				$height = EXTRA_SMALL_IMAGE_HEIGHT;
			}
			else {
				$height = SMALL_IMAGE_HEIGHT;
			}
	  }
	  else {
	  	
	  		$width = MEDIUM_IMAGE_WIDTH;
			if($isSpecialSize){
				$height = EXTRA_MEDIUM_IMAGE_HEIGHT;
			}
			else {
				$height = MEDIUM_IMAGE_HEIGHT;
			}
	  }
	  
	  return($image_string);
 }
 

// The HTML image wrapper function
function tep_product_image($model, $size='sm', $alt='', $width = '', $height = '', $parameters = '', $id='', $aResizeFromSize = NULL) {	

	if (empty($model) && (IMAGE_REQUIRED == 'false') ) 
		return false;

	if(in_array(substr($model, -1), array('L', 'K', 'C')) and is_numeric(substr($model, -2, 1))){
		$image_string = 'images/ties/' . substr($model, 0, -1) . '_'.$size.'_'.SHOP_SHORTNAME.'.jpg';		
	}
	else {
		$image_string = 'images/ties/' . $model . '_'.$size.'_'.SHOP_SHORTNAME.'.jpg';		
	}
	
	if(empty($id)){
		$res = tep_db_query('SELECT products_id FROM '.TABLE_PRODUCTS.' WHERE products_model = \''.$model.'\'');
	    list($id) = tep_db_fetch_row($res); 
	}
	
	$resultImage = checkLocalOrExternalImage($image_string, $id, $aResizeFromSize);  // no [MODEL] on local disk AND external server
	
	if($resultImage['result'] === false){		
		
		if(in_array(substr($model, -1), array('L', 'S', 'K', 'C')) and is_numeric(substr($model, -2, 1))){
			
			$image_string = 'images/ties/' . substr($model, 0, -1) . '_'.$size.'_'.SHOP_SHORTNAME.'.jpg';	
			$resultImage = checkLocalOrExternalImage($image_string, $id, $aResizeFromSize);	
		    $image_string = ($resultImage['result'] === false) ? $image_string = getBlankoImage($id, substr($image_string, -10, 2)) : $resultImage['image_string'];
		}
		else {
				
			$image_string = getBlankoImage($id, substr($image_string, -10, 2));
		}
	}
	else {
		$image_string = $resultImage['image_string'];
	}
	
	$image = '<img src="'.(($aResizeFromSize) ? str_replace($aResizeFromSize, $size, $image_string) : $image_string).'" border="0" alt="' . tep_output_string($alt) . '"';
	
	if (tep_not_null($alt))
		$image .= ' title=" ' . tep_output_string($alt) . ' "';
	
	if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true') && (empty($width) || empty($height)) ) {
	
		if ($image_size = @getimagesize($image_string)) {
	
			if (empty($width) && tep_not_null($height)) {
				$ratio = $height / $image_size[1];
				$width = floor($image_size[0] * $ratio);
			}
			elseif (tep_not_null($width) && empty($height)) {
				$ratio = $width / $image_size[0];
				$height = floor($image_size[1] * $ratio);
			}
			elseif (empty($width) && empty($height)) {
				$width = floor($image_size[0]);
				$height = floor($image_size[1]);
			}
		}
		elseif (IMAGE_REQUIRED == 'false') {
			return false;
		}
	}
	
	if (tep_not_null($width) && tep_not_null($height))
		$image .= ' width="' . tep_output_string($width) . '" height="' . tep_output_string($height) . '"';
	
	if (tep_not_null($parameters)) $image .= ' ' . $parameters;
	
	$image .= ' />';
	
	return $image;
	
}

////
// The Image Resize Funktion
// by http://www.sitepoint.com/article/image-resizing-php
// modified by Gurkcity 16.06.2006 f�r automatische Angebotsverwaltung Startseite
function imageResize($width, $height, $target) {

//takes the larger size of the width and height and applies the  
//formula accordingly...this is so this script will work  
//dynamically with any size image

if ($width > $height) {
$percentage = ($target / $width);
} else {
$percentage = ($target / $height);
}

//gets the new value and applies the percentage, then rounds the value
$width = round($width * $percentage);
$height = round($height * $percentage);

//returns the new sizes in html image tag format...this is so you
//can plug this function inside an image tag and just get the

return "width=\"$width\" height=\"$height\"";

}



////
// The HTML form submit button wrapper function
// Outputs a button in the selected language
  function tep_image_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="image" src="' . tep_output_string(DIR_WS_IMAGES.'layout/buttons/' . $image) . '" alt="' . tep_output_string($alt) . '"';

    if (tep_not_null($alt)) $image_submit .= ' title=" ' . tep_output_string($alt) . ' "';

    if (tep_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= ' />';

    return $image_submit;
  }

////
// Output a function button in the selected language
  function tep_image_button($image, $alt = '', $parameters = '') {
    global $language;

    return tep_image(DIR_WS_IMAGES.'layout/buttons/' . $image, $alt, '', '', $parameters);
  }

////
// Output a separator either through whitespace, or with an image
  function tep_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return tep_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

////
// Output a form
  function tep_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . tep_output_string($name) . '" action="' . tep_output_string($action) . '" method="' . tep_output_string($method) . '"';

    if (tep_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    return $form;
  }

////
// Output a form input field
  function tep_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Output a form password field
  function tep_draw_password_field($name, $value = '', $parameters = 'maxlength="40"') {
    return tep_draw_input_field($name, $value, $parameters, 'password', false);
  }

////
// Output a selection field - alias function for tep_draw_checkbox_field() and tep_draw_radio_field()
  function tep_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input type="' . tep_output_string($type) . '" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) $selection .= ' value="' . tep_output_string($value) . '"';

    if ( ($checked == true) || ( isset($_SESSION[$name]) && is_string($_SESSION[$name]) && ( ($_SESSION[$name] == 'on') || (isset($value) && (stripslashes($_SESSION[$name]) == $value)) ) ) ) {
      $selection .= ' checked="checked"';
    }
	elseif (  isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ( ($GLOBALS[$name] == 'on') || (isset($value) && (stripslashes($GLOBALS[$name]) == $value)) ) ) {
      $selection .= ' checked="checked"';
    }
	
    if (tep_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= ' />';

    return $selection;
  }
  
////
// Output a form checkbox field
  function tep_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

////
// Output a form radio field
  function tep_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return tep_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }

////
// Output a form textarea field
  function tep_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    $field = '<textarea name="' . tep_output_string($name) . '" wrap="' . tep_output_string($wrap) . '" cols="' . tep_output_string($width) . '" rows="' . tep_output_string($height) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= stripslashes($GLOBALS[$name]);
    } elseif (tep_not_null($text)) {
      $field .= $text;
    }

    $field .= '</textarea>';

    return $field;
  }

////
// Output a form hidden field
  function tep_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . tep_output_string($name) . '"';

    if (tep_not_null($value)) {
      $field .= ' value="' . tep_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name])) {
      $field .= ' value="' . tep_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= ' />';

    return $field;
  }

////
// Hide form elements
  function tep_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && tep_not_null($SID)) {
      return tep_draw_hidden_field(tep_session_name(), tep_session_id());
    }
  }

////
// Output a form pull down menu
  function tep_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false) {
    $field = '<select name="' . tep_output_string($name) . '"';

    if (tep_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);

    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . tep_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' SELECTED';
      }

      $field .= '>' . tep_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }

////
// Creates a pull-down list of countries
  function tep_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
	
	$countries_string = '<select name="'.$name.'"';
	
	if (tep_not_null($parameters)) $countries_string .= ' ' . $parameters;

    $countries_string .= '>';
	
	
    $countries = tep_get_countries('', '', '1');

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
		if($countries[$i]['countries_id']==$selected)
			$countries_string .= '<option value="'.$countries[$i]['countries_id'].'" selected="selected">'.$countries[$i]['countries_name'].'</option>';
		else
			$countries_string .= '<option value="'.$countries[$i]['countries_id'].'">'.$countries[$i]['countries_name'].'</option>';
    }
	
	
	$countries_string .='<optgroup label="'.COUNTRIES_INSIDE_EU.'">';
	$countries = tep_get_countries('', '', '2');
	
	if(defined('COUNTRIES_EU_NONALLOW')){

		$conf_countries_nonallow_EU = explode(',', COUNTRIES_EU_NONALLOW);
		foreach($countries as $c):
			
			if(!in_array($c['countries_id'], $conf_countries_nonallow_EU)){
				$countries_Eu[] = $c;
			}
		endforeach;
	}
	else {
		$countries_Eu = &$countries;
	}

    for ($i=0, $n=sizeof($countries_Eu); $i<$n; $i++) {
     	if($countries_Eu[$i]['countries_id']==$selected)
			$countries_string .= '<option value="'.$countries_Eu[$i]['countries_id'].'" selected="selected">'.$countries_Eu[$i]['countries_name'].'</option>';
		else
			$countries_string .= '<option value="'.$countries_Eu[$i]['countries_id'].'">'.$countries_Eu[$i]['countries_name'].'</option>';
    }

	$countries_string .='</optgroup>';
	
	$countries_string .='<optgroup label="'.COUNTRIES_OUTSIDE_EU.'">';
	$countries = tep_get_countries('', '', '3');
		
	if(defined('COUNTRIES_NONEU_ALLOW')){
	
		$conf_countries_allow_nonEU = explode(',', COUNTRIES_NONEU_ALLOW);
		foreach($countries as $c):
			
			if(in_array($c['countries_id'], $conf_countries_allow_nonEU)){
				$countries_nonEu[] = $c;
			}
		endforeach;
	}
	else {
		$countries_nonEu = &$countries;
	}
	
	
    for ($i=0, $n=sizeof($countries_nonEu); $i<$n; $i++) {
		if($countries_nonEu[$i]['countries_id']==$selected)
			$countries_string .= '<option value="'.$countries_nonEu[$i]['countries_id'].'" selected="selected">'.$countries_nonEu[$i]['countries_name'].'</option>';
		else
			$countries_string .= '<option value="'.$countries_nonEu[$i]['countries_id'].'">'.$countries_nonEu[$i]['countries_name'].'</option>';
	}
	
	$countries_string .='</optgroup>';
	
	
	$countries_string .='</select>';
	
	return $countries_string;
	
   //return tep_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }



?>