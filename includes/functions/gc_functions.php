<?php

if (!defined('DIR_WS_MODULES')) {
	define('DIR_WS_MODULES', '/srv/www/basis_components/includes/modules/');
}

require_once DIR_WS_MODULES."Zebra_Image.php";

/************************************************
*
* Funktionen f�r die Men�gestaltung
* 
************************************************/
function ukatLinkAbsolut($cPath, $path, $linktitle)
{
		
	global $section_2;
	
	// fk_cpath -> f�r Firmenkrawatten
	if ($_GET['cPath']==$cPath or $_SESSION['fk_cpath'] == $cPath) 
	{
		$categories_string .= '<div class="uberLink2"><a href="'.tep_href_link($path).'" class="ukat">'.$linktitle.'</a></div>';
	}
	elseif($section_2 == $cPath) {
		$categories_string .= '<div class="uberLink2"><a href="'.tep_href_link($path).'" class="ukat">'.$linktitle.'</a></div>';
	}
	else
	{
		$categories_string .= '<div><a href="'.tep_href_link($path).'" class="ukat">'.$linktitle.'</a></div>';
	}
	return $categories_string;
}  

function mainkatLink($link, $linktext, $msection, $cat_arr, $aGenerateTrenner = false){
	
	global $section;
	global $rootdir;
	global $section_2;
	#echo $section;
		
		if (($section==$msection and $section_2 == '')) 
		{
			$categories_string .= '<div class="uberLink1"><a title="'.$linktext.'" href="'.tep_href_link($link,'').'">'.$linktext.'</a></div>';
		}
		else if(in_array($_GET['cPath'],$cat_arr))
		{
			$categories_string .= '<div class="mkat"><a title="'.$linktext.'" href="'.tep_href_link($link,'').'">'.$linktext.'</a></div>';
		}	
		else
		{
			$categories_string .= '<div><a title="'.$linktext.'" href="'.tep_href_link($link,'').'">'.$linktext.'</a></div>';
		}		
		if($aGenerateTrenner){
			$categories_string .= '<div class="mainCatTrenner"></div>';
		}
	return $categories_string;
}
function ukatLinkFolddown($link, $linktitle, $cPath = false)
{
	global $section;
	if ($section==$cPath) 
	{
		$categories_string .= '<div class="uberLink2"><a href="'.tep_href_link($link).'" class="ukat" onfocus="this.blur()">'.$linktitle.'</a></div>';
	}
	else
	{
		$categories_string .= '<div><a href="'.tep_href_link($link).'" class="ukat" onfocus="this.blur()">'.$linktitle.'</a></div>';
	}
	return $categories_string;
} 
function ukatLinkFarben($link, $linktitle, $cPath = false, $aTitle = NULL)
{
	
	if ($_GET['cPath']==$cPath) 
	{
		$categories_string .= '<div class="uberLink2"><a '.(($aTitle) ? 'title="'.$aTitle.'"' : '').' href="'.tep_href_link($link).'" class="ukat" id="ukat'.$cPath.'" onfocus="this.blur()">'.$linktitle.'</a></div>';
	}
	else
	{
		$categories_string .= '<div><a '.(($aTitle) ? 'title="'.$aTitle.'"' : '').' href="'.tep_href_link($link).'" class="ukat" id="ukat'.$cPath.'" onfocus="this.blur()">'.$linktitle.'</a></div>';
			}
	return $categories_string;
} 

function specialkatLink($link, $linktext, $msection, $cat_arr, $cat_gif)
{
	global $section;
	global $rootdir;
	#echo $section;
	$categories_string .= '';
	#$categories_string .= '<a href="'.tep_href_link($link,'').'">';
	
		if ($section==$msection) 
		{
			$categories_string .= '<div class="uberLink1"><a href="'.tep_href_link($link,'').'" class="skat">'.tep_image($rootdir.DIR_WS_IMAGES . $cat_gif, '').$linktext.'</a></div>';
		}
		else if(in_array($_GET['cPath'],$cat_arr))
		{
			$categories_string .= '<div class="mkat"><a href="'.tep_href_link($link,'').'" class="skat">'.tep_image($rootdir.DIR_WS_IMAGES . $cat_gif, '').$linktext.'</a></div>';
		}	
		else
		{
			$categories_string .= '<div><a href="'.tep_href_link($link,'').'" class="skat">'.tep_image($rootdir.DIR_WS_IMAGES . $cat_gif, '').$linktext.'</a></div>';
		}		
	$categories_string .= '';
	return $categories_string;
}

/************************************************
*
* Variablen auf REgul�ren Ausdruck Pr�fen f�r SQL Abfragen
* 
************************************************/
function gc_param_check() {

	$numargs = func_num_args();
	if ($numargs < 2) {
        trigger_error('Zu wenig Argumente', E_USER_WARNING);
    }
	$arg_list = func_get_args();
	
	$param = trim(mysql_real_escape_string($arg_list[0]));
	for ($i = 1; $i < $numargs; $i++) {
		if(preg_match($arg_list[$i], $param))
			return $param;
	}
	return false;
	
}

/************************************************
*
* Automatische Breaks
* 
************************************************/
function brlf($argCount=1) {
	while ($argCount-- >0)
	echo "<br />\n";
}

/************************************************
*
* Dump von Variablen
* 
************************************************/
function go_dump($argArray){
	echo '<pre>';
	var_dump($argArray);
	echo '</pre>';
}

/************************************************
*
* MySql Iso DATE in Deutsches Datum umwandeln
* 
************************************************/
function date_mysql2german($datum) {
    list($jahr, $monat, $tag) = explode("-", $datum);

    return sprintf("%02d.%02d.%04d", $tag, $monat, $jahr);
}

/************************************************
*
* MySql Iso DATETIME in Deutsches Datum umwandeln
* 
************************************************/
function datetime_mysql2german($datum) {
    list($jahr, $monat, $tag) = explode("-", $datum);
	list($tag, $zeit) = explode(' ',$tag);
	list($stunde, $minute, $sekunde) = explode(':',$zeit);

    return sprintf("%02d.%02d.%04d %02d:%02d:%02d", $tag, $monat, $jahr, $stunde, $minute, $sekunde);
}

/************************************************
*
*  Deutsches Datum in MySql Iso DATE umwandeln
* 
************************************************/
function date_german2mysql($datum) {
    list($tag, $monat, $jahr) = explode(".", $datum);

    return sprintf("%04d-%02d-%02d", $jahr, $monat, $tag);
}

/************************************************
*
*  Deutsches Datum + Zeit in MySql Iso DATETIME umwandeln
* 
************************************************/
function datetime_german2mysql($datum) {
    list($tag, $monat, $jahr) = explode(".", $datum);
	list($jahr, $zeit) = explode(' ',$jahr);
	list($stunde, $minute, $sekunde) = explode(':',$zeit);

    return sprintf("%04d-%02d-%02d %02d:%02d:%02d", $jahr, $monat, $tag, $stunde, $minute, $sekunde);
}


/************************************************
*
* MySql Iso TIMESTAMP in Deutsches Datum umwandeln
* 
************************************************/
function timestamp_mysql2german($t) {
    return sprintf("%02d.%02d.%04d",
                    substr($t, 6, 2),
                    substr($t, 4, 2),
                    substr($t, 0, 4));
}

/**
 * php_timestamp_mysql2german
 * wandelt ein MySQL-Timestamp
 * in ein traditionelles deutsches Datum um.
*/
function datetime_mysql2php_timestamp_german($datum) {
    list($jahr, $monat, $tag) = explode("-", $datum);
	list($tag, $zeit) = explode(' ',$tag);
	list($stunde, $minute, $sekunde) = explode(':',$zeit);

	return mktime($stunde, $minute, $sekunde, $monat, $tag, $jahr);
}

/*
 *hidden-Fields bei Formularen
*/
function hidden_input($string) {
	$hidden_arr = explode(',',$string);
	$hidden_str = '';
	foreach($hidden_arr as $hiddenval){
		$hidden_str.= '<input type="hidden" name="'.$hiddenval.'" value="'.$_POST[$hiddenval].'" />'."\n";
	}
	return $hidden_str;
}



/*
 *Fehlerbehandlung
*/
function bg_error($field) {
	global $error_arr;
	if(isset($error_arr))
	{
		if(in_array($field, $error_arr))
			$class = ' error" onfocus="this.style.background=\'#FFFFFF\'';
			return $class;
	}
}




/**
* replace URIs with appropriate HTML code to be clickable.
*/
function replace_uri($str, $target) {
  $pattern = '#(^|[^\"=]{1})(http://|ftp://|mailto:|news:)([^\s<>]+)([\s\n<>]|$)#sm';
  return preg_replace($pattern,"\\1<a href=\"\\2\\3\" target=\"$target\"><u>\\2\\3</u></a>\\4",$str);
}

function gcResizeImage($src, $breite, $hoehe, $offset_x, $shop_shortname, $newdir)
			{
			
				//Abschneiden nach Links (negativer Wert) Versatz zur Ursprungsdatei
				$dst_x = 0 - ($offset_x);
				//Abschneiden nach Oben (negativer Wert) Versatz zur Ursprungsdatei, hier Null
				$dst_y = 0;
				//Ursprungsdatei beschneiden x
				$src_x = 0;
				//Ursprungsdatei beschneiden y
				$src_y = 0;
				//Die neuen Bildma�e Breite
				$src_w = $breite + $offset_x;
				//Die neuen Bildma�e H�he
				$src_h = $hoehe;
				
				//altes Bild als Ressource erzeugen
				$imgsrc = imagecreatefromjpeg($src);
				
				//neues Bild als Ressource erzeugen
				$imgdst = imagecreatetruecolor ($breite,$hoehe);				
				
				//Bilder ineinander kopieren
				imagecopy($imgdst,$imgsrc,$dst_x,$dst_y,$src_x,$src_y,$src_w,$src_h);
				#imagecopyresampled($imgdst,$imgsrc,$dst_x,$dst_y,$src_x,$src_y,$breite,$hoehe,$src_w,$src_h);
				
				//neuer Dateiname
				$dst = substr($src,0,strrpos($src,"."))."_".$shop_shortname.".jpg";
				$dstpng = substr($src,0,strrpos($src,"."))."_".$shop_shortname.".png";
				
				//Dateinamen ausgeben
				#echo $newdir.$dst.'<br>';
				
				
				//wenn Datei �lter als 10 Minuten ist oder nicht existiert, dann Datei neu schreiben
				//umgeht Scriptlaufzeitbeschr�nkung
				
				#echo "<h4>DEBUG: (time() - filemtime($newdir.$dst))=".(time() - filemtime($newdir.$dst))."</h4>";
				if(file_exists($newdir.$dst) && (time() - filemtime($newdir.$dst)) < 900)
				{
					echo "Skipping: Datei existiert und ist keine 15 Minuten alt: ".$newdir.$dst." und wurde erzeugt am " . date ("F d Y H:i:s.", filemtime($newdir.$dst))."<br>
					";
				}
				else
				{
					//Bilddatei neu schreiben
					imagejpeg($imgdst,$newdir.$dst,100);
					imagepng($imgdst,$newdir.$dstpng);
					echo "Datei existiert nicht oder Datei existiert und ist �lter als 15 Minuten: ".$newdir.$dst." und wurde erzeugt am " . date ("F d Y H:i:s.", filemtime($newdir.$dst))."<br>
					";
					#echo "Datei: $datei Breite: $breite, H�he: $hoehe<br>";
				}
				
			}

//alte Funktion zum Bilderkopieren, wird benutzt in gc_copy_images, Problem mit Wartezeiten und Laufzeitbeschr�nkung
function gcMultipleImageRezize ($dir) 
			{
				 $fp=opendir($dir);
				 while($datei=readdir($fp)) 
				 {
				  	if (is_dir("$dir$datei") && $datei!="." && $datei!="..") 
					{
						echo "$dir$datei (dir)<br>";
						gcMultipleImageRezize("$dir$datei");
					}
				  	else 
					{
						//�berpr�fung ob es sich um ein jpeg-Bild handelt
						if(substr($datei,strrpos($datei,".")+1) == 'jpg')
						{
							//Fallunterscheidung Medium-Dateien und Small-Dateien
							if(strstr($datei,'_md') && !strstr($dir,'mannschettenknoepfe'))
							{	
								#echo "Dir: ".$dir."<br>";								
								gcResizeImage("$dir/$datei",350,510,25,'LAC','../../dom.lacravate.com/images/');
							}
							if(strstr($datei,'_sm') && !strstr($dir,'mannschettenknoepfe'))
							{
								gcResizeImage("$dir/$datei",150,170,5,'LAC','../../dom.lacravate.com/images/');
							}
							
							//F�r Manschettenkn�pfe (Verzeichnis mannschettenkn�pfe ;-)
							if(strstr($datei,'_md') && strstr($dir,'mannschettenknoepfe'))
							{	
								#echo "Dir: ".$dir."<br>";								
								gcResizeImage("$dir/$datei",350,272,25,'LAC','../../dom.lacravate.com/images/');
							}
							if(strstr($datei,'_sm') && strstr($dir,'mannschettenknoepfe'))
							{
								gcResizeImage("$dir/$datei",150,108,5,'LAC','../../dom.lacravate.com/images/');
							}
							
							#echo $datei."<br>";
						}
						else
						{
							#echo 'keine jpeg-Grafik';
						}
					}
				 }
				 closedir($fp);
			} 			

//neue Funktion zum Bilderkopieren, wird benutzt in gc_shop_synchro.php
//nutzt Datenbank-Queue
//imgsrc ist der urspr�ngliche Dateiname

function gcResizeImage2($src, $breite, $hoehe, $cut_x, $cut_y, $src_x, $src_y, $shop_shortname, $newdir)
{
	$srcdir = '/homepages/37/d91333445/htdocs/dom.krawatten-ties.com/images/';
	//Abschneiden nach Links (negativer Wert) Versatz zur Ursprungsdatei
	$dst_x = 0 - ($cut_x);
	//Abschneiden nach Oben (negativer Wert) Versatz zur Ursprungsdatei, hier Null
	$dst_y = 0;
	//Ursprungsdatei beschneiden x
	$src_x = 0 + ($src_x);
	//Ursprungsdatei beschneiden y
	$src_y = 0 + ($src_y);
	//Die neuen Bildma�e Breite
	$src_w = $breite + $cut_x;
	//Die neuen Bildma�e H�he
	$src_h = $hoehe;
	
	#echo $cut_x.'/'.$cut_y.'//'.$dst_x.'/'.$dst_y.'/'.$src_x.'/'.$src_y.'/'.$src_w.'/'.$src_h.'<br>';
	
	//neues Bild als Ressource erzeugen
	$imgdst = imagecreatetruecolor ($breite,$hoehe);	
	$white = imagecolorallocate($imgdst, 255, 255, 255);
	$grey = imagecolorallocate($imgdst, 232, 232, 232);
	
	//Pr�fen ob Datei im Ursprung existiert 
	if(file_exists($srcdir.$src))
	{
		
		//altes Bild als Ressource erzeugen
		$imgsrc = imagecreatefromjpeg($srcdir.$src);
		//Bidgr��en ermitteln
		$src_w = imagesx($imgsrc);
		$src_h = imagesy($imgsrc);
		
   		#imagefill( $imgsrc, 0, 0, $white ); //Problem:Hemden haben einen ausgefransten Rand, Pixelst�rungen oberhalb des Kragens, Transparenzen???
		
		//Fehlermeldung zum loggen generieren
		if(!imgsrc)
		{
			return false;
		}
	}
	else
	{
		return false;
	}	
	
	
	
	
	//fill the background with white (see http://de2.php.net/manual/de/function.imagecopy.php#68940)
    imagefill( $imgdst, 0, 0, $white );
		
	//Bilder ineinander kopieren
	imagecopy($imgdst,$imgsrc,$dst_x,$dst_y,$src_x,$src_y,$src_w,$src_h);
	#imagecopyresampled($imgdst,$imgsrc,$dst_x,$dst_y,$src_x,$src_y,$breite,$hoehe,$src_w,$src_h);
	
	//neuer Dateiname
	$dst = substr($src,0,strrpos($src,"."))."_".$shop_shortname.".jpg";
	$dst_thumb = preg_replace('/_sm/','_th',substr($src,0,strrpos($src,"."))."_".$shop_shortname.".jpg"); //_sm wird durch _th ersetzt -> Thumbnail Dateiname
	$dstpng = substr($src,0,strrpos($src,"."))."_".$shop_shortname.".png";
	
	//Dateinamen ausgeben
	#echo $newdir.$dst.'<br>';
	
	
	//Bilddatei neu schreiben, nur wenn Verzeichnis existiert, ansonsten Fehler melden
	$cleandir=substr($newdir.$dst,0,
	strrpos($newdir.$dst,'/')+1//letzter Slash
	);
	
	
	if(file_exists($cleandir))
	{
		#echo $cleandir;
		if($breite > 50)
		{
			//Bild mit normalen Dateinamen erzeugen
			imagejpeg($imgdst,$newdir.$dst,100);
		}
		else
		{
			//ansonsten Thumbdatei erzeugen (hat nur anderen dateinamen=>PAY)
			imagejpeg($imgdst,$newdir.$dst_thumb,100);
		}
		
	}
	else
	{
		return false;
	}
	#imagepng($imgdst,$newdir.$dstpng);
	#echo "Datei: ".$newdir.$dst." wurde erzeugt am " . date ("F d Y H:i:s.", filemtime($newdir.$dst))."<br>";
	#echo "Datei: $datei Breite: $breite, H�he: $hoehe<br>";
	
	return true;
	
}
			
//Welche Funktion soll zum Bilder kopieren angewendet werden?
//Shopabh�ngig -> $shop_shortname
//je nach Bild andere Parameter -> $img			
function imgCopyCommand($shop_shortname,$resize,$img,$breite=false,$hoehe=false,$cut_x=false,$cut_y=false,$src_x=false,$src_y=false)
{
	
	if($resize == true)
	{
		#$command = 'gcResizeImage2(\''.$DOCUMENT_ROOT.'/images/'.$products['products_image'].'\',350,510,25,0,\''. $_GET['shop_shortname'].'\', \'/kunden/homepages/37/d91333445/htdocs/dom.'.strtolower($shop['shop_name']).'/images/\'');
		$command = 'gcResizeImage2#'.$img.'#'.$breite.'#'.$hoehe.'#'.$cut_x.'#'.$cut_y.'#'.$src_x.'#'.$src_y;
	}
	else
	{
		$shop = getShopArrayFromShortname($shop_shortname);
		$command = 'cp -v /homepages/37/d91333445/htdocs/dom.krawatten-ties.com/images/'.$img.' /kunden/homepages/37/d91333445/htdocs/dom.'.strtolower($shop['shop_name']).'/images/'.renameImage($img,$shop_shortname);
	}
	
	return $command;
}

function renameImageThumb($image_orig,$shop_shortname)
{
	$image_new = preg_replace('/_sm/','_th',preg_replace('/\.jpg/','_'.$shop_shortname.'.jpg',$image_orig));
	return $image_new;
}
function getShopArrayFromShortname ($shop_shortname)
{
	$sql = "SELECT * FROM gc_shops WHERE shop_shortname = '".$shop_shortname."'";
	$gcshop_qry = tep_db_query($sql);
	$gcshop = tep_db_fetch_array($gcshop_qry);
	
	return $gcshop;
}

function getShopsFromServer($sShopServer='') {
	
	if(empty($sShopServer))
		$sShopServer = SERVER_ID;
	
	$hShopQuery = mysql_query("SELECT * FROM gc_shops AS sh, gc_server AS se WHERE sh.shop_server=se.server_id AND sh.shop_server='".$sShopServer."'");
	
	$aShops = array();
	
	while($aShopFetch = mysql_fetch_array($hShopQuery)) {
		$aShops[] = $aShopFetch;
	}
	
	return $aShops;
	
}

//Berechnet die Quersumme einer Zahl
//http://www.php.de/php-einsteiger/1458-erledigt-wiederholte-quersumme.html
function gc_quer_summe($z){
    while ($z > 9){
        $sum = 0;
        $str = "$z";
        $len = strlen($str);
        for ($i = 0;$i < $len;$i ++){
            $sum += $str{$i};
        }
        $z = $sum;
    }
    return($z);
}

function gc_quer_summe_einfach($z){
    if ($z > 9){
        $sum = 0;
        $str = "$z";
        $len = strlen($str);
        for ($i = 0;$i < $len;$i ++){
            $sum += $str{$i};
        }
        $z = $sum;
    }
    return($z);
}

function gc_sequery() {

	$parsed_url = parse_url($_SERVER['HTTP_REFERER']);
	$ref_host = $parsed_url['host'];
	$ref_http = $parsed_url['scheme'];
	
	if($ref_http.'://'.$ref_host != HTTP_SERVER and empty($_SESSION['sequery']) and !empty($_SERVER['HTTP_REFERER'])) {
		
		if(preg_match('/google/i', $ref_host)) {
			preg_match('/q=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Google';
		}
		elseif(preg_match('/yahoo/i', $ref_host)) {
			preg_match('/p=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Yahoo';
		}
		elseif(preg_match('/web/i', $ref_host)) {
			preg_match('/su=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Web';
		}
		elseif(preg_match('/live/i', $ref_host)) {
			preg_match('/q=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Live (MSN)';
		}
		elseif(preg_match('/lycos/i', $ref_host)) {
			preg_match('/query=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Lycos';
		}
		elseif(preg_match('/altavista/i', $ref_host)) {
			preg_match('/q=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Alta Vista';
		}
		elseif(preg_match('/ask/i', $ref_host)) {
			preg_match('/q=([^\&]{1,})/i', $parsed_url['query'], $sequery_raw);
			$searchengine = 'Ask';
		}
		else {
			$sequery_raw[1] = $parsed_url['query'];
			$searchengine = 'Undefined Search';
		}
			
		if(empty($_SESSION['sequery']['accesstime']))
			$accesstime = (string)date('YmdHis');
		else
			$accesstime = $_SESSION['sequery']['accesstime'];
			
		$_SESSION['sequery'] = array('url' => $sequery_raw[1], 'accesstime' => $accesstime, 'searchengine' => $searchengine);
		//tep_db_query("INSERT INTO gc_sequery (id, orders_id, query) VALUES ('', '', '".serialize($_SESSION['sequery'])."')");
		//$_SESSION['sequery_id'] = mysql_insert_id();
		
	}
//if(DEBUG) go_dump($_SESSION['sequery']);
}

function gc_sequery_checkout_process($orders_id) {
	
	if(!empty($_SESSION['sequery'])) {
		//tep_db_query("UPDATE gc_sequery SET orders_id='".$orders_id."' WHERE id='".$_SESSION['sequery_id']."'");
		tep_db_query("INSERT INTO gc_sequery (id, orders_id, query) VALUES ('', '".$orders_id."', '".serialize($_SESSION['sequery'])."')");
		//unset($_SESSION['sequery_id']);
		unset($_SESSION['sequery']);
	}
}

function gc_sequery_show_admin($orders_id) {

	$query = tep_db_query("SELECT gcs.orders_id, gcs.query, DATE_FORMAT(o.date_purchased,'%Y%m%d%H%i%s') as date_purchased2 FROM gc_sequery AS gcs, orders AS o WHERE gcs.orders_id='".$orders_id."' AND gcs.orders_id=o.orders_id");
	
	if(tep_db_num_rows($query)) {
	
		$fetch = tep_db_fetch_array($query);
		$values = unserialize($fetch['query']);
		$est_time = ($fetch['date_purchased2']-$values['accesstime'])/60;

		return array('query' => urldecode($values['url']), 'searchengine' => $values['searchengine'], 'time' => $est_time);
		
	}
}
function gc_checkpoints($checkpoint = '')
{
	
	
	$places = 5; // hier bestimmt man die Anzahl der Nachkommastellen
	
	$mic_time = explode(" ",microtime()); 
	$mictime = $mic_time[1] + $mic_time[0]; 
	
	if($checkpoint != '')
	{
		$cptime_array = array($checkpoint => $mictime);	
		
		if(!isset($_SESSION['checkpoints']))
		{
			$_SESSION['checkpoints'] = array();
		}
	
		$_SESSION['checkpoints'][] = $cptime_array;
		//�bergabeparameter in der Form: 
		//array(3) { [0]=>  array(1) { ["Top"]=>  float(1235761926.4431) } [1]=>  array(1) { ["Middle"]=>  float(1235761926.4434) } [2]=>  array(1) { ["End"]=>  float(1235761926.4435) } } 
	}
	else
	{
#		var_dump($_SESSION['checkpoints']);
		
		$msg_body = '';
		
		foreach($_SESSION['checkpoints'] AS $checkpoint)
		{	
			foreach($checkpoint AS $cpname => $cptime)
			{
				$debug_msg .= "Von ".$cpname." bis zum Ende sind ". number_format(round(($mictime - $cptime),$places),$places) ." secs vergangen\n" ;
			}
		}
		$_SESSION['checkpoints']='';
		
		$debug_msg .= "\n\n";
		$debug_msg .= "REMOTE_ADDR\t".$_SERVER['REMOTE_ADDR']."\n\n";
		$debug_msg .= "Seite\t\t".$_SERVER['PHP_SELF']."\n\n";
		$debug_msg .= "Request\t\t".$_SERVER['REQUEST_URI']."\n\n";
		$debug_msg .= "Referrer\t\t".$_SERVER['HTTP_REFERER']."\n\n";
		$debug_msg .= "UA\t\t".$_SERVER['HTTP_USER_AGENT']."\n\n";
		
		
		mail('info@gurkcity.de','Zeitmessung Checkpoints', $debug_msg);
	}
}

// Modellnummern suchen
function gc_getRelatetProducts($model='') {

	if(empty($model))
		return false;
		
	$product_info_model = array();
	$product_models = array();
	
	// Wenn Produkt Standart (ohne Suffix)
	//if(preg_match('/^([A-Z]{2})([0-9]{1,6})$/i', $model, $product_info_model)) {  doesnt work for JAS-N313-3 f.e.
	$isSubProduct = in_array(substr($model, -1, 1), array('L', 'C', 'S', 'K', 'T', 'D', 'Y', 'F', 'X', 'P', 'E', 'Q')); // Wenn Produkt XXL, Clip ... (mit Suffix)
	$checkModel = ($isSubProduct) ? substr($model, 0, -1): $model;
	if(substr($model, -2, 2) == 'SF'){
	   $checkModel = substr($model, 0, -2);
	}elseif(substr($model, -2, 2) == 'CQ'){
		$checkModel = substr($model, 0, -2);
	}elseif(substr($model, -2, 2) == 'SQ'){
		$checkModel = substr($model, 0, -2);
	}

	
	// Suche alle zugeh�rigen Produkte mit Suffix
	$query = tep_db_query("SELECT t1.products_id, t1.products_model, t2.products_name, t1.products_tax_class_id, t1.".PRODUCTS_PRICE." FROM products as t1 LEFT JOIN products_description as t2 ON (t1.products_id = t2.products_id AND t2.language_id = ".LANGUAGE_ID.") WHERE t1.products_model LIKE '".$checkModel."%' AND t1.".PRODUCTS_PRICE." > 0 AND t1.products_quantity > 0  AND t1.products_status = '1'");
	//echo "SELECT t1.products_id, t1.products_model, t2.products_name, t1.products_tax_class_id, t1.".PRODUCTS_PRICE." FROM products as t1 LEFT JOIN products_description as t2 ON (t1.products_id = t2.products_id AND t2.language_id = ".LANGUAGE_ID.") WHERE t1.products_model LIKE '".$checkModel."%' AND t1.".PRODUCTS_PRICE." > 0 AND t1.products_quantity > 0";

	
	while($fetch = tep_db_fetch_array($query)) {
		
		if($fetch['products_model'] != $model)
			$product_models[] = $fetch;
		
	}
	 
	if(!empty($product_models))
		return $product_models;
	else
		return false;
		
}

// Firmenrechnung
function gc_setCompany($vat) {

	$vat = trim($vat);
	
	if(preg_match('/^(DE)|(DE )(DE-)[0-9{9}]$/i', $vat) or $vat=='inland')
		$_SESSION['company_vat'] = 'inland';
	elseif(!empty($vat) or $vat=='ausland')
		$_SESSION['company_vat'] = 'ausland';
	else
		unset($_SESSION['company_vat']);
		
}

// Upload File from remote FTP
// Besonderheit bei UNT:
// DIe id gibt an, dass es sich um ein Thumbnail handelt.
// hierbei muss ein bildausschnitt gew�hlt werden
function gc_image_live_syncro($image_src, $shop_shortname='', $id='', $resizeFromSize = NULL) {

    global $aExtraImageCategories;

    //  echo '<br>params:'.$image_src.'___'.$shop_shortname.'___'.$id.'___'.$resizeFromSize.'<br>';

    $targetSize = substr($image_src, (strpos($image_src, '_')+1), 2);

    if($resizeFromSize === NULL){

        if($targetSize == 'sm'  && defined('SMALL_IMAGE_RESIZEFROMMD')  && SMALL_IMAGE_RESIZEFROMMD === true){
            $resizeFromSize = 'md';
        }
    }

    if($resizeFromSize){
        $remote_imagesize = $resizeFromSize;
    }
    else {
        $remote_imagesize = substr($image_src, (strpos($image_src, '_')+1), 2);
    }

    if(!empty($remote_imagesize)){
        $image_src_remote = substr($image_src, 0, strpos($image_src, '_')).'_'.$remote_imagesize.'.jpg';
    }
    else {
        $image_src_remote = substr($image_src, 0, -11).'_sm'.substr($image_src, -4);
    }

    if(substr($image_src, 0, 3) == '../') {
        $image_src = substr($image_src, 3);
        $image_src_remote = substr($image_src_remote, 3);
    }

    $image_src_remote = str_replace('images/ties/', 'images/articles_compressed/', $image_src_remote);
    $imgSrc = file_get_contents(FTP_HTTPSERVER_GETCONTENTS.'/'.$image_src_remote);

    //echo FTP_HTTPSERVER_GETCONTENTS.'/'.$image_src_remote.'<br>';
    if(strlen($imgSrc) > 1000){

        $fp = fopen(DIR_FS_CATALOG.$image_src, 'w+');
        fwrite($fp, $imgSrc);
        fclose($fp);
        $found = true;

        shell_exec('chmod 755 '.DIR_FS_CATALOG.$image_src);
    }
    else {

        $found = false;
        $image = array('name' => $image_src,
            'width' => $blankoImgWidth,
            'height' => $blankoImgHeight
        );

        return array('img'=>$image, 'found'=>false);
    }

    $image_size = @getimagesize(DIR_FS_CATALOG.$image_src);

    $query ="Select * from products_to_categories where products_id = '".$id."'";

    $cats_query = mysql_query($query);
    while($fetch = mysql_fetch_array($cats_query))
        $productsCategories[] = $fetch['categories_id'];

    if(!empty($productsCategories)  &&  !empty($aExtraImageCategories)){
        $checkSpecialSizes = array_intersect($productsCategories, $aExtraImageCategories);
    }

    // Bild ist z.B. Manschettenknopf small
    if(!empty($checkSpecialSizes)  and strpos($image_src, '_sm') and ($image_size[0]!=SMALL_IMAGE_WIDTH or $image_size[1]!=EXTRA_SMALL_IMAGE_HEIGHT)){
        meImageCutResampled($image_src, $image_src, array(0,0,$image_size[0],$image_size[1]), array(SMALL_IMAGE_WIDTH,EXTRA_SMALL_IMAGE_HEIGHT));
    }
    // Bild ist z.B. Manschettenknopf medium
    elseif(!empty($checkSpecialSizes)  and strpos($image_src, '_md') and ($image_size[0]!=MEDIUM_IMAGE_WIDTH or $image_size[1]!=EXTRA_MEDIUM_IMAGE_HEIGHT))
        meImageCutResampled($image_src, $image_src, array(0,0,$image_size[0],$image_size[1]), array(MEDIUM_IMAGE_WIDTH,EXTRA_MEDIUM_IMAGE_HEIGHT));
    // Bild ist Krawatte small
    elseif(strpos($image_src, '_sm') and ($image_size[0]!=SMALL_IMAGE_WIDTH or $image_size[1]!=SMALL_IMAGE_HEIGHT)){

        if($targetSize == 'sm'  && defined('SMALL_IMAGE_RESIZEFROMMD')  && SMALL_IMAGE_RESIZEFROMMD === true){

            $heightTemp = ceil((SMALL_IMAGE_WIDTH/$image_size[0] * $image_size[1]));

            meImageCutResampled($image_src, $image_src, array(0,0,$image_size[0],$image_size[1]), array(SMALL_IMAGE_WIDTH, $heightTemp));
            $image_size = @getimagesize(DIR_FS_CATALOG.$image_src);
        }

        if(defined('SMALL_IMAGE_CROPSIZE') &&  SMALL_IMAGE_CROPSIZE == true){  // cropping instead of resizing

            $diffWidth = $image_size[0] - SMALL_IMAGE_WIDTH;
            $diffHeight = $image_size[1] - SMALL_IMAGE_HEIGHT;

            if($diffWidth % 2 == 0  &&  $diffWidth > 0){
                $xSrc = $diffWidth / 2;
            }
            else {
                $xSrc = 0;
            }

            if($diffHeight % 2 == 0  &&  $diffHeight > 0){
                $ySrc = $diffHeight / 2;
            }
            else {
                $ySrc = 0;
            }
            meImageCutResampled($image_src, $image_src, array($xSrc,$ySrc,SMALL_IMAGE_WIDTH,SMALL_IMAGE_HEIGHT), array(SMALL_IMAGE_WIDTH,SMALL_IMAGE_HEIGHT));
        }
        else{
            meImageCutResampled($image_src, $image_src, array(0,0,$image_size[0],$image_size[1]), array(SMALL_IMAGE_WIDTH,SMALL_IMAGE_HEIGHT));
        }
    } // Bild ist Krawatte medium
    elseif(strpos($image_src, '_md') and ($image_size[0]!=MEDIUM_IMAGE_WIDTH or $image_size[1]!=MEDIUM_IMAGE_HEIGHT)){

        if(defined('MEDIUM_IMAGE_CROPSIZE') &&  MEDIUM_IMAGE_CROPSIZE == true){  // cropping instead of resizing

            $diffWidth = $image_size[0] - SMALL_IMAGE_WIDTH;

            if($diffWidth % 2 == 0  &&  $diffWidth > 0){
                $xSrc = $diffWidth / 2;
            }
            else {
                $xSrc = 0;
            }

            if($diffHeight % 2 == 0  &&  $diffHeight > 0){
                $ySrc = $diffheight / 2;
            }
            else {
                $ySrc = 0;
            }
            meImageCutResampled($image_src, $image_src, array($xSrc,$ySrc,MEDIUM_IMAGE_WIDTH,MEDIUM_IMAGE_HEIGHT), array(MEDIUM_IMAGE_WIDTH,MEDIUM_IMAGE_HEIGHT));
        }
        else{
            meImageCutResampled($image_src, $image_src, array(0,0,$image_size[0],$image_size[1]), array(MEDIUM_IMAGE_WIDTH,MEDIUM_IMAGE_HEIGHT));
        }
    }

    $image = array('name' => $image_src,
        'width' => $image_size[0],
        'height' => $image_size[1]
    );

    return array('img'=>$image, 'found'=>true);
}

function gcFileUpload($src, $dest) {

	if(empty($src) or empty($dest))
		return false;
		
	if(!move_uploaded_file($_FILES[$src]['tmp_name'], DIR_FS_CATALOG.$dest))
		return false;
	
	return true;
	
}

function get_all_shops($aShopsPermit) {
	
	$sShopsPermit = "shop_id='". implode("' OR shop_id='", $aShopsPermit)."'";
	
	$hQueryShops = tep_db_query("SELECT * FROM gc_shops WHERE ".$sShopsPermit);
	
	$aShops = array();
	
	while($aFetchShops = tep_db_fetch_array($hQueryShops))
		$aShops[$aFetchShops['shop_id']] = $aFetchShops;
	
	return $aShops;	
	
}

function meImageCutResampled($sFileSource, $sFileDestination, $aSourceXYWH, $aDestinationWH) {

    $image = new Zebra_Image();
    $image->source_path = $sFileSource;
    $image->target_path = $sFileDestination;
    $image->jpeg_quality = 92;
    $image->preserve_aspect_ratio = true;
    $image->sharpen_images = true;
    $image->resize($aDestinationWH[0], $aDestinationWH[1], ZEBRA_IMAGE_CROP_CENTER);
}

function gc_getImageFilenameFromModel($model, $size) {

	return in_array(substr($model, -1), array('S', 'L', 'K', 'C')) ? substr($model, 0, -1).'_'.$size.'_'.SHOP_SHORTNAME.'.jpg' : $model.'_'.$size.'_'.SHOP_SHORTNAME.'.jpg';

}

// Diese Funktion �berpr�ft eine Variable nach einem Regex
// Im Fehlerfall wird die Variable gel�scht
// return [true|false]
function gc_filter_var(&$rVar, $sRegex) {

	if(!preg_match($sRegex, $rVar)) {
		unset($rVar);
		return false;
	}
	
	return true;
}

//BOF No-Spam E-Mails V1.0
function no_spam($mail) {
    /*
	$str = "";
    $a = unpack("C*", $mail);
    foreach ($a as $b)
    $str .= sprintf("%%%X", $b);
    return $str;
	*/
	
	//Mailadresse zerlegen
	$name = substr($mail,0,strpos($mail,'@'));
	$domain = substr($mail,strpos($mail,'@')+1);
	$seclvldomain = substr($domain,0,strpos($domain,'.'));
	$toplvldomain = substr($domain,strpos($domain,'.')+1);
	
	$str = '<script type="text/javascript"><!--
	var mailadr="'.$name.'";
	var maildom="'.$domain.'";
	var mailarg="";
	document.write(\'<a href="mailto:\'+mailadr+\'@\'+maildom+mailarg+\'">\'+mailadr+\'@\'+maildom+\'</a>\');
	//--></script>
	<noscript>'.$name.'&nbsp;[at]&nbsp;'.$seclvldomain.'&nbsp;[dot]&nbsp;'.$toplvldomain.'</noscript>';
	return $str;
	
}
//EOF No-Spam E-Mails


//KRT3000GUTSCHEINE-Aktion (Juni2007), Neukunde setzt einen Werbegutschein ein, Altkunde bekommt als Belohnung ebenfalls 10 EUR, by Gurkcity 04.06.2007 BOF


function generiereGutscheinCode()
{
	$gutscheincode = '';
	
	$alphaArray = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
	shuffle($alphaArray);
	
	$randLettersShortString = substr(implode('',$alphaArray),0,4);
	
	$gutscheincode .= $randLettersShortString;
	
	$numberArray = range(1,9);
	shuffle($numberArray);
	
	$randNumbersShortString = substr(implode('',$numberArray),0,4);
	
	$gutscheincode .= $randNumbersShortString;
	
	return $gutscheincode;
}

//KRT3000GUTSCHEINE-Aktion (Juni2007), Neukunde setzt einen Werbegutschein ein, Altkunde bekommt als Belohnung ebenfalls 10 EUR, by Gurkcity 04.06.2007 EOF


function getPriceFieldsFromServerID($serverID='') {
	
	if(empty($serverID)) {
		$serverID = SERVER_ID;
	}
	
	$hQueryPriceField = tep_db_query("SELECT shop_shortname, products_price_fieldname FROM gc_shops WHERE shop_server='".$serverID."'");
	
	$aPriceFields = array();
	
	while($aFetchPriceField = tep_db_fetch_array($hQueryPriceField)) {
		$aPriceFields[$aFetchPriceField['shop_shortname']] = $aFetchPriceField['products_price_fieldname'];
	}
	
	return $aPriceFields;
	
}

function getLanguageIDsFromServerID($serverID='') {
	
	if(empty($serverID)) {
		$serverID = SERVER_ID;
	}
	
	$hQueryLanguages = tep_db_query("SELECT shop_id, language_id FROM gc_shops WHERE shop_server='".$serverID."'");
	
	$aLanguages = array();
	
	while($aFetchLanguages= tep_db_fetch_array($hQueryLanguages)) {
		$aLanguages[$aFetchLanguages['shop_id']] = $aFetchLanguages['language_id'];
	}
	
	return $aLanguages;
	
}

function gcRenameProductImage($old_model, $new_model) {

	if (in_array(substr($old_model, -1), array('L', 'S', 'K', 'C')))
			$old_model = substr($old_model, 0, -1);
	
	if (in_array(substr($new_model, -1), array('L', 'S', 'K', 'C')))
			$new_model = substr($new_model, 0, -1);	
	
	if($new_model != $old_model) {
	
		rename(DIR_FS_CATALOG.'images/ties/'.$old_model.'_sm.jpg', DIR_FS_CATALOG.'images/ties/'.$new_model.'_sm.jpg');
		rename(DIR_FS_CATALOG.'images/ties/'.$old_model.'_md.jpg', DIR_FS_CATALOG.'images/ties/'.$new_model.'_md.jpg');		
	}	
}

function getLastShoppingUrlFromNavigation(){
		
	global $navigation;
	
	if(is_object($navigation)  &&  !empty($navigation->path)){
		$history = array_reverse($navigation->path);

		foreach($history as $elem):
			if(empty($elem['get']['products_id'])  &&  $elem['page'] != 'shopping_cart.php'  &&  !strstr($elem['page'], 'ajax')  &&  !strstr($elem['page'], 'checkout')){
					
				$url = $elem['page'];
				if($elem['get']){
					$url .= (strstr($url, '?')) ? '&' : '?';
					foreach($elem['get'] as $k=>$v):
						
						if(strstr($url, $k.'='))   continue;
						$url .= $k.'='.$v.'&';
					endforeach;
				}
				return($url);
				break;
			}
		endforeach;
	}
	
	return(NULL);
}

function detectMobileEnddevice(){
	
	if($_SESSION['is_mobile_enddevice'] === true){
		return(true);
	}
	
	$regexMobile = '/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/';
	
	if(preg_match($regexMobile, $_SERVER['HTTP_USER_AGENT'])){
		return(true);
	}
	
	return(false);
}


function correctDBtexterrors( $language, $text)
{
	// correct errors, e.g. '?' within the text

	if ( $language == "fr" ) {

		$search_array = array("\'", "'", "`", "’", "‘", " l?", " L?", " qu?", " Qu?", " d?", " D?");
		$replace_array = array("'", "'", "'", "'", "'", " l'", " L'", " qu'", " Qu'", " d'", " D'");
		$text = str_replace($search_array, $replace_array, $text);

	} elseif ( $language == "it" ) {

		$search_array = array("`", "’", "‘", " l?", " L?", "all?", "ell?");
		$replace_array = array("'", "'", "'", " l'", " L'", "all'", "ell'");
		$text = str_replace($search_array, $replace_array, $text);

	}

	return($text);
}




?>