<?PHP

// letzte Aktualisierung 13.6.16

// Daten von DB f�r ElasticSearch umwandeln
function DB2ES( $text, $encode = true ){
    $text = strip_tags( $text );
    if( $encode ){
        $text = utf8_encode( $text );
    }
    $text = html_entity_decode( $text, ENT_COMPAT | ENT_HTML401, "UTF-8" );
    // $search = array("'", '"', "|", "\n", "\r");
    // $replace = array("\'", '\"', " ", " ", " ");
    $search = array('"', "|", "\n", "\r");
    $replace = array('\"', " ", " ", " ");
    $text = str_replace($search, $replace, $text);
    $text = trim($text);

    return $text;
}


// Anbindung an die ElasticSearch-Suche
function ElasticSearch( $PATH, $POST_FIELD, $METHOD = 'GET' ) {

    $URL = 'localhost:9200/'.$PATH;

    if( $METHOD == 'GET' ){
        if( strpos($PATH, '?') === false ){
            $URL .= '?pretty';
        }else{
           $URL .= '&pretty';
        }
    }

    $ch = curl_init( $URL );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $METHOD);
    if( $POST_FIELD != "" ){
        curl_setopt($ch, CURLOPT_POSTFIELDS, $POST_FIELD);
    }
    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}


// Typ / Variante anhand des Modells bestimmen
function ProductType( $products_model, $div = false, $link = false, $icon = false, $class = '', $image_add = '', $lang = 'de' ){
    $products_model_1 = substr($products_model, -1);
    $products_model_2 = substr($products_model, -2);
    $products_model_3 = substr($products_model, 0, 2);
    $output = "";

    if($div ){
        $output .= "<div>";
    }

    // Sonderfall extra schmale Krawatten
    $extraschmal = array(
                                                "CS1082", "CS1083", "CS1084",
                            "CS1085", "CS1086", "CS1087", "CS1088", "CS1089",
                            "CS1090", "CS1091", "CS1092",
                                                                    "CS7064",
                            "CS7065", "CS7066", "CS7067", "CS7068", "CS7069",
                            "CS7070", "CS7071", "CS7072", "CS7073", "CS7074",
                            "CS7075", "CS7076", "CS7077",
                                                          "CS7118", "CS7119",
                            "CS7120", "CS7121", "CS7122", "CS7123", "CS7124",
                            "CS7125", "CS7126", "CS7127", "CS7128", "CS7129",
                            "CS7130", "CS7131",
                            "DA5509"
    );

    if (in_array($products_model, $extraschmal)) {
        // schmale Krawatte
        if( $icon ){ $output .= '<img src="'.$icon.'schmal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=60&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_NARROW') && PRODUCT_INFO_NARROW != "" ){
            $output .= PRODUCT_INFO_NARROW;
        }else{
            if($lang == 'de') {
                $output .= "Extra schmale Krawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata extra estrecha";
            }elseif( $lang == 'fr') {
                $output .= "Cravate extra �troites";
            }elseif( $lang == 'nl') {
                $output .= "Extra smalle stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta extra stretta";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif( $products_model_3 == "MP" ){
        // Manschettenknoepfe
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=98&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_CUFFLINKS') && PRODUCT_INFO_CUFFLINKS != "" ){
            $output .= PRODUCT_INFO_CUFFLINKS;
        }else{
            if($lang == 'de') {
                $output .= "Manschettenkn�pfe";
            }elseif( $lang == 'es') {
                $output .= "Gemelos";
            }elseif( $lang == 'fr') {
                $output .= "Boutons de manchette";
            }elseif( $lang == 'nl') {
                $output .= "Manchetknopen";
            }elseif( $lang == 'it') {
                $output .= "Gemelli";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif( $products_model_3 == "KN" ) {
        // Krawattennadel / Krawattenspange
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=1606&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_TIECLIP') && PRODUCT_INFO_TIECLIP != "" ){
            $output .= PRODUCT_INFO_TIECLIP;
        }else{
            if($lang == 'de') {
                $output .= "Krawattennadel / -spange";
            }elseif( $lang == 'es') {
                $output .= "Alfiler de corbata";
            }elseif( $lang == 'fr') {
                $output .= "�pingle de cravate";
            }elseif( $lang == 'nl') {
                $output .= "Stropdasclip";
            }elseif( $lang == 'it') {
                $output .= "Spillo da cravatta";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif(!in_array($products_model_1, array('L', 'E', 'C', 'S', 'K', 'T', 'D', 'Y', 'F', 'X', 'P', 'Q', 'G'))) {
        // Normale Krawatte tep_href_link('product_info.php','cPath=21&products_id='. $relatet_product['products_id'])
        if( $icon ){ $output .= '<img src="'.$icon.'normal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=367&products_id='. $link, 'SSL').'">';}
        if( defined('PRODUCT_INFO_NORMALSIZE') && PRODUCT_INFO_NORMALSIZE != "" ){
            $output .= PRODUCT_INFO_NORMALSIZE;
        }else{
            if($lang == 'de') {
                $output .= "Krawatte mit Standardma�en";
            }elseif( $lang == 'es') {
                $output .= "Corbata normales";
            }elseif( $lang == 'fr') {
                $output .= "Cravate normale";
            }elseif( $lang == 'nl') {
                $output .= "Normale stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta normale";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'L') {
        // XXL Krawatte
        if( $icon ){ $output .= '<img src="'.$icon.'xxl'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=21&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_XXLSIZE') && PRODUCT_INFO_XXLSIZE != "" ){
            $output .= PRODUCT_INFO_XXLSIZE;
        }else{
            if($lang == 'de') {
                $output .= "XXL-Krawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata XXL";
            }elseif( $lang == 'fr') {
                $output .= "Cravate XXL";
            }elseif( $lang == 'nl') {
                $output .= "XXL stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta XXL";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'E') {
        //Easy-Tie
        if( $icon ){ $output .= '<img src="'.$icon.'easytie'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=801&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_EASYTIE') && PRODUCT_INFO_EASYTIE != "" ){
            $output .= PRODUCT_INFO_EASYTIE;
        }else{
            if($lang == 'de') {
                $output .= "Easy-Tie Krawatte";
            }elseif( $lang == 'es') {
                $output .= "Easy-Tie corbata";
            }elseif( $lang == 'fr') {
                $output .= "Easy-Tie cravate";
            }elseif( $lang == 'nl') {
                $output .= "Easy-Tie stropdas";
            }elseif( $lang == 'it') {
                $output .= "Easy-Tie cravatta";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'C') {
        // Clipkrawatte
        if( $icon ){ $output .= '<img src="'.$icon.'clip'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=61&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_CLIP') && PRODUCT_INFO_CLIP != "" ){
            $output .= PRODUCT_INFO_CLIP;
        }else{
            if($lang == 'de') {
                $output .= "Clip-Krawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata clip";
            }elseif( $lang == 'fr') {
                $output .= "Cravate � clip";
            }elseif( $lang == 'nl') {
                $output .= "Clip stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta clip";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'S') {
        // schmale Krawatte
        if( $icon ){ $output .= '<img src="'.$icon.'schmal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=60&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_NARROW') && PRODUCT_INFO_NARROW != "" ){
            $output .= PRODUCT_INFO_NARROW;
        }else{
            if($lang == 'de') {
                $output .= "Schmale Krawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata estrecha";
            }elseif( $lang == 'fr') {
                $output .= "Cravate �troites";
            }elseif( $lang == 'nl') {
                $output .= "Smalle stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta stretta";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'K') {
        // Kinderkrawatte
        if( $icon ){ $output .= '<img src="'.$icon.'kind'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=62&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_KIDS') && PRODUCT_INFO_KIDS != "" ){
            $output .= PRODUCT_INFO_KIDS;
        }else{
            if($lang == 'de') {
                $output .= "Kinderkrawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata para ni�o";
            }elseif( $lang == 'fr') {
                $output .= "Cravate enfants";
            }elseif( $lang == 'nl') {
                $output .= "Kinder stropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta per bambini";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'T') {
        // T�cher
        if( $icon ){ $output .= '<img src="'.$icon.'tuch'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=51&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_POCKETSQUARES') && PRODUCT_INFO_POCKETSQUARES != "" ){
            $output .= PRODUCT_INFO_POCKETSQUARES;
        }else{
            if($lang == 'de') {
                $output .= "Einstecktuch";
            }elseif( $lang == 'es') {
                $output .= "Pa�uelo";
            }elseif( $lang == 'fr') {
                $output .= "Pochette";
            }elseif( $lang == 'nl') {
                $output .= "Pochet";
            }elseif( $lang == 'it') {
                $output .= "Fazzoletto da taschino";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'D') {
        // Damentuch / Halstuch
        if( $icon ){ $output .= '<img src="'.$icon.'tuch'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=237&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_WOMENSCARF') && PRODUCT_INFO_WOMENSCARF != "" ){
            $output .= PRODUCT_INFO_WOMENSCARF;
        }else{
            if($lang == 'de') {
                $output .= "Halstuch";
            }elseif( $lang == 'es') {
                $output .= "Foulard";
            }elseif( $lang == 'fr') {
                $output .= "Foulard";
            }elseif( $lang == 'nl') {
                $output .= "Damessjaal";
            }elseif( $lang == 'it') {
                $output .= "Fazzoletto per donne";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'Y') {
        // Damenkrawatte
        if( $icon ){ $output .= '<img src="'.$icon.'damen'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=508&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_WOMENTIE') && PRODUCT_INFO_WOMENTIE != "" ){
            $output .= PRODUCT_INFO_WOMENTIE;
        }else{
            if($lang == 'de') {
                $output .= "Damenkrawatte";
            }elseif( $lang == 'es') {
                $output .= "Corbata mujer";
            }elseif( $lang == 'fr') {
                $output .= "Cravates pour femmes";
            }elseif( $lang == 'nl') {
                $output .= "Damesstropdas";
            }elseif( $lang == 'it') {
                $output .= "Cravatta da donna";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'F') {
        // Fliegen
        if( $products_model_2 == 'KF' ){
            // Kinderfliegen
            if( $icon ){ $output .= '<img src="'.$icon.'fliege'.$image_add.'.gif" class="'.$class.'" />'; }
            if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=52&products_id='. $link, 'SSL').'">'; }
            if( defined('PRODUCT_INFO_BOWTIE_KIDS') && PRODUCT_INFO_BOWTIE_KIDS != "" ){
                $output .= PRODUCT_INFO_BOWTIE_KIDS;
            }else{
                if($lang == 'de') {
                    $output .= "Kinderfliege";
                }elseif( $lang == 'es') {
                    $output .= "Pajarita para ni�o";
                }elseif( $lang == 'fr') {
                    $output .= "Noeud papillon enfant";
                }elseif( $lang == 'nl') {
                    $output .= "Kinder vlinderdas";
                }elseif( $lang == 'it') {
                    $output .= "Papillon per bambini";
                }
            }
            if( $link ){ $output .= '</a>'; }
        }elseif( $products_model_2 == 'SF'){
            // Selbstbindefliegen
            if( $icon ){ $output .= '<img src="'.$icon.'fliege'.$image_add.'.gif" class="'.$class.'" />'; }
            if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=605&products_id='. $link, 'SSL').'">'; }
            if( defined('PRODUCT_INFO_BOWTIE_SELFTIED') && PRODUCT_INFO_BOWTIE_SELFTIED != "" ){
                $output .= PRODUCT_INFO_BOWTIE_SELFTIED;
            }else{
                if($lang == 'de') {
                    $output .= "Selbstbindefliege";
                }elseif( $lang == 'es') {
                    $output .= "Pajarita sin anudar";
                }elseif( $lang == 'fr') {
                    $output .= "Noeud papillon � nouer";
                }elseif( $lang == 'nl') {
                    $output .= "Zelfstrikker vlinderdas";
                }elseif( $lang == 'it') {
                    $output .= "Papillon da annodare";
                }
            }
            if( $link ){ $output .= '</a>'; }
        }else{
            // Herrenfliege
            if( $icon ){ $output .= '<img src="'.$icon.'fliege'.$image_add.'.gif" class="'.$class.'" />'; }
            if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=52&products_id='. $link, 'SSL').'">'; }
            if( defined('PRODUCT_INFO_BOWTIE') && PRODUCT_INFO_BOWTIE != "" ){
                $output .= PRODUCT_INFO_BOWTIE;
            }else{
                if($lang == 'de') {
                    $output .= "Fliege";
                }elseif( $lang == 'es') {
                    $output .= "Pajarita";
                }elseif( $lang == 'fr') {
                    $output .= "Noeud papillon";
                }elseif( $lang == 'nl') {
                    $output .= "Vlinderdas";
                }elseif( $lang == 'it') {
                    $output .= "Farfalla";
                }
            }
            if( $link ){ $output .= '</a>'; }
        }
    }elseif($products_model_1 == 'X') {
        // Krawattenschal
        if( $icon ){ $output .= '<img src="'.$icon.'schal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=296&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_TIESCARF') && PRODUCT_INFO_TIESCARF != "" ){
            $output .= PRODUCT_INFO_TIESCARF;
        }else{
            if($lang == 'de') {
                $output .= "Krawattenschal";
            }elseif( $lang == 'es') {
                $output .= "Corbata-pa�uelo";
            }elseif( $lang == 'fr') {
                $output .= "Cravate lavalli�re";
            }elseif( $lang == 'nl') {
                $output .= "Stropdas sjaal";
            }elseif( $lang == 'it') {
                $output .= "Cravatta sciarpa";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'P') {
        // ASCOT
        if( $icon ){ $output .= '<img src="'.$icon.'ascot'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=296&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_ASCOT') && PRODUCT_INFO_ASCOT != "" ){
            $output .= PRODUCT_INFO_ASCOT;
        }else{
            if($lang == 'de') {
                $output .= "Ascot";
            }elseif( $lang == 'es') {
                $output .= "Ascot";
            }elseif( $lang == 'fr') {
                $output .= "Ascot";
            }elseif( $lang == 'nl') {
                $output .= "Ascot";
            }elseif( $lang == 'it') {
                $output .= "Ascot";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_2 == 'CQ') {
        // Damenschal/Chorschal neu
        if( $icon ){ $output .= '<img src="'.$icon.'damenschal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=1200&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_CHOIRSCARF_CHIFFON') && PRODUCT_INFO_CHOIRSCARF_CHIFFON != "" ){
            $output .= PRODUCT_INFO_CHOIRSCARF_CHIFFON;
        }else{
            if($lang == 'de') {
                $output .= "Damenschal (Chiffon)";
            }elseif( $lang == 'es') {
                $output .= "Bufanda coro (Gasa)";
            }elseif( $lang == 'fr') {
                $output .= "Echarpe choir (Chiffon)";
            }elseif( $lang == 'nl') {
                $output .= "Choir sjaal (Chiffon)";
            }elseif( $lang == 'it') {
                $output .= "Sciarpa choir (Chiffon)";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_2 == 'SQ') {
        // Damenschal/Chorschal neu
        if( $icon ){ $output .= '<img src="'.$icon.'damenschal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=1200&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_CHOIRSCARF_SILKCHIFFON') && PRODUCT_INFO_CHOIRSCARF_SILKCHIFFON != "" ){
            $output .= PRODUCT_INFO_CHOIRSCARF_SILKCHIFFON;
        }else{
            if($lang == 'de') {
                $output .= "Damenschal (Seiden-Chiffon)";
            }elseif( $lang == 'es') {
                $output .= "Bufanda coro (Gasa de seda)";
            }elseif( $lang == 'fr') {
                $output .= "Echarpe choir (Mousseline de soie)";
            }elseif( $lang == 'nl') {
                $output .= "Choir sjaal (Zijde chiffon)";
            }elseif( $lang == 'it') {
                $output .= "Sciarpa choir (Chiffon di seta)";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'Q') {
        // Damenschal/Chorschal neu
        if( $icon ){ $output .= '<img src="'.$icon.'damenschal'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=1200&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_CHOIRSCARF') && PRODUCT_INFO_CHOIRSCARF != "" ){
            $output .= PRODUCT_INFO_CHOIRSCARF;
        }else{
            if($lang == 'de') {
                $output .= "Damenschal (Satin)";
            }elseif( $lang == 'es') {
                $output .= "Bufanda coro (Sat�n)";
            }elseif( $lang == 'fr') {
                $output .= "Echarpe choir (Satin)";
            }elseif( $lang == 'nl') {
                $output .= "Choir sjaal (Satijn)";
            }elseif( $lang == 'it') {
                $output .= "Sciarpa choir (Raso)";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }elseif($products_model_1 == 'G') {
        // Gummizug
        if( $icon ){ $output .= '<img src="'.$icon.'gummizug'.$image_add.'.gif" class="'.$class.'" />'; }
        if( $link ){ $output .= '<a href="'.tep_href_link('product_info.php','cPath=61&products_id='. $link, 'SSL').'">'; }
        if( defined('PRODUCT_INFO_ELASTICBAND') && PRODUCT_INFO_ELASTICBAND != "" ){
            $output .= PRODUCT_INFO_ELASTICBAND;
        }else{
            if($lang == 'de') {
                $output .= "Krawatte mit Gummizug";
            }elseif( $lang == 'es') {
                $output .= "Corbata con el�stico";
            }elseif( $lang == 'fr') {
                $output .= "Cravate �lastique";
            }elseif( $lang == 'nl') {
                $output .= "Stropdas met elastiek";
            }elseif( $lang == 'it') {
                $output .= "Cravatta con elastico";
            }
        }
        if( $link ){ $output .= '</a>'; }
    }

    if($div ){
        $output .= "</div>";
    }

    return $output;
}

?>