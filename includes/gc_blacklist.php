<?php
//Blacklist - Modul 
//Copyright by Gurkcity 2007/2008
//Stand 03.01.2008

//Funktion zum S�ubern eines Strings, damit Werte vergleichbar werden
function gcBlacklistFieldClean ($string)
{
	//alles kleinschreiben
	$string_new1 = strtolower($string);
	
	//Leerzeichen entfernen
	$string_new2 = preg_replace('/ /','',$string_new1);
	

	return $string_new2;
}

//Beim ersten Besuch Session registrieren, um referrer festzuhalten
//Reason 1
//gibt true aus, wenn Referer mit bestimmten Phrasen erkannt wurden, ansonsten false
function checkBlacklistReferer ()
{
	global $kill_sid;
	
	if(!tep_session_is_registered('ref') || !tep_session_is_registered('blacklist_points') || !tep_session_is_registered('blacklist_reason'))
	{
		tep_session_register('ref');
		tep_session_register('blacklist_points');
		tep_session_register('blacklist_reason');
		tep_session_register('blacklist_comment');
		
		
		//Referrer Check
		$_SESSION['ref'] = $_SERVER['HTTP_REFERER'];
		
	}
	
	//Referer-Text pr�fen
	#if(preg_match('/kauf\+auf\+rechnung/', $_SESSION['ref']))
	if(preg_match('/rechnung/', strtolower($_SESSION['ref'])))
	{
		#echo 'treffer';
		
			
		//Blacklist-Points vergeben
		$_SESSION['blacklist_points'] = 100;
		$_SESSION['blacklist_reason'] = 1;
		$_SESSION['blacklist_comment'] = '<a href="'.$_SESSION['ref'].'" target="_blank" class="boese">Referer</a>';
		
		//auch bei deaktivierten Cookies die Parameter weiterleiten
		$_SESSION['kill_sid'] = false;
	
		return true;
			
	}
	
	return false;
	
	
}
//Referer-Pr�fung auf allen Seiten jetzt durchf�hren
checkBlacklistReferer ();


//gleiche Versandadresse, verschieden Personen?
//Reason 2
//gibt $oid-liste aus, wenn gleiche Adressen mit unterschiedlichem Namen auftreten, ansonsten false
function checkBlacklistAddress($delivery_name, $delivery_street_address, $delivery_postcode, $delivery_city)
{
	
	$sql = "SELECT orders_id, delivery_name, delivery_street_address, delivery_postcode, delivery_city
			FROM orders";
	
	
	$bl_orders_query = tep_db_query($sql);
	
	//$bl_orders_array nimmt alle orders auf, die verd�chtig sind
	$bl_orders_array = array();
	
	while($bl_orders = tep_db_fetch_array($bl_orders_query))
	{
		//Pr�fen, ob die aktuelle Adresse in der Vergangenheit bei Bestellungen angegeben wurde, aber unterschiedliche Namen!
		if(
			gcBlacklistFieldClean($bl_orders['delivery_street_address']) == gcBlacklistFieldClean($delivery_street_address) 
			 && gcBlacklistFieldClean($bl_orders['delivery_postcode']) == gcBlacklistFieldClean($delivery_postcode) 
			 && gcBlacklistFieldClean($bl_orders['delivery_city']) == gcBlacklistFieldClean($delivery_city) 
			 && gcBlacklistFieldClean($bl_orders['delivery_name']) != gcBlacklistFieldClean($delivery_name)
			)
		{
			$bl_orders_array[] = $bl_orders['orders_id'];			
		}
	}
	
	#go_dump($bl_orders_array);
	
	if(count($bl_orders_array) > 0)//Treffer?
	{	
		//Blacklist-Points vergeben
		$_SESSION['blacklist_points'] = 90;
		$_SESSION['blacklist_reason'] = 2;
		
		//Blacklist-OID-Liste erzeugen, mit allen verd�chtigen orders der Vergangenheit
		$bl_orders_list = implode(',',$bl_orders_array);
		
		//Liste zur�ckgeben
		return $bl_orders_list;
	}
	else
	{
		return false;
	}
		
}

//gleiche Rechnungsadresse, verschieden Personen? #CG20080819
//Reason 6 (neu in DB INSERT INTO `db170513580`.`gc_blacklist_reason` (`id` ,`reasonId` ,`reasonText` ,`points`) VALUES (NULL , '6', 'Rechnungsadresse gleich, Name neu? ', '100');
//gibt $oid-liste aus, wenn gleiche Adressen mit unterschiedlichem Namen auftreten, ansonsten false
function checkBlacklistBillingAddress($customers_name, $customers_street_address, $customers_postcode, $customers_city)
{
	
	$sql = "SELECT orders_id, customers_name, customers_street_address, customers_postcode, customers_city
			FROM orders";
	
	
	$bl_orders_query = tep_db_query($sql);
	
	//$bl_orders_array nimmt alle orders auf, die verd�chtig sind
	$bl_orders_array = array();
	
	while($bl_orders = tep_db_fetch_array($bl_orders_query))
	{
		//Pr�fen, ob die aktuelle Adresse in der Vergangenheit bei Bestellungen angegeben wurde, aber unterschiedliche Namen!
		if(
			gcBlacklistFieldClean($bl_orders['customers_street_address']) == gcBlacklistFieldClean($customers_street_address) 
			 && gcBlacklistFieldClean($bl_orders['customers_postcode']) == gcBlacklistFieldClean($customers_postcode) 
			 && gcBlacklistFieldClean($bl_orders['customers_city']) == gcBlacklistFieldClean($customers_city) 
			 && gcBlacklistFieldClean($bl_orders['customers_name']) != gcBlacklistFieldClean($customers_name)
			)
		{
			$bl_orders_array[] = $bl_orders['orders_id'];			
		}
	}
	
	#go_dump($bl_orders_array);
	
	if(count($bl_orders_array) > 0)//Treffer?
	{	
		//Blacklist-Points vergeben
		$_SESSION['blacklist_points'] = 100;
		$_SESSION['blacklist_reason'] = 6;
		
		//Blacklist-OID-Liste erzeugen, mit allen verd�chtigen orders der Vergangenheit
		$bl_orders_list = implode(',',$bl_orders_array);
		
		//Liste zur�ckgeben
		return $bl_orders_list;
	}
	else
	{
		return false;
	}
		
}

//mehrmals hintereinander bestellt und Bestellungen noch offen?
//Reason 3
//gibt $oid-liste aus, wenn mehrmals hintereinander bestellt wurde, aber noch nicht bezahlt wurde, Abgleich �ber Mailadresse, ansonsten false
function checkOrderHistoryByMail($oid)
{
	$customer_order_mail = getOrderEmail($oid);
	
	$sql = "SELECT orders_id
			FROM orders
			WHERE customers_email_address = '".$customer_order_mail."'";
	
	$bl_orders_query = tep_db_query($sql);
	
	//$bl_orders_array nimmt alle orders auf, die verd�chtig sind
	$bl_orders_array = array();
	$_SESSION['blacklist_points'] = 0;
	
	//Gibt es mehr als eine Bestellung? Dann weiterpr�fen
	if(tep_db_num_rows($bl_orders_query) > 1)
	{
		
		//Pr�fen ob eine vergangene Bestellung noch unbezahlt ist
		while($bl_orders = tep_db_fetch_array($bl_orders_query))
		{
			
			//nur bei Bestellungen, die nicht die aktuelle $oid betreffen
			if($oid != $bl_orders['orders_id'])
			{
			
				//Pr�fen, ob die alten Bestellungen angemahnt sind
				//7 bedeutet bezahlt
				if(getLastOrderStatus($bl_orders['orders_id']) > 7)
				{
				
					//Blacklist-Points vergeben
					$_SESSION['blacklist_points'] += 200;
					$bl_orders_array[] =  $bl_orders['orders_id'];
				}
				
				//Pr�fen, ob die alten Bestellungen noch offen sind
				//7 bedeutet bezahlt
				else if(getLastOrderStatus($bl_orders['orders_id']) < 7)
				{
				
					//Blacklist-Points vergeben
					$_SESSION['blacklist_points'] += 50;
					$bl_orders_array[] =  $bl_orders['orders_id'];
				}
			}
					
		}
		
		if(count($bl_orders_array) > 0)//Treffer?
		{	
			//Blacklist-Reason vergeben
			$_SESSION['blacklist_reason'] = 3;
			
			//Blacklist-OID-Liste erzeugen, mit allen verd�chtigen orders der Vergangenheit
			$bl_orders_list = implode(',',$bl_orders_array);
			
			//Liste zur�ckgeben
			return $bl_orders_list;
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false; //noch keine Bestellung vorher dagewesen
	}
		
}

//gleicher Name, aber neue Adresse
//Reason 4
//gibt $oid-liste aus, wenn gleiche Namen mit unterschiedlichen Adressen auftreten, ansonsten false
function checkBlacklistName($delivery_name, $delivery_street_address, $delivery_postcode, $delivery_city)
{
	

	$sql = "SELECT orders_id, delivery_name, delivery_street_address, delivery_postcode, delivery_city
			FROM orders";
	
	
	$bl_orders_query = tep_db_query($sql);
	
	//$bl_orders_array nimmt alle orders auf, die verd�chtig sind
	$bl_orders_array = array();
	
	while($bl_orders = tep_db_fetch_array($bl_orders_query))
	{
		//Pr�fen, ob die aktueller Name in der Vergangenheit bei Bestellungen angegeben wurde, aber unterschiedliche Adressen!
		if(
			(
			    gcBlacklistFieldClean($bl_orders['delivery_street_address']) != gcBlacklistFieldClean($delivery_street_address) 
			 && gcBlacklistFieldClean($bl_orders['delivery_postcode']) != gcBlacklistFieldClean($delivery_postcode) 
			 && gcBlacklistFieldClean($bl_orders['delivery_city']) != gcBlacklistFieldClean($delivery_city) 
			 )
			 && gcBlacklistFieldClean($bl_orders['delivery_name']) == gcBlacklistFieldClean($delivery_name)
			)
		{
			$bl_orders_array[] = $bl_orders['orders_id'];	
			#echo $bl_orders['delivery_street_address'].$delivery_street_address;
			#exit;		
		}
	}
	
	#go_dump($bl_orders_array);
	
	if(count($bl_orders_array) > 0)//Treffer?
	{	
		//Blacklist-Points vergeben
		$_SESSION['blacklist_points'] = 20;
		$_SESSION['blacklist_reason'] = 4;
		
		//Blacklist-OID-Liste erzeugen, mit allen verd�chtigen orders der Vergangenheit
		$bl_orders_list = implode(',',$bl_orders_array);
		
		//Liste zur�ckgeben
		return $bl_orders_list;
	}
	else
	{
		return false;
	}
		
}


//gleicher Name, gleiche Adresse, aber neue E-Mail
//Reason 5
//gibt $oid-liste aus, wenn gleiche Namen mit gleichen Adressen, aber unterschiedlicher E-Mail auftreten, ansonsten false
function checkBlacklistNameAdress($delivery_name, $delivery_street_address, $delivery_postcode, $delivery_city, $customers_email_address)
{
	

	$sql = "SELECT orders_id, delivery_name, delivery_street_address, delivery_postcode, delivery_city, customers_email_address
			FROM orders";
	
	
	$bl_orders_query = tep_db_query($sql);
	
	//$bl_orders_array nimmt alle orders auf, die verd�chtig sind
	$bl_orders_array = array();
	
	while($bl_orders = tep_db_fetch_array($bl_orders_query))
	{
		/*if(defined(DEBUG))
		{
			echo  '<h1>'.gcBlacklistFieldClean($bl_orders['customers_email_address']).'</h1>'.
			gcBlacklistFieldClean($bl_orders['delivery_street_address']) .'=='. gcBlacklistFieldClean($delivery_street_address) .'<br>'.
			  gcBlacklistFieldClean($bl_orders['delivery_postcode']) .'=='. gcBlacklistFieldClean($delivery_postcode)  .'<br>'.
			  gcBlacklistFieldClean($bl_orders['delivery_city']) .'=='. gcBlacklistFieldClean($delivery_city)  .'<br>'.
			  gcBlacklistFieldClean($bl_orders['delivery_name']) .'=='. gcBlacklistFieldClean($delivery_name) .'<br>'.
			  gcBlacklistFieldClean($bl_orders['customers_email_address']) .'=='. gcBlacklistFieldClean($customers_email_address) .'<br><br>';
		}*/
		//Pr�fen, ob die aktueller Name in der Vergangenheit bei Bestellungen angegeben wurde, aber unterschiedliche Adressen!
		if(
			    gcBlacklistFieldClean($bl_orders['delivery_street_address']) == gcBlacklistFieldClean($delivery_street_address) 
			 && gcBlacklistFieldClean($bl_orders['delivery_postcode']) == gcBlacklistFieldClean($delivery_postcode) 
			 && gcBlacklistFieldClean($bl_orders['delivery_city']) == gcBlacklistFieldClean($delivery_city) 
			 && gcBlacklistFieldClean($bl_orders['delivery_name']) == gcBlacklistFieldClean($delivery_name)
			 && gcBlacklistFieldClean($bl_orders['customers_email_address']) != gcBlacklistFieldClean($customers_email_address)
			)
		{
			$bl_orders_array[] = $bl_orders['orders_id'];	
			#echo $bl_orders['customers_email_address'].$customers_email_address;
			#exit;
		}
	}
	
	#go_dump($bl_orders_array);
	
	if(count($bl_orders_array) > 0)//Treffer?
	{	
		//Blacklist-Points vergeben
		$_SESSION['blacklist_points'] = 20;
		$_SESSION['blacklist_reason'] = 5;
		
		//Blacklist-OID-Liste erzeugen, mit allen verd�chtigen orders der Vergangenheit
		$bl_orders_list = implode(',',$bl_orders_array);
		
		//Liste zur�ckgeben
		return $bl_orders_list;
	}
	else
	{
		return false;
	}
		
}


//Hilfsfunktionen

//Abgleich Stra�e -> Name?
function checkStreetAndName($street)
{
	$sql = "SELECT *
			FROM `orders`
			WHERE `customers_street_address` LIKE '".$street."'
			GROUP BY customers_name";
	if(tep_db_num_rows(tep_db_query($sql))> 1)
	
	return true;
}

//Mailadresse aus Order holen
function getOrderEmail($oid)
{
	$sql = "SELECT customers_email_address
			FROM `orders`
			WHERE `orders_id` = '".$oid."'";
	$ordersmail_qry = tep_db_query($sql);
	while($ordermail = tep_db_fetch_array($ordersmail_qry))
	{
		$customer_order_mail = $ordermail['customers_email_address'];
	}
	
	return $customer_order_mail;
}

//Orderstatus aus Order holen
function getLastOrderStatus($oid)
{
	$sql = "SELECT orders_status_id
			FROM `orders_status_history`
			WHERE `orders_id` = '".$oid."'
			ORDER BY date_added ASC";
			
	#echo $sql;
			
	$ordersstatus_qry = tep_db_query($sql);
	while($ordersstatus = tep_db_fetch_array($ordersstatus_qry))
	{
		$orders_status_id = $ordersstatus['orders_status_id'];
	}
	
	#echo '<br>'. $orders_status_id . '<br>';
	
	return $orders_status_id;
}

//in Blackliste einsetzen
function insertBlacklist($oid, $black, $white, $points, $reason, $comment = '',$problemOid)
{
	$sql_data_array = array('date' => 'now()',
							'oid' => $oid,
							'black' => $black,
							'white' => $white,
							'points' => $points,
							'reason' => $reason,
							'comment' => $comment,
							'problemOid' => $problemOid
							);
	
	tep_db_perform('gc_blacklist',$sql_data_array);
}



?>