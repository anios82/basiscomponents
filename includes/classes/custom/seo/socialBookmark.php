<?

class seoSocialbookmark{	

	var $picsPath;		  
	var $url;
	var $title;
	var $imgHeight = false;
	var $imgSmall = false;
	//var $overOutgoingClick = false;		
	var  $services = array(
		
		/*'mister_wong' => array(
						'url' => 'http://www.mister-wong.com/add_url/?action=addurl&bm_url=@URL@&bm_description=@TITLE@&plugin=soc',
						'name' => 'Mister Wong', 
						'ico' => 'wong.png',
						'icoSmall' => 'wongSmall.png'
						),*/
						
		'delicious' => array(
						'url' => 'http://del.icio.us/post?url=@URL@&amp;title=@TITLE@',
						'name' => 'Del.icio.us',
						'ico' => 'icio.png',
						'icoSmall' => 'icioSmall.png',
						),	

		'technorati' => array(
						'url' => 'http://technorati.com/faves?add=@URL@',
						'name' => 'Technorati', 
						'ico' => 'technorati.png',
						'icoSmall' => 'technoratiSmall.png',
						),

		'digg' => array(
						'url' => 'http://digg.com/submit?phase=2&amp;url=@URL@&amp;title=@TITLE@',
						'name' => 'Digg',
						'ico' => 'digg.png',
						'icoSmall' => 'diggSmall.png'
						),
		
		'myshare' => array(
						'url' => 'http://myweb2.search.yahoo.com/myresults/bookmarklet?u=@URL@',
						'name' => 'YahooMyWeb', 
						'ico' => 'yahoo.png',
						'icoSmall' => 'yahooSmall.png'
						  ),
						  
		'stumbleupon' => array(
						'url' => 'http://www.stumbleupon.com/submit?url=@URL@&amp;title=@TITLE@',
						'name' => 'StumbleUpon', 
						'ico' => 'stumbleupon.png',
						'icoSmall' => 'stumbleuponSmall.png'
						  ), 
			
			  );
	
	var $texts = array('de' => 
					  array('add'=>'@SERVICE@'),
					  'en' => 
					  array('add'=>'@SERVICE@')
			  );
	
		
	function generateLinks()
	{
		if(empty($this->picsPath) || empty($this->title) || empty($this->url))
		return(false);

		foreach($this->services as $service=>$data)
		{
			$url = str_replace(
					array('@URL@', '@TITLE@'),
					array(rawurlencode($this->url), rawurlencode($this->title)),
					$data['url']
			);
			
			/*if($this->overOutgoingClick == true)
			{
				$outClickUrl = Traffic::getUrlOutgoingClick();
				
				if(!empty($outClickUrl))
				$url = str_replace(array(Traffic::Ident_type, Traffic::Ident_url), array('socialBookmark', urlencode($url)), $outClickUrl);
			}*/
			
			$alt = str_replace(
					array('@TITLE@', '@SERVICE@'),
					array($this->title, $data['name']),
					$this->texts['de']['add']
			);
						
			$link = '<a title="'.$alt.'" href="'.$url.'" target="_blank"><img '.(($this->imgHeight) ? 'height = "'.$this->imgHeight.'"'  : '').' src="'.$this->picsPath.'/'.((!$this->imgSmall) ? $data['ico'] : $data['icoSmall']).'" border="0" alt="'.$alt.'" /></a>';
		
			$arLinks[] = $link;
		}
		
		return($arLinks);
	}
	
}

?>