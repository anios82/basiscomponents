<?

class seoBlogsHandle {
  
  
  private static  $DB;
  private static  $instance;
  
  private  $blogID;
  
  const  imagesWebPath = 'forum/static/docsPics';
  
  public static function _getInstance($aBlogID = NULL){
    
        $c = __CLASS__;
        
        if(!self::$instance instanceof $c){
           self::$instance = new $c($aBlogID);  
        }
        
        return(self::$instance);          
  }
  
  private function __construct($aBlogID = NULL){
    
    $this->doEnvironmentChecks();
    
    $this->blogID = $aBlogID;
    
    $this->connectBlogDb();
  }
  
  public function doEnvironmentChecks(){
    
    // Database constants set?
    if(!defined('DB_SERVER_CMS')  ||  !defined('DB_SERVER_USERNAME_CMS')  ||  !defined('DB_SERVER_PASSWORD_CMS')  ||  !defined('DB_DATABASE_CMS')){
      die('Database configuration constants not set..');
    }
    // blog installed? 
    if(!file_exists(DIR_FS_CATALOG.'forum/')){
      die('Blog directory not installed..');
    }    
    // writable pics directory
    if(!file_exists(DIR_FS_CATALOG.self::imagesWebPath)  ||  !is_writable(DIR_FS_CATALOG.self::imagesWebPath)){
      die('Blog pictures directory not writable..');
    }
  }
  
  public static function selDB(){
    mysql_select_db(DB_DATABASE_CMS);
  }
  
  public static function deselDB(){
    mysql_select_db(DB_DATABASE);
  }
  
  public static function getAllDocuments($aBlogID = NULL){
    
    if(!$aBlogID)   return(false);
    
    self::selDB();
    
    $ret = array();
    $res = mysql_query('SELECT * FROM documents WHERE projectId = \''.$aBlogID.'\' ORDER BY created DESC');
    
    while($data = mysql_fetch_assoc($res)):
      
        $doc = new blogDocument($data['id']);
        $doc->loadArray($data);
        
        if($doc->isLoaded()){
          $ret[] = $doc;
        }
    endwhile;
    
    self::deselDB();    
    
    return($ret);
  }
  
  public static function getDefaultAuthor($aBlogID = NULL){
    
    if(!$aBlogID)   return(false);
    
    self::selDB();
    
    $res = mysql_query('SELECT id FROM authors WHERE projectId = \''.$aBlogID.'\' ORDER BY created ASC');
    
    list($authorID) = mysql_fetch_row($res);
    
    self::deselDB();    
    
    return($authorID);
  }
  
  public static function import_decodeXMLData($aString = NULL){
    
    if(!$aString)   return(false);
    
    return(html_entity_decode(utf8_decode($aString)));
  }
  
  public function import_intoDB($aLocalFile = NULL, $aBlogSettings = NULL, $aSrcBlogID = NULL, &$aFTPCon = NULL){
    
    if(!$aLocalFile  ||  !is_file($aLocalFile)  ||  !$aBlogSettings  ||  !$aSrcBlogID  ||  !is_resource($aFTPCon))   return(false);
    
    $ret = 0;
    
    if(!class_exists('XMLReader'))   die('PHP Klasse XMLReader nicht installiert..');
    
    $parser = new Xml2Assoc();
    $xmlData = $parser->parseFile($aLocalFile, true);
        
    if(empty($xmlData['blogs']['blog']))   return(0);
    
    if($xmlData['blogs']['blog']['title']){  // only 1 blog document
      $blogsArr[0] = $xmlData['blogs']['blog'];
    }
    else {
      $blogsArr = $xmlData['blogs']['blog'];  
    }
    
    foreach($blogsArr as $blogData):

        self::selDB();
        $resCheck = mysql_query('SELECT id FROM documents WHERE (projectId = \''.$this->blogID.'\' OR projectId = \''.$this->blogID.'_tmp\') AND src_blogID = \''.$aSrcBlogID.'\' AND src_docID =  '.self::import_decodeXMLData($blogData['id']));
        
         if(mysql_num_rows($resCheck) > 0)   {
          continue;       
        } 
        self::deselDB();          

        $objDoc = new blogDocument(NULL, seoBlogsHandle::getBlogDb());
        $objDoc->setProperty('title', self::import_decodeXMLData($blogData['title']));
        $objDoc->setProperty('teaser', self::import_decodeXMLData($blogData['teaser']));
        $objDoc->setProperty('content', self::import_decodeXMLData($blogData['content']));
        $objDoc->setProperty('custom_1', self::import_decodeXMLData($blogData['custom_1']));
        $objDoc->setProperty('pictures', self::import_decodeXMLData($blogData['pictures']));
        $objDoc->setProperty('created', date('Y-m-d H:i:s', time()));
        $objDoc->setProperty('modified', date('Y-m-d H:i:s', time()));
        $objDoc->setProperty('authorId', self::getDefaultAuthor($this->blogID));
        
		if(isset($aBlogSettings[$this->blogID]['furtherNodeIds'][$blogData['nodeId']])){
		   $objDoc->setProperty('nodeId', $blogData['nodeId']);
		}
		elseif($aBlogSettings[$aSrcBlogID]['nodeId']  == $blogData['nodeId']){
          $objDoc->setProperty('nodeId', $aBlogSettings[($this->blogID)]['nodeId']);
        }
		else {
		  $objDoc->setProperty('nodeId', $blogData['nodeId']);
		}
	
        
        // pictures ftp file transfer
        if(!empty($blogData['pictures'])){
          
          //echo $blogData['pictures'].'<br />';
          $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
          $pictures = $json->decode(self::import_decodeXMLData($blogData['pictures']));
          //echo '<pre>';
          //print_r($pictures);
          if(is_array($pictures)){
              
              foreach($pictures as $picPath):
                
                //echo 'FTP from:'.DIR_FS_DOCUMENT_ROOT.$picPath.'<br />';
                //echo 'FTP from:'.$blogData['FTPRoot'].$picPath.'<br /><br />';
                ftp_get($aFTPCon, DIR_FS_CATALOG.$picPath, $blogData['FTPRoot'].$picPath, FTP_BINARY);              
              endforeach;
          }
        }

        $objDoc->setProperty('src_blogID', $aSrcBlogID);
        $objDoc->setProperty('src_docID', self::import_decodeXMLData($blogData['id']));
        $objDoc->setProperty('projectId', $this->blogID.'_tmp');
        
        $created = $objDoc->create();
        
        if($created){
           $ret++;      
        }  
    endforeach;
    
    return($ret);
  }
  
  public function import_getImportedDocIDsForSrc($aSrcBlogId = NULL){
    
    if(!$aSrcBlogId)   return(false);
    
    self::selDB();
    
    $ret = array();
    $res = mysql_query('SELECT src_docID FROM documents WHERE (projectId = \''.$this->blogID.'\' OR projectId = \''.$this->blogID.'_tmp\') and src_blogID = \''.$aSrcBlogId.'\' ORDER BY created ASC');
    
    while(list($docID) = mysql_fetch_row($res)):
      
      $ret[] = $docID;
    endwhile;
    
    self::deselDB();  
    
    return($ret);   
  }
  
  private function connectBlogDb(){
    
    self::$DB = mysql_connect(DB_SERVER_CMS, DB_SERVER_USERNAME_CMS, DB_SERVER_PASSWORD_CMS);
    
    if(!self::$DB){
      die('Could not connect blog database');
    }
  }
  
   public static function getBlogDb(){
      return(self::$DB);
    }
   
   public static function savePrevPic($aFile = array(), $aAllowedFileTypes = array(), $aScaleWidth = 220){
     
     if(!$aFile  ||  !$aAllowedFileTypes)   return(false);
     
    list($srcWidth, $srcHeight, $srcType) = getimagesize($aFile['tmp_name']);

    $fileTypes = array('1' => 'gif', '2' => 'jpg', '3' => 'png', '6' => 'bmp', '7' => 'tiff', '8' => 'tiff');           
   
    if(!in_array($fileTypes[$srcType], $aAllowedFileTypes)){
      return(array(false, 'filetype'));
    }
   
    if($aFile['error']){
      return(array(false, 'upload'));
    }    
        
    // scale image
    if($aScaleWidth != $srcWidth){
        
        if($fileTypes[$srcType] == 'jpg'){
          $srcIm = imagecreatefromjpeg($aFile['tmp_name']);     
        }
        else {  // gif
          $srcIm = imagecreatefromgif($aFile['tmp_name']);     
        }
                
        $newHeight = ceil(($aScaleWidth / $srcWidth * $srcHeight));
;

        $dstIm = imagecreatetruecolor($aScaleWidth, $newHeight);
        imagecopyresampled($dstIm, $srcIm, 0, 0, 0, 0, $aScaleWidth, $newHeight, $srcWidth, $srcHeight);
        
         if($fileTypes[$srcType] == 'jpg'){
          imagejpeg($dstIm, $aFile['tmp_name'], 100);
        }
        else {  // gif
          imagejpeg($dstIm, $aFile['tmp_name'], 100);
        }
    }      
      
    if(!move_uploaded_file($aFile['tmp_name'], DIR_FS_CATALOG.self::imagesWebPath.'/'.$aFile['name'])){
      return(array(false, 'filemove'));
    }
    
    
    return(array(true));
   }  
}

class blogDocument extends simple_dbobject {
  
  protected  $id;
  protected  $title;
  protected  $teaser;
  protected  $content;
  protected  $projectId;
  protected  $nodeId;
  protected  $pictures;
  protected  $created;
  protected  $modified;
  protected  $authorId;
  protected  $custom_1;
  protected  $src_blogID;
  protected  $src_docID;
  
  protected  $type = 'cmsDocument';
  protected  $dbtable = 'documents';
  
  protected  $DB;
  
  protected $propertyNonDB = array('propertyNonDB', 'propertyRequ', 'isLoaded', 'dbtable', 'id', 'DB');
  protected $propertyRequ = array('id', 'projectId', 'title', 'teaser', 'content', 'nodeId', 'type', 'authorId', 'custom_1');
  
  const  regex_doclink = '/\[doclink_(.+?)\]/';
  
  public static function parseDeadLinks($aContent = NULL){    
    
    if(!$aContent)   return(false);

     $ret = array();
     preg_match_all('/\<a.*?href=[\'"]\/(.*?)[\'"].*?>.*?<\/a>/', stripslashes($aContent), $hits);
     
     if($hits[1]){
       
       foreach($hits[1] as $i=>$link):
          
          if(!file_exists(DIR_FS_DOCUMENT_ROOT.'/'.$link)){

            $ret[(strip_tags($hits[0][$i]))] = $link;
          }
       endforeach;
     }
     
     return($ret);
  }
  
  public static function extractContent($aContent = NULL){
    
    $formContent = trim($aContent);
    $formContent = str_replace("\r\n", '<br /><br />', $formContent);
    $formContent = str_replace("\n\n", '<br /><br />', $formContent);

    preg_match_all(self::regex_doclink, $formContent, $hits);
        
    if(!empty($hits)){  // editmode, already docs linked in it
      $formContent = preg_replace(self::regex_doclink, '', $formContent);  
      $formContent = trim($formContent);       
    }
    
    return(array('content'=>$formContent, 'linkstoDocs'=>$hits[1]));
  }
  
  public function createContent($aText = NULL, $aLinkstoDocs = array()){
  
    
    $dbContent = str_replace('<br />'."\r\n", "<br />", stripslashes($aText));
    
    $dbContent = str_replace('<p>'."\r\n\r\n", '<p>', $dbContent);
    $dbContent = str_replace('<p>'."\r\n", '<p>', $dbContent);
    
    $dbContent = str_replace('</p>'."\r\n\r\n", '</p>', $dbContent);
    $dbContent = str_replace('</p>'."\r\n", '</p>', $dbContent);
    
    $dbContent = str_replace('<p style="margin-bottom: 0cm;">'."\r\n\r\n", '<p>', $dbContent);
    $dbContent = str_replace('<p style="margin-bottom: 0cm;">'."\r\n", '<p>', $dbContent);
    
    
    preg_match_all('/<(.*?)>\r\n/U', $dbContent, $tags);
    
    if($tags[0]){
      
      foreach($tags[0] as $i=>$tag):
          $dbContent = str_replace($tag, '<'.$tags[1][$i].'>', $dbContent);
      endforeach;
    }
    
    $dbContent = str_replace("\r\n\r\n\r\n", '', $dbContent);
    
        //die();
    if(!empty($aLinkstoDocs)){
      
      foreach($aLinkstoDocs as $docID):
        $dbContent .= "\r\n".'[doclink_'.$docID.']';
      endforeach;
    }       
    
    return($dbContent);
  }

  public function addLinkInRelatedBlog($aDocID = NULL){
    
    if(!$aDocID)   return(false);
    
    $editDoc = new blogDocument($aDocID, $this->DB);
    $editDoc->load();
    
    if($editDoc->isLoaded()){
      $content = trim($editDoc->getProperty('content'));
      $editDoc->setProperty('content', $content."\r\n".'[doclink_'.$this->getProperty('id').']');
      
      $saved = $editDoc->save();
      return($saved);
    }
    
    return(false);
  }
  
  public function create(){
    
    seoBlogsHandle::selDB();
    $created = parent::create();
    seoBlogsHandle::deselDB();
    
    return($created);
  }
  
  public function load(){

    seoBlogsHandle::selDB();
    $loaded = parent::load();
    seoBlogsHandle::deselDB();
    
    return($loaded);
  }
  
  public function save(){
    
    seoBlogsHandle::selDB();
    $saved = parent::save();
    seoBlogsHandle::deselDB();
    
    return($saved);
  }
  
  public function delete(){

    seoBlogsHandle::selDB();
    $deleted = parent::delete();
    seoBlogsHandle::deselDB();
    
    return($deleted);
  }
  
}
?>