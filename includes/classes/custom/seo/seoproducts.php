<?
class seoproducts{
	
	function getSEOkeywordsForCat($aCats = array()){
		
		if(!$aCats)   return(false);
		
		$sql = 'SELECT t2.products_seo_keywords FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' as t1 '.
						'LEFT JOIN '.TABLE_PRODUCTS.' as t3 ON (t1.products_id = t3.products_id) '.
						'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as t2 ON (t1.products_id = t2.products_id AND t2.language_id = '.LANGUAGE_ID.') '.
					
						'WHERE t3.products_status = 1 AND t3.'.PRODUCTS_PRICE.' > 0 AND t1.categories_id IN ('.implode(', ', $aCats).') AND LOCATE(\' \', t2.products_seo_keywords) > 0';

		$res = tep_db_query($sql);
		
		while(list($keywordStr) = tep_db_fetch_row($res)):
		
			$keywords = explode('|', $keywordStr);
			
			if(!empty($keywords)){
				
				foreach($keywords as $_):
				
					if(!strstr($_, ' ') && !strstr(strtolower($_), 'krawatte')){
						continue;
					}
				
					$list[$_]['num']++;
					$list[$_]['keyword'] = $_;
				endforeach;
			}		
		endwhile;
		
		if($list){			
					
			function sortIT($a, $b){
			    if ($a['num'] == $b['num']) {
			        return(0);
			    }
			    return(($a['num'] < $b['num']) ? 1 : -1);
			}			
			
			usort($list, 'sortIT');
			
			
			return($list);		
		}	
	}
	
	function getSEOkeywordsForIndex(){
	
		$sql = 'SELECT t2.products_seo_keywords FROM '.
						TABLE_PRODUCTS.' as t1 '.
						'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as t2 ON (t1.products_id = t2.products_id AND t2.language_id = '.LANGUAGE_ID.') '.
					
						'WHERE t1.products_status = 1 AND t1.'.PRODUCTS_PRICE.' > 0 AND LOCATE(\' \', t2.products_seo_keywords) > 0';

		$res = tep_db_query($sql);
		
		while(list($keywordStr) = tep_db_fetch_row($res)):
		
			$keywords = explode('|', $keywordStr);
			
			if(!empty($keywords)){
				foreach($keywords as $_):
				   if(!empty($_)){
				   	 
				    if(!strstr($_, ' ') && !strstr(strtolower($_), 'krawatte')){
						continue;
					}
				   	
				   	 $keywordsList[] = trim($_);
				   }
				endforeach;
			}		
		endwhile;
		
		if($keywordsList){			

			sort($keywordsList, SORT_STRING);	
			
			foreach($keywordsList as $_):
					$_ = trim($_);					
					
					$ind = strtoupper(substr($_, 0, 1));
					$list[$ind][$_]['keyword'] = $_;	
					
					if($list[$ind][$_]['num'] == 0){
						$list[$ind][$_]['num'] = 1;
					}
					else {
						$list[$ind][$_]['num']++;
					}
			endforeach;
			
			return($list);		
		}	
	}
	
	
	function getProductsOnKeyword($aKeyword = NULL, $aFlags = array()){
		
		if(!$aKeyword)   return(false);
		
		$sql = 'SELECT t1.products_id, t1.products_name, t2.products_model, t1.products_description, t2.'.PRODUCTS_PRICE.', t2.products_tax_class_id, t1.products_seo_keywords FROM '.TABLE_PRODUCTS_DESCRIPTION.' as t1 '.
					'LEFT JOIN '.TABLE_PRODUCTS.' as t2 ON t1.products_id = t2.products_id '.
					'WHERE t1.language_id = '.LANGUAGE_ID.' AND (LOCATE(\'|'.$aKeyword.'|\', t1.products_seo_keywords) > 0 OR LOCATE(\'|'.$aKeyword.'\', t1.products_seo_keywords) > 0  OR  LOCATE(\''.$aKeyword.'|\', t1.products_seo_keywords) > 0)';

		if($aFlags['random'] == true){			
			$sql .= ' ORDER BY RAND()';
		}
		
		$res = tep_db_query($sql);
		
		while($data = tep_db_fetch_array($res)):
		
			$keywords = explode('|', $data['products_seo_keywords']);
			
			$found = false;
			foreach($keywords as $kw):
				if($kw == $aKeyword){
				
					$found = true;
					break;
				}
			endforeach;
			
			if($found){
			   $ret[] = $data;
			}
		endwhile;
		
		return($ret);
	}
	
	
}
?>
