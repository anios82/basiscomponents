<?php

class Languages {

	function Languages($iShopId) {
		
		$hLanguages = tep_db_query("SELECT * FROM languages ORDER BY languages_id");
		
		while($aLanguages = tep_db_fetch_array($hLanguages)) {
			$this->aLanguages[$aLanguages['languages_id']] = $aLanguages;
		}
		
		if(!empty($iShopId) and is_numeric($iShopId))
			$this->aShopLanguage = $this->_getShopLanguage($iShopId);

	}
	
	function getShopLanguageCode($iShopId='') {
	
		if(!empty($iShopId) and is_numeric($iShopId))
			$this->aShopLanguage = $this->_getShopLanguage($iShopId);
		
		if($this->aShopLanguage)
			return $this->aLanguages[$this->aShopLanguage]['language_code'];
		else
			return false;
		
	}
	
	function _getShopLanguage($iShopId) {
	
		if(!empty($iShopId) and is_numeric($iShopId)) {
			$hShopLanguage = tep_db_query("SELECT language_id FROM gc_shops WHERE shop_id='".$iShopId."'");
			$aShopLanguage = tep_db_fetch_array($hShopLanguage);
			return $aShopLanguage['language_id'];
		}
		else {
			return false;
		}
		
	}
	

}


?>