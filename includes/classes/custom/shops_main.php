<?php

class shops_main {
	
	function getAllLanguagesWithShopData($aParams = array()){		
		
		$sql = 'SELECT t1.languages_id as id, t1.name, t1.code, t1.image, t1.directory, t1.sort_order, t1.language_code, t1.is_external, t2.shop_id, t2.shop_name, t2.shop_shortname, t2.shop_url, t2.products_price_fieldname as shop_pricefield  FROM '.TABLE_LANGUAGES.' as t1 '.
					'JOIN gc_shops as t2 '.
						'ON t1.languages_id = t2.language_id ';
		
		if($aParams['where']['language_code']){			
			$conditions[] = 't1.language_code = \''.$aParams['where']['language_code'].'\'';
		}
		if($aParams['where']['is_external']){			
			$conditions[] = 't1.is_external = \''.$aParams['where']['is_external'].'\'';
		}
				
		if($conditions){
		   $sql .= 'WHERE ';
		   $sql .= implode(' AND ', $conditions).' ';
		}
		
		$sql .= 'ORDER BY t1.languages_id ASC';

		$res = tep_db_query($sql);
		
		$ret = array();
	  	while($data = tep_db_fetch_array($res)):
	  	  $item = new shops_language($data['id']);
		  $item->loadArray($data);
		  if($item->isLoaded()){
		  	
			if($aParams['options']['index_shop_id'] == true){
				$index = $data['shop_id'];
			}
			else {
				$index = $data['id'];
			}
		  	$ret[$index] = $item;
		  }
	    endwhile;
	  
	    return($ret);
	}
}

class shops_language {
	
	var $id;
	var $name;
	var $code;
	var $image;
	var $directory;
	var $sort_order;
	var $is_external;
	var $language_code;
	var $shop_id;
	var $shop_name;
	var $shop_shortname;
	var $shop_url;
	var $shop_pricefield;
	
	
	var $propertyRequ = array('id', 'name', 'code', 'directory');
	var $isLoaded = false;	
	
	function isLoaded(){
		
		return($this->isLoaded);
	} 
	
	function shops_language($aId = NULL){
		
		$this->id = $aId;
	}	
		
	function load(){

		if(!$this->id)   return(false);
		
		if($this->isLoaded())   return(true);
				
		$dbArr = $this->getDBArrayById($this->id);
		
		if(is_array($dbArr)){
			return($this->loadArray($dbArr));
		}
		
		return(false);		
	}
	
	function getDBArrayById($aId = NULL){

		if($aId){
			  $sql = 'SELECT * FROM '.TABLE_LANGUAGES.' '.
				'WHERE languages_id = \''.$aId.'\'';
				
			  $result = tep_db_query($sql);
			  $data = tep_db_fetch_array($result);
			  			  
			  return($data);
		}		
		
		return(false);
	}
	
	function loadArray($aProperties = NULL){
		
		if(!is_array($aProperties))   return(false);
		
		if($this->isLoaded())   return(true);

		return($this->setLoadedProperties($aProperties));
	}
	
	function setLoadedProperties($aProperties = NULL){
		
		if(!$aProperties)   return(false);

		if(isset($aProperties['id']))   $this->id = $aProperties['id'];
		if(isset($aProperties['name']))   $this->name = $aProperties['name'];
		if(isset($aProperties['code']))   $this->code = $aProperties['code'];
		if(isset($aProperties['image']))   $this->image = $aProperties['image'];
		if(isset($aProperties['directory']))   $this->directory = $aProperties['directory'];		
		if(isset($aProperties['sort_order']))   $this->sort_order = $aProperties['sort_order'];
		if(isset($aProperties['is_external']))   $this->is_external = $aProperties['is_external'];
		if(isset($aProperties['shop_id']))   $this->shop_id = $aProperties['shop_id'];
		if(isset($aProperties['is_external']))   $this->is_external = $aProperties['is_external'];
		if(isset($aProperties['language_code']))   $this->language_code = $aProperties['language_code'];
		if(isset($aProperties['shop_name']))   $this->shop_name = $aProperties['shop_name'];
		if(isset($aProperties['shop_shortname']))   $this->shop_shortname = $aProperties['shop_shortname'];
		if(isset($aProperties['shop_url']))   $this->shop_url = $aProperties['shop_url'];
		if(isset($aProperties['shop_pricefield']))   $this->shop_pricefield = $aProperties['shop_pricefield'];
		
		$this->isLoaded = ($this->checkPropertyRequirements());
		
		return($this->isLoaded);
	}
	
	function checkPropertyRequirements($aIgnoreParams = array()){
		
		foreach($this->propertyRequ as $prop){
			
			if($aIgnoreParams){
				 if($this->getProperty($prop) === false && !in_array($prop, $aIgnoreParams))  return(false);
			}
			else {
			   if($this->getProperty($prop) === false)  return(false);
			}
		}
		
		return(true);
	}
	
	function getProperty($aProp = NULL){
		
		if(!$aProp)   return(false);
		
		if(isset($this->$aProp)){ // value can be '0' so must check on is set
			return($this->$aProp);
		}
		
		return(false);		
	}	
}


?>