<?
class productsSelection{
	
	function getTopOffers($aOffersCatID = 43, $aLimit = 32){		
		
		$sql = 'SELECT t2.products_id, t2.'.PRODUCTS_PRICE.', t2.products_tax_class_id, t3.products_name, t2.products_model FROM '.
					TABLE_PRODUCTS_TO_CATEGORIES.' as t1 '.
					'LEFT JOIN '.TABLE_PRODUCTS.' as t2 ON t1.products_id = t2.products_id '.
					'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as t3 ON t1.products_id = t3.products_id AND '.LANGUAGE_ID.' = t3.language_id '.
					'WHERE t1.categories_id = '.$aOffersCatID.' AND t2.products_status = 1 AND t2.'.PRODUCTS_PRICE.' > 0 AND products_quantity > 0 ORDER BY RAND() LIMIT '.$aLimit;
					

		$res = tep_db_query($sql);
		$i=1;
		while(list($prID, $prPrice, $prTaxClass, $prName, $prModel) = tep_db_fetch_row($res)):
			
			$ret[$i] = array(
				'id' => $prID,
				'price' => $prPrice,
				'taxClass' => $prTaxClass,
				'name' => $prName, 
				'model' => $prModel	
			);
			$i++;
		endwhile;
			
		return($ret);	
	}	
	
	function getTopOffersDyn($catArray, $minStock, $minPrice, $maxPrice, $aLimit = 32){		

		$sql = 'SELECT t1.products_id, 
					   t2.'.PRODUCTS_PRICE.',
					   t2.products_tax_class_id, 
					   t3.products_name, 
					   t2.products_model, 
					   t1.categories_id, 
					   t4.categories_id 
				FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' as t1
				LEFT JOIN '.TABLE_PRODUCTS.' as t2 ON t1.products_id = t2.products_id
				LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as t3 ON (t2.products_id = t3.products_id AND '.LANGUAGE_ID.' = t3.language_id )
				LEFT JOIN '.TABLE_PRODUCTS_TO_CATEGORIES.' as t4 ON (t2.products_id = t4.products_id AND 235 = t4.categories_id) 
		        WHERE t1.categories_id IN('. implode(',', $catArray) .')

				AND t3.extshops_translated="done"  

		        AND t2.products_quantity >= '.$minStock.' 
		        AND (	(t4.categories_id IS NULL AND t2.'.PRODUCTS_PRICE.' <= '.$maxPrice.' AND t2.'.PRODUCTS_PRICE.' > '.$minPrice.')  
		        	 OR (t4.categories_id = 235   AND t2.'.PRODUCTS_PRICE.' <= 6.72          AND t2.'.PRODUCTS_PRICE.' > '.$minPrice.')
				)
				GROUP BY t1.products_id
		        ORDER BY RAND()
		        LIMIT '.$aLimit;
 

		$res = tep_db_query($sql);
		$i=1;
		while(list($prID, $prPrice, $prTaxClass, $prName, $prModel) = tep_db_fetch_row($res)):
			
			$ret[$i] = array(
				'id' => $prID,
				'price' => $prPrice,
				'taxClass' => $prTaxClass,
				'name' => $prName, 
				'model' => $prModel	
			);
			$i++;
		endwhile;
			
		return($ret);	
	}		

	function getRandomProducts($aCat = NULL, $aLimit = 12){		
		
		$sql = 'SELECT t2.products_id, t2.'.PRODUCTS_PRICE.', t2.products_tax_class_id, t3.products_name, t2.products_model FROM '.
					(($aCat) ? TABLE_PRODUCTS_TO_CATEGORIES.' as t1 LEFT JOIN '.TABLE_PRODUCTS.' as t2 ON t1.products_id = t2.products_id ' : TABLE_PRODUCTS.' as t2 ').					
					'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as t3 ON t1.products_id = t3.products_id AND '.LANGUAGE_ID.' = t3.language_id '.
					'WHERE '.(($aCat) ? 't1.categories_id = '.$aCat.' AND ' : '').' t2.'.PRODUCTS_PRICE.' > 0 and t2.products_status = 1 AND t2.products_quantity > 0 ORDER BY RAND() LIMIT '.$aLimit;
					
		$res = tep_db_query($sql);
		$i=1;
		while(list($prID, $prPrice, $prTaxClass, $prName, $prModel) = tep_db_fetch_row($res)):
			
			$ret[] = array(
				'id' => $prID,
				'price' => $prPrice,
				'taxClass' => $prTaxClass,
				'name' => $prName, 
				'model' => $prModel	
			);
		endwhile;
			
		return($ret);	
	}	
}
?>