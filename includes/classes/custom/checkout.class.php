<?php
// test test
define("AUTHORIZENET_SANDBOX", false);

class Checkout {

    var $coupon   = array();
    var $subtotal = array();
    var $shipping = array();
    var $tax      = array();
    var $total    = array();

    var $billing  = array();
    var $delivery = array();
    var $cc       = array();
    var $successfulCreditcardType;
    var $sucessfulCreditcardAuthcode;
    var $note     = '';

    var $deliverysameasbilling = true;

    var $rushShipping = 32.40;
    var $rushShippingSaturday = 32.40;
    var $taxRate = 19;
    var $freeShippingOrderTotal;


    var $selectedPaymentMethod;
    var $paymentMethods = array();

    var $zonesShipping;

    var $ordersStati = array(
        'gc_ipayment' => array('pre'=>25, 'ready'=>39),
        'pn_sofortueberweisung' => array('pre'=>36),
        'gc_paypal' => array('pre'=>6),
        'invoice' => array('pre'=>1)
    );

    var $shipping_modules;
    var $insertedOrdersID;
    var $billingFields;
    var $deliveryFields;
    var $ccFields;

    var $currency_code;
    var $currencies = array(

        'EUR' =>array('symbolweb'=>' &euro;', 'symbolemail'=>' EUR'),
        'BRL'=>array('symbolweb'=>'R$', 'symbolemail'=>'R$'),
        'GPB'=>array('symbolweb'=>' GPB', 'symbolemail'=>' GPB'),
        'YEN'=>array('symbolweb'=>' 円', 'symbolemail'=>' YEN', 'custom_format'=>array('enabled'=>true, 'thousands_point'=>',', 'decimal_places'=>0)),

    );

    var $geozonePreSelected;

    function numberFormat($aVal = NULL){

        if($aVal === NULL)   return(NULL);

        if(array_key_exists('custom_format', $this->currencies[$this->currency_code]) &&  $this->currencies[$this->currency_code]['custom_format']['enabled'] === true){

            $tp = isset($this->currencies[$this->currency_code]['custom_format']['thousands_point']) ? $this->currencies[$this->currency_code]['custom_format']['thousands_point'] : ',';
            $dp = isset($this->currencies[$this->currency_code]['custom_format']['decimal_point']) ? $this->currencies[$this->currency_code]['custom_format']['decimal_point'] : '.';
            $dpl = isset($this->currencies[$this->currency_code]['custom_format']['decimal_places']) ? $this->currencies[$this->currency_code]['custom_format']['decimal_places'] : 2;

            if($this->currencies[$this->currency_code]['custom_format']['decimal_places'] === 0){
                $ret = ceil($ret);
            }

            $ret = number_format($aVal, $dpl, $dp, $tp);

            return($ret);
        }

        return(number_format($aVal, 2, ',', '.'));
    }

    function setGeozonePre($aValue = NULL){

        $this->geozonePreSelected = $aValue;
    }

    function getProductsPrice($pid, $qty) {

        $product_info_query = tep_db_query("select p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_quantity, pd.products_url, p.".PRODUCTS_PRICE.", p.products_tax_class_id, p.products_date_added, p.products_date_available from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . $pid . "' and pd.products_id = p.products_id and pd.language_id = '" . LANGUAGE_ID . "'");
        $product_info = tep_db_fetch_array($product_info_query);
        if ($new_price = tep_get_products_special_price($product_info['products_id'])) {

            return $this->numberFormat((tep_add_tax($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) * $qty));

        }
        else {
            return $this->numberFormat((tep_add_tax($product_info[PRODUCTS_PRICE], tep_get_tax_rate($product_info['products_tax_class_id'])) * $qty));
        }
    }

    function Checkout() {

        if(defined('RUSHSHIPPING_COSTS')){
            $this->rushShipping = RUSHSHIPPING_COSTS;
            $this->rushShippingSaturday = RUSHSHIPPING_COSTS;
        }

        $this->zonesShipping = array(1 => SHIPPING_COSTS_ZONE1, 2 => SHIPPING_COSTS_ZONE2, 3 => SHIPPING_COSTS_ZONE3);

        $this->freeShippingOrderTotal = SHIPPING_MINAMOUNT_FREESHIPPING;

        $this->billingFields = array(
            'gender' => array(ENTRY_GENDER, true),
            'firstname' => array(ENTRY_FIRST_NAME, 2),
            'lastname'  => array(ENTRY_LAST_NAME, 2),
            'company'   => array(ENTRY_COMPANY, false),
            'street'    => array(ENTRY_STREET_ADDRESS, 5),
            'addressadd'    => array(ENTRY_ADDRESSADD, false),
            'city'      => array(ENTRY_CITY, 3),
            'zipcode'   => array(ENTRY_POST_CODE, 4),
            'country'   => array(ENTRY_COUNTRY, true),
            'email'     => array(ENTRY_EMAIL_ADDRESS, true),
            'phone'     => array(ENTRY_TELEPHONE_NUMBER, ((defined('CREATEACCOUNT_TELEPHONE_NONDE_MUSTHAVE') && CREATEACCOUNT_TELEPHONE_NONDE_MUSTHAVE == true) ? true : false)),
            'ustid'	=> array(ENTRY_VAT, false),
        );

        $this->deliveryFields = array(
            'gender' => array(ENTRY_GENDER, true),
            'firstname' => array(ENTRY_FIRST_NAME, 2),
            'lastname'  => array(ENTRY_LAST_NAME, 2),
            'company'   => array(ENTRY_COMPANY, false),
            'street'    => array(ENTRY_STREET_ADDRESS, 5),
            'addressadd'    => array(ENTRY_ADDRESSADD, false),
            'city'      => array(ENTRY_CITY, 3),
            'zipcode'   => array(ENTRY_POST_CODE, 4),
            'country'   => array(ENTRY_COUNTRY, true)
        );


        $this->ccFields = array(
            'name'        => array(CHECKOUT_CREDITCARD_OWNER, 2),
            'number'      => array(CHECKOUT_CREDITCARD_NUMBER, '#^[0-9]{10,20}$#'),
            'expireyear'  => array(CHECKOUT_CREDITCARD_EXPIRY, '#^20[1-3]{1}[0-9]{1}$#'),
            'expiremonth' => array('', '#^(?:0[1-9]{1})|(?:1[0-2]{1})$#'),
            'security'    => array(CHECKOUT_CREDITCARD_CVS, '#^[0-9]{2,5}$#')
        );

        if(defined('CURRENCY_CODE')  &&  CURRENCY_CODE !== 'EUR'){
            $this->currency_code = CURRENCY_CODE;
        }
        else {
            $this->currency_code = 'EUR';
        }

        $this->deliverysameasbilling = true;
        $this->billing['state'] = $this->delivery['state'] = '';
        //$this->billing['country'] = $this->delivery['country'] = SERVER_COUNTRY_ID;

        $this->cc['expireyear'] = date('Y');
        $this->cc['expiremonth'] = date('m');

        $this->setCoupon();
        $this->setSubtotal();
        $this->setShipping();
        $this->setTax();
        $this->setTotal();

        $this->fresh = true;
    }

    /* M.W. */
    function setPaymentMethod($id, $name, $description, $extrainfo = NULL){

        $this->paymentMethods[$id] = array('id'=>$id, 'name'=>$name);

        if($description){
            $this->paymentMethods[$id]['description'] = $description;
        }

        if($extrainfo){
            $this->paymentMethods[$id]['extrainfo'] = $extrainfo;
        }
    }

    function getPaymentMethods(){

        return($this->paymentMethods);
    }
    /* end M.W. */

    /******************************************
     *	Subtotal
     ******************************************/

    function getDefaultStoreGeoZone(){

        if(defined('SERVER_COUNTRY_ID')  &&  SERVER_COUNTRY_ID != ''){

            $res = tep_db_query('SELECT geo_zone_id FROM '.TABLE_ZONES_TO_GEO_ZONES.' WHERE zone_country_id = '.SERVER_COUNTRY_ID);
            list($defaultShopZone) = tep_db_fetch_row($res);

            return($defaultShopZone);
        }
        else {

            $res = tep_db_query('SELECT geo_zone_id FROM '.TABLE_ZONES_TO_GEO_ZONES.' WHERE zone_country_id = '.STORE_COUNTRY);
            list($defaultShopZone) = tep_db_fetch_row($res);

            return($defaultShopZone);
        }
    }

    function setSubtotal() {

        global $cart;

        $subtotal = $_SESSION['cart']->show_total();
        $this->_log("\n".'setSubtotal():'.$subtotal."\n");

        // NETTO shops (=shops which defaultStoreZone = 3) dont reduce product prices, it\'s already computed in netto in cart-class or display-price function
        // BRUTTO shops like in Germany, France have to reduce 19% tax from product prices
        if($this->getDefaultStoreGeoZone() != '3'  && CHECKOUT_CART_PRODUCES_NEPRICES_ALREADY !== true && ($this->getNetto()  ||  $this->isInvoicetypeNetPositionsGrossSum())){
            $subtotal = $this->calc_eval_tax($subtotal);
        }

        $this->subtotal = array('title' => CHECKOUT_SUBTOTAL, 'value' => $subtotal, 'text' => $this->numberFormat($subtotal).$this->currencies[$this->currency_code]['symbolweb']);

    }

    function getOtSubtotal() {

        return '<span id="ot_subtotal">' . $this->subtotal['title'].': <em>'.str_replace(' ', '&nbsp;', $this->subtotal['text']).'</em></span>';

    }

    /******************************************
     *	Tax
     ******************************************/
    /* M.W. */
    function getNetto(){

        // aus sicht von deutschland
        if($this->getGeoZoneId() == 3){
            return(true);
        }

        if($this->getGeoZoneId() == 2  && $this->delivery['country'] != 81  && !empty($this->billing['company'])  &&  !empty($this->billing['ustid'])){
            return(true);
        }

        if($this->getGeoZoneId() == 1  && $this->delivery['country'] != 81  &&   !empty($this->billing['company'])  &&  !empty($this->billing['ustid'])){
            return(true);
        }

        return(false);
    }


    function setTax() {

        if($this->getNetto()){
            $tax = 0.00;
        }
        else {
            if($this->isInvoicetypeNetPositionsGrossSum()){
                // from net positions
                $tax = ($this->subtotal['value'] + $this->shipping['value'] - $this->coupon['value']) / 100 * $this->taxRate;
            }
            else {
                // from gross positions
                $tax = ($this->subtotal['value'] + $this->shipping['value'] - $this->coupon['value']) / (100+$this->taxRate) * $this->taxRate;
            }
        }

        $taxText = ($this->isInvoicetypeNetPositionsGrossSum() && defined('CHECKOUT_TAX_ADDED')) ? CHECKOUT_TAX_ADDED : CHECKOUT_TAX;
        $this->tax = array(
            'title' => $taxText,
            'text' => $this->numberFormat($tax).$this->currencies[$this->currency_code]['symbolweb'],
            'value' => $tax
        );
    }

    function getOtTax() {

        if($this->tax['value'] > 0) {
            return '<span id="ot_tax">' . $this->tax['title'].': <em>'.str_replace(' ', '&nbsp;', $this->tax['text']).'</em></span>';
        }
        else {
            return '<span id="ot_tax" style="display:none"></span>';
        }

    }

    /******************************************
     *	Couponcode
     ******************************************/

    function deleteCoupon() {
        $this->coupon = array(
            'type' => 'empty',
            'code' => false,
            'title' => CHECKOUT_ENTERCOUPON,
            'text' => '0.00'.$this->currencies[$this->currency_code]['symbolweb'],
            'value' => 0.0,
            'error' => '',
            'hash' => '',
            'name' => '',
            'email' => '',
            'extra' => ''
        );
    }

    function setCoupon($code='') {

        if(empty($this->coupon)) {

            $this->deleteCoupon();
            return;

        }

        if(!empty($code)) {
            $this->deleteCoupon();
            $code = trim($code);
            $code = strip_tags($code);
            $this->checkCoupon($code);
        }
        else {
            $this->checkCoupon($this->coupon['code']);
        }

    }


    function checkMinOrderAmount(){

        if(!defined('ORDER_MINAMOUNT') || ORDER_MINAMOUNT == '0'){

            return true;
        }

        return ($_SESSION['cart']->total >= ORDER_MINAMOUNT);
    }

    function checkCoupon($code) {

        if(empty($code)) {

            $this->deleteCoupon();
            return false;

        }

        global $cart;

        $this->coupon['code'] = $code;
        $this->coupon['error'] = '';

        // connecting to united e commerce and getting voucher info from there
        $voucher = file_get_contents('http://admin.uecsystem.de/communication/server/sendvoucherinfo?shopId='.SHOP_SHORTNAME.'&voucherName='.$code);

        if(substr($voucher, 0, 1) == '{') {
            $voucher = json_decode($voucher);

            if ($voucher->status != 'success') {
                $this->coupon['error'] = ENTRY_COUPON_CODE_ERROR;
                return false;
            }
        }
        else {
            $this->coupon['error'] = ENTRY_COUPON_CODE_ERROR;
            return false;
        }

        $couponsMinOrder = ($this->getNetto() === true) ? ($voucher->data->cartMinValue * ($this->taxRate + 100) / 100) : $voucher->data->cartMinValue;

        if(!empty($couponsMinOrder) && $_SESSION['cart']->show_total() < $couponsMinOrder){
            $this->coupon['error'] = sprintf(ENTRY_COUPON_CODE_MIN_ERROR, $voucher->data->cartMinValue);
        }

        if($voucher->data->type == 'percental') {
            $this->coupon['type'] = 'percent';
            $this->coupon['value'] = $voucher->data->value;
            $this->coupon['title'] = CHECKOUT_ENTERCOUPON.' (-'.$voucher->data->value.'%)';
            $this->coupon['text'] = '- '.$this->numberFormat(($_SESSION['cart']->show_total()/100) * $voucher->data->value).$this->currencies[$this->currency_code]['symbolweb'];
        }
        else {
            $this->coupon['type'] = 'fixed';
            $this->coupon['value'] = $voucher->data->value;
            $this->coupon['title'] = CHECKOUT_ENTERCOUPON;
            $this->coupon['text'] = '- '.$voucher->data->value.$this->currencies[$this->currency_code]['symbolweb'];
        }

        $this->coupon['code'] = $code;
        $this->coupon['name'] = $code;

        return true;

    }

    function getCoupon($input=false) {

        $coupon = '<form id="form_coupon" onsubmit="return false;" method="post" action="' . tep_href_link('checkout.php', '', 'SSL') . '">';

        if(empty($this->coupon['code'])) {

            $coupon .= '<input type="text" id="couponcode" name="couponcode" value="' . (!empty($this->coupon['code']) ? $this->coupon['code'] : CHECKOUT_ENTERCOUPON) . '" />
				<input type="submit" id="submit_coupon" onclick="setCoupon(\'#couponcode\', this);return false;" value="'.CHECKOUT_REDEEMCOUPON.'" />';

        }
        elseif(!empty($this->coupon['error'])) {

            $coupon .= '
				<span>'.CHECKOUT_ENTERCOUPON.'</span><br />

				<input type="text" id="couponcode" class="validation-failed" name="couponcode" value="' . $this->coupon['code'] . '" />
				<input type="submit" id="submit_coupon" onclick="setCoupon(\'#couponcode\', this);return false;" value="'.CHECKOUT_REDEEMCOUPON.'" /><br />
				<span class="error validation-error">'.$this->coupon['error'].'</span>';

        }
        else {

            $coupon .= '
				<span>'.sprintf(CHECKOUT_COUPONAPPLIED, $this->coupon['code']).'</span><br />
				<a href="' . tep_href_link('checkout.php', '', 'SSL') . '" onclick="deleteCoupon(); return false;">'.CHECKOUT_DELETECOUPON.'</a>';

        }

        $coupon .= '</form>';

        return $coupon;

    }

    function getOtCoupon() {

        if($this->coupon['type']=='empty' or $this->coupon['type']=='freeshipping')
            return '<span id="ot_coupon" style="display:none"></span>';
        elseif($this->coupon['error'] != '')
            return '<span id="ot_coupon"><s>'.$this->coupon['title'].': <em>'.str_replace(' ', '&nbsp;', $this->coupon['text']).'</em></s></span>';
        else
            return '<span id="ot_coupon">'.$this->coupon['title'].': <em>'.str_replace(' ', '&nbsp;', $this->coupon['text']).'</em></span>';

    }

    /******************************************
     *	Shipping
     ******************************************/

    function setShipping() {

        if(empty($this->shipping)) {
            return $this->setShippingStandard();
        }
        elseif($this->shipping['type']=='rush') {
            return $this->setShippingRush();
        }
        elseif($this->shipping['type']=='rush_saturday') {
            return $this->setShippingRushSaturday();
        }
        else {
            return $this->setShippingStandard();
        }

    }

    function calc_eval_tax($val){

        if(!$val)   return(false);

        if($this->getNetto() === true  ||  $this->isInvoicetypeNetPositionsGrossSum()){
            return(( $val / (($this->taxRate+100)/100) ));
        }
        else {
            return($val);
        }
    }

    function taxcalculation($val){

        if(!$val)   return(false);

        if($this->getNetto() === true){
            return(( $val / (($this->taxRate+100)/100) ));
        }
        else {
            return($val);
        }
    }

    function setShippingRush() {

        if($this->getGeoZoneId() != 1) {
            return $this->setShippingStandard();
        }

        $this->shipping = array(
            'id' => 'dhlexpressde_dhlexpressde',
            'type'=>'rush',
            'title' => 'DHL Express',
            'value' => $this->calc_eval_tax($this->rushShipping),
            'text' => $this->numberFormat($this->calc_eval_tax($this->rushShipping)).$this->currencies[$this->currency_code]['symbolweb']);

        return true;
    }

    function setShippingRushSaturday() {

        if($this->getGeoZoneId() != 1) {
            return $this->setShippingStandard();
        }

        $this->shipping = array(
            'id' => 'dhlexpressde_dhlexpressde',
            'type'=>'rush_saturday',
            'title' => 'DHL Express',
            'value' => $this->calc_eval_tax($this->rushShippingSaturday),
            'text' => $this->numberFormat($this->calc_eval_tax($this->rushShippingSaturday)).$this->currencies[$this->currency_code]['symbolweb']);

        return true;
    }

    /* M.W. */
    function _log($toLog = NULL){

        $fp = fopen('checkout.log', 'a+');

        if(is_array($toLog)){
            ob_start();
            print_r($toLog);
            $out = ob_get_clean();
            fwrite($fp, $out."\n\n");
        }
        else {
            fwrite($fp, $toLog."\n\n");
        }

        fclose($fp);
    }


    function setShippingStandard() {

        $zone = $this->getGeoZoneId();

        $zoneToModule = array(
            '1' => array('table_table', CHECKOUT_STANDARDSHIPPING),
            '2' => array('table2_table2', CHECKOUT_STANDARDSHIPPING_HINT_EU),
            '3' => array('table3_table3', CHECKOUT_STANDARDSHIPPING_HINT_NONEU),
        );

        $shipping_module = $zoneToModule[$zone][0];

        if($zone == '1'  ||  $zone == '2'){

            if($this->subtotal['value'] >= $this->freeShippingOrderTotal){
                $cost = 0.00;
            }
            else {
                $cost = $this->calc_eval_tax($this->zonesShipping[(int)$zone]);
            }
        }
        else {
            $cost = $this->calc_eval_tax($this->zonesShipping[(int)$zone]);
        }

        $this->shipping = array(
            'id' => $shipping_module,
            'type'=>'standard',
            'title' => $zoneToModule[$zone][1],
            'value' => $cost,
            'text' => $this->numberFormat($cost).$this->currencies[$this->currency_code]['symbolweb']);

        /*
        if($zone == 3) {
            $this->shipping = array('type' => 'standard', 'title' => CHECKOUT_EXPRESS, 'value' => $this->zonesShipping[$zone], 'text' => number_format($this->zonesShipping[$zone], 2, ",", ".").' &euro;');
        }
        elseif($this->subtotal['value'] >= 100.0) {
            $this->shipping = array('type' => 'standard', 'title' => CHECKOUT_STANDARDSHIPPING, 'value' => 0.0, 'text' => '0.00 &euro;');
        }
        else {
            $this->shipping = array('type' => 'standard', 'title' => CHECKOUT_STANDARDSHIPPING, 'value' => $this->zonesShipping[$zone], 'text' => number_format($this->zonesShipping[$zone], 2, ",", ".").' &euro;');
        }*/
    }

    function getOtShipping() {

        return '<span id="ot_shipping">' . $this->shipping['title'].': <em>'.str_replace(' ', '&nbsp;', $this->shipping['text']).'</em></span>';

    }

    function getShipping() {

        $zone = $this->getGeoZoneId();

        $shipping = '<form action="' . tep_href_link('checkout.php', '', 'SSL') . '" id="form_shipping" onsubmit="return false;" method="post">';

        if($this->getDefaultStoreGeoZone() != '3'){
            $shipping .= '<p id="shipping_standard"><input type="radio" name="shipping" value="standard" ' . ($this->shipping['type']=='standard' ? 'checked="checked"' : '') . ' onclick="setShipping(this, 1, \'#shipping_standard label\');" id="shipping_standard_radio" /><label for="shipping_standard_radio"><b>'.CHECKOUT_STANDARDSHIPPING.'</b> - '.$this->numberFormat($this->calc_eval_tax($this->zonesShipping[1])).$this->currencies[$this->currency_code]['symbolweb'].'<br /></label><div>'.CHECKOUT_STANDARDSHIPPING_HINT.'</div></p>';
        }

        if($zone == 1  &&  defined('CREATEACCOUNT_EXPRESSENABLED') && CREATEACCOUNT_EXPRESSENABLED === true) {

            $localtime = localtime(time(), true);


            $wday = $localtime['tm_wday'];

            $clock = date('Hi', time());

            if(($wday == '4'  &&  (int)$clock >= 1430)    ||    ($wday == '5'  &&  (int)$clock < 1430)){
                $optionSaturdayExpress = true;
            }

            $shipping .= '<p id="shipping_rush"><input type="radio" name="shipping" value="rush" ' . ($this->shipping['type']=='rush' ? ' checked="checked"' : '') . ' onclick="showDhlexpress(); setShipping(this, 1, \'#shipping_rush label\');" id="shipping_rush_radio" /><label for="shipping_rush_radio"><b>'.(($optionSaturdayExpress) ? CREATE_ACCOUNT_DHL_LABEL_MONDAY : CREATE_ACCOUNT_DHL_LABEL).'</b> - '.$this->numberFormat($this->calc_eval_tax($this->rushShipping)).$this->currencies[$this->currency_code]['symbolweb'].'<br /></label><div>'.CHECKOUT_EXPRESS_HINT.'</div></p>';

            if($optionSaturdayExpress){
                $shipping .= '<p id="shipping_rush"><input type="radio" name="shipping" value="rush_saturday" ' . ($this->shipping['type']=='rush' ? ' checked="checked"' : '') . ' onclick="showDhlexpress();setShipping(this, 1, \'#shipping_rush label\');" id="shipping_rush_radio" /><label for="shipping_rush_radio"><b>'.CREATE_ACCOUNT_DHL_LABEL_SATURDAY.'</b> - '.$this->numberFormat($this->calc_eval_tax($this->rushShippingSaturday)).$this->currencies[$this->currency_code]['symbolweb'].'<br /></label><div>'.CHECKOUT_EXPRESS_HINT_SATURDAY.'</div></p>';
            }
        }

        if($this->getDefaultStoreGeoZone() != '3'){
            $shipping .= '<p id="shipping_standard"><input type="radio" name="shipping" value="standard" ' . ($this->shipping['type']=='standard' && $zone == 2? 'checked="checked"' : '') . ' onclick="setShipping(this, 2, \'#shipping_standard label\');" id="shipping_standard_radio" /><label for="shipping_standard_radio"><b>'.CHECKOUT_STANDARDSHIPPING_EU.'</b> - '.$this->numberFormat($this->calc_eval_tax($this->zonesShipping[2])).$this->currencies[$this->currency_code]['symbolweb'].'<br /></label><div>'.CHECKOUT_STANDARDSHIPPING_HINT_EU.'</div></p>';
        }

        $shipping .= '<p id="shipping_standard"><input type="radio" name="shipping" value="standard" ' . ($this->shipping['type']=='standard' && $zone == 3? 'checked="checked"' : '') . ' onclick="setShipping(this, 3, \'#shipping_standard label\');" id="shipping_standard_radio" /><label for="shipping_standard_radio"><b>'.CHECKOUT_STANDARDSHIPPING_NONEU.'</b> - '.$this->numberFormat($this->calc_eval_tax($this->zonesShipping[3])).$this->currencies[$this->currency_code]['symbolweb'].'<br /></label><div>'.CHECKOUT_STANDARDSHIPPING_HINT_NONEU.'</div></p>';

        $shipping .= '</form>';

        return $shipping;

    }

    /******************************************
     *	Total
     ******************************************/

    function setTotal() {

        $subtotal = $this->subtotal['value'];

        if($this->coupon['error']=='' and $this->coupon['type'] == 'percent') {
            $subtotal -= ($subtotal/100)*$this->coupon['value'];
        }
        elseif($this->coupon['error']=='' and $this->coupon['type'] == 'fixed') {
            $subtotal -= $this->coupon['value'];
        }

        $total = round((floatval($subtotal) + floatval($this->shipping['value'])), 2);


        // special case
        //		german company		and: test
        //		foreign company without VAT
        // must get billing in net, only sum is gross, test
        if($this->isInvoicetypeNetPositionsGrossSum() && !$this->getNetto()){
            $total = (( $total * (($this->taxRate+100)/100) ));
        }

        $this->total = array(
            'title' => CHECKOUT_TOTAL,
            'value' => $total,
            'text' => $this->numberFormat($total).$this->currencies[$this->currency_code]['symbolweb']
        );
    }

    function getOtTotal() {

        return '<span id="ot_total"><b>' . $this->total['title'].'</b>: <em>'.str_replace(' ', '&nbsp;', $this->total['text']).'</em></span>';

    }

    /******************************************
     *	Account
     ******************************************/

    function getBilling($field, $preFilledData = NULL, $disabled = false) {

        if($field == 'country') {

            $billing = '';

            if($this->billingFields['country'][1]  ===  true  &&  $this->billingFields['country'][2] === true  &&  empty($this->billing['country'])){  // must parameter && not-first getting-in
                $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields['country'][0]);
                $class = 'validation-error';
                $errorMsg = (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
            }

            $billing .= $this->getBillingCountries($errorMsg, $preFilledData);

            return $billing;
        }

        if($field == 'gender') {
            return $this->getBillingGender((($this->billing['firstname'] == '') ? true : false), $preFilledData);
        }

        if($this->billingFields[$field][2]!==true) {
            $error = false;
            $class = '';
        }
        elseif($this->billingFields[$field][1]===true) {

            if($field == 'email'  &&  !tep_validate_email($this->billing[$field])){
                $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields[$field][0]);
                $class = 'validation-error';
            }
            elseif(empty($this->billing[$field])){
                $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields[$field][0]);
                $class = 'validation-error';
            }
        }
        elseif(is_int($this->billingFields[$field][1]) and strlen($this->billing[$field])<$this->billingFields[$field][1]) {
            $error = call_user_func_array('sprintf', array(CHECKOUT_BILLING_SHORT, $this->billingFields[$field][0], $this->billingFields[$field][1]));
            $class = 'validation-error';
        }
        else {
            $error = false;
            $class = 'validation-ok';
        }

        if($this->billingFields[$field][1]!==false) {
            $required = ' *';
        }

        $billing  = '<div class="billing'.$field.'">';
        $billing .= '<label for="billing'.$field.'">'.$this->billingFields[$field][0].':'.$required.'</label>';
        $billing .= '<input type="text" id="billing'.$field.'" name="billing'.$field.'" onblur="updateAccount(\''.$field.'\', \'billing\', this);" value="'.((!empty($preFilledData)) ? $preFilledData : $this->billing[$field]).'" class="'.$class.'" '.(($disabled === true) ? 'disabled="true"': '').' />';
        $billing .= (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
        $billing .= '</div>';

        return $billing;
    }

    function getPayment($method) {

        if(!$this->paymentMethods[$method])   return NULL;

        $payment  = '<div class="payment'.$method.'">';
        $payment .= '<div class="sel">
								<input type="radio" name="payment" value="'.$method.'" onClick="updatePaymeth(this); showPaymethodExtraInput('.(($method == 'gc_ipayment') ? 'true' : 'false').', \'gc_ipayment\')"'.(($this->selectedPaymentMethod == $method) ? ' checked="checked"' : '').' />'.
            '</div>
              <div class="descr">'.
            '<b>'.$this->paymentMethods[$method]['name'].'</b>'.( ($this->paymentMethods[$method]['extrainfo']) ? ' '.$this->paymentMethods[$method]['extrainfo'] : '').
            '<p>'.$this->paymentMethods[$method]['description'].'</p>';

        if($method == 'gc_ipayment'){
            $payment .= '<div id="extrainput_gc_ipayment" class="extrainput_required'.(($this->selectedPaymentMethod == 'gc_ipayment') ? '_visible' : '').'">'.
                $this->getCC('name', (($this->selectedPaymentMethod != 'gc_ipayment') ? false : true)).
                $this->getCC('number', (($this->selectedPaymentMethod != 'gc_ipayment') ? false : true)).
                $this->getCCDate().
                $this->getCC('security',  (($this->selectedPaymentMethod != 'gc_ipayment') ? false : true)).'<span id="cchint" class="warning hidden"></span>';
            if( defined('SHOPPING_CART_LEGALNOTE') ){ $payment .= '<p style="margin-top: 15px;">'.SHOPPING_CART_LEGALNOTE.'</p>'; }
            $payment .= '</div>';
        }

        $payment .= '</div>'.
            '</div>';

        return $payment;
    }

    function getDelivery($field) {

        if($field == 'country') {

            if($this->deliveryFields['country'][1]  ===  true  &&  $this->deliveryFields['country'][2] === true  &&  empty($this->delivery['country'])){  // must parameter && not-first getting-in
                $error = sprintf(CHECKOUT_BILLING_INVALID, $this->deliveryFields['country'][0]);
                $class = 'validation-error';
                $errorMsg = (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
            }

            $delivery .= $this->getDeliveryCountries($errorMsg);

            return $delivery;
        }

        if($field == 'gender') {
            return $this->getDeliveryGender();
        }

        if($this->deliveryFields[$field][2]!==true) {
            $error = false;
            $class = '';
        }
        elseif($this->deliveryFields[$field][1]===true and !tep_validate_email($this->delivery[$field])) {
            $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields[$field][0]);
            $class = 'validation-error';
        }
        elseif(is_int($this->deliveryFields[$field][1]) and strlen($this->delivery[$field])<$this->deliveryFields[$field][1]) {
            $error = 'Your '.$this->deliveryFields[$field][0].' must contain a minimum of '.$this->deliveryFields[$field][1].' characters.';
            $class = 'validation-error';
        }
        else {
            $error = false;
            $class = 'validation-ok';
        }

        if($this->billingFields[$field][1]!==false) {
            $required = ' *';
        }

        $delivery  = '<div class="delivery'.$field.'">';
        $delivery  .= '<label for="delivery'.$field.'">'.$this->deliveryFields[$field][0].':'.$required.'</label>';
        $delivery .= '<input type="text" id="delivery'.$field.'" name="delivery'.$field.'" onblur="updateAccount(\''.$field.'\', \'delivery\', this);" value="'.$this->delivery[$field].'" class="'.$class.'" />';
        $delivery .= (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
        $delivery .= '</div>';

        return $delivery;

    }

    function getCC($field, $aValidate = true) {

        if($this->ccFields[$field][2]!==true) {
            $error = false;
            $class = '';
        }
        elseif($aValidate  &&   is_int($this->ccFields[$field][1]) and strlen($this->cc[$field])<$this->ccFields[$field][1]) {
            $error = call_user_func_array('sprintf', array(CHECKOUT_BILLING_SHORT, $this->ccFields[$field][0], $this->ccFields[$field][1]));
            $class = 'validation-error';
        }
        elseif($aValidate  &&  !is_int($this->ccFields[$field][1]) and !empty($this->ccFields[$field][1]) and !preg_match($this->ccFields[$field][1], $this->cc[$field])) {
            $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields[$field][0]);
            $class = 'validation-error';
        }
        else {
            $error = false;
            $class = 'validation-ok';
        }

        if($this->billingFields[$field][1]!==false) {
            $required = ' *';
        }

        $cc  = '<div class="cc'.$field.'">';
        $cc  .= '<label for="cc'.$field.'">'.$this->ccFields[$field][0].':'.$required.'</label>';
        $cc .= '<input type="text" id="cc'.$field.'" name="cc'.$field.'" onblur="updateAccount(\''.$field.'\', \'cc\', this);" value="'.$this->cc[$field].'" class="'.$class.'" />';
        $cc .= (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
        $cc .= '</div>';

        return $cc;
    }

    function getBillingCountries($aErrorMsg = NULL, $aSelectedCid = NULL) {

        $billingCountries  = '<div class="billingcountry">';


        $billingCountries .= '<label for="billingcountry">'.$this->billingFields['country'][0].': *</label>';

        $countriesEU = tep_get_countries('', false, 2);
        $countriesNONEU = tep_get_countries('', false, 3);

        $countries_array[] = array('id' => '', 'text' => CHECKOUT_PLEASE_SELECT);

        if(defined('SERVER_COUNTRY_ID')  &&  SERVER_COUNTRY_ID != ''){

            $countries_array[] = array('id' => SERVER_COUNTRY_ID, 'text' => $this->getCountryName(SERVER_COUNTRY_ID));
        }

        if(defined('COUNTRIES_EU_NONALLOW')){

            $conf_countries_nonallow_EU = explode(',', COUNTRIES_EU_NONALLOW);

            $countries_array[] = array('id' => ' ', 'text' => CHECKOUT_STANDARDSHIPPING_HINT_EU);
            foreach($countriesEU as $c):

                if(!in_array($c['countries_id'], $conf_countries_nonallow_EU)){
                    $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
                }
            endforeach;
        }
        else {
            foreach($countriesEU as $c):
                $countries_array[] = array('id' => ' ', 'text' => CHECKOUT_STANDARDSHIPPING_HINT_EU);
                $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
            endforeach;
        }

        if(defined('COUNTRIES_NONEU_ALLOW')){

            $conf_countries_allow_nonEU = explode(',', COUNTRIES_NONEU_ALLOW);
            $countries_array[] = array('id' => ' ', 'text' => CHECKOUT_STANDARDSHIPPING_HINT_NONEU);
            foreach($countriesNONEU as $c):

                if(in_array($c['countries_id'], $conf_countries_allow_nonEU)){
                    $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
                }
            endforeach;
        }
        else {
            foreach($countriesNONEU as $c):
                $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
            endforeach;
        }

        $billingCountries .= tep_draw_pull_down_menu('billingcountry', $countries_array, ((!empty($aSelectedCid)) ? $aSelectedCid : $this->billing['country']), 'id="billingcountry" onchange="updateAccount(\'country\', \'billing\', this);"');

        if($aErrorMsg){
            $billingCountries .= $aErrorMsg;
        }

        $billingCountries .= '</div>';

        return $billingCountries;
    }

    function getBillingGender($aFirstGen = false, $aSelectedValue = NULL) {

        if($this->billingFields['gender'][1]===true  &&  $this->billing['gender'] == ''  &&  $aFirstGen === false) {
            $error = sprintf(CHECKOUT_BILLING_INVALID, $this->billingFields['gender'][0]);
            $class = 'validation-error';
        }

        $billingGender  = '<div class="billinggender">';
        $billingGender .= '<label for="billinggender">'.$this->billingFields['gender'][0].':</label>';

        $gender_array[] = array('id' => '', 'text' => CHECKOUT_PLEASE_SELECT);
        $gender_array[] = array('id' => 'm', 'text' => MALE);
        $gender_array[] = array('id' => 'f', 'text' => FEMALE);

        $billingGender .= tep_draw_pull_down_menu('billinggender', $gender_array, ((!empty($aSelectedValue)) ? $aSelectedValue : $this->billing['gender']), 'id="billinggender" onchange="updateAccount(\'gender\', \'billing\', this);"');
        $billingGender .= (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
        $billingGender .= '</div>';

        return $billingGender;
    }

    function getDeliveryGender() {

        if($this->deliveryFields['gender'][1]===true  &&  $this->delivery['gender'] == ''  &&  !$this->deliverysameasbilling) {
            $error = sprintf(CHECKOUT_BILLING_INVALID, $this->deliveryFields['gender'][0]);
            $class = 'validation-error';
        }

        $deliveryGender  = '<div class="deliverygender">';
        $deliveryGender .= '<label for="deliverygender">'.$this->deliveryFields['gender'][0].':</label>';

        $gender_array[] = array('id' => '', 'text' => CHECKOUT_PLEASE_SELECT);
        $gender_array[] = array('id' => 'm', 'text' => MALE);
        $gender_array[] = array('id' => 'f', 'text' => FEMALE);

        $deliveryGender .= tep_draw_pull_down_menu('deliverygender', $gender_array, $this->delivery['gender'], 'id="deliverygender" onchange="updateAccount(\'gender\', \'delivery\', this);"');
        $deliveryGender .= (empty($error) ? '' : '<span class="'.$class.'">'.$error.'</span>');
        $deliveryGender .= '</div>';

        return $deliveryGender;
    }


    function getDeliveryCountries($aErrorMsg = NULL) {

        $deliveryCountries  = '<div class="deliverycountry">';

        if($aErrorMsg){
            $deliveryCountries .= $aErrorMsg;
        }

        $deliveryCountries .= '<label for="deliverycountry">'.$this->deliveryFields['country'][0].':</label>';

        $countriesEU = tep_get_countries('', false, 2);
        $countriesNONEU = tep_get_countries('', false, 3);

        $countries_array[] = array('id' => '', 'text' => CHECKOUT_PLEASE_SELECT);

        if(defined('SERVER_COUNTRY_ID')  &&  SERVER_COUNTRY_ID != ''){

            $countries_array[] = array('id' => SERVER_COUNTRY_ID, 'text' => $this->getCountryName(SERVER_COUNTRY_ID));
        }

        if(defined('COUNTRIES_EU_NONALLOW')){

            $conf_countries_nonallow_EU = explode(',', COUNTRIES_EU_NONALLOW);
            foreach($countriesEU as $c):

                if(!in_array($c['countries_id'], $conf_countries_nonallow_EU)){
                    $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
                }
            endforeach;
        }
        else {
            foreach($countriesEU as $c):
                $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
            endforeach;
        }

        if(defined('COUNTRIES_NONEU_ALLOW')){

            $conf_countries_allow_nonEU = explode(',', COUNTRIES_NONEU_ALLOW);
            foreach($countriesNONEU as $c):

                if(in_array($c['countries_id'], $conf_countries_allow_nonEU)){
                    $countries_array[] = array('id' => $c['countries_id'], 'text' => $c['countries_name']);
                }
            endforeach;
        }
        else {
            foreach($countriesNONEU as $c):
                $countries_array[] = array('id' => $c[$i]['countries_id'], 'text' => $c['countries_name']);
            endforeach;
        }

        $deliveryCountries .= tep_draw_pull_down_menu('deliverycountry', $countries_array, $this->delivery['country'], 'id="deliverycountry" onchange="updateAccount(\'country\', \'delivery\', this);"');
        $deliveryCountries .= '</div>';

        return $deliveryCountries;

    }

    function getCCDate() {

        $ccdate  = '<div>';
        $ccdate .= '<div><label for="ccexpireyear">'.$this->ccFields['expireyear'][0].': *</label></div>';

        $rangeYear = array();
        $rangeMonth = array();

        foreach(range(date('Y'), date('Y')+5) as $tmprangeYear) {
            $rangeYear[] = array('id' => $tmprangeYear, 'text' => $tmprangeYear);
        }

        foreach(range(1, 12) as $tmprangeMonth) {
            $rangeMonth[] = array('id' => $tmprangeMonth, 'text' => $tmprangeMonth);
        }

        $ccdate .= '<div>';
        $ccdate .= tep_draw_pull_down_menu('ccexpiremonth', $rangeMonth, $this->cc['expiremonth'], 'id="ccexpiremonth" onchange="updateAccount(\'expiremonth\', \'cc\', this);"');
        $ccdate .= tep_draw_pull_down_menu('ccexpireyear', $rangeYear, $this->cc['expireyear'], 'id="ccexpireyear" onchange="updateAccount(\'expireyear\', \'cc\', this);"');
        $ccdate .= '</div>';

        $ccdate .= '</div>';

        return $ccdate;

    }

    /* M.W. */
    function updatePaymentMethod($value){

        $this->selectedPaymentMethod = $value;
        return(true);
    }
    function updateAccount($field, $group, $value) {

        $return = array();

        $value = trim(strip_tags($value));

        if($group=='billing' and in_array($field, array_keys($this->billingFields))) {

            $this->billing[$field] = $value;
            $this->billingFields[$field][2] = true;

            //if($field!='country' and $field!='state') {

            $return[] = array('id' => 'div.billing'.$field, 'html' => $this->getBilling($field, NULL, ((tep_session_is_registered('customer_id')) ? true : false)));
            //}

            if($field=='email' and !empty($this->coupon['code'])) {

                $this->checkCoupon($this->coupon['code']);

                if(!empty($this->coupon['error'])) {

                    $return[] = array('id' => '#couponemailerror', 'html' => '<span id="couponemailerror" class="warning">'.$this->coupon['error'].'</span>');
                }
                else {
                    $return[] = array('id' => '#couponemailerror', 'html' => '<span id="couponemailerror" class="warning hidden"></span>');
                }

                $this->setShipping();
                $this->setTotal();
                $return[] = array('id' => '#form_coupon', 'html' => $this->getCoupon());
                $return[] = array('id' => '#ot_coupon', 'html' => $this->getOtCoupon());
                $return[] = array('id' => '#ot_shipping', 'html' => $this->getOtShipping());
                $return[] = array('id' => '#ot_total', 'html' => $this->getOtTotal());
                $return[] = array('id' => '#sc_headerinfo', 'html' => $this->getHeaderInfo());

            }

        }
        elseif($group=='delivery' and in_array($field, array_keys($this->deliveryFields))) {
            $this->delivery[$field] = $value;
            $this->deliveryFields[$field][2] = true;
            //if($field!='country' and $field!='state') {
            $return[] = array('id' => 'div.delivery'.$field, 'html' => $this->getDelivery($field));
            //}
        }
        elseif($group=='cc' and in_array($field, array_keys($this->ccFields))) {
            $this->cc[$field] = $value;
            $this->ccFields[$field][2] = true;
            if($field!='expireyear' and $field!='expiremonth') {
                $return[] = array('id' => 'div.cc'.$field, 'html' => $this->getCC($field));
            }
        }
        elseif($group=='note' and $field=='note') {
            $this->note = $value;
            $return[] = array();
        }

        if(($this->deliverysameasbilling == true and $group=='billing' and $field=='country') or ($group=='delivery' and $field=='country')) {
            if($this->getGeoZoneId()==3) {
                $this->setShippingStandard();
            }
        }

        return $return;

    }

    function deliverySameAsBilling($state=true) {

        $this->deliverysameasbilling = $state;

        $_SESSION['checkout']->setShipping();
        $_SESSION['checkout']->setTotal();
    }

    function authorizeCC() {

        if(DEBUG===true) {
            return true;
        }

        return true;

        if(!empty($this->cc['name']) and !empty($this->cc['number']) and !empty($this->cc['expiremonth']) and !empty($this->cc['expireyear']) and !empty($this->cc['security'])) {

            require_once DIR_FS_CATALOG.'includes/anet_php_sdk/AuthorizeNet.php';
            #$transaction = new AuthorizeNetAIM('4a7j6TvK6', '2jADtN78z2h6zz7q'); // Testaccount: markusengel08/Bulletproof08
            $transaction = new AuthorizeNetAIM('658XcfKx2', '6G2aFtV9xc5C485s');
            $transaction->amount = $this->total['value'];
            $transaction->card_num = $this->cc['number'];
            $transaction->card_code = $this->cc['security'];
            $transaction->exp_date = $this->cc['expiremonth'].$this->cc['expireyear'];

            $transaction->first_name = $this->billing['firstname'];
            $transaction->last_name = $this->billing['lastname'];
            $transaction->company = $this->billing['company'];
            $transaction->address = $this->billing['street'];
            $transaction->city = $this->billing['city'];
            $transaction->zip = $this->billing['zipcode'];
            $transaction->country = tep_get_country_name($this->billing['country']);

            if (MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TRANSACTION_MODE == 'Test') {
                $transaction->test_request = 'TRUE';
            }

            $response = $transaction->authorizeOnly();

            if ($response->approved) {
                return true;
            }
            else {

                if($response->response_reason_code == 290) {

                    return CHECKOUT_FILLOUT_ALLINFORMATION.'<br /><a href="' . tep_href_link('checkout.php', '', 'SSL') . '" onclick="validateCC(); return false;">'.CHECKOUT_REVALIDATE.'</a>';

                }

                return $response->response_reason_text;
            }

        }

        return false;

    }

    function captureCC($aHash = NULL, $aReference = NULL) {

        if(DEBUG===true) {
            return true;
        }


        if(!$aHash  ||   !$aReference)   return(false);

        if(!empty($this->cc['name']) and !empty($this->cc['number']) and !empty($this->cc['expiremonth']) and !empty($this->cc['expireyear']) and !empty($this->cc['security'])) {

            $gateway_url = IPAYMENT_URL.'?';
            $params = array(
                'trxuser_id'=>IPAYMENT_TRXUSER_ID,
                'trxpassword'=>IPAYMENT_TRXPASSWORD,
                'trx_paymenttyp'=>'cc',
                'trx_amount'=>(100*round($this->total['value'],2)),
                'trx_currency'=>LANGUAGE_CURRENCY,
                'addr_name'=>$this->billing['firstname'].' '.$this->billing['lastname'],
                'addr_email'=>$this->billing['email'],
                'maxSuggestions'=>'0',
                'addr_street'=>$this->delivery['street'],
                'addr_city'=>$this->delivery['city'],
                'addr_zip'=>$this->getPostcodeForAPI(),
                'addr_country'=>$this->getCountrycodeForAPI(),
                'cc_number'=>$this->cc['number'],
                'cc_expdate_month'=>$this->cc['expiremonth'],
                'cc_expdate_year'=>$this->cc['expireyear'],
                'cc_checkcode'=>$this->cc['security'],
                'silent'=>'0',
                'gateway'=>'1',
                'from_ip'=>$this->getClientIP(),
                'send_confirmation_email'=>'0',
                'trx_securityhash'=>md5(IPAYMENT_TRXUSER_ID.(100*$this->total['value']).LANGUAGE_CURRENCY.IPAYMENT_TRXPASSWORD.IPAYMENT_SECURITYKEY),
                'hash'=>$aHash,
                'shopper_id'=>$aReference,
                'error_lang'=>(($this->getShopLanguageCode() == 'de') ? 'de' : 'en')
            );

            foreach($params as $k=>$v):
                $gateway_url .= $k.'='.urlencode($v).'&';
            endforeach;

            $this->_log("\ngateway:".$gateway_url);
            $this->_log("total:".$this->total['value']."\n"."subtotal:".$this->subtotal['value']."\n"."shipping:".$this->shipping['value']."\n");

            $response = file_get_contents($gateway_url);

            if(empty($response))   return(CHECKOUT_CREDITCARD_GENERALERROR);

            list($responseStatus, $responseParams) = explode("\n", $response);
            $params = explode('&', $responseParams);

            foreach($params as $p):
                list($k, $v) = explode('=', $p);
                $paramsArray[$k] = $v;
            endforeach;

            if($responseStatus == 'Status=0'){
                $this->_log('return:'.json_encode($paramsArray));
                $this->successfulCreditcardType = $paramsArray['trx_paymentmethod'];
                $this->sucessfulCreditcardAuthcode = $paramsArray['ret_authcode'];

                /*$this->_log('CC payment successful:'."\n");
                $this->_log($paramsArray);*/
                return(true);
            }
            else {


                if($paramsArray['ret_errormsg']){
                    return(urldecode($paramsArray['ret_errormsg']));
                }
                else {
                    return(CHECKOUT_CREDITCARD_GENERALERROR);
                }
            }
        }

        return false;
    }

    function getPostcodeForAPI(){

        $c = (int)$this->delivery['country'];
        $p = $this->delivery['zipcode'];

        if(in_array($c, array(81, 204, 14))){  // Deutschland, Österreich, Schweiz
            //$this->_log("1\n\n");
            if(strstr($p, '-')){
                //$this->_log("2\n\n");
                list(, $postcode) = explode('-', $p);
                return($postcode);
            }
            else {
                return($p);
            }
        }
        elseif($c === 150) {  // Niederlande
            return('1234AB');
        }
        else {
            return($p);
        }
    }

    function getCountrycodeForAPI(){

        $c = $this->getISOCountryCode($this->delivery['country']);

        if($c == 'FX'){
            return('FR');
        }

        return($c);
    }

    // M.W.
    function getClientIP(){

        if($_SERVER['HTTP_CLIENT_IP'])
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif($_SERVER['REMOTE_ADDR'])
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = 'unidentified';

        return($ip);
    }

    /******************************************
     *	General
     ******************************************/

    function getHeaderInfo() {

        global $cart;

        $countContents = $_SESSION['cart']->count_contents();
        return '<span id="sc_headerinfo"><b>' . (($countContents > 1) ? $countContents.' '.CHECKOUT_ITEMS : $countContents.' '.CHECKOUT_ITEM) . '</b>'.CHECKOUT_TOTALAMOUNT.'<b>'.$this->numberFormat($this->total['value']).$this->currencies[$this->currency_code]['symbolweb'].'</b> <a href="'.tep_href_link('checkout.php', '', 'SSL').'" onclick="gotoCart(); return false;">('.CHECKOUT_EDIT.')</a></span>';

    }

    function getOt() {

        $ot  = $this->getOtSubtotal();
        $ot .= $this->getOtCoupon();
        $ot .= $this->getOtShipping();
        $ot .= $this->getOtTax();
        $ot .= $this->getOtTotal();

        return $ot;

    }

    function getGeoZoneId() {

        $countryID = ($this->deliverysameasbilling==true) ? $this->billing['country'] : $this->delivery['country'];

        if(empty($countryID)){

            // case: no country selected so far, but shipping zone set by customer radioShipping first page
            if($this->geozonePreSelected){
                return($this->geozonePreSelected);
            }
            else {
                return($this->getDefaultStoreGeoZone());
            }
        }
        else {

            $res = tep_db_query('SELECT geo_zone_id FROM '.TABLE_ZONES_TO_GEO_ZONES.' WHERE zone_country_id = '.$countryID);
            list($zone) = tep_db_fetch_row($res);

            return($zone);
        }

        return(1);
    }

    /* M.W. */
    function getCountryName($aCountryID = NULL){

        if(!$aCountryID)   return(false);

        $res = tep_db_query('SELECT countries_localname FROM '.TABLE_COUNTRIES.' WHERE countries_id = '.$aCountryID);
        list($countryName) = tep_db_fetch_row($res);

        return($countryName);
    }

    /* M.W. */
    function getISOCountryCode($aCountryID = NULL){

        if(!$aCountryID)   return(false);

        $res = tep_db_query('SELECT countries_iso_code_2 FROM '.TABLE_COUNTRIES.' WHERE countries_id = '.$aCountryID);
        list($iso) = tep_db_fetch_row($res);

        return($iso);
    }

    /* M.W. */
    function getShopLanguageCode(){

        $res = tep_db_query('SELECT language_code FROM '.TABLE_LANGUAGES.' WHERE languages_id = '.LANGUAGE_ID);
        list($langCode) = tep_db_fetch_row($res);

        return($langCode);
    }

    function getProductStock($pid) {

        global $cart;

        $product = $_SESSION['cart']->get_product($pid);

        if (isset($product['attributes']) && is_array($product['attributes'])) {

            while (list($option, $value) = each($product['attributes'])) {

                $attributes = tep_db_query("select popt.products_options_name, popt.special, poval.products_options_values_name, pa.options_values_price, pa.price_prefix
											from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa
											where pa.products_id = '" . $product['id'] . "'
											and pa.options_id = '" . $option . "'
											and pa.options_id = popt.products_options_id
											and pa.options_values_id = '" . $value . "'
											and pa.options_values_id = poval.products_options_values_id
											and popt.language_id = '" . $languages_id . "'
											and poval.language_id = '" . $languages_id . "'");

                $attributes_values = tep_db_fetch_array($attributes);

                $product[$option]['products_options_name'] = $attributes_values['products_options_name'];
                $product[$option]['options_values_id'] = $value;
                $product[$option]['products_options_values_name'] = $attributes_values['products_options_values_name'];
                $product[$option]['options_values_price'] = $attributes_values['options_values_price'];
                $product[$option]['price_prefix'] = $attributes_values['price_prefix'];
                $product[$option]['special'] = $attributes_values['special'];

            }

        }

        if (isset($product['attributes']) && is_array($product['attributes'])) {

            return tep_check_stock_new($product['id'], $product['attributes'], $product['quantity']);

        }
        else {

            return tep_check_stock($product['id'], $product['quantity']);

        }

    }



    function getProductsFromCart() {

        global $cart;

        $products = $_SESSION['cart']->get_products();

        for ($i=0, $n=sizeof($products); $i<$n; $i++) {

            $this->products[$i] = array(
                'qty' => $products[$i]['quantity'],
                'name' => $products[$i]['name'],
                'model' => $products[$i]['model'],
                'tax' => tep_get_tax_rate($products[$i]['tax_class_id'], ($this->deliverysameasbilling==true) ? $this->billing['country'] : $this->delivery['country'], $this->getGeoZoneId()),
                'tax_description' => tep_get_tax_description($products[$i]['tax_class_id'], $this->billing['country'], $this->getGeoZoneId()),
                'price' => $products[$i]['price'],
                'final_price' => $products[$i]['price'] + $_SESSION['cart']->attributes_price($products[$i]['id']),
                'weight' => $products[$i]['weight'],
                'id' => $products[$i]['id']
            );

            if ($products[$i]['attributes']) {

                $subindex = 0;
                reset($products[$i]['attributes']);

                while (list($option, $value) = each($products[$i]['attributes'])) {

                    $attributes_query = tep_db_query("select popt.products_options_name, popt.special, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . (int)$products[$i]['id'] . "' and pa.options_id = '" . (int)$option . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . (int)$value . "' and pa.options_values_id = poval.products_options_values_id and popt.language_id = '" . LANGUAGE_ID . "' and poval.language_id = '" . LANGUAGE_ID . "'");
                    $attributes = tep_db_fetch_array($attributes_query);

                    $this->products[$i]['attributes'][$subindex] = array(
                        'option' => $attributes['products_options_name'],
                        'value' => $attributes['products_options_values_name'],
                        'option_id' => $option,
                        'value_id' => $value,
                        'prefix' => $attributes['price_prefix'],
                        'price' => $attributes['options_values_price'],
                        'special' => $attributes['special']
                    );

                    $subindex++;
                }
            }
        }
    }


    function checkIfCartHasChanged(){

        $res = tep_db_query('SELECT products_id, products_quantity FROM '.TABLE_ORDERS_PRODUCTS.' WHERE orders_id = '.$this->insertedOrdersID);
        while(list($check_prId, $check_prQu) = tep_db_fetch_row($res)):
            $previousOrderCart[$check_prId] = $check_prQu;
        endwhile;

        $this->getProductsFromCart();

        $cartHasChanged = false;
        for ($i=0, $n=sizeof($this->products); $i<$n; $i++) {

            if(!$previousOrderCart[$this->products[$i]['id']]){

                $cartHasChanged = true;
            }
            else {
                if($previousOrderCart[$this->products[$i]['id']] != $this->products[$i]['qty']){
                    $cartHasChanged = true;
                }
            }
        }

        if(count($this->products) != count($previousOrderCart)){
            $cartHasChanged = true;
        }

        // if cart is the same
        if(!$cartHasChanged){
            if(count($this->products) == 1  &&  $this->products[0]['qty']  ==  1){  // special case, 1 necktie in 1 quantity => we allow a 2nd order
                $cartHasChanged = true;
            }
        }

        return($cartHasChanged);
    }


    function processCheckout(){

        global $currencies, $cart, $shop_name;

        if($this->getGeoZoneId() == 1 && empty($this->billing['company'])  &&  !empty($this->billing['ustid'])){
            $this->billing['ustid'] = '';
        }

        $this->getProductsFromCart();

        $return = array();

        if(empty($this->selectedPaymentMethod)){
            $return[] = array('id' => '#paymentmethodhint', 'html' => '<span id="paymentmethodhint" class="warning">'.CHECKOUT_WARNING_PAYMENTMETHODNOTCHOSEN.'</span>');
        }
        elseif(empty($this->products)  && defined('CHECKOUT_WARNING_NOPRODUCTSINCART')){
            $return[] = array('id' => '#paymentmethodhint', 'html' => '<span id="paymentmethodhint" class="warning">'.CHECKOUT_WARNING_NOPRODUCTSINCART.'</span>');
        }

        foreach($this->billingFields as $field => $values) {

            $this->billingFields[$field][2] = true;

            if($values[1] !== false and $this->billing[$field] == '') {
                $return[] = array('id' => 'div.billing'.$field, 'html' => $this->getBilling($field));
            }
        }

        if($this->deliverysameasbilling === false) {

            foreach($this->deliveryFields as $field => $values) {

                $this->deliveryFields[$field][2] = true;

                if($values[1] !== false and $this->delivery[$field] == '') {
                    $return[] = array('id' => 'div.delivery'.$field, 'html' => $this->getDelivery($field));
                }
            }
        }

        if($this->selectedPaymentMethod == 'gc_ipayment'){

            foreach($this->ccFields as $field => $values) {

                $this->ccFields[$field][2] = true;
                if($values[1] !== false and $this->cc[$field] == '') {
                    $return[] = array('id' => 'div.cc'.$field, 'html' => $this->getCC($field));
                }
            }
        }

        if(!empty($return)) {

            return $return; // test
        }

        if($this->deliverysameasbilling===true) {  // test

            $this->delivery['gender'] = $this->billing['gender'];
            $this->delivery['firstname'] = $this->billing['firstname'];
            $this->delivery['lastname']  = $this->billing['lastname'];
            $this->delivery['company']   = $this->billing['company'];
            $this->delivery['street']    = $this->billing['street'];
            $this->delivery['addressadd']    = $this->billing['addressadd'];
            $this->delivery['city']      = $this->billing['city'];
            $this->delivery['zipcode']   = $this->billing['zipcode'];
            $this->delivery['country']   = $this->billing['country'];
            $this->delivery['addressadd']   = $this->billing['addressadd'];
        }

        // Recalculation, because NOW we might only know if its a company or not
        $this->setCoupon();
        $this->setSubtotal();
        $this->setShipping();
        $this->setTax();
        $this->setTotal();

        if(!empty($this->insertedOrdersID)){
            // already an inserted order -> can be preparing paypal, preparing sofortue or failed credit card
            // if products in cart are the same, then use "old" orderid for this order
            $cartHasChanged = $this->checkIfCartHasChanged();
        }

        // payment CC-Abbuchung
        if($this->selectedPaymentMethod == 'gc_ipayment') {

            $ccHash = md5(mktime().'ABC');

            $res = tep_db_query('SELECT (MAX(orders_id)+1) FROM '.TABLE_ORDERS);
            list($nextOrderId) = tep_db_fetch_row($res); // this is a 99% solution ;-(, we must know next orderid because it must be logged for search purposes on 1und1, but on other hand we have to CAPTURE FIRST and then insert order because capturing Creditcard can fail / if other user comes in within "this second" we might get a wrong orderid here

            $authorizeAndCapture = $this->captureCC($ccHash, SHOP_SHORTNAME.'_'.$nextOrderId);

            if($authorizeAndCapture !== true) {
                $return[] = array('id' => '#cchint', 'html' => '<span id="cchint" class="warning">'.$authorizeAndCapture.'</span>');
            }
            else {

                if($cartHasChanged   ||   empty($this->insertedOrdersID)){  // NEW ORDER
                    $orderid = $this->insertOrder($ccHash);
                }
                else {  // EXISTING ORDER
                    $orderid = $this->insertedOrdersID;
                    $this->updateOrder($orderid, $ccHash);

                    tep_db_query('DELETE FROM '.TABLE_ORDERS_TOTAL.' WHERE orders_id = '.$orderid);
                    tep_db_query('DELETE FROM '.TABLE_ORDERS_PRODUCTS.' WHERE orders_id = '.$orderid);
                    tep_db_query('DELETE FROM '.TABLE_ORDERS_PRODUCTS_ATTRIBUTES.' WHERE orders_id = '.$orderid);
                    tep_db_query('DELETE FROM '.TABLE_ORDERS_STATUS_HISTORY.' WHERE orders_id = '.$orderid);
                }

                $this->insertOrdertotalsAndHistory($orderid);
                $products_ordered = $this->insertProducts($orderid);

                tep_db_perform(TABLE_ORDERS, array('payment_method'=>$this->paymentMethods[$this->selectedPaymentMethod]['name'].' ('.$this->successfulCreditcardType.')'), 'update', 'orders_id = '.$orderid);

                $sql_data_array = array(
                    'orders_id' => $orderid,
                    'orders_status_id' => $this->ordersStati[$this->selectedPaymentMethod]['ready'],
                    'date_added' => 'now()',
                    'customer_notified' => 1,
                    'comments' => $this->note
                );

                tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
                tep_db_perform(TABLE_ORDERS, array('orders_status'=>$this->ordersStati[$this->selectedPaymentMethod]['ready']), 'update', 'orders_id='.$orderid);
            }
        }
        else {

            if($cartHasChanged  ||  empty($this->insertedOrdersID)){  // NEW ORDER
                $orderid = $this->insertOrder($ccHash);
            }
            else {  // EXISTING ORDER
                $orderid = $this->insertedOrdersID;

                // update whole order, customer or delivery data could have changed
                $this->updateOrder($orderid);

                // update to payment method (could have changed)
                tep_db_perform(TABLE_ORDERS, array('payment_method'=>$this->paymentMethods[$this->selectedPaymentMethod]['name']), 'update', 'orders_id = '.$orderid);

                // clean all totals, products and status history (products and totals should not have changed, but we do it as a precaution)
                tep_db_query('DELETE FROM '.TABLE_ORDERS_TOTAL.' WHERE orders_id = '.$orderid);
                tep_db_query('DELETE FROM '.TABLE_ORDERS_PRODUCTS.' WHERE orders_id = '.$orderid);
                tep_db_query('DELETE FROM '.TABLE_ORDERS_PRODUCTS_ATTRIBUTES.' WHERE orders_id = '.$orderid);
                tep_db_query('DELETE FROM '.TABLE_ORDERS_STATUS_HISTORY.' WHERE orders_id = '.$orderid);
            }

            $this->insertOrdertotalsAndHistory($orderid);
            $products_ordered = $this->insertProducts($orderid);
        }


        if(!empty($return)) {
            return $return;
        }


        if($this->coupon['type'] != 'empty' and $this->coupon['error'] == '') {

            tep_db_query("UPDATE coupons SET coupons_quantity = (coupons_quantity-1) WHERE coupons_code = '" . $this->coupon['code'] . "'");

            //Coupon eines Altkunden, aktivieren, wenn der Neukunde einen Werbegutschein eingesetzt hat by Gurkcity 01.06.2006
            if($this->coupon['hash'] != '') {

                $coupon2_query = tep_db_query("SELECT coupons_id, coupons_min_order, coupons_value, coupons_code, coupons_quantity, coupons_valid, coupons_extra, coupons_name, coupons_email FROM coupons where coupons_extra = '" . $this->coupon['hash'] . "'");

                if(tep_db_num_rows($coupon2_query) > 0) {

                    $coupon2 = tep_db_fetch_array($coupon2_query);

                    $sql_data_array = array(
                        'coupons_status'	=>	1,
                        'coupons_datetime' => 'now()'
                    );

                    tep_db_perform(TABLE_COUPONS, $sql_data_array, 'update', "coupons_extra='".$this->coupon['hash']."'");

                    $msg_body = call_user_func_array('sprintf', array(CHECKOUT_COUPON_EMAIL, $shop_name, $this->coupon['name'], $this->coupon['email'], $coupon2['coupons_code'], $coupon2['coupons_valid'], STORE_OWNER));


                    tep_mail($coupon2['coupons_email'], $coupon2['coupons_email'], CHECKOUT_COUPON_SUBJECT, $msg_body, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

                }
            }
        }


        #Newslettermodul by Gurkcity BOF

        //prÃ¼fen ob E-Mail bereits existiert
        //true wenn E-Mail nicht existiert


        // lets start with the email confirmation
        $email_order = $shop_name . "\n\n" .
            EMAIL_TEXT_THANK_YOU . "\n" .
            EMAIL_SEPARATOR . "\n" .
            EMAIL_TEXT_DATE_ORDERED . ' ' . strftime(DATE_FORMAT_LONG) . "\n\n";

        $email_order .= EMAIL_TEXT_PRODUCTS . "\n" .
            EMAIL_SEPARATOR . "\n" .
            $products_ordered.
            EMAIL_SEPARATOR . "\n";

        $email_order .= strip_tags($this->subtotal['title']) . ': ' . strip_tags($this->subtotal['text']) . "\n";
        if($this->tax['value'] > 0 && empty($this->billing['company']) ) {
            $email_order .= strip_tags($this->tax['title']) . ': ' . strip_tags($this->tax['text']) . "\n";
        }
        if($this->coupon['type'] != 'empty') {
            $email_order .= strip_tags($this->coupon['title']) . ': ' . strip_tags($this->coupon['text']) . "\n";
        }
        $email_order .= strip_tags($this->shipping['title']) . ': ' . strip_tags($this->shipping['text']) . "\n";
        if($this->tax['value'] > 0 && !empty($this->billing['company']) ) {
            $email_order .= strip_tags($this->tax['title']) . ': ' . strip_tags($this->tax['text']) . "\n";
        }

        $email_order .= strip_tags($this->total['title']) . ': ' . strip_tags($this->total['text']) . "\n";

        $email_order .= "\n" . EMAIL_TEXT_DELIVERY_ADDRESS . "\n" .
            EMAIL_SEPARATOR . "\n";

        $email_order .= $this->delivery['company'] . "\n";
        $email_order .= $this->delivery['firstname'] . ' ' . $this->delivery['lastname'] . "\n";
        $email_order .= $this->delivery['street'] . "\n";
        if(isset($this->delivery['addressadd']) && !empty($this->delivery['addressadd'])) {
            $email_order .= $this->delivery['addressadd'] . "\n";
        }
        $email_order .= $this->delivery['zipcode'] . " ";
        $email_order .= $this->delivery['city'] . " ";
        $email_order .= $this->delivery['state'] . "\n";
        $email_order .= tep_get_country_name($this->delivery['country']) . "\n\n";

        $email_order .= "\n" . EMAIL_TEXT_BILLING_ADDRESS . "\n" .
            EMAIL_SEPARATOR . "\n";

        $email_order .= $this->billing['company'] . "\n";
        $email_order .= $this->billing['firstname'] . ' ' . $this->billing['lastname'] . "\n";
        $email_order .= $this->billing['street'] . "\n";
        if(isset($this->billing['addressadd']) && !empty($this->billing['addressadd'])) {
            $email_order .= $this->billing['addressadd'] . "\n";
        }
        $email_order .= $this->billing['zipcode'] . " ";
        $email_order .= $this->billing['city']  . " ";
        $email_order .= $this->billing['state'] . "\n";
        $email_order .= tep_get_country_name($this->billing['country']) . "\n";
        $email_order .= $this->billing['email'] . "\n";
        $email_order .= $this->billing['phone'] . "\n\n";

        if($this->coupon['type'] != 'empty') {

            $email_order .= "\n\n" .
                EMAIL_SEPARATOR . "\n" .
                EMAIL_TEXT_USED_COUPONCODE." ".$this->coupon['code']."\n\n";

        }

        if ($this->note) {
            $email_order .= CHECKOUT_NOTE. "\n";;
            $email_order .= tep_db_output($this->note) . "\n\n";
        }

        $email_order .= "\n" . EMAIL_TEXT_NACHSATZ_RECHNUNG . "\n\n";

        $email_order = str_replace($this->currencies[$this->currency_code]['symbolweb'], $this->currencies[$this->currency_code]['symbolemail'], $email_order);


        if(($this->selectedPaymentMethod == 'gc_ipayment'  &&  $authorizeAndCapture  === true)   ||   $this->selectedPaymentMethod == 'invoice'){
            tep_mail($this->billing['firstname'] . ' ' . $this->billing['lastname'], $this->billing['email'], EMAIL_TEXT_SUBJECT, $email_order, ((defined('CHECKOUT_STORE_OWNER')) ? CHECKOUT_STORE_OWNER : STORE_OWNER), ((defined('CHECKOUT_STORE_OWNER_EMAIL_ADDRESS')) ? CHECKOUT_STORE_OWNER_EMAIL_ADDRESS : STORE_OWNER_EMAIL_ADDRESS),
               file_exists(DIR_FS_CATALOG.'Widerruf.pdf') ? array(array('file'=>DIR_FS_CATALOG.'Widerruf.pdf', 'name'=>'Widerrufs-Musterformular.pdf', 'type'=>'application/octet-stream')) : array());

            // send emails to other people
            if (SEND_EXTRA_ORDER_EMAILS_TO != '') {
                tep_mail('', SEND_EXTRA_ORDER_EMAILS_TO, EMAIL_TEXT_SUBJECT, $email_order, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            }
        }

        if($this->paymentMethods[$this->selectedPaymentMethod]['id'] == 'invoice'){
            $_SESSION['cart']->reset(true);
        }

        // GC Referer Tracking
        // Eintrag der Trackingdaten in die Datenbank
        gc_sequery_checkout_process($orderid);

        $this->insertedOrdersID = $_SESSION['id_order'] = $orderid;
        return true;

    }

    function insertProducts($aOrdersId = NULL){

        global $currencies;

        // initialized for the email confirmation
        $products_ordered = '';
        $subtotal = 0;
        $total_tax = 0;

        $isNetto = $this->getNetto();

        for ($i=0, $n=sizeof($this->products); $i<$n; $i++) {

            if (STOCK_LIMITED == 'true') {

                $products_attributes = $this->products[$i]['attributes'];

                if (is_array($products_attributes)) {

                    $products_stock_attributes_array = array();

                    for($k=0, $n3=sizeof($products_attributes); $k<$n3; $k++) {

                        if ($products_attributes[$k]['special'] == 0) {

                            $products_stock_attributes_array[] = $products_attributes[$k]['option_id']."-".$products_attributes[$k]['value_id'];

                        }

                    }

                    asort($products_stock_attributes_array);
                    reset($products_stock_attributes_array);

                    $products_stock_attributes = implode(",", $products_stock_attributes_array);
                    $attributes_stock_query = tep_db_query("select products_stock_quantity from " . TABLE_PRODUCTS_STOCK . " where products_stock_attributes = '$products_stock_attributes' AND products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");

                    if (tep_db_num_rows($attributes_stock_query) > 0) {

                        $attributes_stock_values = tep_db_fetch_array($attributes_stock_query);
                        $attributes_stock_left = $attributes_stock_values['products_stock_quantity'] - $this->products[$i]['qty'];

                        if ($attributes_stock_left < 1) {

                            tep_db_query("update " . TABLE_PRODUCTS_STOCK . " set products_stock_quantity = '0' where products_stock_attributes = '$products_stock_attributes' AND products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");
                            $actual_stock_bought = $attributes_stock_values['products_stock_quantity'];

                        }
                        else {

                            tep_db_query("update " . TABLE_PRODUCTS_STOCK . " set products_stock_quantity = '" . $attributes_stock_left . "' where products_stock_attributes = '$products_stock_attributes' AND products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");
                            $actual_stock_bought = $this->products[$i]['qty'];

                        }
                    }
                    else {

                        $actual_stock_bought = $this->products[$i]['qty'];

                    }

                    $bundle_query = tep_db_query("select bundle_id, hemd_id, krawatte_id from products_bundles where bundle_id = '" . tep_get_prid($order->products[$i]['id']) . "'");
                    $bundle_values = tep_db_fetch_array($bundle_query);

                    $products_stock_attributes_array = array();

                    for($k=0, $n3=sizeof($products_attributes); $k<$n3; $k++) {

                        if ($products_attributes[$k]['special'] == 0) {

                            $products_stock_attributes_array[] = $products_attributes[$k]['option_id']."-".$products_attributes[$k]['value_id'];

                        }

                    }

                    asort($products_stock_attributes_array);
                    reset($products_stock_attributes_array);

                    $products_stock_attributes=implode(",", $products_stock_attributes_array);

                    $attributes_hemd_stock_query = tep_db_query("select products_stock_quantity from " . TABLE_PRODUCTS_STOCK . " where products_stock_attributes = '$products_stock_attributes' AND products_id = '".$bundle_values['hemd_id']."'");

                    if (tep_db_num_rows($attributes_hemd_stock_query) > 0) {

                        $attributes_stock_values = tep_db_fetch_array($attributes_hemd_stock_query);
                        $attributes_stock_left = $attributes_stock_values['products_stock_quantity'] - $this->products[$i]['qty'];
                        $temp = "select products_stock_quantity from " . TABLE_PRODUCTS_STOCK . " where products_stock_attributes = '$products_stock_attributes' AND products_id = '".$bundle_values['hemd_id']."'";

                        if ($attributes_stock_left < 1) {

                            tep_db_query("update " . TABLE_PRODUCTS_STOCK . " set products_stock_quantity = '0' where products_stock_attributes = '$products_stock_attributes' AND products_id = '" . $bundle_values['hemd_id'] . "'");
                            $actual_stock_bought = $attributes_stock_values['products_stock_quantity'];

                        }
                        else {

                            tep_db_query("update " . TABLE_PRODUCTS_STOCK . " set products_stock_quantity = '" . $attributes_stock_left . "' where products_stock_attributes = '$products_stock_attributes' AND products_id = '" . $bundle_values['hemd_id'] . "'");
                            $actual_stock_bought = $this->products[$i]['qty'];

                        }

                    }
                    else {

                        $actual_stock_bought = $this->products[$i]['qty'];

                    }

                }
                else {

                    $actual_stock_bought = $this->products[$i]['qty'];

                }


                $stock_query = tep_db_query("select products_quantity from " . TABLE_PRODUCTS . " where products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");

                if (tep_db_num_rows($stock_query) > 0) {

                    $stock_values = tep_db_fetch_array($stock_query);

                    if ((DOWNLOAD_ENABLED != 'true') || (!$stock_values['products_attributes_filename'])) {
                        $stock_left = $stock_values['products_quantity'] - $actual_stock_bought;
                    }
                    else {
                        $stock_left = $stock_values['products_quantity'];
                    }

                    tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = '" . $stock_left . "' where products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");

                    $hemd_query = tep_db_query("select products_quantity from " . TABLE_PRODUCTS . " where products_id = '".$bundle_values['hemd_id']."'");
                    $hemd_values = tep_db_fetch_array($hemd_query);
                    $hemd_left = $hemd_values['products_quantity'] - $actual_stock_bought;

                    tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = '" . $hemd_left . "' where products_id = '".$bundle_values['hemd_id']."'");

                    $krawatte_query = tep_db_query("select products_quantity from " . TABLE_PRODUCTS . " where products_id = '".$bundle_values['krawatte_id']."'");
                    $krawatte_values = tep_db_fetch_array($krawatte_query);
                    $krawatte_left = $krawatte_values['products_quantity'] - $actual_stock_bought;

                    tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = '" . $krawatte_left . "' where products_id = '".$bundle_values['krawatte_id']."'");

                }

            }

            tep_db_query("update " . TABLE_PRODUCTS . " set products_ordered = products_ordered + " . sprintf('%d', $this->products[$i]['qty']) . " where products_id = '" . tep_get_prid($this->products[$i]['id']) . "'");

            $sql_data_array = array(
                'orders_id' => $aOrdersId,
                'products_id' => tep_get_prid($this->products[$i]['id']),
                'products_model' => $this->products[$i]['model'],
                'products_name' => $this->products[$i]['name'],
                'products_price' => $this->products[$i]['price'],
                'final_price' => $this->products[$i]['final_price'],
                'products_tax' => $this->products[$i]['tax'],
                'products_quantity' => $this->products[$i]['qty']
            );


            tep_db_perform(TABLE_ORDERS_PRODUCTS, $sql_data_array);
            $order_products_id = tep_db_insert_id();

            $attributes_exist = '0';
            $products_ordered_attributes = '';

            if (isset($this->products[$i]['attributes'])) {

                $attributes_exist = '1';

                for ($j=0, $n2=sizeof($this->products[$i]['attributes']); $j<$n2; $j++) {

                    $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . $this->products[$i]['id'] . "' and pa.options_id = '" . $this->products[$i]['attributes'][$j]['option_id'] . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . $this->products[$i]['attributes'][$j]['value_id'] . "' and pa.options_values_id = poval.products_options_values_id and popt.language_id = '" . LANGUAGE_ID . "' and poval.language_id = '" . LANGUAGE_ID . "'");

                    $attributes_values = tep_db_fetch_array($attributes);

                    $sql_data_array = array(
                        'orders_id' => $aOrdersId,
                        'orders_products_id' => $order_products_id,
                        'products_options' => $attributes_values['products_options_name'],
                        'products_options_values' => $attributes_values['products_options_values_name'],
                        'options_values_price' => $attributes_values['options_values_price'],
                        'price_prefix' => $attributes_values['price_prefix']
                    );

                    tep_db_perform(TABLE_ORDERS_PRODUCTS_ATTRIBUTES, $sql_data_array);

                    $products_ordered_attributes .= "\n\t" . $attributes_values['products_options_name'] . ' ' . $attributes_values['products_options_values_name'];
                }
            }

            if( $isNetto || !empty($this->billing['company']) ){
                $products_ordered .= $this->products[$i]['qty'] . ' x ' . $this->products[$i]['name'] . ' (' . $this->products[$i]['model'] . ') = ' . $this->numberFormat($this->products[$i]['final_price'] * $this->products[$i]['qty']) . $this->currencies[$this->currency_code]['symbolweb'] . $products_ordered_attributes . "\n";
            }else {
                $products_ordered .= $this->products[$i]['qty'] . ' x ' . $this->products[$i]['name'] . ' (' . $this->products[$i]['model'] . ') = ' . $this->numberFormat((tep_add_tax($this->products[$i]['final_price'], $this->products[$i]['tax']) * $this->products[$i]['qty'])) . $this->currencies[$this->currency_code]['symbolweb'] . $products_ordered_attributes . "\n";
            }

        }

        return($products_ordered);
    }

    function insertOrder($aHash = NULL){

        $sql_data_array = array(
            'customers_id' => (!empty($_SESSION['customer_id']) ? $_SESSION['customer_id'] : 0),
            'customers_gender' => $this->billing['gender'],
            'customers_vat' => $this->billing['ustid'],
            'customers_name' => $this->billing['firstname'].' '.$this->billing['lastname'],
            'customers_company' => $this->billing['company'],
            'customers_street_address' => $this->billing['street'],
            'customers_addressadd' => $this->billing['addressadd'],
            'customers_city' => $this->billing['city'],
            'customers_postcode' => $this->billing['zipcode'],
            'customers_country' => $this->getCountryName($this->billing['country']),
            'customers_telephone' => $this->billing['phone'],
            'customers_email_address' => $this->billing['email'],
            'customers_address_format_id' => 2,
            'delivery_name' => $this->delivery['firstname'] . ' ' . $this->delivery['lastname'],
            'delivery_gender' => $this->delivery['gender'],
            'delivery_company' => $this->delivery['company'],
            'delivery_street_address' => $this->delivery['street'],
            'delivery_addressadd' => $this->delivery['addressadd'],
            'delivery_city' => $this->delivery['city'],
            'delivery_postcode' => $this->delivery['zipcode'],
            'delivery_state' => $this->delivery['state'],
            'delivery_country' => $this->getCountryName($this->delivery['country']),
            'delivery_address_format_id' => 5,
            'billing_gender' => $this->billing['gender'],
            'billing_name' => $this->billing['firstname'] . ' ' . $this->billing['lastname'],
            'billing_company' => $this->billing['company'],
            'billing_street_address' => $this->billing['street'],
            'billing_city' => $this->billing['city'],
            'billing_postcode' => $this->billing['zipcode'],
            'billing_state' => $this->billing['state'],
            'billing_country' => $this->getCountryName($this->billing['country']),
            'billing_address_format_id' => 2,
            'payment_method' => $this->paymentMethods[$this->selectedPaymentMethod]['name'],
            'shipping_module' => $this->shipping['id'], // Modul notwendig?
            'cc_type' => (($this->selectedPaymentMethod == 'gc_ipayment') ? $this->successfulCreditcardType : ''),
            'cc_owner' => (($this->selectedPaymentMethod == 'gc_ipayment') ? $this->cc['name'] : ''),
            #'cc_number' => $this->cc['number'],
            #'cc_expires' => $this->cc['expiremonth'].$this->cc['expireyear'],
            'ipayment_authcode' => (($this->selectedPaymentMethod == 'gc_ipayment') ? $this->sucessfulCreditcardAuthcode : ''),
            'date_purchased' => 'now()',
            'orders_status' => $this->ordersStati[$this->selectedPaymentMethod]['pre'],
            'currency' => $this->currency_code,
            'currency_value' => 1.0,
            'shop_id' => SHOP_ID,
            'express' => (($this->shipping['type'] == 'rush' || $this->shipping['type'] == 'rush_saturday') ? '1' : '0'),
            'netto' => (($this->getNetto() === true) ? '1' : '0'),
            'firma' => ((!empty($this->billing['company'])) ? '1' : '0'),
            'order_comment' => ((!empty($this->note)) ? '1' : '0'),
            'is_mobile' => ((MOBILE_VERSION === true  ||  $_SESSION['MOBILE_VERSION'] === true) ? 1 : 0),
            'couponCode' => ((!empty($this->coupon['code'])) ? $this->coupon['code'] : '')
        );

        if($this->selectedPaymentMethod == 'gc_ipayment') {

            $sql_data_array['gc_orders_hash'] = $aHash;
        }

        tep_db_perform(TABLE_ORDERS, $sql_data_array);
        $orderid = tep_db_insert_id();

        return($orderid);
    }

    function updateOrder($aOrdersId = NULL, $aHash = NULL){

        if(!$aOrdersId){
            return(false);
        }

        $sql_data_array = array(
            'customers_id' => 0,
            'customers_gender' => $this->billing['gender'],
            'customers_vat' => $this->billing['ustid'],
            'customers_name' => $this->billing['firstname'].' '.$this->billing['lastname'],
            'customers_company' => $this->billing['company'],
            'customers_street_address' => $this->billing['street'],
            'customers_addressadd' => $this->billing['addressadd'],
            'customers_city' => $this->billing['city'],
            'customers_postcode' => $this->billing['zipcode'],
            'customers_country' => $this->getCountryName($this->billing['country']),
            'customers_telephone' => $this->billing['phone'],
            'customers_email_address' => $this->billing['email'],
            'customers_address_format_id' => 2,
            'delivery_name' => $this->delivery['firstname'] . ' ' . $this->delivery['lastname'],
            'delivery_gender' => $this->delivery['gender'],
            'delivery_company' => $this->delivery['company'],
            'delivery_street_address' => $this->delivery['street'],
            'delivery_addressadd' => $this->delivery['addressadd'],
            'delivery_city' => $this->delivery['city'],
            'delivery_postcode' => $this->delivery['zipcode'],
            'delivery_state' => $this->delivery['state'],
            'delivery_country' => $this->getCountryName($this->delivery['country']),
            'delivery_address_format_id' => 5,
            'billing_gender' => $this->billing['gender'],
            'billing_name' => $this->billing['firstname'] . ' ' . $this->billing['lastname'],
            'billing_company' => $this->billing['company'],
            'billing_street_address' => $this->billing['street'],
            'billing_city' => $this->billing['city'],
            'billing_postcode' => $this->billing['zipcode'],
            'billing_state' => $this->billing['state'],
            'billing_country' => $this->getCountryName($this->billing['country']),
            'billing_address_format_id' => 2,
            'payment_method' => $this->paymentMethods[$this->selectedPaymentMethod]['name'],
            'shipping_module' => $this->shipping['id'], // Modul notwendig?
            #'cc_type' => '',
            'cc_owner' => (($this->selectedPaymentMethod == 'gc_ipayment') ? $this->cc['name'] : ''),
            #'cc_number' => $this->cc['number'],
            #'cc_expires' => $this->cc['expiremonth'].$this->cc['expireyear'],
            'date_purchased' => 'now()',
            'orders_status' => $this->ordersStati[$this->selectedPaymentMethod]['pre'],
            'currency' => 'EUR',
            'currency_value' => 1.0,
            'shop_id' => SHOP_ID,
            'express' => (($this->shipping['type'] == 'rush') ? '1' : '0'),
            'netto' => (($this->getNetto() === true) ? '1' : '0'),
            'firma' => ((!empty($this->billing['company'])) ? '1' : '0'),
            'payment_method'=>$this->paymentMethods[$this->selectedPaymentMethod]['name']
        );

        if($this->selectedPaymentMethod == 'gc_ipayment') {

            $sql_data_array['gc_orders_hash'] = $aHash;
        }


        tep_db_perform(TABLE_ORDERS, $sql_data_array, 'update', 'orders_id = '.$aOrdersId);
        return(true);
    }

    function isInvoicetypeNetPositionsGrossSum(){

        // special case
        //		german company		and:
        //		foreign company without VAT
        // must get billing in net, only sum is gross

        if($this->delivery['country'] == 81 && (!empty($this->billing['company']))){

            return true;
        }
        elseif(($this->getGeoZoneId() != 3  && $this->delivery['country'] != 81) && empty($this->billing['ustid'])  &&  !empty($this->billing['company'])) {

            return true;
        }

        return false;
    }

    function insertOrdertotalsAndHistory($aOrdersId = NULL){

        if($this->coupon['type'] != 'empty'  &&  $this->coupon['error'] == '') {
            $sql_data_array = array(
                'orders_id' => $aOrdersId,
                'title' => $this->coupon['title'].':',
                'text' => $this->coupon['text'],
                'value' => $this->coupon['value'],
                'class' => 'ot_coupon',
                'sort_order' => 2
            );
            tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);
        }

        if($this->tax['value'] > 0) {
            $sql_data_array = array(
                'orders_id' => $aOrdersId,
                'title' => $this->tax['title'].':',
                'text' => $this->tax['text'],
                'value' => $this->tax['value'],
                'class' => 'ot_tax',
                'sort_order' => 4
            );
            tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);
        }

        $sql_data_array = array(
            'orders_id' => $aOrdersId,
            'title' => $this->shipping['title'].':',
            'text' => $this->shipping['text'],
            'value' => $this->shipping['value'],
            'class' => 'ot_shipping',
            'sort_order' => 3
        );
        tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);

        $sql_data_array = array(
            'orders_id' => $aOrdersId,
            'title' => $this->subtotal['title'].':',
            'text' => $this->subtotal['text'],
            'value' => $this->subtotal['value'],
            'class' => 'ot_subtotal',
            'sort_order' => 1
        );

        tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);

        $sql_data_array = array(
            'orders_id' => $aOrdersId,
            'title' => $this->total['title'].':',
            'text' => $this->total['text'],
            'value' => $this->total['value'],
            'class' => 'ot_total',
            'sort_order' => 5
        );

        tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);

        $sql_data_array = array(
            'orders_id' => $aOrdersId,
            'orders_status_id' => $this->ordersStati[$this->selectedPaymentMethod]['pre'],
            'date_added' => 'now()',
            'customer_notified' => 1,
            'comments' => $this->note
        );

        tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data_array);
    }

    /* M.W.
     * paypal submit button / form generate
     */
    function createPaypalForm($aOrdersId = NULL){

        ob_start();
        ?>
        <form action="<?
        if(defined('PAYMENT_PAYPAL_GATEWAYURL')){
            echo PAYMENT_PAYPAL_GATEWAYURL;
        }
        else {
            echo 'https://www.paypal.com/cgi-bin/webscr';
        }	?>" method="post" name="paypalzahlung">
            <div align="center" style="margin:20px; padding:15px;">

                <h2>
                    <?=PAYMENT_PAYPAL_PAYNOW?>
                </h2>
                <br /><?
                if(MOBILE_VERSION === true  ||  $_SESSION['MOBILE_VERSION'] === true){ ?>
                    <button class="medium" title="<?=PAYMENT_PAYPAL_PAYNOW?>" border="0"><?=CHECKOUT_ORDER_PAYPAL?></button>
                <? }
                else { ?>
                    <input id="paypalActionBtn" type="image" name="imageField" src="images/layout/button_paypal.gif" title="<?=PAYMENT_PAYPAL_PAYNOW?>" />
                <? }
                ?>

                <input type="hidden" name="amount" value="<?=round($this->total['value'], 2)?>">
                <input type="hidden" name="bn" value="osCommerce PayPal IPN v2.1>">
                <input type="hidden" name="business" value="<?=MODULE_PAYMENT_GC_PAYPAL_ID?>">
                <input type="hidden" name="cancel_return" value="<?=tep_href_link(FILENAME_CHECKOUT_INPUTMASK, '', 'SSL');?>">
                <input type="hidden" name="cbt" value="<?=MODULE_PAYMENT_GC_PAYPAL_ID?>">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="currency_code" value="<?=substr(MODULE_PAYMENT_GC_PAYPAL_CURRENCY, 5)?>">
                <input type="hidden" name="custom" value="">
                <input type="hidden" name="invoice" value="<?=SHOP_ID.'_'.$aOrdersId?>">
                <input type="hidden" name="item_name" value="<?=CHECKOUT_PAYPAL_BUSINESS?>">
                <input type="hidden" name="no_node" value="">
                <input type="hidden" name="no_shipping" value="1">
                <input type="hidden" name="notify_url" value="<?=HTTP_SERVER.'/paypal_ipn_notify.php'?>">
                <input type="hidden" name="return" value="<?=tep_href_link(FILENAME_CHECKOUT_SUCCESS, '', 'SSL')?>">
                <input type="hidden" name="shipping" value="0">
                <input type="hidden" name="tax" value="">
                <? /*  <input type="hidden" name="x" value="<?=$_SESSION['paypal_x']?>">
				  <input type="hidden" name="y" value="<?=$_SESSION['paypal_y']?>"> */ ?>
                <input type="hidden" name="lc" value="<?php echo strtoupper(PAYMENT_PAYPAL_REGIONCODE); ?>">
                <br />

                <br />
                <script>
                    $( document ).ready(function(){
                        $('#paypalActionBtn').trigger('click');
                    })
                </script>

            </div>
        </form>

        <?

        $form = ob_get_clean();
        return($form);
    }

    /* M.W.
         * paypal submit button / form generate
         */
    function createSofortueForm($aOrdersId = NULL){


        require_once(DIR_FS_CATALOG.'ext/modules/payment/pn_sofortueberweisung/classPnSofortueberweisung.php');

        $objSofortue = new classPnSofortueberweisung(PAYMENT_SOFORTUE_PROJECTPASS, MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_HASH_ALGORITHM);

        // taken and modifief from /includes/modules/payment/pn_sofortueberweisung.php function process_button() {
        $order_id = $aOrdersId;
        $amount = $this->numberFormat($this->total['value']);

        $reason_1 = str_replace('{{order_id}}', $order_id, MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_REASON_1);
        $reason_1 = str_replace('{{customer_id}}', '0', $reason_1);
        $reason_1 = tep_output_string(substr($reason_1, 0, 27));

        //$reason_2 = str_replace('{{order_id}}', $order_id, MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_TEXT_REASON_2);
        $reason_2 = str_replace('{{order_id}}', $order_id, PAYMENT_SOFORTUE_REASON);
        $reason_2 = str_replace('{{customer_id}}', $customer_id, $reason_2);
        $reason_2 = str_replace('{{order_date}}', strftime(DATE_FORMAT_SHORT), $reason_2);
        $reason_2 = str_replace('{{customer_name}}', $this->billing['fistname'] . ' ' . $this->billing['lastname'], $reason_2);
        $reason_2 = str_replace('{{customer_company}}', $this->billing['company'], $reason_2);
        $reason_2 = str_replace('{{customer_email}}', $this->billing['email'], $reason_2);
        $reason_2 = tep_output_string(substr($reason_2, 0, 27));

        $user_variable_0 = tep_output_string($order_id);
        $user_variable_1 = '';

        $session = session_name() . '=' . session_id();

        if (ENABLE_SSL == true)
            $server = HTTPS_SERVER;
        else
            $server = HTTP_SERVER;

        //protocol is defined on server
        $server = str_replace('https://', '', $server);
        $server = str_replace('http://', '', $server);

        // success return url:
        $user_variable_2 = $server . DIR_WS_CATALOG . FILENAME_CHECKOUT_SUCCESS . '?' . $session;
        // cancel return url:
        $user_variable_3 = $server . DIR_WS_CATALOG . FILENAME_CHECKOUT_INPUTMASK;
        // notification url: (depricated)
        $user_variable_4 = $server . DIR_WS_CATALOG . 'ext/modules/payment/pn_sofortueberweisung/callback.php';
        //$user_variable_5 =  tep_output_string($cart->cartID);

        /*return $this->pnSofortueberweisung->getPaymentUrl(MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_USER_ID,
            MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_PROJECT_ID, $amount, $currency, $reason_1, $reason_2,
            $user_variable_0, $user_variable_1, $user_variable_2, $user_variable_3, $user_variable_4, $user_variable_5);*/
        $procUrl = $objSofortue->getPaymentUrl(MODULE_PAYMENT_PN_SOFORTUEBERWEISUNG_USER_ID,
            PAYMENT_SOFORTUE_PROJECTID, $amount, LANGUAGE_CURRENCY, $reason_1, $reason_2,
            $user_variable_0, $user_variable_1, $user_variable_2, $user_variable_3, $user_variable_4, $user_variable_5);

        ob_start();
        ?>
        <div align="center" style="margin:20px; padding:15px;">
            <h2><?=CHECKOUT_SOFORTUE_PAYNOW?></h2>
            <br />
            <a href="<?=$procUrl?>"><?
                if(MOBILE_VERSION === true  ||  $_SESSION['MOBILE_VERSION'] === true){ ?>
                    <button class="medium" title="<?=CHECKOUT_SOFORTUE_PAYNOW_BUTTON?>" border="0"><?=CHECKOUT_ORDER_SOFORTUE?></button>
                <? }
                else { ?>
                    <img  src="images/layout/buttons/button_confirm_order.gif" title="<?=CHECKOUT_SOFORTUE_PAYNOW_BUTTON?>" border="0" />
                <? }
                ?></a>
            <br />
        </div>
        <?
        $form = ob_get_clean();
        return($form);
    }
}
?>