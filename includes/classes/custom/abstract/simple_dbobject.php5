<?
/* php5 version */
abstract class simple_dbobject {
  
  protected $id;
  protected $isLoaded = false;
  
  protected $dbtable;
  protected $DB; // optional 
  
  protected $propertyNonDB = array();
  protected $propertyRequ = array();
  
  public function __construct($aID = NULL, &$aDBConnection = NULL){
    
      $this->id = $aID;
      
      if(is_resource($aDBConnection)){
         $this->DB = $aDBConnection;
      }
  }
  
  public function isLoaded(){
    
    return($this->isLoaded);
  }
  
  public function save(){
      
    $props = get_object_vars($this);
    $sql = array();
        
    foreach($props as $prop=>$val):      

      if(in_array($prop, $this->propertyNonDB))   continue;
      
      $sql_Fields[$prop] = $val;
    endforeach;
    
    if(is_resource($this->DB)){
        
       global $DBRes;
       $DBRes = $this->DB;

       $saved = tep_db_perform($this->dbtable, $sql_Fields, 'update', 'id='.$this->id, 'DBRes');     
    }
    else {
       $saved = tep_db_perform($this->dbtable, $sql_Fields, 'update', 'id='.$this->id);     
    }    
    
    return($saved);
  } 
  
  public function delete(){
    
    if(!$this->id)   return(false);
    
      if(is_resource($this->DB)){
        
       global $DBRes;
       $DBRes = $this->DB;

       $deleted = tep_db_query('DELETE FROM '.$this->dbtable.' WHERE id = '.$this->id, 'DBRes');   
    }
    else {
        $deleted = tep_db_query('DELETE FROM '.$this->dbtable.' WHERE id = '.$this->id);
    }  
    
    
    return($deleted);
  }
  
  public function  create(){
      
    $props = get_object_vars($this);
    $sql = array();
        
    foreach($props as $prop=>$val):      

      if(in_array($prop, $this->propertyNonDB))   continue;
      
      $sql_Fields[$prop] = $val;
    endforeach;
        
    if(is_resource($this->DB)){
        
       global $DBRes;
       $DBRes = $this->DB;

       $saved = tep_db_perform($this->dbtable, $sql_Fields, 'insert', '', 'DBRes');       
    }
    else {
        $saved = tep_db_perform($this->dbtable, $sql_Fields, 'insert');
    }  
    
    if($saved){
       $this->id = tep_db_insert_id();
    }
    
    return($saved);
  } 
  
  protected function load(){

    if(!$this->id)   return(false);
    
    if($this->isLoaded())   return(true);
        
    $dbArr = $this->getDBArrayById($this->id);
    
    if(is_array($dbArr)){
      return($this->loadArray($dbArr));
    }
    
    return(false);    
  }
  
  protected function setLoadedProperties($aProperties = NULL){
    
    if(!$aProperties)   return(false);
    
    $props = get_object_vars($this);

    $sql = array();
        
    foreach($props as $prop=>$val):      

      if(in_array($prop, $this->propertyNonDB))   continue;
      
      if(isset($aProperties[$prop]))   $this->$prop = $aProperties[$prop];   
    endforeach;
        
    $this->isLoaded = ($this->checkPropertyRequirements());
    
    return($this->isLoaded);
  }
  
  protected function getDBArrayById($aId = NULL){

    if($aId){
        $sql = 'SELECT * FROM '.$this->dbtable.' '.
        'WHERE id = \''.$aId.'\'';
        
        if(is_resource($this->DB)){
        
           global $DBRes;
           $DBRes = $this->DB;
            $result = tep_db_query($sql, 'DBRes');
        }
        else {
           $result = tep_db_query($sql);
        }
       
        $data = tep_db_fetch_array($result);
        
        return($data);
    }   
    
    return(false);
  }
  
  protected function checkPropertyRequirements(){
    
    foreach($this->propertyRequ as $prop){
      if($this->getProperty($prop) === false)   return(false);  
    }
    
    return(true);
  }
  
  public function getProperty($aProp = NULL){
    
    if(!$aProp)  return(false);
    
    if(isset($this->$aProp)){ // value can be '0' so must check on is set
      return($this->$aProp);
    }
    
    return(false);    
  } 
  
  public function loadArray($aProperties = NULL){
    
    if(!is_array($aProperties))   return(false);
    
    if($this->isLoaded())   return(true);

    return($this->setLoadedProperties($aProperties));
  }
  
  public function setProperty($aProp = NULL, $aValue = NULL){

        if (!$aProp  ||  !isset($aValue)) return (false);

        $this->$aProp =  $aValue;

        return (false);
    }  
}
?>