<?
class customer_rating {
	
	var $dbtable = TABLE_EKOMI_REVIEWS;
	
	function getProductAverageRating(){
		
		
		$res = tep_db_query('SELECT AVG(rating) FROM '.$this->dbtable.' WHERE shop_id = '.SHOP_ID.' AND is_shop = 0');
		list($avg) = tep_db_fetch_row($res);
		
		return(round($avg, 1));
	}
	
	function getProductNumberPositiveRatings(){
			
		$res = tep_db_query('SELECT COUNT(*) FROM '.$this->dbtable.' WHERE shop_id = '.SHOP_ID.' AND is_shop = 0 AND rating > 3');
		list($num) = tep_db_fetch_row($res);
		
		return($num);
	}
	
	function getShopAverageRating(){
		
		
		$res = tep_db_query('SELECT AVG(rating) FROM '.$this->dbtable.' WHERE shop_id = '.SHOP_ID.' AND is_shop = 1');
		list($avg) = tep_db_fetch_row($res);
		
		return(round($avg, 1));
	}
	
	function getShopNumberPositiveRatings(){
			
		$res = tep_db_query('SELECT COUNT(*) FROM '.$this->dbtable.' WHERE shop_id = '.SHOP_ID.' AND is_shop = 1 AND rating > 3');
		list($num) = tep_db_fetch_row($res);
		
		return($num);
	}
}

?>