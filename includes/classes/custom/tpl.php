<?
/*
 * php5 class to manage small templates like the productsListing
 * call the template, give paramater, class will parse the template and return the code
 */

class tplPlatform {
	
	private static $TPL_DIRECTORY = DIR_WS_TEMPLATES;
	
	private   $tplId;
	private   $output;
	
	private   $params = array();
	
	public function setParams($aParams = array()){
		
		$this->params = $aParams;
	}
	
	public function setParam($aKey, $aVar){
		
		if(!$aKey || !$aVar)   return(false);
		
		$this->params[$aKey] = $aVar;
	}
	
	public function __construct($aTplId=NULL){

		if(!$aTplId)   return(false);		
		$this->tplId = $aTplId;
	}
	
	
	public function retrieve(){

		ob_start();
		$tpl = tplPlatform::getConst('TPL_DIRECTORY').$this->tplId.'.inc.php';

		include($tpl);
		
		$this->output = ob_get_clean();
		
		return($this->output);
	}
	
	public static function getConst($aVar = NULL){
		
		if(!$aVar)   return(false);
		
		if(!property_exists(__CLASS__, $aVar)){		  
		   return(false);
		}
		
		return(self::$$aVar);
	}
}
?>