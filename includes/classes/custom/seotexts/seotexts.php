<?

class seotexts
{
	// language id to price
	var $prices = array(
				5 => 'products_price3',
				6 => 'products_price4'
			);
	
	function getSubCatsForCat($aCatId = NULL, $aHierarchy = 1){
		
		global $languages_id;

		if(!$aCatId)   return(false);
		
		 $sql = 'SELECT t1.*, t2.txt '.
					'FROM '.TABLE_CONTENT_CATS.' as t1 '.
						'LEFT JOIN '.TABLE_CONTENT_TEXTS.' as t2 '.
						'ON (t1.id = t2.content_catid AND t1.language_id = t2.language_id) '.
					'WHERE t1.language_id = \''.$languages_id.'\' AND t1.id LIKE \''.$aCatId.'%\' AND (LENGTH(t1.id) = '.(strlen($aCatId) + ($aHierarchy*2)).' OR LENGTH(t1.id) = '.(1+ strlen($aCatId) + ($aHierarchy*2)).') '.
					'ORDER BY LENGTH(t1.id) ASC, t1.id ASC';

		  $ret = array();
		  $result = tep_db_query($sql);

		  while($data = tep_db_fetch_array($result)): 

				$catObj = new cat_item($data['id']);
				$catObj->loadArray($data);
				
				if($catObj->isLoaded()){
					$ret[] = $catObj;
				}
		  endwhile;
		  
		  return($ret);
	}
	
	/* for admin-tool */
	function getCatsForShop($aLanguageId = NULL, $aHierarchy = 1){
		
		if(!$aLanguageId)   return(false);
		
		 $sql = 'SELECT t1.*, t2.txt '.
					'FROM '.TABLE_CONTENT_CATS.' as t1 '.
						'LEFT JOIN '.TABLE_CONTENT_TEXTS.' as t2 '.
						'ON (t1.id = t2.content_catid AND t1.language_id = t2.language_id) '.
					'WHERE t1.language_id = \''.$aLanguageId.'\' AND LENGTH(t1.id) <= '.(1 + ($aHierarchy*2)).' '.
					'ORDER BY t1.id ASC, LENGTH(t1.id) ASC';

		  $ret = array();
		  $result = tep_db_query($sql);

		  while($data = tep_db_fetch_array($result)): 

				$catObj = new cat_item($data['id']);
				$catObj->loadArray($data);
				
				if($catObj->isLoaded()){
					$ret[] = $catObj;
				}
		  endwhile;
		  
		  return($ret);
	}
	
	/* checks if the current click on the tipps.php comes from an external visitor (by Http-Referer)
	 * there are special criteria (like is it google?), we dont want to save clicks that come from internal clicks
	 * 
	 * google sample:
	 * http://www.google.de/search?hl=de&q=site%3Akrawatten.com+inurl%3ATipps&meta=&aq=f&oq= 
	 */
	function traffic_checkExternalRefererVisit(){
		
		if(!empty($_SERVER['HTTP_REFERER'])){
				
				$httpRefParts = parse_url($_SERVER['HTTP_REFERER']);
				
				/* Google */
				if(strstr($httpRefParts['host'], 'google.')){
					
					if(!empty($httpRefParts['query'])){					
						parse_str($httpRefParts['query']);  // creates variable $q which is the search query of google
					}
					
					$this->traffic_createExternalRefererVisit('Google', $q);
				}
				
				/* Yahoo */
				if(strstr($httpRefParts['host'], 'yahoo.')){
					
					if(!empty($httpRefParts['query'])){					
						parse_str($httpRefParts['query']);  // creates variable $q which is the search query of google
					}
					
					$this->traffic_createExternalRefererVisit('Yahoo', $p);
				}
				
				/* Bing */
				if(strstr($httpRefParts['host'], 'bing.')){
					
					if(!empty($httpRefParts['query'])){					
						parse_str($httpRefParts['query']);  // creates variable $q which is the search query of google
					}
					
					$this->traffic_createExternalRefererVisit('Bing', $q);
				}				
				
				/* Altavista */
				if(strstr($httpRefParts['host'], 'altavista.')){
					
					if(!empty($httpRefParts['query'])){					
						parse_str($httpRefParts['query']);  // creates variable $q which is the search query of google
					}
					
					$this->traffic_createExternalRefererVisit('Altavista', $q);
				}
				
			}	
	}
	
	function traffic_createExternalRefererVisit($aProvider = NULL, $aSearchQuery = NULL){
		
		global $languages_id;
		
		$insertData = array(
		
				'content_catid' => $this->url_getCatId(),
				'language_id' => $languages_id,
				'session_id' => tep_session_id(),
				'provider' => $aProvider,
				'search_string' => $aSearchQuery,
				'created' => date('Y-m-d H:i:s', time())
		);
		
		return(tep_db_perform(TABLE_CONTENT_VISITS, $insertData, 'insert'));		
	}
	
	
	function url_getCatId(){
		
		list($tmp, $tmp, $id) = explode('/', $_SERVER['REQUEST_URI']);
		
		return($id);
	}
	
	
	function getHierarchyForCatid($aCatId){
		
		return(ceil((strlen($aCatId)/2)));	
	}
	
	function getProductInfosForCategories($aCatArray = NULL){
				
		global $languages_id;
		
		if(!$aCatArray)   return(false);
		
		$product_ids = array();
		foreach($aCatArray as $catObj):	
		
			$a = $catObj->getProperty('product_ids');
			
			if(!empty($a[0])){
			   $product_ids = array_merge($product_ids, $a); 
			}
		endforeach;
		
		if($product_ids){
			
			$sql = 'SELECT p.products_id as id, p.products_model as model, pd.products_name as name, p.'.$this->prices[$languages_id].' as price, p.products_tax_class_id as taxClass '.	
					'FROM '.TABLE_PRODUCTS.' as p '.
					'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as pd ON p.products_id = pd.products_id '.
					'WHERE pd.language_id = '.$languages_id.' AND p.products_id  IN ('.implode(', ', $product_ids).')';

			$result = tep_db_query($sql);
			
			while($data = tep_db_fetch_array($result)):				
				$productsData[$data['id']] = array('id' => $data['id'], 'name' => $data['name'], 'model'=>$data['model'], 'price' => $data['price'], 'taxClass' => $data['taxClass']);
			endwhile;	
				
			$productData = array();
			foreach($aCatArray as $catObj):	
				
				$a = $catObj->getProperty('product_ids');
				$id = $catObj->getProperty('id');
								
				if(!empty($a[0])){
				   
				   foreach($a as $product_id):						  
				   		$productData[$id][$product_id] = $productsData[((int)$product_id)];
				   endforeach;			   
				}
			endforeach;	

			return($productData);			
		}		
	}
	
}

class cat_item {
	
	var $id;
	var $lang;
	var $name;
	var $nav_name;
	var $language_id;
	var $product_ids = array();
	
	var $txt;
	var $keywords = array();
	
	var $propertyRequ = array('id', 'lang', 'name', 'language_id');
	var $isLoaded = false;	

	function isLoaded(){
		
		return($this->isLoaded);
	}
	
	function cat_item($aId = NULL){
		
		$this->id = $aId;
	}	
		
	function setLang($aLang = NULL){
		
		$this->lang = $aLang;
	}
	
	function setName($aName = NULL){
		
		$this->name = $aName;
	}
	
	function setLanguageid($aLanguageId = NULL){
		
		$this->language_id = $aLanguageId;
	}
	
	function load(){

		if(!$this->id)   return(false);
		
		if($this->isLoaded())   return(true);
				
		$dbArr = $this->getDBArrayById($this->id);
		
		if(is_array($dbArr)){
			return($this->loadArray($dbArr));
		}
		
		return(false);		
	}
	
	function getDBArrayById($aId = NULL){
		
		global $languages_id;
		
		if($aId){
			 $sql = 'SELECT * '.
						'FROM '.TABLE_CONTENT_CATS.' '.
						'WHERE language_id = \''.$languages_id.'\' AND id = \''.$aId.'\'';

			  $result = tep_db_query($sql);
			  $data = tep_db_fetch_array($result);
			  return($data);
		}		
		
		return(false);
	}
	
	function checkPropertyRequirements(){
		
		foreach($this->propertyRequ as $prop){
			if($this->getProperty($prop) === false)   return(false);	
		}
		
		return(true);
	}
	
	function getProperty($aProp = NULL){
		
		if(!$aProp)   return(false);
		
		if(isset($this->$aProp)){ // value can be '0' so must check on is set
			return($this->$aProp);
		}
		
		return(false);		
	}	
	
	function loadArray($aProperties = NULL){
		
		if(!is_array($aProperties))   return(false);
		
		if($this->isLoaded())   return(true);

		return($this->setLoadedProperties($aProperties));
	}
	
	function setLoadedProperties($aProperties = NULL){
		
		if(!$aProperties)   return(false);
		
		if(isset($aProperties['lang']))   $this->lang = $aProperties['lang'];		
		if(isset($aProperties['name']))   $this->name = $aProperties['name'];
		if(isset($aProperties['language_id']))   $this->language_id = $aProperties['language_id'];
		if(isset($aProperties['nav_name']))   $this->nav_name = $aProperties['nav_name'];
		if(isset($aProperties['txt']))   $this->txt = $aProperties['txt'];
		
		if(isset($aProperties['product_ids'])){			
			$this->product_ids = explode("\n", $aProperties['product_ids']);
		}
		
		$this->isLoaded = ($this->checkPropertyRequirements());
		
		return($this->isLoaded);
	}
	
	function loadTxt(){
		
		global $languages_id;
		
		if($this->txt){
			return(true);
		}
		
 	    $sql = 'SELECT txt '.
				'FROM '.TABLE_CONTENT_TEXTS.' '.
				'WHERE language_id = \''.$languages_id.'\' AND content_catid = \''.$this->id.'\'';

	    $result = tep_db_query($sql);
	    list($txt) = tep_db_fetch_row($result);
		
		if($txt){
			$this->txt = $txt;
			return(true);
		}
		
		return(false);
	}
	
	
	function loadKeywords(){
		
		global $languages_id;
		
		if($this->keywords){
			return(true);
		}
		
 	    $sql = 'SELECT keyword '.
				'FROM '.TABLE_CONTENT_KEYWORDS.' '.
				'WHERE language_id = \''.$languages_id.'\' AND content_catid = \''.$this->id.'\' '.
				'ORDER BY id ASC';

	    $result = tep_db_query($sql);
	    
		while(list($kw) = tep_db_fetch_row($result)):
			$this->keywords[] = $kw;
		endwhile;
		
		if($this->keywords){
			return(true);
		}
		
		return(false);
	}
	
	function getInternalLink(){ 
		
		$name = str_replace('+', '-', urlencode(html_entity_decode($this->getProperty('name'))));
		
		return('Tipps/'.$this->getProperty('id').'/'.str_replace(' ', '-', $name));
	}
	
	function getShortText($aRaw = false){
		
		if(!$this->txt)   return(false);
		
		if($aRaw){
			
			if(strlen($this->txt) > 400){
				return((substr($this->txt, 0, 400).' ..'));
			}
			
			return($this->txt);
		}
		else {
		
			if(strlen($this->txt) > 400){
				return((substr($this->formatOutput($this->txt), 0, 400).' ..'));
			}
			
			return($this->formatOutput($this->txt));
		}
	}
	
	function getShortestText(){
		
		if(!$this->txt)   return(false);
				
		if(strlen($this->txt) > 250){
			return((substr($this->formatOutput($this->txt), 0, 250).' ..'));
		}
		
		return($this->formatOutput($this->txt));
		
	}
	
	function getText(){
		
		if(!$this->txt)   return(false);
			
		return($this->formatOutput($this->txt));
	}
	
	function formatOutput($aOutput = NULL){
		
		if($aOutput){
			
			$out = str_replace(array("\r\n", "\n"), array('<br />', '<br />'), $aOutput);
			return($out);
		}
		
	}
}

?>