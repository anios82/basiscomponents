<?
require_once('tpl.php');
class teaserProducts{
  
  private   $categories;
  private   $numberProducts;
  private   $numberCols;
  
  public function __construct($aCategories = array(), $aNumberProducts = 12, $aNumberCols = 4){
    
    if(!$aCategories  ||  !$aNumberCols)   return(false);
    
    $this->categories = $aCategories;
    $this->numberProducts = $aNumberProducts;
    $this->numberCols = $aNumberCols;
  }  
  
  public function output($aURL = NULL, $aHeadText = NULL, $aSubText = NULL){
    
    if(!$aURL  ||  !$aHeadText  ||  !$aSubText)   return(NULL);
    
    $products = $this->getProducts();
    
    if(!empty($products)){
     
      $count = 1;
      
      $output .= '<p><h2>'.$aHeadText.'</h2></p>';
      $output .= '<p>';
      foreach($products as $prID=>$pr):

        $output .= $this->genProductThumb($pr, $count);
        $count++;
      endforeach;
      
      $output .= '</p><div class="clear"></div>';
    }
    
    $output .= '<p><br /></p><div id="teaserProductsToCategory"><a href="'.tep_href_link($aURL).'">'.$aSubText.'</a></div><p><br /><br /></p>';
    return($output);
  }
  
  private function genProductThumb($aPrData = NULL, $aCount = NULL){
    
    if(!$aPrData  ||  !$aCount)   return(NULL);
    
    $tplObj = new tplPlatform('products_listing');
    $tplObj->setParams(array(
        PRODUCTS_PRICE=>$aPrData[PRODUCTS_PRICE],
        'products_id'=>$aPrData['products_id'],
        'products_model'=>$aPrData['products_model'],
        'products_name'=>$aPrData['products_name'],
        'products_tax_class_id'=>$aPrData['products_tax_class_id'],
        'lastColumn'=>($aCount % $this->numberCols == 0  &&  $aCount > 0),
      ));    
      return($tplObj->retrieve());
  }
  
  private function getProducts(){
    
    $sql = "SELECT 
      products.products_id, 
      products.products_model, 
      products.".PRODUCTS_PRICE.", 
      products.products_prioritaet, 
      products.products_last_modified, 
      products_date_added, 
      products.products_tax_class_id, 
      products.prio_test, 
      ".TABLE_PRODUCTS_DESCRIPTION.".products_name, 
      orders_products.orders_id,  
      orders_products.products_id, 
      orders.orders_id,
      count(DISTINCT orders_products.orders_id) as verkauefe
    FROM ".TABLE_PRODUCTS_TO_CATEGORIES." as p2c 
    LEFT JOIN ".TABLE_PRODUCTS." ON p2c.products_id = products.products_id 
    left join ".TABLE_PRODUCTS_DESCRIPTION." on (".TABLE_PRODUCTS.".products_id = ".TABLE_PRODUCTS_DESCRIPTION.".products_id AND ".LANGUAGE_ID." = ".TABLE_PRODUCTS_DESCRIPTION.".language_id) 
    left join ".TABLE_ORDERS_PRODUCTS." on orders_products.products_id = products.products_id 
    left join ".TABLE_ORDERS." on orders.orders_id = orders_products.orders_id ";
    /*
    foreach($this->categories as $cat):
      $sql .= "JOIN ".TABLE_PRODUCTS_TO_CATEGORIES." as p2c ON (products.products_id = p2c.products_id AND p2c.categories_id = ".$cat.") "; 
    endforeach;*/
   
    $sql .= "WHERE  ".TABLE_ORDERS_PRODUCTS.".products_quantity > 0 AND (";
    
    foreach($this->categories as $cat):            
        $sqlArr[] =  "p2c.categories_id = ".$cat;        
    endforeach;
    
    $sql .= implode(' OR ', $sqlArr).") AND DATE_FORMAT(orders.date_purchased, '%Y-%m-%d') >= '".date('Y-m-d', strtotime('-3 months', time()))."' 
             AND 
             DATE_FORMAT(orders.date_purchased, '%Y-%m-%d') <= '".date('Y-m-d', time())."' 
    GROUP by products.products_id, products.products_model 
    ORDER BY verkauefe DESC, p2c.categories_id 
    LIMIT ".$this->numberProducts;
    
    $res = tep_db_query($sql);
    
    if(tep_db_num_rows($res) < ($this->numberProducts/2)){
        
        $sqlArr = array();
        $sql = "SELECT 
              products.products_id, 
              products.products_model, 
              products.".PRODUCTS_PRICE.", 
              products.products_prioritaet, 
              products.products_last_modified, 
              products_date_added, 
              products.products_tax_class_id, 
              products.prio_test, 
              ".TABLE_PRODUCTS_DESCRIPTION.".products_name 
            FROM ".TABLE_PRODUCTS_TO_CATEGORIES." as p2c 
            LEFT JOIN ".TABLE_PRODUCTS." ON p2c.products_id = products.products_id 
            left join ".TABLE_PRODUCTS_DESCRIPTION." on (".TABLE_PRODUCTS.".products_id = ".TABLE_PRODUCTS_DESCRIPTION.".products_id AND ".LANGUAGE_ID." = ".TABLE_PRODUCTS_DESCRIPTION.".language_id) 
            left join ".TABLE_ORDERS_PRODUCTS." on orders_products.products_id = products.products_id        
            WHERE  ".TABLE_PRODUCTS.".products_quantity > 0 AND products.".PRODUCTS_PRICE.">0 AND (";
    
            foreach($this->categories as $cat):            
                $sqlArr[] =  "p2c.categories_id = ".$cat;        
            endforeach;
            
            $sql .= implode(' OR ', $sqlArr).") 
            GROUP by products.products_id, products.products_model 
            ORDER BY p2c.categories_id 
            LIMIT ".$this->numberProducts;
        
       $res = tep_db_query($sql);
    }
        
    while($data = tep_db_fetch_array($res)):
        
        $ret[($data['products_id'])] = $data;
    endwhile;
    
    return($ret);    
  }
}
?>