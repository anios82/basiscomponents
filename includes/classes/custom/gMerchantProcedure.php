<?

class gMerchantProcedure{
  
  private   $shops;
  private   $cat_data;
  private   $current_GBRss;
  private   $current_GBRssItems;  
  private   $current_products;
  private   $current_salesProducts;
  private   $current_productsInCategories;
  private   $modelOptionGroups;
  private   $currencyObj;
  private   $designerCats;
  private   $colorCats;
  private   $colorCatsOld;
  private   $feedDir;
  private   $excludeDesignerBrands = array(75, 247, 245, 246, 36);
  private   $importantCats = array(1,2,3,6,7,8,9,61,62,60,21,98,296,441,50,51,492,491,490,489,603,604,605,19,235, 237, 508); // for adwords_labels
  private   $productRatings;
  private   $colors = array(

      381 => array('de'=>'Blau', 'fr'=>'Bleu', 'es'=>'azul', 'en'=>'Blue', 'it'=>'blu', 'nl'=>'blauw'),
      384 => array('de'=>'Braun', 'fr'=>'Brun', 'es'=>'marrones', 'en'=>'Brown', 'it'=>'marroni', 'nl'=>'bruin'),
      386 => array('de'=>'Multicolor', 'fr'=>'Multicolor', 'es'=>'Multicolor', 'en'=>'Multicolor', 'it'=>'colorato', 'nl'=>'kleurrijk'),
      369 => array('de'=>'Creme', 'fr'=>'Cr�me', 'es'=>'crema', 'en'=>'Cream', 'it'=>'crema', 'nl'=>'beige'),
      382 => array('de'=>'Dunkelblau', 'fr'=>'Bleu Fonc�', 'es'=>'Azul oscuro', 'en'=>'Dark Blue', 'it'=>'blu scuro', 'nl'=>'donkerblauw'),
      375 => array('de'=>'Dunkelgr�n', 'fr'=>'vert fonc�', 'es'=>'verde oscuro', 'en'=>'Dark Green', 'it'=>'verde scuro', 'nl'=>'donkergroen'),
      379 => array('de'=>'Dunkelrot', 'fr'=>'rouge sombre', 'es'=>'rojo oscuro', 'en'=>'Dark Red', 'it'=>'rosso scuro', 'nl'=>'donkerrood'),
      372 => array('de'=>'Gelb', 'fr'=>'jaune', 'es'=>'amarillas', 'en'=>'Yellow', 'it'=>'giallo', 'nl'=>'geel'),
      380 => array('de'=>'Hellblau', 'fr'=>'bleu clair', 'es'=>'azul claro', 'en'=>'Light Blue', 'it'=>'blu chiaro', 'nl'=>'lichtblauw'),
      374 => array('de'=>'Hellgr�n', 'fr'=>'vert clair', 'es'=>'verde claro', 'en'=>'Light Green', 'it'=>'verde chiaro', 'nl'=>'lichtgroen'),
      377 => array('de'=>'Hellrot', 'fr'=>'rouge vif', 'es'=>'rojo brillante', 'en'=>'Bright Red', 'it'=>'rosso brillante', 'nl'=>'lichtrood'),
      373 => array('de'=>'Orange', 'fr'=>'Orange', 'es'=>'naranja', 'en'=>'Orange', 'it'=>'arancioni', 'nl'=>'oranje'),
      376 => array('de'=>'Pink', 'fr'=>'rose', 'es'=>'rosa', 'en'=>'Pink', 'it'=>'rosa', 'nl'=>'roze'),
      378 => array('de'=>'Rot', 'fr'=>'rouge', 'es'=>'rojo', 'en'=>'Red', 'it'=>'rosso', 'nl'=>'rood'),
      371 => array('de'=>'Schwarz', 'fr'=>'noir', 'es'=>'negro', 'en'=>'Black', 'it'=>'nero', 'nl'=>'zwart'),
      370 => array('de'=>'Grau', 'fr'=>'gris', 'es'=>'gris', 'en'=>'Grey', 'it'=>'grigio', 'nl'=>'grijs'),
      383 => array('de'=>'T�rkis', 'fr'=>'turquoise', 'es'=>'turquesa', 'en'=>'Turquoise', 'it'=>'turchese', 'nl'=>'turquoise'),
      385 => array('de'=>'Violett', 'fr'=>'violette', 'es'=>'violeta', 'en'=>'Violet', 'it'=>'viola', 'nl'=>'violet'),
      368 => array('de'=>'Wei�', 'fr'=>'blanc', 'es'=>'blanco', 'en'=>'White', 'it'=>'bianco', 'nl'=>'wit')
  );

  public function __construct($aShops = array(), $aCatData = array(), $aCurrencyObj = NULL, $aDesignerCats = array(), $aColorCats = array(), $aColorCatsOld = array(), $aFeedDir = NULL){

    $this->shops = $aShops;
    $this->cat_data = $aCatData;
    $this->currencyObj = $aCurrencyObj;
    $this->designerCats = $aDesignerCats;
    $this->colorCats = $aColorCats;
    $this->colorCatsOld = $aColorCatsOld;
    $this->feedDir = $aFeedDir;
	
	$this->importantCats = array_merge($this->importantCats, array_keys($aDesignerCats), array_keys($aColorCats), array_keys($aColorCatsOld));
  } 
  
  private static function convertIllegalFrenchCharacters($str){
  	
	  $cp1252HTML401Entities = array(

    "\x80" => '&euro;',    # 128 -> euro sign, U+20AC NEW
    "\x82" => '&sbquo;',   # 130 -> single low-9 quotation mark, U+201A NEW
    "\x83" => '&fnof;',    # 131 -> latin small f with hook = function = florin, U+0192 ISOtech
    "\x84" => '&bdquo;',   # 132 -> double low-9 quotation mark, U+201E NEW
    "\x85" => '&hellip;',  # 133 -> horizontal ellipsis = three dot leader, U+2026 ISOpub
    "\x86" => '&dagger;',  # 134 -> dagger, U+2020 ISOpub
    "\x87" => '&Dagger;',  # 135 -> double dagger, U+2021 ISOpub
    "\x88" => '&circ;',    # 136 -> modifier letter circumflex accent, U+02C6 ISOpub
    "\x89" => '&permil;',  # 137 -> per mille sign, U+2030 ISOtech
    "\x8A" => '&Scaron;',  # 138 -> latin capital letter S with caron, U+0160 ISOlat2
    "\x8B" => '&lsaquo;',  # 139 -> single left-pointing angle quotation mark, U+2039 ISO proposed
    "\x8C" => '&OElig;',   # 140 -> latin capital ligature OE, U+0152 ISOlat2
    "\x8E" => '&#381;',    # 142 -> U+017D
    "\x91" => '&lsquo;',   # 145 -> left single quotation mark, U+2018 ISOnum
    "\x92" => '&rsquo;',   # 146 -> right single quotation mark, U+2019 ISOnum
    "\x93" => '&ldquo;',   # 147 -> left double quotation mark, U+201C ISOnum
    "\x94" => '&rdquo;',   # 148 -> right double quotation mark, U+201D ISOnum
    "\x95" => '&bull;',    # 149 -> bullet = black small circle, U+2022 ISOpub
    "\x96" => '&ndash;',   # 150 -> en dash, U+2013 ISOpub
    "\x97" => '&mdash;',   # 151 -> em dash, U+2014 ISOpub
    "\x98" => '&tilde;',   # 152 -> small tilde, U+02DC ISOdia
    "\x99" => '&trade;',   # 153 -> trade mark sign, U+2122 ISOnum
    "\x9A" => '&scaron;',  # 154 -> latin small letter s with caron, U+0161 ISOlat2
    "\x9B" => '&rsaquo;',  # 155 -> single right-pointing angle quotation mark, U+203A ISO proposed
    "\x9C" => '&oelig;',   # 156 -> latin small ligature oe, U+0153 ISOlat2
    "\x9E" => '&#382;',    # 158 -> U+017E
    "\x9F" => '&Yuml;',    # 159 -> latin capital letter Y with diaeresis, U+0178 ISOlat2
	    );
		
	    return strtr($str, $cp1252HTML401Entities);
  }

    private static function xmlEncodeCDATA($aString = NULL){

        return
            (htmlspecialchars(html_entity_decode(strip_tags($aString), ENT_HTML401, 'ISO-8859-1'), ENT_HTML401, 'ISO-8859-1'));

        //$aString = self::convertIllegalFrenchCharacters($aString);

       // return
         //   (utf8_encode(htmlspecialchars(html_entity_decode(strip_tags(preg_replace('#\r|\n|"|\\\#s', " ", $aString))))));
    }

    private static function xmlEncodePLAIN($aString = NULL){

        return (html_entity_decode($aString, ENT_HTML401, 'ISO-8859-1'));
    }
  
  private function getRatings(){  	
	
	$res = tep_db_query('SELECT ROUND(AVG(rating),1), products_id FROM '.TABLE_EKOMI_REVIEWS.' WHERE is_shop = 0 AND shop_id='.SHOP_ID.' GROUP BY products_id');
	
	while(list($rating, $prID) = tep_db_fetch_row($res)):
		
		$this->productRatings['average'][$prID] = $rating;
	endwhile;
	
	$res = tep_db_query('SELECT count(id), products_id FROM '.TABLE_EKOMI_REVIEWS.' WHERE is_shop = 0 AND shop_id='.SHOP_ID.' GROUP BY products_id');
	
	while(list($rating, $prID) = tep_db_fetch_row($res)):
		
		$this->productRatings['count'][$prID] = $rating;
	endwhile;	
  }
  
  private function getCurrentSalesProducts(){
  	
		$res = tep_db_query('SELECT products_id FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' WHERE categories_id = '.MERCHANT_OPTIMIZED_SALES_CATID);
		
		while(list($prID) = tep_db_fetch_row($res)):
			
			$this->current_salesProducts[] = $prID;
		endwhile;
  }
  
  private function getProductsForCat($aShopData = array(), $aCatId = NULL){

	$res = tep_db_query('SHOW COLUMNS FROM products');
	
	$topModelFieldAvailable = false;
	while($fields = tep_db_fetch_array($res)):
	   
	   if($fields['Field'] == 'is_topmodel'){
	      $topModelFieldAvailable = true;
		  break;
	   }
	endwhile;
	
    $sql = "SELECT  t3.products_name,
                    t3.products_description,
                    t3.products_seo_subhead, 
                    t2.products_model,
                    t2.products_id,
                    t2.products_tax_class_id, 
                    t2.".$aShopData['products_price_fieldname']." as price,
                    t2.products_quantity, 
                    t2.google_products_id_".$aShopData['shop_id']." as google_products_id,
                    t2.google_products_mpn_".$aShopData['shop_id']." as google_products_mpn 
                    
                    FROM                    
                      products_to_categories as t1
                    JOIN
                    products as t2 ON t1.products_id = t2.products_id 
                    JOIN
                    products_description as t3 ON (t1.products_id = t3.products_id AND '".$aShopData['language_id']."' = t3.language_id) 
                    
                    WHERE 
                     t3.products_name != '' AND t3.products_description != '' AND
                      ".((is_array($aCatId)) ? "t1.categories_id IN (".implode(', ', $aCatId).")" : ("t1.categories_id = ".$aCatId))." AND 
                    t2.products_status = '1' AND 
                    t2.products_quantity > 0 AND
                    t2.".$aShopData['products_price_fieldname']." > 0";

    $res = tep_db_query($sql);
    
    while($data = tep_db_fetch_array($res)):    
      $products[] = $data;
    endwhile; 

    return($products);
  }
  
  private function setCategoriesForProducts(){

      if(!empty($this->current_products)){

          foreach($this->current_products as $data):

              $result = tep_db_query('SELECT categories_id, products_id FROM '.TABLE_PRODUCTS_TO_CATEGORIES.' WHERE products_id = '.$data['products_id']);
              list($catID, $prID) = tep_db_fetch_row($result);
              $this->current_productsInCategories[$prID][] = $catID;
          endforeach;
      }
  }
  
  private function getProduktFarbe($model, $prID){

      if(defined('GOOGLE_MERCHANT_LANGUAGE_CODE') && GOOGLE_MERCHANT_LANGUAGE_CODE != ''){

          $specialSizes = array('L', 'K', 'S', 'C', 'Y', 'T', 'D', 'A', 'X', 'Y', 'F');

          if(in_array(substr($model, -1), $specialSizes)){

              $model = substr($model, 0, -1);
              $res = tep_db_query('SELECT products_id FROM products where products_model = "'.$model.'"');
              list($prID) = tep_db_fetch_row($res);
          }

          if(!empty($prID)){

              $sql = 'SELECT t2.categories_id
                      FROM products_to_categories t1
                      JOIN categories_description as t2 ON t1.categories_id = t2.categories_id
                      WHERE t1.products_id = '.$prID.' AND t1.categories_id IN('.implode(',', array_keys($this->colors)).')
                      LIMIT 1';

              $res = tep_db_query($sql);
              list($colorCatId) = tep_db_fetch_row($res);

              if(!empty($colorCatId)){
                  return $this->colors[$colorCatId][GOOGLE_MERCHANT_LANGUAGE_CODE];
              }
          }
      }

      return '';
  }


    private function getProduktFarbeNew($title, $description, $shop_id){

        $text_to_color = str_replace(array("-","/"), array(" ", " "), $title);
        $text_to_color_array = explode(" ", $text_to_color);
        $color = "";
        $icolor = 0;
        // Nur Deutsche Shops
        // 1: KRT; 2: KRH; 4: LAC; 5: KRC; 6 PER;  7:PAR;  14: KRF; 21: KRN; 25: ETC
        $shop_id_array_de = array(1,2,4,5,6,7,14,21,15);
        if( in_array($shop_id, $shop_id_array_de ) ) {
            $color_array = array(
                "rot" => "rot",
                "gr�n" => "gr�n",
                "blau" => "blau",
                "gelb" => "gelb",
                "orange" => "orange",
                "rosa" => "rosa",
                "violett" => "violett",
                "t�rkis" => "t�rkis",
                "beige" => "beige",
                "braun" => "braun",
                "silber" => "silber",
                "gold" => "gold",
                "grau" => "grau",
                "anthrazit" => "anthrazit",
                "schwarz" => "schwarz",
                "wei�" => "wei�",

                "hellrot" => "rot",
                "hellgr�n" => "gr�n",
                "hellblau" => "blau",
                "hellgelb" => "gelb",
                "hellgrau" => "grau",
                "hellbraun" => "braun",
                "blassrot" => "rot",
                "blassgr�n" => "gr�n",
                "blassblau" => "blau",
                "blassgelb" => "gelb",

                "dunkelrot" => "rot",
                "dunkelgr�n" => "gr�n",
                "dunkelblau" => "blau",
                "dunkelgelb" => "gelb",
                "dunkelgrau" => "grau",
                "dunkelbraun" => "braun",

                "rose" => "rosa",
                "ros�" => "rosa",
                "pink" => "rosa",
                "karamell" => "gelb",
                "senfgelb" => "gelb",
                "sonne" => "gelb",
                "zitrone" => "gelb",
                "smaragd" => "gr�n",
                "mint" => "gr�n",
                "mintgr�n" => "gr�n",
                "moos" => "gr�n",
                "lind" => "gr�n",
                "giftgr�n" => "gr�n",
                "apfelgr�n" => "gr�n",
                "senfgr�n" => "gr�n",
                "apfel" => "gr�n",
                "waldgr�n" => "gr�n",
                "tanne" => "gr�n",
                "signalgr�n" => "gr�n",
                "blue" => "blau",
                "k�nigsblau" => "blau",
                "himmelblau" => "blau",
                "indigo" => "blau",
                "rauchblau" => "blau",
                "nachtblau" => "blau",
                "marineblau" => "blau",
                "navy" => "blau",
                "navyblau" => "blau",
                "royalblau" => "blau",
                "apricot" => "rot",
                "korall" => "rot",
                "tomatenrot" => "rot",
                "himbeerrot" => "rot",
                "kirschrot" => "rot",
                "erdbeerrot" => "rot",
                "feuerrot" => "rot",
                "terracotta" => "rot",
                "mittelrot" => "rot",
                "sherryrot" => "rot",
                "rostrot" => "rot",
                "weinrot" => "rot",
                "bordeaux" => "rot",
                "burgunder" => "rot",
                "lachs" => "rosa",
                "altrosa" => "rosa",
                "kaffee" => "braun",
                "capuccino" => "braun",
                "mocca" => "braun",
                "mokka" => "braun",
                "kupfer" => "braun",
                "nuss" => "braun",
                "kastanie" => "braun",
                "rostbraun" => "braun",
                "champagner" => "beige",
                "elfenbein" => "beige",
                "creme" => "beige",
                "ecru" => "beige",
                "perlmutt" => "beige",
                "sand" => "beige",
                "vanille" => "beige",
                "hyazinth" => "violett",
                "brombeer" => "violett",
                "aubergine" => "violett",
                "fucsia" => "violett",
                "magenta" => "violett",
                "purpur" => "violett",
                "flieder" => "violett",
                "lila" => "violett",
                "lavendel" => "violett",
                "petrol" => "t�rkis",
                "tintenschwarz" => "schwarz",
                "nachtschwarz" => "schwarz",
                "tiefschwarz" => "schwarz",
                "graphit" => "schwarz",
                "black" => "schwarz",
                "asphalt" => "schwarz",
                "reinwei�" => "wei�",
                "perlwei�" => "wei�",
                "schneewei�" => "wei�",
                "wei&#223;" => "wei�",
                "weiss" => "wei�"
            );
        }else{
            return false;
        }

        for ($i = 0; $i < count($text_to_color_array); $i++) {
            foreach ($color_array as $key => $value) {
                if( $key == strtolower( substr( $text_to_color_array[$i],0,strlen($key) ) ) ){
                    $color .= "/".$value;
                    $icolor++;
                    if($icolor >= 3){ break 2; }
                }
            }
        }

        if( $color == "" ){
            $text_to_color = str_replace(array("-","/"), array(" ", " "), $description);
            $text_to_color_array = explode(" ", $text_to_color);
            for ($i = 0; $i < count($text_to_color_array); $i++) {
                foreach ($color_array as $key => $value) {
                    if( $key == strtolower( substr( $text_to_color_array[$i],0,strlen($key) ) ) ){
                        $color = $value;
                        break 2;
                    }
                }
            }
        }else{
            $color = substr( $color, 1);
        }

        return $color;
    }

    private function getProduktGewicht($aProductsId = NULL){
    
      // Fliege 52, Einstecktuch 51, Schal 50, Hemd 90, ManschettenknÃ¯Â¿Â½pfe 98, Halstuch 237, Krawattenschal 296, GÃ¯Â¿Â½rtel 264, Aktentasche 257
      if(in_array(52, $this->current_productsInCategories[$aProductsId])){
        return('40');       
      }
      elseif(in_array(51, $this->current_productsInCategories[$aProductsId])){
        return('5');          
      }
      elseif(in_array(50, $this->current_productsInCategories[$aProductsId])){
        return('185');  
      }
      elseif(in_array(90, $this->current_productsInCategories[$aProductsId])){
        return('260');              
      }
      elseif(in_array(98, $this->current_productsInCategories[$aProductsId])){
        return('68');             
      }
      elseif(in_array(237, $this->current_productsInCategories[$aProductsId])){
        return('25');       
      }
      elseif(in_array(296, $this->current_productsInCategories[$aProductsId])){
        return('40');       
      }
      elseif(in_array(264, $this->current_productsInCategories[$aProductsId])){
        return('115');        
      }
      elseif(in_array(257, $this->current_productsInCategories[$aProductsId])){
        return('1500');       
      }
      
      return('65');
  }

  private function getProduktPattern($aProductsName = NULL, $aProductsId = NULL){
  	
	   if(!$aProductsName  ||  !$aProductsId)   return(false);
	   
	   //echo $aProductsName.'__'.$aProductsId.'<br>';
	   
	   if($this->isNecktie($aProductsId)){
	   	
	      if(in_array(1, $this->current_productsInCategories[$aProductsId])  ||  in_array(2, $this->current_productsInCategories[$aProductsId])  ||  in_array(3, $this->current_productsInCategories[$aProductsId])){
         	return(MERCHANT_PATTERN_STRIPES);
		  }
	   }
		
	  // echo $aProductsName."\n";
	   if(preg_match('/.*'.MERCHANT_PATTERN_STRIPES_SEARCH.'.*/', $aProductsName)){  // Streifen	   	
		   return(MERCHANT_PATTERN_STRIPES);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_KAROS_SEARCH.'.*/', $aProductsName)){  // Karos	   	
		   return(MERCHANT_PATTERN_KAROS);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_FLOWERS_SEARCH.'.*/', $aProductsName)){  // Blumen	   	
		   return(MERCHANT_PATTERN_FLOWERS);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_PAISLEYS_SEARCH.'.*/', $aProductsName)){  // Paisleys	   	
		   return(MERCHANT_PATTERN_PAISLEYS);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_TUPFEN_SEARCH.'.*/', $aProductsName)){  // Tupfen	   	
		   return(MERCHANT_PATTERN_TUPFEN);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_KREISE_SEARCH.'.*/', $aProductsName)){  // Kreise	   	
		   return(MERCHANT_PATTERN_KREISE);
	   }
	   elseif(preg_match('/.*'.MERCHANT_PATTERN_PUNKTE_SEARCH.'.*/', $aProductsName)){  // Punkte	   	
		   return(MERCHANT_PATTERN_PUNKTE);
	   }
	   
	   return('');
  }
 
  private function getProduktMaterial($aProductsId = NULL){
  	
	  if(in_array(51, $this->current_productsInCategories[$aProductsId])){
        
		 if(in_array(490, $this->current_productsInCategories[$aProductsId])){
         	return(MERCHANT_MATERIAL_MICRO);
		 }
		 elseif(in_array(489, $this->current_productsInCategories[$aProductsId])){
         	return(MERCHANT_MATERIAL_SILK);
		 }
	  } 
	  elseif($this->isNecktie($aProductsId)){
	  	
		if(in_array(235, $this->current_productsInCategories[$aProductsId])){
         	return(MERCHANT_MATERIAL_MICRO);
		 }
		else {
			return(MERCHANT_MATERIAL_SILK);
		}
	  }  
  }
  
  private function getProduktType($aProductsId = NULL){
      if(defined('MERCHANT_PRODUCTTYPE_EINSTECKTUCH') && in_array(51, $this->current_productsInCategories[$aProductsId])){
          return(MERCHANT_PRODUCTTYPE_EINSTECKTUCH);
      }
      elseif(defined('MERCHANT_PRODUCTTYPE_KRSCHAL') && in_array(296, $this->current_productsInCategories[$aProductsId])){
          return(MERCHANT_PRODUCTTYPE_KRSCHAL);
      }
      elseif(defined('MERCHANT_PRODUCTTYPE_KNOPF') && in_array(98, $this->current_productsInCategories[$aProductsId])){
          return(MERCHANT_PRODUCTTYPE_KNOPF);
      }
      elseif(defined('MERCHANT_PRODUCTTYPE_HALSTUCH') && in_array(237, $this->current_productsInCategories[$aProductsId])){
          return(MERCHANT_PRODUCTTYPE_HALSTUCH);
      }else{
          return(MERCHANT_PRODUCTTYPE);
      }

  }
  
  private function isNecktie($aProductsId = NULL){
  	
	  if(!in_array(51, $this->current_productsInCategories[$aProductsId])  &&  !in_array(296, $this->current_productsInCategories[$aProductsId])  &&  !in_array(257, $this->current_productsInCategories[$aProductsId])  &&  !in_array(264, $this->current_productsInCategories[$aProductsId])  &&  !in_array(90, $this->current_productsInCategories[$aProductsId])  &&  !in_array(98, $this->current_productsInCategories[$aProductsId])){
	  	 
	     return(true);
	  }

	  return(false);
  }
  
  private function isForWomen($aProductsId){
  		
  	if(in_array(508, $this->current_productsInCategories[$aProductsId])){  // Damenkrawatten
        return true;
	  }
	if(in_array(237, $this->current_productsInCategories[$aProductsId])){   // Dament�cher
        return true;
	  } 
	
	return false;
  }
  
  private function isForKids($aProductsId){
  		
  	if(in_array(62, $this->current_productsInCategories[$aProductsId])){  // Damenkrawatten
        return true;
	}
	
	return false;
  }

  private function getProduktCategory($aProductsId = NULL){    
      
	    // Fliege 52, Einstecktuch 51, Schal 50, Hemd 90, ManschettenknÃ¯Â¿Â½pfe 98, Halstuch 237, Krawattenschal 296, GÃ¯Â¿Â½rtel 264, Aktentasche 257
	  if(defined('MERCHANT_PRODUCTCATEGORY_EINSTECKTUCH') && in_array(51, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_EINSTECKTUCH);   
	  } 
	  elseif(defined('MERCHANT_PRODUCTCATEGORY_KRSCHAL') && in_array(296, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_KRSCHAL);        
      }
	  elseif(defined('MERCHANT_PRODUCTCATEGORY_AKTENTASCHE') && in_array(257, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_AKTENTASCHE);       
      } 
	  elseif(defined('MERCHANT_PRODUCTCATEGORY_GUERTEL') && in_array(264, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_GUERTEL);       
      }
	  elseif(defined('MERCHANT_PRODUCTCATEGORY_HEMD') && in_array(90, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_HEMD);             
      }
	  elseif(defined('MERCHANT_PRODUCTCATEGORY_KNOPF') && in_array(98, $this->current_productsInCategories[$aProductsId])){
        return(MERCHANT_PRODUCTCATEGORY_KNOPF);        
      }
      elseif(defined('MERCHANT_PRODUCTCATEGORY_HALSTUCH') && in_array(237, $this->current_productsInCategories[$aProductsId])){
          return(MERCHANT_PRODUCTCATEGORY_HALSTUCH);
      }

      if($this->isNecktie($aProductsId)){
	  		
	  	if(defined('MERCHANT_PRODUCTCATEGORY_KRAWATTEN')){
	  	   return(MERCHANT_PRODUCTCATEGORY_KRAWATTEN);
		}
	  }
  }
  

  private function getProduktGroesse($aProductsModel = NULL){   
    
      $lastSignModel = substr($aProductsModel, -1);
      
      switch($lastSignModel){
        
        case 'L':
          return('L');
        case 'S':
          return('S');
      }

      return('M');
  }
  
  private function getProduktFeatures($aProductsModel = NULL){   
    
	  $return = array();
      $lastSignModel = substr($aProductsModel, -1);
      
      switch($lastSignModel){
        
        case 'L':
		  $return[] = MERCHANT_FEATURES_XXLTIE;
		  break;
        case 'K':
		   $return[] = MERCHANT_FEATURES_KIDTIE;
		   break;
		case 'S':
		   $return[] = MERCHANT_FEATURES_NARROWTIE;
		   break;
		case 'C':
		 $return[] = MERCHANT_FEATURES_CLIPTIE;
		  break;
      }
	  
	  return($return);
  }
  
  
  // only if krawatten
  private function getProduktLaenge($aProductsModel = NULL){    
      
      $lastSignModel = substr($aProductsModel, -1);
      
      switch($lastSignModel){
        
        case 'L':
          return('160');
        case 'S':
          return('148');
        case 'K':
          return('120');
        
        default:
          return('148');
      }
  }
  
  
  private function getProductAverageRating($aProductsId = NULL){
  	
	if(!$aProductsId)   return(false);
	
	if($this->productRatings['average'][$aProductsId]){
		return($this->productRatings['average'][$aProductsId]);	
	}
  }
  
   private function getProductCountRating($aProductsId = NULL){
  	
	if(!$aProductsId)   return(false);
	
	if($this->productRatings['count'][$aProductsId]){
		return($this->productRatings['count'][$aProductsId]);	
	}
  }
   
   private function determineModelOptionGroups(){
   	
		if(empty($this->current_products))   return;
		
		$specialSizes = array('L', 'K', 'S', 'C', 'Y', 'T', 'D');
		$this->modelOptionGroups = array();
		foreach($this->current_products as $product):
			
			if(in_array(substr($product['products_model'], -1), $specialSizes)){
				
				$baseModel = substr($product['products_model'], 0, -1);
				$this->modelOptionGroups[$baseModel][] = $product['products_model']; 
			}
			else {
				$this->modelOptionGroups[$product['products_model']][] = $product['products_model']; 
			}
		endforeach;
   }
  

  public function getBrand($model, $prID){

      $specialSizes = array('L', 'K', 'S', 'C', 'Y', 'T', 'D', 'A', 'X', 'Y', 'F');

      if(in_array(substr($model, -1), $specialSizes)){

          $model = substr($model, 0, -1);
          $res = tep_db_query('SELECT products_id FROM products where products_model = "'.$model.'"');
          list($prID) = tep_db_fetch_row($res);
      }

      if(!empty($prID)){

          $designerCats = array_diff(array_keys($this->designerCats), $this->excludeDesignerBrands);

          $sql = 'SELECT t2.categories_name
                  FROM products_to_categories t1
                  JOIN categories_description as t2 ON t1.categories_id = t2.categories_id
                  WHERE t1.products_id = '.$prID.' AND t1.categories_id IN('.implode(', ', $designerCats).')
                  LIMIT 1';

          /*if($model == 'CS0555'){
              die($sql);
          }*/

          $res = tep_db_query($sql);
          list($cname) = tep_db_fetch_row($res);

          return $cname;
      }

      return '';
  }

  public function start(){


	global $optimized_Phase1;
    global $use_amazom_image;
    global $use_square_image;

	
	if($optimized_Phase1  &&  defined('MERCHANT_OPTIMIZED_SALES_CATID')  &&  MERCHANT_OPTIMIZED_SALES_CATID != ''){
		$this->getCurrentSalesProducts();
	}
	
    foreach($this->shops as $shopData):

	  $this->getRatings();

      foreach($this->cat_data[$shopData['shop_id']] as $identifier=>$catData):

      $processedProducts[] = array();

        $this->current_GBRss = new seoGoogleBaseRss_custom();
        $this->current_GBRss->setTitle(self::xmlEncodePLAIN($catData['title']));

        $this->current_GBRss->setLink($shopData['shop_url'].$catData['url']);
        $this->current_GBRss->setDescription(self::xmlEncodeCDATA($catData['description']));

        $this->current_products = $this->getProductsForCat($shopData, $catData['catId']); 
        
		if($optimized_Phase1){
			$this->determineModelOptionGroups();
		}
		
        $this->setCategoriesForProducts();
        
		$c=1;
        $this->current_GBRssItems = array();
        foreach($this->current_products as $data):

		 /* if($data['products_model'] != 'TA36956') continue;
			
		  echo $data['products_model'].'<br />';*/
		  
		  if(DEBUG_PARSE_100_PRODUCTS === true){
			 
				if($c>=300)   continue;
				$c++;
		  }
		  
          if(!$data['products_name']){
            continue;
          }
          
          if(in_array($data['products_model'], $processedProducts)){
            continue;
          }

          $item = array();
          $item['title'] = self::xmlEncodePLAIN($data['products_name']); // TODO hier spezielle PRODUKT TITLES vergeben
          //$item['link'] = $shopData['shop_url'].'/product_info.php?products_id='.$data['products_id'];
          $item['link'] = tep_href_link(FILENAME_PRODUCT_INFO, 'products_id='.$data['products_id']);
          $item['description'] = (($data['products_seo_subhead']) ? self::xmlEncodeCDATA($data['products_seo_subhead']).'. ' : '').self::xmlEncodeCDATA($data['products_description']);

          // google attributes
          // Marke
          $brand = $this->getBrand($data['products_model'], $data['products_id']);
		  
          // MPN
         if($optimized_Phase1){  // bislang nur KRC -> Versuch der Optimierung des Feeds nach Phase1
          	
				if(empty($data['google_products_mpn'])){
					$newMerchantMPN = (($brand) ? $brand : '').substr(strtoupper(md5(mktime().mt_rand(10000,99999))), 0, 6);
					$item['google_attributes']['mpn'] =  $newMerchantMPN;
					
					tep_db_perform(TABLE_PRODUCTS, array('google_products_mpn_'.$shopData['shop_id']=>$newMerchantMPN), 'update', 'products_id='.$data['products_id']
					);
				}
				else {
					$item['google_attributes']['mpn'] = $data['google_products_mpn'];
				}          	
		  }
		  else {
		  	$item['google_attributes']['mpn'] = self::xmlEncodePLAIN($data['products_model']);
		  }

          // Marke darf nicht leer sein - Fallback L�sung
          if( trim($brand) == "" || !$brand ){
                $brand_array = array("Blackbird", "Cantucci", "Cavalieri", "Chevalier", "Parsley", "Puccini");
                for ($i = 0; $i < count($brand_array); $i++) {
                    if( $brand_array[$i] == substr($item['google_attributes']['mpn'],0, strlen($brand_array[$i])) ){
                        $brand = $brand_array[$i];
                    }
                }
          }

          // Marke darf nicht leer sein - 2. Fallback L�sung
          if( trim($brand) == "" || !$brand ){
              $brand = "Parsley";
          }

          // Title um Markenname ergaenzt und wieder entfernt A.J. 20.9.17
          // $item['title'] .= self::xmlEncodePLAIN( strtoupper( " - " . $brand ) );

            // ID
          if($optimized_Phase1){  // bislang nur KRC -> Versuch der Optimierung des Feeds nach Phase1
          	
				if(empty($data['google_products_id'])){
					$newMerchantID = SHOP_SHORTNAME.mt_rand(1000000, 9999999);
					$item['google_attributes']['id'] = $newMerchantID;
					
					tep_db_perform(TABLE_PRODUCTS, array('google_products_id_'.$shopData['shop_id']=>$newMerchantID), 'update', 'products_id='.$data['products_id']
					);
				}
				else {
					$item['google_attributes']['id'] = $data['google_products_id'];
				}          	
		  }
		  else {
		  	$item['google_attributes']['id'] = self::xmlEncodePLAIN($data['products_model']);
		  }
          
          // Bild Url
          $image_file = $shopData['shop_url'].'/images/ties/'.gc_getImageFilenameFromModel($data['products_model'], 'md', $shopData['shop_shortname']);
          if( $use_amazom_image ) {
              // AmazonBild als erstes and Standard als zweites Bild
              $image_file_az = str_replace("_md", "_az", $image_file);
              $image_path_az = str_replace($shopData['shop_url'], "..", $image_file_az);
              if (file_exists($image_path_az)) {
                  $item['google_attributes']['image_link'] = $image_file_az;
                  $item['google_attributes']['additional_image_link'] = $image_file;
              } else {
                  $item['google_attributes']['image_link'] = $image_file;
              }
          }elseif( $use_square_image ) {
              // Quadratische sq Bilder als erstes and Standard als zweites Bild
              $image_file_sq = str_replace("_md", "_sq", $image_file);
              $image_path_sq = str_replace($shopData['shop_url'], "..", $image_file_sq);
              if (file_exists($image_path_sq)) {
                  $item['google_attributes']['image_link'] = $image_file_sq;
                  $item['google_attributes']['additional_image_link'] = $image_file;
              } else {
                  $item['google_attributes']['image_link'] = $image_file;
              }
          }else{
              $item['google_attributes']['image_link'] = $image_file;
          }

          // Preis
          list($price) = explode(' ', $this->currencyObj->display_price($data['price'], tep_get_tax_rate($data['products_tax_class_id'])));
		  $price = self::xmlEncodePLAIN(str_replace(',', '.', $price));
		  
		  if($optimized_Phase1  &&  !empty($this->current_salesProducts)  &&  in_array($data['products_id'], $this->current_salesProducts)){
		  	  $item['google_attributes']['sale_price'] = $price;
			  $item['google_attributes']['sale_price_effective_date'] = date('Y-m-d', time()).'T'.date('H:i:s', time()).'Z/'.date('Y-m-d', strtotime('+3 months', time())).'T'.date('H:i:s', strtotime('+3 months', time())).'Z'; 
		  	  $price = self::xmlEncodePLAIN(str_replace(',', '.', $price));
		  }
		  else {		  	
			  $item['google_attributes']['price'] = $price;
		  }
		  
		  if($optimized_Phase1){
		  	    $baseModel = substr($data['products_model'], 0, -1);
			    if(count($this->modelOptionGroups[$baseModel]) > 1){
				   $item['google_attributes']['item_group_id'] = $baseModel.'_Models';
				} 
		  } 
		  
          // Zustand
          $item['google_attributes']['condition'] = self::xmlEncodePLAIN(MERCHANT_CONDITION_NEU);

          if($brand){           
             $item['google_attributes']['brand'] = self::xmlEncodePLAIN(str_replace(array('-', ' '), array('', ''), $brand));
          }
          
          // Produktart, // Fliege 52, Einstecktuch 51, Schal 50, Hemd 90, ManschettenknÃ¯Â¿Â½pfe 98, Halstuch 237, Krawattenschal 296, GÃ¯Â¿Â½rtel 264, Aktentasche 257
          $productsType = self::getProduktType($data['products_id']);
          $item['google_attributes']['product_type'] = self::xmlEncodePLAIN($productsType);

          $item['google_attributes']['size_system'] = 'US';

          $item['google_attributes']['size'] = self::getProduktGroesse($data['products_model']);


		  $productsCategory = self::getProduktCategory($data['products_id']);
		  
		  if($productsCategory){
          	$item['google_attributes']['google_product_category'] = self::xmlEncodePLAIN($productsCategory);
		  }		  
		  
		  $productsPattern = self::getProduktPattern($data['products_name'], $data['products_id']);
		  
		  if($productsPattern){
          	$item['google_attributes']['pattern'] = self::xmlEncodePLAIN($productsPattern);
		  }
		  
		  $productsMaterial = self::getProduktMaterial($data['products_id']);
		  
		  if($productsMaterial){		  	
          	$item['google_attributes']['material'] = self::xmlEncodePLAIN($productsMaterial);
		  }
		            
          // Transport-Gewicht
         $item['google_attributes']['shipping_weight'] = self::getProduktGewicht($data['products_id']).' '.MERCHANT_WEIGHT_GRAM;
          
		 // Durchschnittliche Produktbewertung [product_review_average], zwischen 1 u. 5
		 if($ratingAvg = self::getProductAverageRating($data['products_id'])){
		    $item['google_attributes']['product_review_average'] = str_replace('.', ',', $ratingAvg);
		 }
		 // Anzahl Produktbewertungen
		 if($ratingCount = self::getProductCountRating($data['products_id'])){
		    $item['google_attributes']['product_review_count'] = $ratingCount;
		 }
		  
          // Farbe
          $color = $this->getProduktFarbe($data['products_model'], $data['products_id']);

          // Fallbackloesung Farbe bestimmen
          if(!$color){
             $color = $this->getProduktFarbeNew($data['products_name'],$data['products_description'],$shopData['shop_id']);
          }

          if($color){
             $item['google_attributes']['color'] = self::xmlEncodePLAIN($color);
          }          
          // Menge
           $item['google_attributes']['quantity'] = self::xmlEncodePLAIN($data['products_quantity']);  

           // VerfÃ¼gbarkeit
           $item['google_attributes']['availability'] = ((int)$data['products_quantity'] > 0) ? 'in stock' : 'preorder';
		   
		   // Geschlecht 
		   $item['google_attributes']['gender'] = ($this->isForWomen($data['products_id'])) ? 'Female' : 'Male';
		   
		    // Agegroup (adult, kid) 
		   $item['google_attributes']['age_group'] = ($this->isForKids($data['products_id'])) ? 'Kids' : 'Adult';
		   
		   // Online
		   $item['google_attributes']['online_only'] = 'n';
           
		   // spezielle Features
		   if($productsType == MERCHANT_PRODUCTCATEGORY_KRAWATTEN){
		   		$features = self::getProduktFeatures($data['products_model']);
				
				if(!empty($features)){
					
					$item['custom_attributes']['feature'] = array();
					foreach($features as $feature):
						$item['custom_attributes']['feature'][] = $feature;
					endforeach;
				}
		   }

		  // fÃ¼r adwords spezifische Attribute
		  if($catData['adwords_grouping']){
		  	 foreach($catData['adwords_grouping'] as $group):
		   	 	 $item['google_attributes']['adwords_grouping'][] = self::xmlEncodePLAIN($group);
			  endforeach;
		  }
		  

		  foreach($this->current_productsInCategories[$data['products_id']] as $cID):
		   	 
			 if(!in_array($cID, $this->importantCats))   continue;
		     $item['google_attributes']['adwords_labels'][] = self::xmlEncodePLAIN($this->cat_data['allCategories'][$cID]['catName']);
		  endforeach;
		  
           
          $this->current_GBRssItems[] = $item;
          $this->current_GBRss->makeItem($item);
          
          $processedProducts[] = $data['products_model'];
        endforeach; 
        
        if(!empty($this->current_GBRssItems)){
          
          $this->generateFeed($shopData['shop_shortname'], $identifier);          
        }
      endforeach;
    endforeach;   
  }
  
  private function generateFeed($aShopShortname = NULL, $aIdent = NULL){
    
    if(!$aShopShortname  ||  !$aIdent)   return(false);
    
    $file = $this->feedDir.'/GB_'.$aShopShortname.'_'.$aIdent.'.xml'; 
    echo 'file:'.$file.'<br />';
    $xml = $this->current_GBRss->toXml();
    $fp = fopen($file, 'w+');
    fwrite($fp, str_replace('<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="ISO-8859-1"?>', $xml));
    fclose($fp);
  }
  
  
}?>