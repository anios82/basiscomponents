<?php
/*
  $Id: order.php,v 1.33 2003/06/09 22:25:35 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class order {
    var $info, $totals, $products, $customer, $delivery, $content_type;

    function order($order_id = '') {
      $this->info = array();
      $this->totals = array();
      $this->products = array();
      $this->customer = array();
      $this->delivery = array();

      if (tep_not_null($order_id)) {
        $this->query($order_id);
      } else {
        $this->cart();
      }
    }

    function query($order_id) {
      global $languages_id;

      $order_id = tep_db_prepare_input($order_id);

      $order_query = tep_db_query("select customers_id, customers_name, customers_company, customers_vat, customers_street_address, customers_suburb, customers_city, customers_postcode, customers_state, customers_country, customers_telephone, customers_email_address, customers_address_format_id, delivery_name, delivery_company, delivery_street_address, delivery_suburb, delivery_city, delivery_postcode, delivery_state, delivery_country, delivery_address_format_id, billing_name, billing_company, billing_street_address, billing_suburb, billing_city, billing_postcode, billing_state, billing_country, billing_address_format_id, payment_method, cc_type, cc_owner, cc_number, cc_expires, currency, currency_value, date_purchased, orders_status, last_modified from " . TABLE_ORDERS . " where orders_id = '" . (int)$order_id . "'");
      $order = tep_db_fetch_array($order_query);

      $totals_query = tep_db_query("select title, text from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "' order by sort_order");
      while ($totals = tep_db_fetch_array($totals_query)) {
        $this->totals[] = array('title' => $totals['title'],
                                'text' => $totals['text']);
      }

      $order_total_query = tep_db_query("select text from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "' and class = 'ot_total'");
      $order_total = tep_db_fetch_array($order_total_query);

      $shipping_method_query = tep_db_query("select title from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "' and class = 'ot_shipping'");
      $shipping_method = tep_db_fetch_array($shipping_method_query);

      $order_status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . $order['orders_status'] . "' and language_id = '" . (int)$languages_id . "'");
      $order_status = tep_db_fetch_array($order_status_query);

      $this->info = array('currency' => $order['currency'],
                          'currency_value' => $order['currency_value'],
                          'payment_method' => $order['payment_method'],
                          'cc_type' => $order['cc_type'],
                          'cc_owner' => $order['cc_owner'],
                          'cc_number' => $order['cc_number'],
                          'cc_expires' => $order['cc_expires'],
                          'date_purchased' => $order['date_purchased'],
                          'orders_status' => $order_status['orders_status_name'],
                          'last_modified' => $order['last_modified'],
                          'total' => strip_tags($order_total['text']),
                          'shipping_method' => ((substr($shipping_method['title'], -1) == ':') ? substr(strip_tags($shipping_method['title']), 0, -1) : strip_tags($shipping_method['title'])));

      $this->customer = array('id' => $order['customers_id'],
                              'gender' => $order['customers_gender'],
							  'name' => $order['customers_name'],
                              'company' => $order['customers_company'],
							  'vat' => $order['customers_vat'],
                              'street_address' => $order['customers_street_address'],
                              'suburb' => $order['customers_suburb'],
                              'city' => $order['customers_city'],
                              'postcode' => $order['customers_postcode'],
                              'state' => $order['customers_state'],
                              'country' => $order['customers_country'],
                              'format_id' => $order['customers_address_format_id'],
                              'telephone' => $order['customers_telephone'],
                              'email_address' => $order['customers_email_address']);

      $this->delivery = array('name' => $order['delivery_name'],
                              'gender' => $order['customers_gender'],
							  'company' => $order['delivery_company'],
                              'street_address' => $order['delivery_street_address'],
                              'suburb' => $order['delivery_suburb'],
                              'city' => $order['delivery_city'],
                              'postcode' => $order['delivery_postcode'],
                              'state' => $order['delivery_state'],
                              'country' => $order['delivery_country'],
                              'format_id' => $order['delivery_address_format_id']);

      if (empty($this->delivery['name']) && empty($this->delivery['street_address'])) {
        $this->delivery = false;
      }

      $this->billing = array('name' => $order['billing_name'],
                             'gender' => $order['customers_gender'],
							 'company' => $order['billing_company'],
							 'vat' => $order['customers_vat'],
                             'street_address' => $order['billing_street_address'],
                             'suburb' => $order['billing_suburb'],
                             'city' => $order['billing_city'],
                             'postcode' => $order['billing_postcode'],
                             'state' => $order['billing_state'],
                             'country' => $order['billing_country'],
                             'format_id' => $order['billing_address_format_id']);

      $index = 0;
      $orders_products_query = tep_db_query("select orders_products_id, products_id, products_name, products_model, ".PRODUCTS_PRICE.", products_tax, products_quantity, final_price from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$order_id . "'");
      while ($orders_products = tep_db_fetch_array($orders_products_query)) {
        $this->products[$index] = array('qty' => $orders_products['products_quantity'],
	                                'id' => $orders_products['products_id'],
                                        'name' => $orders_products['products_name'],
                                        'model' => $orders_products['products_model'],
                                        'tax' => $orders_products['products_tax'],
                                        'price' => $orders_products[PRODUCTS_PRICE],
                                        'final_price' => $orders_products['final_price']);

        $subindex = 0;
        $attributes_query = tep_db_query("select products_options, products_options_values, options_values_price, price_prefix from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . (int)$order_id . "' and orders_products_id = '" . (int)$orders_products['orders_products_id'] . "'");
        if (tep_db_num_rows($attributes_query)) {
          while ($attributes = tep_db_fetch_array($attributes_query)) {
            $this->products[$index]['attributes'][$subindex] = array('option' => $attributes['products_options'],
                                                                     'value' => $attributes['products_options_values'],
                                                                     'prefix' => $attributes['price_prefix'],
                                                                     'price' => $attributes['options_values_price']);

            $subindex++;
          }
        }

        $this->info['tax_groups']["{$this->products[$index]['tax']}"] = '1';

        $index++;
      }
    }

    function cart() {
      global $customer_id, $sendto, $billto, $cart, $languages_id, $currency, $currencies, $shipping, $payment;
	  global $pwa_array_customer, $pwa_array_address, $pwa_array_address2, $dhlexpress; //pwa_array_address2 = Shipping

      $this->content_type = $_SESSION['cart']->get_content_type();

// Ingo PWA Beginn
if ($_SESSION['customer_id'] === '0') {
      
         $address = array('customers_gender' => $_SESSION['pwa_array_customer']['customers_gender'],
                       'customers_firstname' => $_SESSION['pwa_array_customer']['customers_firstname'],
					   'customers_lastname'  => $_SESSION['pwa_array_customer']['customers_lastname'],
                       'customers_telephone' => $_SESSION['pwa_array_customer']['customers_telephone'],
                   'customers_email_address' => $_SESSION['pwa_array_customer']['customers_email_address'],
                             'entry_company' => (isset($_SESSION['pwa_array_address']['entry_company'])?$_SESSION['pwa_array_address']['entry_company']:''),
								 'entry_vat' => (isset($_SESSION['pwa_array_address']['entry_vat'])?$_SESSION['pwa_array_address']['entry_vat']:''),
                      'entry_street_address' => $_SESSION['pwa_array_address']['entry_street_address'],
                              'entry_suburb' => $_SESSION['pwa_array_address']['entry_suburb'],
                            'entry_postcode' => $_SESSION['pwa_array_address']['entry_postcode'],
                                'entry_city' => $_SESSION['pwa_array_address']['entry_city'],
                             'entry_zone_id' => $_SESSION['pwa_array_address']['entry_zone_id'],
                              'countries_id' => $_SESSION['pwa_array_address']['entry_country_id'],
                          'entry_country_id' => $_SESSION['pwa_array_address']['entry_country_id'],
                               'entry_state' => $_SESSION['pwa_array_address']['entry_state']);
							   
		//Versandadresse-Modul by Gurkcity 19.04.2007 BOF					   
	    $address2 = array('customers_gender' => $_SESSION['pwa_array_address2']['entry_gender'],
					   'customers_firstname' => $_SESSION['pwa_array_address2']['entry_firstname'],	  
                       'customers_lastname'  => $_SESSION['pwa_array_address2']['entry_lastname'],
                             'entry_company' => $_SESSION['pwa_array_address2']['entry_company'],
                      'entry_street_address' => $_SESSION['pwa_array_address2']['entry_street_address'],
                            'entry_postcode' => $_SESSION['pwa_array_address2']['entry_postcode'],
                                'entry_city' => $_SESSION['pwa_array_address2']['entry_city'],
                              'countries_id' => $_SESSION['pwa_array_address2']['entry_country_id'],
                          'entry_country_id' => $_SESSION['pwa_array_address2']['entry_country_id']);
		//Versandadresse-Modul by Gurkcity 19.04.2007 EOF
							   					   
      $country_query = tep_db_query("select c.countries_name, c.countries_iso_code_2, c.countries_iso_code_3, c.address_format_id, z.zone_name from " . TABLE_COUNTRIES . " c left join " . TABLE_ZONES . " z on z.zone_id = '" . intval($_SESSION['pwa_array_address']['entry_zone_id']) . "' where countries_id = '" . intval($_SESSION['pwa_array_address']['entry_country_id']) . "'");
      $country = tep_db_fetch_array($country_query);
      
	  if(is_array($country)) {
		$customer_address = $shipping_address = $billing_address = array_merge($address, $country); //Hier werden alle Adressen erstmal gleich gesetzt! Damit sind alle arrays gef�llt
	  }
	  else {
		$country = array($country);
		$customer_address = $shipping_address = $billing_address = array_merge($address, $country); //Hier werden alle Adressen erstmal gleich gesetzt! Damit sind alle arrays gef�llt
	  }

      $tax_address = array('entry_country_id' => ($_SESSION['pwa_array_address2']['entry_country_id']) ? $_SESSION['pwa_array_address2']['entry_country_id'] : $_SESSION['pwa_array_address']['entry_country_id'], 'entry_zone_id' => ($_SESSION['pwa_array_address2']['entry_zone_id']) ? $_SESSION['pwa_array_address2']['entry_zone_id'] : $_SESSION['pwa_array_address']['entry_zone_id']);
      
     /* echo '<pre>';
      print_r($tax_address);
	  echo '</pre>';*/
      
      $this->pwa_label = array('gender' => $customer_address['customers_gender'],
                               'firstname' => $customer_address['customers_firstname'],
							   'lastname'  => $customer_address['customers_lastname'],
                                 'company' => $customer_address['entry_company'],
                          'street_address' => $customer_address['entry_street_address'],
                                  'suburb' => $customer_address['entry_suburb'],
                                    'city' => $customer_address['entry_city'],
                                'postcode' => $customer_address['entry_postcode'],
                                   'state' => $customer_address['entry_state'],
                                 'zone_id' => $customer_address['entry_zone_id'],
                              'country_id' => $customer_address['entry_country_id']);
		
      if($this->delivery['country']['id'] == 81){
        $tax_address = array('entry_country_id' => 81, 'entry_zone_id' => 1);
      }
    
	  //Versandadresse-Modul by Gurkcity 19.04.2007 BOF
	  //Shipping-Adresse gesetzt?
	  
	  $country_query_shipping = tep_db_query("select c.countries_name, c.countries_iso_code_2, c.countries_iso_code_3, c.address_format_id, z.zone_name from " . TABLE_COUNTRIES . " c left join " . TABLE_ZONES . " z on z.zone_id = '" . intval($_SESSION['pwa_array_address2']['entry_zone_id']) . "' where countries_id = '" . intval($_SESSION['pwa_array_address2']['entry_country_id']) . "'");
      $country_shipping = tep_db_fetch_array($country_query_shipping);
	  
	  if($address2['customers_firstname']!='')
	  {
	  	$shipping_address = array_merge($address2, $country_shipping);
	  }
	  
	  $this->pwa_label2 = array('gender' => $shipping_address['customers_gender'],
                               'firstname' => $shipping_address['customers_firstname'],
							   'lastname'  => $shipping_address['customers_lastname'],
                                 'company' => $shipping_address['entry_company'],
                          'street_address' => $shipping_address['entry_street_address'],
                                  'suburb' => $shipping_address['entry_suburb'],
                                    'city' => $shipping_address['entry_city'],
                                'postcode' => $shipping_address['entry_postcode'],
                              'country_id' => $shipping_address['entry_country_id']);
							  
	  //Versandadresse-Modul by Gurkcity 19.04.2007 EOF					  
	 
} 

      $this->info = array('order_status' => DEFAULT_ORDERS_STATUS_ID,
                          'currency' => $_SESSION['currency'],
                          'currency_value' => $currencies->currencies[$_SESSION['currency']]['value'],
                          'payment_method' => $_SESSION['payment'],
                          'cc_type' => (isset($GLOBALS['cc_type']) ? $GLOBALS['cc_type'] : ''),
                          'cc_owner' => (isset($GLOBALS['cc_owner']) ? $GLOBALS['cc_owner'] : ''),
                          'cc_number' => (isset($GLOBALS['cc_number']) ? $GLOBALS['cc_number'] : ''),
                          'cc_expires' => (isset($GLOBALS['cc_expires']) ? $GLOBALS['cc_expires'] : ''),
                          'shipping_method' => $_SESSION['shipping']['title'],
                          'shipping_cost' => $_SESSION['shipping']['cost'],
                          'subtotal' => 0,
                          'tax' => 0,
                          'tax_groups' => array(),
      					  'express' => $_SESSION['dhlexpress'],
                          'comments' => (isset($_SESSION['comments']) ? $_SESSION['comments'] : ''));
      
      
     
      
      
      if (isset($GLOBALS[$_SESSION['payment']]) && is_object($GLOBALS[$_SESSION['payment']])) {
        $this->info['payment_method'] = $GLOBALS[$_SESSION['payment']]->title;

        if ( isset($GLOBALS[$_SESSION['payment']]->order_status) && is_numeric($GLOBALS[$_SESSION['payment']]->order_status) && ($GLOBALS[$_SESSION['payment']]->order_status > 0) ) {
          $this->info['order_status'] = $GLOBALS[$_SESSION['payment']]->order_status;
        }
      }

      $this->customer = array('gender' => $customer_address['customers_gender'],
	  						  'firstname' => $customer_address['customers_firstname'],
                              'lastname' => $customer_address['customers_lastname'],
                              'company' => $customer_address['entry_company'],
							  'vat' => $customer_address['entry_vat'],
                              'street_address' => $customer_address['entry_street_address'],
                              'suburb' => $customer_address['entry_suburb'],
                              'city' => $customer_address['entry_city'],
                              'postcode' => $customer_address['entry_postcode'],
                              'state' => ((tep_not_null($customer_address['entry_state'])) ? $customer_address['entry_state'] : $customer_address['zone_name']),
                              'zone_id' => $customer_address['entry_zone_id'],
                              'country' => array('id' => $customer_address['countries_id'], 'title' => $customer_address['countries_name'], 'iso_code_2' => $customer_address['countries_iso_code_2'], 'iso_code_3' => $customer_address['countries_iso_code_3']),
                              'format_id' => $customer_address['address_format_id'],
                              'telephone' => $customer_address['customers_telephone'],
                              'email_address' => $customer_address['customers_email_address']);

      $this->delivery = array('gender' => $shipping_address['customers_gender'],
							  'firstname' => $shipping_address['customers_firstname'],
                              'lastname' => $shipping_address['customers_lastname'],
                              'company' => $shipping_address['entry_company'],
                              'street_address' => $shipping_address['entry_street_address'],
                              'suburb' => $shipping_address['entry_suburb'],
                              'city' => $shipping_address['entry_city'],
                              'postcode' => $shipping_address['entry_postcode'],
                              'state' => ((tep_not_null($shipping_address['entry_state'])) ? $shipping_address['entry_state'] : $shipping_address['zone_name']),
                              'zone_id' => $shipping_address['entry_zone_id'],
                              'country' => array('id' => $shipping_address['countries_id'], 'title' => $shipping_address['countries_name'], 'iso_code_2' => $shipping_address['countries_iso_code_2'], 'iso_code_3' => $shipping_address['countries_iso_code_3']),
                              'country_id' => $shipping_address['entry_country_id'],
                              'format_id' => $shipping_address['address_format_id']);

      $this->billing = array('gender' => $billing_address['customers_gender'],
                             'firstname' => $billing_address['customers_firstname'],
							 'lastname' => $billing_address['customers_lastname'],
                             'company' => $billing_address['entry_company'],
							 'vat' => $customer_address['entry_vat'],
                             'street_address' => $billing_address['entry_street_address'],
                             'suburb' => $billing_address['entry_suburb'],
                             'city' => $billing_address['entry_city'],
                             'postcode' => $billing_address['entry_postcode'],
                             'state' => ((tep_not_null($billing_address['entry_state'])) ? $billing_address['entry_state'] : $billing_address['zone_name']),
                             'zone_id' => $billing_address['entry_zone_id'],
                             'country' => array('id' => $billing_address['countries_id'], 'title' => $billing_address['countries_name'], 'iso_code_2' => $billing_address['countries_iso_code_2'], 'iso_code_3' => $billing_address['countries_iso_code_3']),
                             'country_id' => $billing_address['entry_country_id'],
                             'format_id' => $billing_address['address_format_id']);
      
            //die($this->delivery['country']['id']);
      if($this->delivery['country']['id'] == 81){
        $tax_address = array('entry_country_id' => 81, 'entry_zone_id' => 1);
      }
	  
      
      $index = 0;
      $products = $_SESSION['cart']->get_products();
      for ($i=0, $n=sizeof($products); $i<$n; $i++) {
        $this->products[$index] = array('qty' => $products[$i]['quantity'],
                                        'name' => $products[$i]['name'],
                                        'model' => $products[$i]['model'],
                                        'tax' => tep_get_tax_rate($products[$i]['tax_class_id'], $tax_address['entry_country_id'], $tax_address['entry_zone_id']),
                                        'tax_description' => tep_get_tax_description($products[$i]['tax_class_id'], $tax_address['entry_country_id'], $tax_address['entry_zone_id']),
                                        'price' => $products[$i]['price'],
                                        'final_price' => $products[$i]['price'] + $_SESSION['cart']->attributes_price($products[$i]['id']),
                                        'weight' => $products[$i]['weight'],
                                        'id' => $products[$i]['id']);

        if ($products[$i]['attributes']) {
          $subindex = 0;
          reset($products[$i]['attributes']);
          while (list($option, $value) = each($products[$i]['attributes'])) {
            $attributes_query = tep_db_query("select popt.products_options_name, popt.special, poval.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa where pa.products_id = '" . (int)$products[$i]['id'] . "' and pa.options_id = '" . (int)$option . "' and pa.options_id = popt.products_options_id and pa.options_values_id = '" . (int)$value . "' and pa.options_values_id = poval.products_options_values_id and popt.language_id = '" . (int)$languages_id . "' and poval.language_id = '" . (int)$languages_id . "'");
            $attributes = tep_db_fetch_array($attributes_query);

            $this->products[$index]['attributes'][$subindex] = array('option' => $attributes['products_options_name'],
                                                                     'value' => $attributes['products_options_values_name'],
                                                                     'option_id' => $option,
                                                                     'value_id' => $value,
                                                                     'prefix' => $attributes['price_prefix'],
                                                                     'price' => $attributes['options_values_price'],
                                                                     'special' => $attributes['special']);

            $subindex++;
          }
        }
        
       /* if($_SESSION['pwa_array_address2']['entry_firstname'] == 'Manuel'){
        echo '<pre>';
        print_r($this->products);
		echo '</pre>';
		echo 'test:'.((DISPLAY_PRICE_WITH_TAX == true) ? '1' : '0');
		}*/
        
        global $order;
        $order = $this;
        $shown_price = tep_add_tax($this->products[$index]['final_price'], $this->products[$index]['tax']) * $this->products[$index]['qty'];
        $this->info['subtotal'] += $shown_price;

        $products_tax = $this->products[$index]['tax'];
        $products_tax_description = $this->products[$index]['tax_description'];
      
	  
      if (DISPLAY_PRICE_WITH_TAX == 'true'  && (!isset($_SESSION['company_vat'])  || $order->delivery['country']['id'] == 81)) {
			$this->info['tax'] += $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));
			if (isset($this->info['tax_groups']["$products_tax_description"])) {
				$this->info['tax_groups']["$products_tax_description"] += $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));
			} 
			else {
				$this->info['tax_groups']["$products_tax_description"] = $shown_price - ($shown_price / (($products_tax < 10) ? "1.0" . str_replace('.', '', $products_tax) : "1." . str_replace('.', '', $products_tax)));
			}
		} 
		elseif($_SESSION['company_vat']=='inland') {
			$this->info['tax'] += ($products_tax / 100) * $shown_price;
			if (isset($this->info['tax_groups']["$products_tax_description"])) {
				$this->info['tax_groups']["$products_tax_description"] += ($products_tax / 100) * $shown_price;
			}
			else {
				$this->info['tax_groups']["$products_tax_description"] = ($products_tax / 100) * $shown_price;
			}
		}
		else {
			$this->info['tax'] = 0;
		}

        $index++;
      }

     
      #CG20081017 Nettopreise http://www.krawatten-ties.com/_ties0021/_wiki/bin/view/Main/NettoPreise BOF
		if (DISPLAY_PRICE_WITH_TAX == 'true'  && (!isset($_SESSION['company_vat'])  || $order->delivery['country']['id'] == 81)) {
			$this->info['total'] = $this->info['subtotal'] + $this->info['shipping_cost'];
		}
		elseif($_SESSION['company_vat']=='inland'  || $this->delivery['country']['id'] == 81) {
			$this->info['tax'] = round(($this->info['subtotal']+$this->info['shipping_cost'])*0.19,2);
			$this->info['total'] = round($this->info['subtotal']+$this->info['shipping_cost'],2)+$this->info['tax'];#-round($this->info['shipping_cost'],2)-$this->info['total'];
			#$this->info['total'] = $this->info['subtotal'] + $this->info['tax'] + $this->info['shipping_cost'];
		}
		else {
			$this->info['tax'] = 0;
			$this->info['total'] = round($this->info['subtotal']+$this->info['shipping_cost'],2);
		}
		#CG20081017 Nettopreise http://www.krawatten-ties.com/_ties0021/_wiki/bin/view/Main/NettoPreise EOF
    }
  }
?>
