<?php
/*
  $Id: PriceFormatter.php,v 1.6 2003/06/25 08:29:26 petri Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
  
  Released under the GNU General Public License
*/

/*
    PriceFormatter.php - module to support quantity pricing

    Created 2003, Beezle Software based on some code mods by WasaLab Oy (Thanks!)
*/

class PriceFormatter {
  var $hiPrice;
  var $lowPrice;
  var $quantity;
  var $hasQuantityPrice;
  
  function PriceFormatter($prices=NULL) {
    $this->productsID = -1;
    
    $this->hasQuantityPrice=false;
    $this->hasSpecialPrice=false;
    
    $this->hiPrice=-1;
    $this->lowPrice=-1;

    for ($i=1; $i<=4; $i++){
      $this->quantity[$i] = -1;
      $this->prices[$i] = -1;
    }
    $this->thePrice = -1;
    $this->specialPrice = -1;
    $this->qtyBlocks = 1;

    if($prices)
      $this->parse($prices);
  }

  function encode() {
	$str = $this->productsID . ":"
	       . (($this->hasQuantityPrice == true) ? "1" : "0") . ":"
	       . (($this->hasSpecialPrice == true) ? "1" : "0") . ":"
	       . $this->quantity[1] . ":"
	       . $this->quantity[2] . ":"
	       . $this->quantity[3] . ":"
	       . $this->quantity[4] . ":"
	       . $this->price[1] . ":"
	       . $this->price[2] . ":"
	       . $this->price[3] . ":"
	       . $this->price[4] . ":"
	       . $this->thePrice . ":"
	       . $this->specialPrice . ":"
	       . $this->qtyBlocks . ":"
	       . $this->taxClass;
	return $str;
  }

  function decode($str) {
	list($this->productsID,
	     $this->hasQuantityPrice,
	     $this->hasSpecialPrice,
	     $this->quantity[1],
	     $this->quantity[2],
	     $this->quantity[3],
	     $this->quantity[4],
	     $this->price[1],
	     $this->price[2],
	     $this->price[3],
	     $this->price[4],
	     $this->thePrice,
	     $this->specialPrice,
	     $this->qtyBlocks,
	     $this->taxClass) = explode(":", $str);

	$this->hasQuantityPrice = (($this->hasQuantityPrice == 1) ? true : false);
	$this->hasSpecialPrice = (($this->hasSpecialPrice == 1) ? true : false);
  }

  function parse($prices) {
    $this->productsID = $prices['products_id'];
    $this->hasQuantityPrice=false;
    $this->hasSpecialPrice=false;
    
    $this->quantity[1]=$prices['products_price1_qty'];
    $this->quantity[2]=$prices['products_price2_qty'];
    $this->quantity[3]=$prices['products_price3_qty'];
    $this->quantity[4]=$prices['products_price4_qty'];
    
    $this->thePrice=$prices[PRODUCTS_PRICE];
    $this->specialPrice=$prices['specials_new_products_price'];
    $this->hasSpecialPrice=tep_not_null($this->specialPrice);
    
    $this->price[1]=$prices['products_price1'];
    $this->price[2]=$prices['products_price2'];
    $this->price[3]=$prices['products_price3'];
    $this->price[4]=$prices['products_price4'];

    $this->qtyBlocks=$prices['products_qty_blocks'];
    
    $this->taxClass=$prices['products_tax_class_id'];
    
    if ($this->quantity[1] > 0) {
      $this->hasQuantityPrice = true;
      $this->hiPrice = $this->thePrice;
      $this->lowPrice = $this->thePrice;
      
      for($i=1; $i<=4; $i++) {
	if($this->quantity[$i] > 0) {
	  if ($this->price[$i] > $this->hiPrice) { 
	    $this->hiPrice = $this->price[$i];
	  }
	  if ($this->price[$i] < $this->lowPrice) { 
	    $this->lowPrice = $this->price[$i];
	  }
	}
      }
    }
  }

  function loadProduct($product_id, $language_id=1)
  {
  //pd.products_description,  hinzugef�gt by Gurkcity 14.12.2006
  //damit ist es m�glich die Beschreibung im Warenkorb und in der rechnung anzuzeigen
    /*$sql="select pd.products_name, pd.products_description, p.products_model, p.products_id," .
        " p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_weight," .
        " p.products_tax_class_id," .
        " IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price," .
        " IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price" .
        " from (( " . TABLE_PRODUCTS_DESCRIPTION . " pd," .
        "      " . TABLE_PRODUCTS . " p ) left join " . TABLE_MANUFACTURERS . " m on p.manufacturers_id = m.manufacturers_id," .
        "      " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ) left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id" .
        " where p.products_status = '1'" .
        "   and p.products_id = '" . (int)$product_id . "'" .
        "   and pd.products_id = '" . (int)$product_id . "'" .
        "   and pd.language_id = '". (int)$language_id ."'";*/
       
      $sql = 'SELECT p.products_tax_class_id, pd.products_name, pd.products_description, p.products_model, p.products_id, '.
			'p.'.PRODUCTS_PRICE.' '.
			'FROM '.TABLE_PRODUCTS.' as p '.
			'LEFT JOIN '.TABLE_PRODUCTS_DESCRIPTION.' as pd ON(p.products_id = pd.products_id AND '.(int)$language_id.' = pd.language_id) '.
 			'where p.products_id = '.(int)$product_id.' AND p.products_status = 1';
			
			//echo $sql;

    $product_info_query = tep_db_query($sql);
    $product_info = tep_db_fetch_array($product_info_query);
    $this->parse($product_info);

    return $product_info;
  }

  function computePrice($qty)
  {
	$qty = $this->adjustQty($qty);

	$price = $this->thePrice;

	if ($this->hasSpecialPrice == true)
		$price = $this->specialPrice;
	else
		for ($i=1; $i<=4; $i++)
			if (($this->quantity[$i] > 0) && ($qty >= $this->quantity[$i]))
				$price = $this->price[$i];

	return $price;
  }
  
  function adjustQty($qty) {
	// Force QTY_BLOCKS granularity
	$qb = $this->getQtyBlocks();
	if ($qty < 1)
		$qty = 1;

	if ($qb >= 1)
	{
		if ($qty < $qb)
			$qty = $qb;

		if (($qty % $qb) != 0)
			$qty += ($qb - ($qty % $qb));
	}
	return $qty;
  }

  function getQtyBlocks() {
    return $this->qtyBlocks;
  }

  function getPrice() {
    return $this->thePrice;
  }

  function getLowPrice() {
    return $this->lowPrice;
  }

  function getHiPrice() {
    return $this->hiPrice;
  }

  function hasSpecialPrice() {
    return $this->hasSpecialPrice;
  }

  function hasQuantityPrice() {
    return $this->hasQuantityPrice;
  }

  function getPriceString($style='productPriceInBox') {
    global $currencies;
    
    if ($this->hasSpecialPrice == true) {
      $lc_text = '&nbsp;<s>' 
	. $currencies->display_price($this->thePrice, 
				     tep_get_tax_rate($this->taxClass)) 
	. '</s><br><span class="productSpecialPrice">' 
	. $currencies->display_price($this->specialPrice, 
				     tep_get_tax_rate($this->taxClass)) 
	. '</span>&nbsp;';
    }
    else {

      // If you want to change the format of the price/quantity table
      // displayed on the product information page, here is where you do it.

      if($this->hasQuantityPrice == true) {
	$lc_text = '<table align="top" border="0" cellspacing="0" cellpadding="0">';
        $lc_text .= '<tr><td align="right" class=' . $style. '>'
	         . $currencies->display_price($this->thePrice, 
					   tep_get_tax_rate($this->taxClass))
                 . '</td></tr>';
	
	for($i=1; $i<=4; $i++) {
	  if($this->quantity[$i] > 0) {
	    $lc_text .= '<tr><td align="right">Rabatt bei ' 
	      . $this->quantity[$i]
	      .' und mehr<br><span class="productPriceInBoxPrice">nur '
	      . $currencies->display_price($this->price[$i], 
					   tep_get_tax_rate($this->taxClass))
	      .'</span></td></tr>';
	  }
	}
	$lc_text .= '</table>';
      }
      else {
	$lc_text = '&nbsp;' 
	  . $currencies->display_price($this->thePrice,
				       tep_get_tax_rate($this->taxClass)) 
	  . '&nbsp;';
      }
    }
    return $lc_text;
  }

  function getPriceStringShort() {
    global $currencies;

    if ($this->hasSpecialPrice == true) {
      $lc_text = '&nbsp;<s>' 
	. $currencies->display_price($this->thePrice, 
				     tep_get_tax_rate($this->taxClass)) 
	. '</s>&nbsp;&nbsp;<span class="productSpecialPrice">' 
	. $currencies->display_price($this->specialPrice, 
				     tep_get_tax_rate($this->taxClass)) 
	. '</span>&nbsp;';
    }
    else {
      if($this->hasQuantityPrice == true) {
	$lc_text = '&nbsp;'
	  . $currencies->display_price($this->lowPrice, 
				       tep_get_tax_rate($this->taxClass)) 
	  . ' - '
	  . $currencies->display_price($this->hiPrice, 
				       tep_get_tax_rate($this->taxClass)) 
	  . '&nbsp;';
      } 
      else {
	$lc_text = '&nbsp;' 
	  . $currencies->display_price($this->thePrice,
				       tep_get_tax_rate($this->taxClass)) 
	  . '&nbsp;';
      }
    }
    return $lc_text;
  }
}

?>
