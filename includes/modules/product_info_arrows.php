<?php
if($_GET['cPath'] == '44')
//Firmenkrawatten sind anders sortiert (cPath=44)
{
	$order_by = 'p.products_quantity DESC, p.products_id ASC';
}
else
{
	$order_by = 'p.products_prioritaet DESC, p.products_id ASC';
}

// Firmenkrawatten seidenkrawatten
if($_SESSION['fk_cpath'] == 'firmen_seidenkrawatten') {
	$listing_sql = "select SUM(IF(p2c.categories_id = '235','1','0')) AS kat235counter, "; // es wird gepr�ft, ob die Krawatte in der Kategorie 235 = Microfaser vorhanden ist, wenn ja wird die Summe > 0
	$listing_sql .= "p.products_quantity, pd.products_name, p.products_id, p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_tax_class_id, p.products_prioritaet, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price from (( products_description pd, products p ) left join manufacturers m on p.manufacturers_id = m.manufacturers_id, products_to_categories p2c ) left join specials s on p.products_id = s.products_id where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '".LANGUAGE_ID."' and p.products_quantity >= '10' AND (p2c.categories_id = '245' OR p2c.categories_id = '246' OR p2c.categories_id = '247' OR p2c.categories_id = '36' OR p2c.categories_id = '75' OR p2c.categories_id = '37' OR p2c.categories_id = '76' OR p2c.categories_id = '235') "; // Kategorie 235 musss mit aufgelistet werden, damit die Summe gebildet werden kann, sonst ist das Ergebnis immer 0
	$listing_sql .= " AND p.".PRODUCTS_PRICE." > 0 "; // Shop-spezifische Angabe! Nur Produkte auflisten, die einen Preis > 0 haben
	$listing_sql .= " group by p.products_id "; // �ber die grop by Anweisung kann die Summe f�r kat235counter gebildet werden
	$listing_sql .= " HAVING kat235counter < 1 "; // wenn der kat235counter aus der if-Abfrage < 1 ist handelt es sich nicht um eine Microfaserkrawatte
	$listing_sql .= " order by p.products_quantity DESC, p.products_id ASC";
}
//firmenkrawatten Microfaser und Fliegen
elseif($_SESSION['fk_cpath'] == '235' or $_SESSION['fk_cpath'] == '52') {
	$listing_sql = "select p.products_quantity, pd.products_name, p.products_id, p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_tax_class_id, p.products_prioritaet, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price from (( products_description pd, products p ) left join manufacturers m on p.manufacturers_id = m.manufacturers_id, products_to_categories p2c ) left join specials s on p.products_id = s.products_id where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '".LANGUAGE_ID."' and p.products_quantity >= '10' AND (p2c.categories_id = '" . (int)$current_category_id . "') "; // Kategorie als Filterkriterium, St�ckzahl >= 10
	$listing_sql .= " AND p.products_price4 > 0 "; // Shop-spezifische Angabe! Nur Produkte auflisten, die einen Preis > 0 haben
	$listing_sql .= " order by p.products_quantity DESC, p.products_id ASC";
}
// Dament�cher
elseif($_SESSION['fk_cpath'] == '237') {
	$listing_sql = "select p.products_quantity, pd.products_name, p.products_id, p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_tax_class_id, p.products_prioritaet, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price from (( products_description pd, products p ) left join manufacturers m on p.manufacturers_id = m.manufacturers_id, products_to_categories p2c ) left join specials s on p.products_id = s.products_id where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '".LANGUAGE_ID."' AND (p2c.categories_id = '237') ";
	$listing_sql .= " AND p.".PRODUCTS_PRICE." > 0 "; // Shop-spezifische Angabe! Nur Produkte auflisten, die einen Preis > 0 haben
	$listing_sql .= " order by p.products_quantity DESC, p.products_id ASC";
}
// Alle anderen Kategorien
else {
	$listing_sql = "select pd.products_name, p.products_id, p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_tax_class_id, p.products_prioritaet, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price from (( products_description pd, products p ) left join manufacturers m on p.manufacturers_id = m.manufacturers_id, products_to_categories p2c ) left join specials s on p.products_id = s.products_id where p.products_status = '1' and p.products_id = p2c.products_id and p.".PRODUCTS_PRICE." > ".LOWER_PRICE_LIMIT." and pd.products_id = p2c.products_id and pd.language_id = '".LANGUAGE_ID."' and p2c.categories_id = '" . (int)$current_category_id . "' order by ".$order_by;
}

#echo $listing_sql.'<br /><br />';

    $arrow_query = tep_db_query($listing_sql);
    
//Produktlisting-Tabelle	
$i=0;
	while ($arrow = tep_db_fetch_array($arrow_query)) {
	

	//Pfeil-Link
	$arrow_text = '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $arrow['products_id']) . '">';
	
	#echo $arrow['products_id'].'<br />';
	
	
	$products[$i]= $arrow_text;
	//Vergleich Produktlisting mit aktueller Get-Product-ID
	if($arrow['products_id']==(int)$_GET['products_id']) {
		//Z�hler merken
		$pid= $i;
	}
	$i++;
	}//While-Schleife zuende
	
	//Zur�ck-Bl�ttern vorbereiten
	if($products[$pid-1])
	{
		$backarrow    = $products[$pid-1]. tep_image(DIR_WS_IMAGES . 'icons/backarrow.gif', TEXT_LINK_BACKARROW) . '</a>';
	}
	else
	{
		$backarrow    = tep_image(DIR_WS_IMAGES . 'icons/backarrow_grau.gif', TEXT_LINK_BACKARROW_GREY);
	}
	//Vorw�rts-Bl�ttern vorbereiten
	if($products[$pid+1])
	{
		$forwardarrow = $products[$pid+1]. tep_image(DIR_WS_IMAGES . 'icons/forwardarrow.gif', TEXT_LINK_FORWARDARROW) . '</a>';
	}
	else
	{
		$forwardarrow = tep_image(DIR_WS_IMAGES . 'icons/forwardarrow_grau.gif', TEXT_LINK_FORWARDARROW_GREY);
	}


?>