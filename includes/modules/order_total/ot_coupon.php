<?php
		
  class ot_coupon {
    var $title, $output;
	
	
		

    function ot_coupon() {
	global $couponcode;
	
	//Definitionen nicht aus der DB holen, sondern hier setzen...
	$sql = "SELECT coupons_min_order, coupons_value, coupons_percent, coupons_code, coupons_quantity, UNIX_TIMESTAMP(coupons_valid) AS valid, coupons_extra FROM coupons where coupons_code = '" . $_SESSION['couponcode'] . "'";
		
		$coupon_query = tep_db_query($sql);
		$coupon = tep_db_fetch_array($coupon_query);
	
	
      $this->code = 'ot_coupon';
      $this->title = MODULE_COUPON_TITLE;
      $this->description = MODULE_COUPON_DESCRIPTION;
      $this->enabled = MODULE_COUPON_STATUS;
      $this->sort_order = MODULE_COUPON_SORT_ORDER;
	  $this->value = $coupon['coupons_value'];
	  $this->percent = $coupon['coupons_percent'];
      $this->minimum = $coupon['coupons_min_order'];
	  $this->couponcode = $coupon['coupons_code'];
      $this->calculate_tax = MODULE_COUPON_CALC_TAX;
//      $this->credit_class = true;
      $this->output = array();
    }

    function process() {
	#echo 'coupon:';
     global $order, $currencies, $couponcode;

      //�berpr�fung ob g�ltiger Couponcode eingegeben wurde
	  if(strtolower($_SESSION['couponcode']) == strtolower($this->couponcode))
	  {
		  #echo $this->get_order_total();
		  $od_amount = $this->calculate_credit($this->get_order_total());
		  
		  if($this->percent > 0)//prozentualer Rabatt
		  {
		  	$od_amount = $order->info['subtotal']*$this->percent/100;
			$this->title = $this->percent.'% "'.$this->couponcode.'"-Rabatt';
		  }
		  #echo $od_amount;
		  //$od_amount_coupon wird zur Anzeige des Couponwertes benutzt
		  //intern wird aber die Differenz zwischen Zwischensumme und Gutscheinwert benutzt,
		  //wenn diese unter Null f�llt bleibt sie bei Null, damit kommt es nicht zu Negativwerten
		  //in der Summe und die Versandkosten werden noch hinzuaddiert
		  $od_amount_coupon = $od_amount;
		  if(($order->info['subtotal'] - $od_amount) <0) $od_amount = $order->info['subtotal'];
		  else $od_amount = $od_amount;
		  if ($od_amount>0) {
			  $this->deduction = $od_amount;
			  $this->output[] = array('title' => $this->title . ':',
									  'text' => '<b>-' . $currencies->format($od_amount_coupon) . '</b>',
									  'value' => $od_amount);
			$order->info['total'] = $order->info['total'] - $od_amount;
			#if($order->info['total']<0) $order->info['total'] = 0;
			if($this->calculate_tax == 'true') {
				$tax = tep_get_tax_rate(MODULE_COUPON_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
				$tax_description = tep_get_tax_description(MODULE_COUPON_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
				  
			  
				$tod_amount = tep_calculate_tax($od_pc, $tax);
				$order->info['tax'] -= tep_calculate_tax($this->value, $tax);
				$order->info['tax_groups']["$tax_description"] -= tep_calculate_tax($this->value, $tax);
			}
		}
	  }
    }
    

  function calculate_credit($amount) {
    global $order, $customer_id, $payment;
    $od_amount=0;
    $od_pc = $this->value;
    $do = false;
    if ($amount > $this->minimum) {
    $table = split("[,]" , MODULE_COUPON_TYPE);
    for ($i = 0; $i < count($table); $i++) {
          if ($_SESSION['payment'] == $table[$i]) $do = true;
        }
    if ($do) {
// Calculate tax reduction if necessary
    if($this->calculate_tax == 'true') {
// Calculate main tax reduction

      $tax = tep_get_tax_rate(MODULE_COUPON_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
          $tax_description = tep_get_tax_description(MODULE_COUPON_TAX_CLASS, $order->delivery['country']['id'], $order->delivery['zone_id']);
		  
	  
	  $tod_amount = tep_calculate_tax($od_pc, $tax);
	  
	  
	  $order->info['tax'] = $order->info['tax'] - $tod_amount;
// Calculate tax group deductions
      /*
	  reset($order->info['tax_groups']);
      while (list($key, $value) = each($order->info['tax_groups'])) {
        $god_amount = round($value*10)/10*$od_pc/100;
        $order->info['tax_groups'][$key] = $order->info['tax_groups'][$key] - $god_amount;
      }
	   */
    }
    $od_amount = $od_pc;
	
    $od_amount = $od_amount + $tod_amount;
	
	 }
    }
    return $od_amount;
  }

   
  function get_order_total() {
    global  $order, $cart;
    $order_total = $order->info['total'];
	
// Check if gift voucher is in cart and adjust total
    $products = $_SESSION['cart']->get_products();
    for ($i=0; $i<sizeof($products); $i++) {
      $t_prid = tep_get_prid($products[$i]['id']);
      $gv_query = tep_db_query("select ".PRODUCTS_PRICE.", products_tax_class_id, products_model from " . TABLE_PRODUCTS . " where products_id = '" . $t_prid . "'");
      $gv_result = tep_db_fetch_array($gv_query);
      if (ereg('^GIFT', addslashes($gv_result['products_model']))) { 
        $qty = $_SESSION['cart']->get_quantity($t_prid);
        $products_tax = tep_get_tax_rate($gv_result['products_tax_class_id']);
        if ($this->include_tax =='false') {
           $gv_amount = $gv_result[PRODUCTS_PRICE] * $qty;
        } else {
          $gv_amount = ($gv_result[PRODUCTS_PRICE] + tep_calculate_tax($gv_result[PRODUCTS_PRICE],$products_tax)) * $qty;
        }
        $order_total=$order_total - $gv_amount;
      }
    }
    if ($this->include_tax == 'false') $order_total=$order_total-$order->info['tax'];
    
	
    return $order_total;
  }  

    
    function check() {
      if (!isset($this->check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_COUPON_STATUS'");
        $this->check = tep_db_num_rows($check_query);
      }

      return $this->check;
    }

    function keys() {
      return array('MODULE_COUPON_STATUS', 'MODULE_COUPON_SORT_ORDER',
	  #'MODULE_COUPON_CODE','MODULE_COUPON_VALUE','MODULE_COUPON_MINIMUM', 
	  'MODULE_COUPON_TYPE', 'MODULE_COUPON_CALC_TAX', 'MODULE_COUPON_TAX_CLASS');
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Display Couponcodes', 'MODULE_COUPON_STATUS', 'true', 'Do you want to enable the Order Discount?', '6', '1','tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_COUPON_SORT_ORDER', '5', 'Sort order of display.', '6', '2', now())");
      #tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Include Tax', 'MODULE_COUPON_INC_TAX', 'true', 'Include Tax in calculation.', '6', '6','tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      #tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Coupon Code', 'MODULE_COUPON_CODE', 'abc06KRT', 'Couponcode', '6', '7', now())");
	  #tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Coupon Value', 'MODULE_COUPON_VALUE', '4.31', 'Amount of Value(EUR).', '6', '7', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function ,date_added) values ('Calculate Tax', 'MODULE_COUPON_CALC_TAX', 'true', 'Re-calculate Tax on discounted amount.', '6', '5','tep_cfg_select_option(array(\'true\', \'false\'), ', now())");
      #tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Minimum Amount', 'MODULE_COUPON_MINIMUM', '1000', 'Minimum order before discount', '6', '2', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Payment Type', 'MODULE_COUPON_TYPE', 'invoice', 'Payment Type to get discount', '6', '2', now())");
	  tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_COUPON_TAX_CLASS', '1', 'Use the following tax class on the coupon.', '6', '7', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(', now())");
    }

    function remove() {
      $keys = '';
      $keys_array = $this->keys();
      for ($i=0; $i<sizeof($keys_array); $i++) {
        $keys .= "'" . $keys_array[$i] . "',";
      }
      $keys = substr($keys, 0, -1);

      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in (" . $keys . ")");
    }
  }
?>