<?php
/*
  $Id: product_listing.php,v 1.44 2003/06/09 22:49:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
if(defined('LISTING_EMBEDDED_IN_CONTROLWINDOW')  &&  LISTING_EMBEDDED_IN_CONTROLWINDOW === true){ ?>
	<div id="listing"><?
	if($h2_listing){ ?>
		<h2><?=$h2_listing?></h2>
	<? } 
	
	if(!$hideoptions_listing){ ?>

		<div id="options">
		<?=tep_draw_form('productsSorter', $_SERVER['REQUEST_URI'], 'GET', 'style="margin: 0px;"')?>
		<font class="menuFont"><?=LISTING_SORTY_HEAD?></font>&nbsp;&nbsp;&nbsp;
		<select name="sortBy" onChange="document.forms['productsSorter'].submit();">
			<option value="priority"<?=((!$_REQUEST['sortBy'] ||  $_REQUEST['sortBy'] == 'priority') ? ' selected' : '')?>><?=LISTING_SORTY_RECOMMENDATION?></option>	
			<option value="price"<?=(($_REQUEST['sortBy'] == 'price') ? ' selected' : '')?>><?=LISTING_SORTY_PRICE?></option>
			<option value="sales"<?=(($_REQUEST['sortBy'] == 'sales') ? ' selected' : '')?>><?=LISTING_SORTY_SALES?></option>	
		</select></form>
		</div><?
	}  ?>

    <br style='clear: both;' /><?
}
if(defined('OVERRIDE_MAX_DISPLAY_SEARCH_RESULTS')){
  $maxResult = OVERRIDE_MAX_DISPLAY_SEARCH_RESULTS;
}
else {
  $maxResult = MAX_DISPLAY_SEARCH_RESULTS;
}


$res = tep_db_query($listing_sql);

$numrows = mysql_num_rows($res);
  if ($numrows > 0 && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td class="smallText"><?php echo $numrows; ?></td>
  </tr>
</table>
<?php
  }

  $list_box_contents = array();

  for ($col=0, $n=sizeof($column_list); $col<$n; $col++) {}

  if ($numrows > 0) {

//by Gurkcity: Produkte anders listen
    #new productListingBox($list_box_contents);
  
  ?>


<?php
if ($numrows > 0) {
  $column_count = 0;
  $i=0;
  $max_cols= PRODUCT_LISTING_COLS; // Anzahl der Spalten beim Produkt Listing; //in Spalte 4 wird eine andere Klasse benï¿½tigt, die keinen rechten Randabstand hat
    $listing_query = $res;
  
  #echo $listing_split->sql_query.'<br /><br />';

//Produktlisting-Tabelle  
  while ($listing = tep_db_fetch_array($listing_query)) {
    
  if( defined( 'hasDBtexterrors' ) && hasDBtexterrors && defined('PAYMENT_PAYPAL_REGIONCODE') ) {  $listing['products_name'] = correctDBtexterrors( PAYMENT_PAYPAL_REGIONCODE, $listing['products_name'] ); }
  
  
  #echo $listing['products_id'].'<br />';
  
  //Produktbezeichnung
  $lc_text = '';
  #$lc_text = '<strong><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'manufacturers_id=' . $HTTP_GET_VARS['manufacturers_id'] . '&amp;products_id=' . $listing['products_id']) . '">' . $listing['products_name'] . '</a></strong><br />';
  
  
  //Produktbild
  $breaks = array("~<br>~","~<br />~","~<br/>~","~\n~","~\r\n~","~\r~",);
  $listing['products_name'] = preg_replace($breaks, "", $listing['products_name']);
  $listing['products_name'] = preg_replace("~&~", "&amp;", $listing['products_name']);
  
  $lc_text .= '<div>';
  
  // echo "###".$listing['products_id']."|".$listing['products_name']."<br />";
  if ((is_array($aExtraImageCategories) && in_array($_GET['cPath'], $aExtraImageCategories)) || $_GET['cPath'] == '98') {//Manschettenknï¿½pfe anders listen mit 108px Hï¿½he und Querformat
      $lc_text .= '<a href="' . tep_href_link(((MOBILE_VERSION === true) ? FILENAME_PRODUCT_INFO_MOBILE : FILENAME_PRODUCT_INFO), ($cPath ? 'cPath=' . $cPath . '&' : '') . 'products_id=' . $listing['products_id']) . '">' . tep_product_image($listing['products_model'], 'sm', $listing['products_name'], SMALL_IMAGE_WIDTH, EXTRA_SMALL_IMAGE_HEIGHT, NULL,  $listing['products_id']) . '</a>';
    } else {
      $lc_text .= '<a href="' . tep_href_link(((MOBILE_VERSION === true) ? FILENAME_PRODUCT_INFO_MOBILE : FILENAME_PRODUCT_INFO), ($cPath ? 'cPath=' . $cPath . '&' : '') . 'products_id=' . $listing['products_id'].(($listing_productsLinkAddParams) ? $listing_productsLinkAddParams : '')) . '">' . tep_product_image($listing['products_model'], 'sm', $listing['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, NULL,  $listing['products_id']) . '</a>';
    }
  $lc_text .= '</div>';
  $lc_text .= '<div class="productListingBottom">'."\n";
  
  //Details-Link
  if($section == "BOX_CATEGORY_FIRMENKRAWATTEN") {
  
    $lc_text .= '<div class="prname"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&' : '') . 'products_id=' . $listing['products_id']) . '">' . $listing['products_quantity'] . ' '.PRODUCT_LISTING_FIRMENKRAWATTEN_ANZAHL.'</a></div>';
  
  }
  else if($listing['products_prioritaet']>=30 && $i<10  &&  (!defined('LISTING_NO_TOP10') || (defined('LISTING_NO_TOP10') && LISTING_NO_TOP10 == false))) {
  
    $lc_text .= '<div class="prtop10"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&' : '') . 'products_id=' . $listing['products_id']) . '">' . tep_image(DIR_WS_IMAGES.'layout/thumbnails/top10_bg.gif','Top 10 Krawatte','','','class="topten"') . '</a></div>';
  
  }
  else {
  
    //$lc_text .= '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '" class="listingStd">&raquo;&nbsp;' . $listing['products_name'] . '</a>';
    $lc_text .= '<div class="prname"><a title="'.$listing['products_name'].'" href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&' : '') . 'products_id=' . $listing['products_id'].(($listing_productsLinkAddParams) ? $listing_productsLinkAddParams : '')) . '" class="listingStdPrName">'.$listing['products_name'].'</a></div>';
  
  }
  
  if(defined('LANGUAGE_CURRENCY')   &&  LANGUAGE_CURRENCY != ''){
  	$decimalPoint = $currencies->currencies[LANGUAGE_CURRENCY]['decimal_point'];
  }
  else {
  	$decimalPoint = $currencies->currencies[DEFAULT_CURRENCY]['decimal_point'];
  }
  
  //echo 'dec:'.$decimalPoint.':enddec___price:'.$listing[PRODUCTS_PRICE].'<br>';
//echo 'taxrate:'.tep_get_tax_rate($listing['products_tax_class_id']).'--endtax<br>';
  $priceOrigin = $currencies->display_price($listing[PRODUCTS_PRICE], tep_get_tax_rate($listing['products_tax_class_id']));

  //Preis
  if (tep_not_null($listing['specials_new_products_price'])) {
    $lc_text .= '&nbsp;<s>' .  $currencies->display_price($listing[PRODUCTS_PRICE], tep_get_tax_rate($listing['products_tax_class_id'])) . '</s><br /><span class="productSpecialPrice">' . $currencies->display_price($listing['specials_new_products_price'], tep_get_tax_rate($listing['products_tax_class_id'])) . '</span><br />';
  }
  elseif(defined('SPLIT_PRICE_AT_DECIMAL_PIONT') and SPLIT_PRICE_AT_DECIMAL_PIONT===true) {

    list($priceNoCurrencySign) = explode(' ', $priceOrigin);
    list($price, $priceCent) = explode($decimalPoint, $priceNoCurrencySign);  
	$decimalPlaces = $currencies->currencies[DEFAULT_CURRENCY]['decimal_places'];

	  
    $lc_text .= '<div class="prprice">
    						<div class="euro"><span class="price">'.$price.'</span></div>';
	if((int)$decimalPlaces > 0){  
		$lc_text .= '<div class="cent"><span class="priceCent">'.$priceCent.'</span></div>';
	}
	$lc_text .= '</div>'."\n";
  }
  elseif(defined('NOCURRENCY_PRICE_LISTING') and NOCURRENCY_PRICE_LISTING===true) {
    
    list($priceNoCurrencySign) = explode(' ', $priceOrigin);
    $lc_text .= '<div class="prprice"><span class="price">' . $priceNoCurrencySign . '</span></div>'."\n";
  }
  else {
  	
    $lc_text .= '<div class="prprice"><span class="price">' . $priceOrigin . '</span></div>'."\n";
  }
  
  $lc_text .= '</div>'."\n";
  
  // ManschettenknÃ¶pfe?
  if($cPath == 98) {
    echo "<div class=\"productListing mknopfListing";
  }
  // keine manschettenknÃ¶pfe
  else {
    echo "<div class=\"productListing";
  }
  
  if((1+$column_count) == $max_cols)
  {
    echo "\" style=\"margin-right: 0px;";
    $column_count = -1;
  }
  echo "\">\n";
  
  
  echo $lc_text."\n";
  echo "</div>";
  
  $i++;
  $column_count++;
  
  if(!empty($maxResult)  &&  $i == $maxResult){
  	break;
  }
  
  }//While-Schleife zuende
  
#$i=0;  
#for($rows=0;$rows<$max_rows;$rows++){

    #$cur_row = sizeof($list_box_contents) - 1;
  #echo "<tr>\n";
  #for($cols=0;$cols<$max_cols;$cols++){
  # $i++;
  #}
  #echo "</tr>\n";
#} 
?><div style="clear: both;"></div><?



}
?>
  

  
  <?php

  // EOF by Gurkcity: Produkte anders listen
  } else {
       ?><div id="listing_noProducts"><?
       echo TEXT_NO_PRODUCTS; ?></div><?

    
  }

  if ( ($numrows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) && !$hideNumberOfProducts) {
?>
<div class="boxListingFooter">
</div>
<?php
  }
  
  if(defined('LISTING_EMBEDDED_IN_CONTROLWINDOW')  &&  LISTING_EMBEDDED_IN_CONTROLWINDOW === true){ ?>
	</div><?
} ?>