<?php
/*
Newsletter-Modul by Gurkcity

Felder:
nl_ID, nl_status, nl_anrede, nl_name, nl_email, nl_hash, nl_anmeldedatum
*/

//ï¿½berprï¿½fungen

//existiert E-Mail?
//liefert false, wenn E-Mail existiert oder keine E_mail angegeben wurde,
//liefert true, wenn E-Mail nicht existiert

function gc_check_email($email){
	if($email=='') return false;
	$sql = "SELECT * FROM newsletter_entries WHERE email = '". $email ."'";
	$db_query = tep_db_query($sql);
	if(tep_db_num_rows($db_query) < 1)
	{
		return true;
	} 
	else 
	{
		return false;
	}
}
function gc_check_email_group($email,$group = 4){

	if($email=='') return false;
	$sql = "SELECT * FROM newsletter_entries WHERE email = '". $email ."' AND `groupId` = '".$group."'";

	$db_query = tep_db_query($sql);
	if(tep_db_num_rows($db_query) < 1)
	{
		return true;
	} 
	else 
	{
		return false;
	}
}

//existiert hash?
//liefert true, wenn hash existiert,
//liefert false, wenn hash nicht existiert
function gc_check_hash($hash){
	$sql = "SELECT * FROM newsletter_entries WHERE abo_actcode = '". $hash ."'";
	$db_query = tep_db_query($sql);
	if(tep_db_num_rows($db_query) < 1)
	{
		return false;
	} 
	else 
	{
		return true;
	}
}

//schon aktiviert?
function gc_check_activation($hash){
	$sql = "SELECT * FROM newsletter_entries WHERE abo_activated = 0 AND abo_actcode = '". $hash ."'";
	$db_query = tep_db_query($sql);
	if(tep_db_num_rows($db_query) < 1)
	{
		return false;
	} 
	else 
	{
		return true;
	}
}

$error = false;
  
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'activate')) {
    $hash = tep_db_prepare_input($HTTP_GET_VARS['hash']);
	
	if(gc_check_activation($hash)) 
	{
		//alles okay, DB Status hochsetzen
		//$sql_data_array = array('nl_status' => '2');
		//tep_db_perform('newsletter', $sql_data_array,'update', "nl_hash = '".$hash."'");
		
		$sql_data_array = array(
							  'abo_activated' => 1,
							  'abo_actdate' => date('Y-m-d H:i:s'));
		tep_db_perform('newsletter_entries', $sql_data_array,'update', "abo_actcode = '".$hash."'");
		
			
		
		
		//...und weiterleiten
		tep_redirect(tep_href_link(FILENAME_NEWSLETTER, 'action=activate_success'));
		
	} else {
	
	  //Fehler aufgetreten, Email schon aktiviert oder hash nicht vorhanden
      $error = true;

      $messageStack->add('contact', ENTRY_ACTIVATION_NEWSLETTER_CHECK_ERROR);
    }
  }
  
  if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'austragen')) {
    $hash = tep_db_prepare_input($HTTP_GET_VARS['hash']);
	
	if(gc_check_hash($hash)) 
	{
		//alles okay, DB Status runtersetzen
		/*
		$sql_data_array = array('nl_status' => '0');
		tep_db_perform('newsletter', $sql_data_array,'update', "nl_hash = '".$hash."'");
		
		$sql_data_array = array(
							  'activated' => '0',
							  'nl_status' => '0');
		tep_db_perform('fn_entries', $sql_data_array,'update', "activation_code = '".$hash."'");
		*/
		//lieber doch lï¿½schen?
		
		//tep_db_query("DELETE FROM newsletter WHERE nl_hash = '".$hash."'");
		tep_db_query("DELETE FROM newsletter_entries WHERE abo_actcode = '".$hash."'");
		
			
		
		
		//...und weiterleiten
		tep_redirect(tep_href_link(FILENAME_NEWSLETTER, 'action=austragen_success'));
		
	} else {
	
	  //Fehler aufgetreten, Email schon aktiviert oder hash nicht vorhanden
      $error = true;

      $messageStack->add('contact', ENTRY_HASH_NEWSLETTER_CHECK_ERROR);
    }
  }
  if ($HTTP_GET_VARS['action'] == 'send') {

	$name = tep_db_prepare_input($HTTP_GET_VARS['name']);
    $email_address = tep_db_prepare_input($HTTP_GET_VARS['email']);
    $anrede = tep_db_prepare_input($HTTP_GET_VARS['anrede']);

	$resGroup = tep_db_query('SELECT id FROM '.TABLE_NEWSLETTER_GROUPS.' WHERE shopId = '.SHOP_ID);
	list($nlGroupID) = tep_db_fetch_row($resGroup);

	//in allen anderen Gruppen darf ein Eintrag vorhanden sein und parallel zu dem Standard NL existieren
    if (tep_validate_email($email_address) && gc_check_email_group($email_address, $nlGroupID )) {
     
	  #tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, EMAIL_SUBJECT, $enquiry, $name, $email_address);
		
		#echo 'E-Mail ist korrekt und nicht in DB';
		#exit;
		
		
		//Hash-Wert generieren
		$hash = md5($email_address.time());
		
		//welche Seite wurde eingetragen?
		$nl_url = 'http://'.HTTP_COOKIE_DOMAIN.$PHP_SELF;
			
			
//E-Mail verschicken
		
		
//Message-Body vorbereiten

if($anrede == '1') $anrede_email = ANREDE_EMAIL_FRAU;
else if($anrede == '0') $anrede_email = ANREDE_EMAIL_HERR; 
else $anrede_email = 'Sehr geehrter Kunde,';


$msg_body= $shop_name .' '.EMAIL_TEXT_BESTELLBESTAETIGUNG."\n\n";
if(isset($name) && $name !='')
{
	$msg_body.=$anrede_email." ".$name.",\n";
}
else
{
	$msg_body.=$anrede_email."\n";
}
$msg_body.=EMAIL_AKTIVIERUNG_KOPFTEXT;
$msg_body.="http://".HTTP_COOKIE_DOMAIN.DIR_TESTSITE."/newsletter/index.php?action=activate&hash=".$hash."\n";
$msg_body.="\n";
$temp1=preg_replace('/NL_URL/',$nl_url,EMAIL_AKTIVIERUNG_FUSSTEXT);
$temp2=preg_replace('/EMAIL_ADRESS/',$email_address,$temp1);
$msg_body.=$temp2;
#$msg_body.=EMAIL_AKTIVIERUNG_FUSSTEXT;
$msg_body.="\n";
$msg_body.="\n";

		//echo "$name, $email_address, ".EMAIL_TEXT_SUBJECT.", $msg_body<br>";


		


		if(tep_mail($name, $email_address, EMAIL_TEXT_SUBJECT, $msg_body, TITLE, CONTACT_US_MAIL_EMAIL))
		{
			
			//DB-Eintrï¿½ge

			$sql_data_array = array(
							  'abo_activated' => '0',
							  'email' => $email_address,
							  '`groupId`' => $nlGroupID, //NL-Abonenten = 1, gewinnspiel-GS = 2, Kunden-Gruppe = 3 #by Gurkcity 06.10.2006
							  'abo_actcode' => $hash,
							  'type' => 'abo',
							  'abo_date' => date('Y-m-d H:i:s', time()));
			tep_db_perform('newsletter_entries', $sql_data_array);
			
			
			tep_redirect(tep_href_link(FILENAME_NEWSLETTER, 'action=success'));
		}
		else
		{
			$error = true;
			$messageStack->add('contact', 'Fehler beim versenden der E-Mail');
		}	
		
		#tep_mail($name, $email_address, EMAIL_TEXT_SUBJECT, $msg_body, 'Gurk', 'info@gurkcity.de');

      
    } else {
      $error = true;      
		$messageStack->add('contact', ENTRY_EMAIL_ADDRESS_NEWSLETTER_CHECK_ERROR);
    }
  }
?>