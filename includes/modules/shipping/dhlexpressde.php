<?php
/*
  $Id: table.php,v 1.27 2003/02/05 22:41:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class dhlexpressde {
    var $code, $title, $description, $icon, $enabled;
    
// class constructor
    function dhlexpressde() {
      global $order, $shipping_weight, $dhlexpress, $dhlexpress_saturday;

      $this->code = 'dhlexpressde';
      $this->title = MODULE_SHIPPING_DHLEXPRESS_DE_TEXT_TITLE;
      $this->description = MODULE_SHIPPING_DHLEXPRESS_DE_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_SHIPPING_DHLEXPRESS_DE_SORT_ORDER;
      $this->icon = '';
      $this->tax_class = MODULE_SHIPPING_DHLEXPRESS_DE_TAX_CLASS;
      $this->enabled = ((MODULE_SHIPPING_DHLEXPRESS_DE_STATUS == 'True') ? true : false);
	
if(defined('DEBUG'))
{
	
	#echo '<hr /><h1>$this->enabled:</h1>';
	#go_dump($this->enabled);
	
	#$sql ="select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_RUSH_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id";	
	#echo '<hr /><h1>$sql:</h1>';
	#go_dump($sql);
	
	
	#echo '<h1>$this->tax_class:</h1>';
	#go_dump($this->tax_class);


}#DEBUG ENDE 



     // $this->install();
	  
        $check_flag = false;
		
		//Dieses Modul nur in einer bestimmten Geo-Zone erlauben
        $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_DHLEXPRESS_DE_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
        
        //echo "select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_DHLEXPRESS_DE_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id";
        
        while ($check = tep_db_fetch_array($check_query)) {
          //Kein Bundesland in der Zone
		  if ($check['zone_id'] < 1) {
            $check_flag = true;
            break;
		  //Zone vorhanden, muss mit der Lieferzone (=Bundesland = zone_id) übereinstimmen
          } elseif ($check['zone_id'] == $order->delivery['zone_id']) {
            $check_flag = true;
			#echo "$order->delivery['zone_id']:".$order->delivery['zone_id'];
            break;
          }
        }

        if ($check_flag == false) {
          $this->enabled = false;
        }
		
      // echo "<br>__DHLEXPRESS: ".$_SESSION['dhlexpress']."__<br>";
	  if ((tep_session_is_registered('dhlexpress')  && $_SESSION['dhlexpress']  ==  1)  ||  (tep_session_is_registered('dhlexpress_saturday')  && $_SESSION['dhlexpress_saturday']  ==  1)) {
	  	
	 // echo ($this->enabled) ? '<br>__DHL EXPRESS WAS ACTIVATED__<br>' :  '<br>__DHL EXPRESS WAS DEACTIVATED__<br>';
	  	
	  	//$this->install();
  	
	  }
	  //wenn rush-shipping nicht gewählt ist Modul deaktiviert
	  else
	  {
	  		//echo '<br>__DHL PARAM MISSING__<br>';
	  		$this->enabled = false;
	  }

    }
	

// class methods
    function quote($method = '') {

      global $order, $cart, $shipping_weight, $shipping_num_boxes, $dhlexpress_saturday;
      //echo '<br>_PRICE: '.MODULE_SHIPPING_DHLEXPRESS_DE_MODE.'_<br>';

      if (MODULE_SHIPPING_DHLEXPRESS_DE_MODE == 'price') {
        $order_total = $cart->show_total();
      } else {
        $order_total = $shipping_weight;
      }

      //echo "<br>_SHIPPING WEIGHT: ".$shipping_weight."_<br>";
    /*  if($shipping_weight > 20000  &&  $shipping_weight <= 31500){
      	
      	   $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_31KG;
      	   //echo "<br>_COST 31 KG_<br>";
      }
      elseif($shipping_weight > 10000){
      	
      	   $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_20KG;
      	   //echo "<br>_COST 20 KG_<br>";
      }
     elseif($shipping_weight > 2000){
      	
      	   $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_10KG;
      	   //echo "<br>_COST 10 KG_<br>";
      }
     elseif($shipping_weight > 1000){
      	
      	   $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_2KG;
      	   //echo "<br>_COST 2 KG_<br>";
      }
     else{
      	
      	   $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_1KG;
      	   //echo "<br>_COST 1 KG_<br>";
      }*/
      
       if(tep_session_is_registered('dhlexpress_saturday')  && $_SESSION['dhlexpress_saturday']  ==  1){
          $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST_SATURDAY;
	   }
	   else {
	      $constantCosts = MODULE_SHIPPING_DHLEXPRESS_DE_COST;
	   }
      
      
      $table_cost = split("[:,]" , $constantCosts);
      $size = sizeof($table_cost);
      for ($i=0, $n=$size; $i<$n; $i+=2) {
        if ($order_total <= $table_cost[$i]) {
          $shipping = $table_cost[$i+1];
          break;
        }
      }

      if (MODULE_SHIPPING_DHLEXPRESS_DE_MODE == 'weight') {
        $shipping = $shipping * $shipping_num_boxes;
      }

      $this->quotes = array('id' => $this->code,
                            'module' => MODULE_SHIPPING_DHLEXPRESS_DE_TEXT_TITLE,
                            'methods' => array(array('id' => $this->code,
                                                     'title' => MODULE_SHIPPING_DHLEXPRESS_DE_TEXT_WAY,
                                                     'cost' => $shipping + MODULE_SHIPPING_DHLEXPRESS_DE_HANDLING)));

if(defined('DEBUG'))
{
	#echo '<h1>$this->tax_class:</h1>';
	#go_dump($this->tax_class);
	
	#echo '<hr /><h1>$order->delivery[\'country\'][\'id\']:</h1>';
	#go_dump($order->delivery['country']['id']);
	
	
	#echo '<hr /><h1>$order->delivery[\'zone_id\']:</h1>';
	#go_dump($order->delivery['zone_id']);


}#DEBUG ENDE
      if ($this->tax_class > 0) {


        $this->quotes['tax'] = tep_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
      }

      if (tep_not_null($this->icon)) $this->quotes['icon'] = tep_image($this->icon, $this->title);

      return $this->quotes;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_DHLEXPRESS_DE_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Enable DHLExpress_De Method', 'MODULE_SHIPPING_DHLEXPRESS_DE_STATUS', 'True', 'Do you want to offer table rate shipping?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_1KG', '99.99999:10.840336, 10000000:6.722689', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_2KG', '99.99999:12.521008, 10000000:8.403361', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_10KG', '99.99999:20.924370, 10000000:16.806723', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_20KG', '99.99999:25.126050, 10000000:21.008403', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_31KG', '99.99999:33.529412, 10000000:29.411765', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST', '10000000:10.840336', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('DHLExpress_De Shipping Table', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_SATURDAY', '10000000:21.848', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('DHLExpress_De Table Method', 'MODULE_SHIPPING_DHLEXPRESS_DE_MODE', 'weight', 'The shipping cost is based on the order total or the total weight of the items ordered.', '6', '0', 'tep_cfg_select_option(array(\'weight\', \'price\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Handling Fee', 'MODULE_SHIPPING_DHLEXPRESS_DE_HANDLING', '0', 'Handling fee for this shipping method.', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_DHLEXPRESS_DE_TAX_CLASS', '1', 'Use the following tax class on the shipping fee.', '6', '0', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Shipping Zone', 'MODULE_SHIPPING_DHLEXPRESS_DE_ZONE', '1', 'If a zone is selected, only enable this shipping method for that zone.', '6', '0', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_DHLEXPRESS_DE_SORT_ORDER', '0', 'Sort order of display.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_SHIPPING_DHLEXPRESS_DE_STATUS', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_1KG', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_2KG', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_10KG', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_20KG', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_31.5KG', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST', 'MODULE_SHIPPING_DHLEXPRESS_DE_COST_SATURDAY', 'MODULE_SHIPPING_DHLEXPRESS_DE_MODE', 'MODULE_SHIPPING_DHLEXPRESS_DE_HANDLING', 'MODULE_SHIPPING_DHLEXPRESS_DE_TAX_CLASS', 'MODULE_SHIPPING_DHLEXPRESS_DE_ZONE', 'MODULE_SHIPPING_DHLEXPRESS_DE_SORT_ORDER');
    }
  }
?>
