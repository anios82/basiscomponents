<?php
/*
  $Id: table.php,v 1.27 2003/02/05 22:41:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  class table2 {
    var $code, $title, $description, $icon, $enabled;

// class constructor
    function table2() {
      global $order;

      $this->code = 'table2';
      $this->title = MODULE_SHIPPING_TABLE2_TEXT_TITLE;
      $this->description = MODULE_SHIPPING_TABLE2_TEXT_DESCRIPTION;
      $this->sort_order = MODULE_SHIPPING_TABLE2_SORT_ORDER;
      $this->icon = '';
      $this->tax_class = MODULE_SHIPPING_TABLE2_TAX_CLASS;
      $this->enabled = ((MODULE_SHIPPING_TABLE2_STATUS == 'True') ? true : false);
  

      if ( ($this->enabled == true) && ((int)MODULE_SHIPPING_TABLE2_ZONE > 0) ) {
        $check_flag = false;
        $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_TABLE2_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
        while ($check = tep_db_fetch_array($check_query)) {
          if ($check['zone_id'] < 1) {
            $check_flag = true;
            break;
          } elseif ($check['zone_id'] == $order->delivery['zone_id']) {
            $check_flag = true;
            break;
          }
        }

        if ($check_flag == false) {
          $this->enabled = false;
        }
		
		//echo ($this->enabled == true) ? 'enabled 2' : 'not enabled 2';
		
				       // M. Wiese 29.06.2010
       // if dhlexpress is active (1), then disable this shipping module
        global $dhlexpress, $dhlexpress_saturday;
        //echo "<br>__DHLEXPRESS: ".$dhlexpress."__<br>";
        if((tep_session_is_registered('dhlexpress')  && $_SESSION['dhlexpress']  ==  1)  ||  (tep_session_is_registered('dhlexpress_saturday')  && $_SESSION['dhlexpress_saturday']  ==  1)){
        	 $this->enabled = false;
        	 //echo '<br>__DISABLE STANDARD SHIPPING MODULE BECAUSE OF DHLEXPRESS__<br>';
        }
      }
    }

// class methods
    function quote($method = '') {
      global $order, $cart, $shipping_weight, $shipping_num_boxes;

      if (MODULE_SHIPPING_TABLE2_MODE == 'price') {
        $order_total = $_SESSION['cart']->show_total();
      } else {
        $order_total = $shipping_weight;
      }

 
	  //doesnt work anymore?!?!
      //$table_cost = explode("[:,]" , MODULE_SHIPPING_TABLE_COST);
	  
	  $table_cost_items = explode(", " , MODULE_SHIPPING_TABLE2_COST);

      foreach ($table_cost_items as $cost_item) {
      	
      	list($maxsubtotal, $cost) = explode(':', $cost_item);
      
        if ($order_total <= $maxsubtotal) {
          $shipping = $cost;
          break;
        }
      }

      if (MODULE_SHIPPING_TABLE2_MODE == 'weight') {
        $shipping = $shipping * $shipping_num_boxes;
      }

      $this->quotes = array('id' => $this->code,
                            'module' => MODULE_SHIPPING_TABLE2_TEXT_TITLE,
                            'methods' => array(array('id' => $this->code,
                                                     'title' => MODULE_SHIPPING_TABLE2_TEXT_WAY,
                                                     'cost' => $shipping + MODULE_SHIPPING_TABLE2_HANDLING)));

      if ($this->tax_class > 0) {
        $this->quotes['tax'] = tep_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
      }

      if (tep_not_null($this->icon)) $this->quotes['icon'] = tep_image($this->icon, $this->title);

      return $this->quotes;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_TABLE2_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) VALUES ('Enable Table Method', 'MODULE_SHIPPING_TABLE2_STATUS', 'True', 'Do you want to offer table rate shipping?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Shipping Table', 'MODULE_SHIPPING_TABLE2_COST', '25:8.50,50:5.50,10000:0.00', 'The shipping cost is based on the total cost or weight of items. Example: 25:8.50,50:5.50,etc.. Up to 25 charge 8.50, from there to 50 charge 5.50, etc', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Table Method', 'MODULE_SHIPPING_TABLE2_MODE', 'weight', 'The shipping cost is based on the order total or the total weight of the items ordered.', '6', '0', 'tep_cfg_select_option(array(\'weight\', \'price\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Handling Fee', 'MODULE_SHIPPING_TABLE2_HANDLING', '0', 'Handling fee for this shipping method.', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_TABLE2_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '0', 'tep_get_tax_class_title', 'tep_cfg_pull_down_tax_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Shipping Zone', 'MODULE_SHIPPING_TABLE2_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '0', 'tep_get_zone_class_title', 'tep_cfg_pull_down_zone_classes(', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_TABLE2_SORT_ORDER', '0', 'Sort order of display.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_SHIPPING_TABLE2_STATUS', 'MODULE_SHIPPING_TABLE2_COST', 'MODULE_SHIPPING_TABLE2_MODE', 'MODULE_SHIPPING_TABLE2_HANDLING', 'MODULE_SHIPPING_TABLE2_TAX_CLASS', 'MODULE_SHIPPING_TABLE2_ZONE', 'MODULE_SHIPPING_TABLE2_SORT_ORDER');
    }
  }
?>
