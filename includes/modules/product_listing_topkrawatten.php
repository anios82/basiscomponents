<?php

// create column list
    $define_list = array('PRODUCT_LIST_MODEL' => PRODUCT_LIST_MODEL,
                         'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
                         'PRODUCT_LIST_MANUFACTURER' => PRODUCT_LIST_MANUFACTURER,
                         'PRODUCT_LIST_PRICE' => PRODUCT_LIST_PRICE,
                         'PRODUCT_LIST_QUANTITY' => PRODUCT_LIST_QUANTITY,
                         'PRODUCT_LIST_WEIGHT' => PRODUCT_LIST_WEIGHT,
                         'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
                         'PRODUCT_LIST_BUY_NOW' => PRODUCT_LIST_BUY_NOW);

    asort($define_list);

    $column_list = array();
    reset($define_list);
    while (list($key, $value) = each($define_list)) {
      if ($value > 0) $column_list[] = $key;
    }

    $select_column_list = '';

    for ($i=0, $n=sizeof($column_list); $i<$n; $i++) {
      switch ($column_list[$i]) {
        case 'PRODUCT_LIST_MODEL':
          $select_column_list .= 'p.products_model, ';
          break;
        case 'PRODUCT_LIST_NAME':
          $select_column_list .= 'pd.products_name, ';
          break;
        case 'PRODUCT_LIST_MANUFACTURER':
          $select_column_list .= 'm.manufacturers_name, ';
          break;
        case 'PRODUCT_LIST_QUANTITY':
          $select_column_list .= 'p.products_quantity, ';
          break;
        case 'PRODUCT_LIST_WEIGHT':
          $select_column_list .= 'p.products_weight, ';
          break;
      }
    }

// show the products in a given categorie

// We show them all
		if($productsOrder){
			foreach($productsOrder as $c=>$model):
				
				$productsOrderSelect .= 'IF(p.products_model = \''.$model.'\', '.($c+1).', ';
			endforeach;
			
			$productsOrderSelect .= count($productsOrder)+2;
			foreach($productsOrder as $model):
				$productsOrderSelect .= ')';
			endforeach;
			
			$productsOrderSelect .= ' as sort_products';
		}
        $listing_sql = "select " . $select_column_list . " p.products_id, p.products_model, p.manufacturers_id, p.".PRODUCTS_PRICE.", p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.".PRODUCTS_PRICE.") as final_price, pd.products_description, pd.products_seo_subhead, ".(($productsOrderSelect) ? $productsOrderSelect : '1')." from (( " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS . " p ) left join " . TABLE_MANUFACTURERS . " m on p.manufacturers_id = m.manufacturers_id, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c ) left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id where 1 ".((!$productsStatus_showAll) ? ' AND p.products_status = \'1\'' : '')." and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '" . (int)$languages_id . "' and p2c.categories_id = '" . (int)$current_category_id . "' and p.".PRODUCTS_PRICE." > 0 ";
        
        if($productsOrder){
        	$listing_sql .= " order by sort_products ASC";
        }
		else {
        	$listing_sql .= " order by products_prioritaet DESC";
        }


//wegen Top 5 werden auch nur 5 dargestellt

  $listing_split = new splitPageResults($listing_sql, (($sqlLimit) ? $sqlLimit : 5), 'p.products_id');
    
//by Gurkcity: Produkte anders listen
    #new productListingBox($list_box_contents);
	
	?>
	<div style="clear:both">&nbsp;</div>


<?php
if ($listing_split->number_of_rows > 0) {
    $rows = 0;
	$cols = 0;
	$i=1;
	$max_cols= 1;
	$max_rows= ceil($listing_split->number_of_rows / $max_cols);
	$tdwidth = floor(100/$max_cols);
    $listing_query = tep_db_query($listing_split->sql_query);

//Produktlisting-Tabelle	
	while ($listing = tep_db_fetch_array($listing_query)) {

		$lc_text  = '<h2><strong>'.$i.'. '.PRODUCTS_LISTING_TOPKRAWATTEN_PLATZ.'';
		//Tabelle Anfang: 2 Spalten
		if($productsName_asHEAD){
			$lc_text  = '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '">'.$listing['products_name'].'';
		}
		$lc_text  .= '</strong></h2>';

		$lc_text .= '<div class="productListing2">'."\n";

		//Produktbezeichnung

		if($productsName_asHEAD){
			$lc_text .= '<strong>' . (($subhead_asSUBHEAD) ? $listing['products_seo_subhead'] : $listing['products_name']) . '</strong>';
		}
		else {
			$lc_text .= '<strong><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '">' . (($subhead_asSUBHEAD) ? $listing['products_seo_subhead'] : $listing['products_name']) . '</a></strong>';
		}

		$lc_text .= "<p>\n";
		//Produktbild

		if($emphasizeFirstProduct  &&  $i==1){
			$lc_text .= '<div style="float: right; margin: 0 0 20px 20px;"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '">' . tep_product_image($listing['products_model'], 'md', $listing['products_name'], '220', '320', 'class="topkrawatten"') . '</a></div>';
		}
		else {
			$lc_text .= '<div style="float: right; margin: 0 0 20px 20px;"><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '">' . tep_product_image($listing['products_model'], 'sm', $listing['products_name'], '138', '174', 'class="topkrawatten"', NULL, 'md') . '</a></div>';
		}

		//Beschreibung
		$lc_text .= '<div style="margin: 0 0 10px 0;">';
		$lc_text .= $listing['products_description'];
		$lc_text .= '</div>'."\n";

		//Preis
		if (tep_not_null($listing['specials_new_products_price'])) {
		  $lc_text .= '&nbsp;<s>' .  $currencies->display_price($listing[PRODUCTS_PRICE], tep_get_tax_rate($listing['products_tax_class_id'])) . '</s><span class="productSpecialPrice">' . $currencies->display_price($listing['specials_new_products_price'], tep_get_tax_rate($listing['products_tax_class_id'])) . '</span>';
		} else {
			$price = str_replace(' ','', $currencies->display_price($listing[PRODUCTS_PRICE], tep_get_tax_rate($listing['products_tax_class_id'])));
			list($priceEuro, $priceCent) = explode(',', $price);
			list($priceCent, $tmp) = explode(' ', $priceCent);

			$lc_text .= '<div class="prprice" style="width:60px;"><div class="euro" style="width:30px; text-align:right;"><span class="price">'.$priceEuro.'</span></div><div class="cent"><span class="priceCent">'.$priceCent.'</span>&nbsp;&nbsp;</div></div>';

				if(!defined('HIDE_INTOCART_BUTTON') && HIDE_INTOCART_BUTTON !== true){
					$lc_text .= '<div>';
					$lc_text .= ''.tep_draw_form('cart_quantity1', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=add_product'),'post','id="cart_quantity1" style=""').
									tep_draw_hidden_field('products_id', $listing['products_id']) . '<input type="image" src="/images/layout_'.LANGUAGE.'/incartBtn.png" title="'.IMAGE_BUTTON_IN_CART.'" /></form>';
					$lc_text .= '</div>';
				}
			}


		/*//Details-Link
		$lc_text .= '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, ($cPath ? 'cPath=' . $cPath . '&amp;' : '') . 'products_id=' . $listing['products_id']) . '">' . TEXT_LINK_DETAILS . '</a>';
		*/

		//Tabelle Ende

		$lc_text .= '</div><div style="clear: both;"></div>'."\n";
		$lc_text .= '<div class="trenner_long">&nbsp;</div>'."\n";



		echo $lc_text;
		$i++;
	}//While-Schleife zuende

$i=0;







}
?>



<?php

// EOF by Gurkcity: Produkte anders listen

?>